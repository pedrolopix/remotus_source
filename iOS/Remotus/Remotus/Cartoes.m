//
//  Cartoes.m
//  RemotusProject
//
//  Created by Carlos Ribeiro on 06/05/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "Cartoes.h"
#import "Cartao.h"

@implementation Cartoes

-(id)initWithModel:(Modelo *)model{
    self = [super init];
    if (self){
        self.listCartao = [[NSMutableArray alloc]init];
        self.nome = @"";
        self.model = model;
    }
    return self;
}
- (NSInteger) qos{
    return 0;
}

- (void) clear{
    for (Cartao *item in self.listCartao) {
        [item clear];
    }
    
    [self.listCartao removeAllObjects];
}

-(void) add:(Cartao*) cartao{
    cartao.cards = self;
    [self.listCartao addObject:cartao];
}

-(BOOL)isRetained{
    return YES;
}


@end
