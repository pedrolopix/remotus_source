//
//  ActionEnum.m
//  Remotus
//
//  Created by Carlos Ribeiro on 09/06/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "ActionEnum.h"

@implementation ActionEnum

-(id)init{
    self = [super init];
    if (self){
        self.listValues = [[NSMutableArray alloc]init];
    }
    return self;
}


-(NSInteger)count{
    return [self.listValues count];
}

-(void) addItem:(NSString*)name imageUrl:(NSString*)imageUrl value:(NSString*)value{
    ActionEnumItem *i = [[ActionEnumItem alloc]init];
    i.name = name;
    i.imageUrl = imageUrl;
    i.value = value;
    [self.listValues addObject:i];
}

-(NSString *)nameOfValue{
    for (ActionEnumItem *item in self.listValues) {
        if ([item.value isEqualToString:self.value])
            return item.name;
    }
    return @"";
}
@end
