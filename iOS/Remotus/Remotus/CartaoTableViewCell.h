//
//  CartaoTableViewCell.h
//  Remotus
//
//  Created by Carlos Ribeiro on 23/05/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>

#import "Cartao.h"

@interface CartaoTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UIImageView *imgCard;


-(void)setupWithCartao:(Cartao *)cartao;

@end
