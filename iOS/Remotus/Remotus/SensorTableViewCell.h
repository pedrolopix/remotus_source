//
//  SensorTableViewCell.h
//  Remotus
//
//  Created by Carlos Ribeiro on 23/05/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "Sensor.h"

@interface SensorTableViewCell : UITableViewCell{
}

@property (weak, nonatomic) IBOutlet UIImageView *imgSensor;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblValue;
@property Sensor *sensor;

-(id)initWithSensor:(Sensor *)sensor;
-(id)initWithTableAndSensor:(UITableView *)table sensor:(Sensor *)sensor;
-(void)update;
@end
