//
//  ActionJSON.m
//  Remotus
//
//  Created by Carlos Ribeiro on 09/06/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "ActionJSON.h"

@implementation ActionJSON

-(NSMutableDictionary *)toJSON:(id)obj{
    Action *a = (Action *)obj;
    
    NSMutableDictionary *dic = [super toJSON:a];
    [dic addEntriesFromDictionary:[NSDictionary dictionaryWithObjectsAndKeys:[a topicOut], TAG_JSON_TOPIC_OUT, nil]];
    [dic addEntriesFromDictionary:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:[a qosOut]], TAG_JSON_QOS_OUT, nil]];
    [dic addEntriesFromDictionary:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:[a isRetained]], TAG_JSON_RETAINED, nil]];
    
    return dic;
}

-(void) fillSensor:(NSDictionary *)json sensor:(Sensor*)sensor{
    Action *a = (Action *)sensor;
    [super fillSensor:json sensor:sensor];
    
    NSString *topicOut = [json objectForKey:TAG_JSON_TOPIC_OUT];
    NSInteger qosOut =[[json objectForKey:TAG_JSON_QOS_OUT]intValue];
    BOOL retained = [[json objectForKey:TAG_JSON_RETAINED] boolValue];

    [a setTopicOut:topicOut];
    [a setQosOut:qosOut];
    [a setRetained:retained];
}

@end
