//
//  AddActionOnOffController.h
//  Remotus
//
//  Created by Carlos Ribeiro on 26/06/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "AddActionController.h"
#import "ActionOnOff.h"

@interface AddActionOnOffController : AddActionController

@property (weak, nonatomic) IBOutlet UITextField *txtTextoOn;
@property (weak, nonatomic) IBOutlet UITextField *txtTextoOff;

@end
