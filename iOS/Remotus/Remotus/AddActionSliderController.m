//
//  AddActionSliderController.m
//  Remotus
//
//  Created by Carlos Ribeiro on 27/06/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "AddActionSliderController.h"

@interface AddActionSliderController ()

@end

@implementation AddActionSliderController

-(id)init {
    return [self initWithNibName:@"AddActionSliderView" bundle:nil];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.txtMinimo setDelegate:self];
    [self.txtMaximo setDelegate:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)save{
    [super save];
    [self.validator putRule:[Rules checkIfNumericAndMinMax:NSLocalizedString(@"MinMaxError", nil)  forTextFieldMin:self.txtMinimo forTextFieldMax:self.txtMaximo]];
    [self.validator validate];
}

-(void)fill{
    [super fill];
    ActionSlider *a = (ActionSlider *)self.action;
    
    [self.txtMinimo setText:[NSString stringWithFormat:@"%d", (int)a.minValue]];
    [self.txtIncremento setText:[NSString stringWithFormat:@"%d", (int)a.incrementalValue]];
    [self.txtMaximo setText:[NSString stringWithFormat:@"%d", (int)a.maxValue]];
    [self.swLiberta setOn:[a isTriggerUp]];
}

#pragma delegate textbox

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return [super textFieldShouldBeginEditing:textField];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [super textFieldShouldReturn:textField];
    
    if ([textField isEqual:self.txtMinimo]){
        [textField resignFirstResponder];
        [self.txtIncremento becomeFirstResponder];
    } if ([textField isEqual:self.txtIncremento]){
        [textField resignFirstResponder];
        [self.txtMaximo becomeFirstResponder];
    } else
        [textField resignFirstResponder];
    return YES;
}

#pragma ValidatorDelegate - Delegate methods

- (void) preValidation
{
    [super preValidation];
}

- (void)onSuccess
{
    [super onSuccess];
    ActionSlider *a = (ActionSlider *)self.action;
    [a setMinValue:[self.txtMinimo.text intValue]];
    [a setIncrementalValue:[self.txtIncremento.text intValue]];
    [a setMaxValue:[self.txtMaximo.text intValue]];
    [a setTriggerUp:[self.swLiberta isOn]];
    [self.cartao.model cartaoChanged:self.cartao];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)onFailure:(Rule *)failedRule
{
    [super onFailure:failedRule];
}


@end
