//
//  AddCartdViewController.m
//  Remotus
//
//  Created by Carlos Ribeiro on 06/07/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "AddCardViewController.h"
#import "Cartao.h"
#import "Modelo.h"
#import "Cartoes.h"
#import "Utils.h"


@interface AddCardViewController ()

@end

@implementation AddCardViewController{
    @private
    Cartao *card;
    Modelo *model;
}

-(id)initWithCardAndModel:(Cartao*)cartao modelo:(Modelo*)modelo{
    self = [super init];
    if (self){
        UIBarButtonItem *savebutton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Save", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(save)];
        self.navigationItem.rightBarButtonItem = savebutton;
        card = cartao;
        model = modelo;
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.txtTitulo setDelegate:self];
    [self.txtImageUrl setDelegate:self];
    if (card==nil)
        [self.navigationItem setTitle:NSLocalizedString(@"NewCard", nil)];
    else
        [self.navigationItem setTitle:card.name];
        
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self fill];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)save {
    if (_tooltipView != nil)
    {
        [_tooltipView removeFromSuperview];
        _tooltipView = nil;
    }
    Validator *validator = [[Validator alloc] init];
    validator.delegate   = self;
    [validator putRule:[Rules minLength:1 withFailureString:NSLocalizedString(@"RequiredField", nil) forTextField:self.txtTitulo]];
    [validator validate];
}

-(void)fill{
    self.txtTitulo.text = card.name;
    self.txtImageUrl.text = card.imageUrl;
    [self.imgCard setImageWithURL:[NSURL URLWithString:card.imageUrl] placeholderImage:[UIImage imageNamed:@"desconhecido.png"]];
}

#pragma delegate textbox

-(void)textFieldDidEndEditing:(UITextField *)textField{
    if ([textField isEqual:self.txtImageUrl]){
        [self.imgCard setImageWithURL:[NSURL URLWithString:textField.text] placeholderImage:[UIImage imageNamed:@"desconhecido.png"]];
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    // Hides Tooltip
    if (_tooltipView != nil)
    {
        [_tooltipView removeFromSuperview];
        _tooltipView = nil;
    }
    return YES;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    // Hide tooltip
    if (_tooltipView != nil)
    {
        [_tooltipView removeFromSuperview];
        _tooltipView = nil;
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([textField isEqual:self.txtTitulo]){
        [textField resignFirstResponder];
        [self.txtImageUrl becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [Utils positionOfScroll:self.scrollView textField:textField];
}


#pragma ValidatorDelegate - Delegate methods

- (void) preValidation
{
    self.txtTitulo.layer.borderWidth = 0;
    self.txtImageUrl.layer.borderWidth = 0;
}

- (void)onSuccess
{
    NSLog(@"Success");
    if (_tooltipView != nil)
    {
        [_tooltipView removeFromSuperview];
        _tooltipView  = nil;
    }
    if (card==nil){
        card = [[Cartao alloc]init];
        card.model = model;
        [model.cartoes.listCartao addObject:card];
    }
    [card setName:self.txtTitulo.text];
    [card setImageUrl:self.txtImageUrl.text];
    [model cartaoChanged:card];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)onFailure:(Rule *)failedRule
{
    NSLog(@"Failed");
    failedRule.textField.layer.borderColor   = [[UIColor redColor] CGColor];
    failedRule.textField.layer.cornerRadius  = 5;
    failedRule.textField.layer.borderWidth   = 2;
    
    CGPoint point           = [failedRule.textField convertPoint:CGPointMake(0.0, failedRule.textField.frame.size.height - 4.0) toView:self.view];
    CGRect tooltipViewFrame = CGRectMake(6.0, point.y, 309.0, _tooltipView.frame.size.height);
    
    _tooltipView       = [[InvalidTooltipView alloc] init];
    _tooltipView.frame = tooltipViewFrame;
    _tooltipView.text  = [NSString stringWithFormat:@"%@",failedRule.failureMessage];
    _tooltipView.rule  = failedRule;
    [self.view addSubview:_tooltipView];
}


@end
