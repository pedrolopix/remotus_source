//
//  EventMqttConnectionLost.m
//  RemotusProject
//
//  Created by Carlos Ribeiro on 10/05/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "EventMqttConnectionLost.h"

@implementation EventMqttConnectionLost
+(NSString*)toString{
    return @"EventMqttConnectionLost";
}
@end
