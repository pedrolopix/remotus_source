//
//  AddSensorOnOffController.m
//  Remotus
//
//  Created by Carlos Ribeiro on 19/06/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "AddSensorOnOffController.h"

@interface AddSensorOnOffController ()

@end

@implementation AddSensorOnOffController

-(id)init  {
    return [self initWithNibName:@"AddSensorOnOffView" bundle:nil];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.txtTextoOn setDelegate:self];
    [self.txtTextoOff setDelegate:self];
    [self.txtUrlLigado setDelegate:self];
    [self.txtUrlDesligado setDelegate:self];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma delegate button save

- (void)save{
    [super save];
    [self.validator validate];
}

-(void) fill{
    [super fill];
    SensorOnOff *s = (SensorOnOff *)self.sensor;
    [self.txtTextoOn setText:[s on]];
    [self.txtTextoOff setText:[s off]];
    [self.txtUrlLigado setText:[s imageUrl]];
    [self.txtUrlDesligado setText:[s imageOffUrl]];
    [self.imgLigado setImageWithURL:[NSURL URLWithString:s.imageUrl] placeholderImage:[UIImage imageNamed:@"desconhecido.png"]];
    [self.imgDesligado setImageWithURL:[NSURL URLWithString:s.imageOffUrl] placeholderImage:[UIImage imageNamed:@"desconhecido.png"]];
}

#pragma delegate textbox

-(void)textFieldDidEndEditing:(UITextField *)textField{
    if ([textField isEqual:self.txtUrlLigado]){
        [self.imgLigado setImageWithURL:[NSURL URLWithString:textField.text] placeholderImage:[UIImage imageNamed:@"desconhecido.png"]];
    } else if ([textField isEqual:self.txtUrlDesligado]){
        [self.imgDesligado setImageWithURL:[NSURL URLWithString:textField.text] placeholderImage:[UIImage imageNamed:@"desconhecido.png"]];
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return [super textFieldShouldBeginEditing:textField];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [super textFieldShouldReturn:textField];
    
    if ([textField isEqual:self.txtTopico]){
        [textField resignFirstResponder];
        [self.txtTextoOn becomeFirstResponder];
    } else if ([textField isEqual:self.txtTextoOn]){
        [textField resignFirstResponder];
        [self.txtTextoOff becomeFirstResponder];
    } else if ([textField isEqual:self.txtTextoOff]){
        [textField resignFirstResponder];
        [self.txtUrlLigado becomeFirstResponder];
    } else if ([textField isEqual:self.txtUrlLigado]){
        [textField resignFirstResponder];
        [self.txtUrlDesligado becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    return YES;
}


#pragma ValidatorDelegate - Delegate methods

- (void) preValidation
{
    [super preValidation];
}

- (void)onSuccess
{
    [super onSuccess];
    
    SensorOnOff *s = (SensorOnOff*)self.sensor;
    [s setOn:self.txtTextoOn.text];
    [s setOff:self.txtTextoOff.text];
    [s setImageUrl:self.txtUrlLigado.text];
    [s setImageOffUrl:self.txtUrlDesligado.text];
    
    [self.cartao.model cartaoChanged:self.cartao];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)onFailure:(Rule *)failedRule
{
    [super onFailure:failedRule];
}

@end
