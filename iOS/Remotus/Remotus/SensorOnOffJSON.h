//
//  SensorOnOffJSON.h
//  RemotusProject
//
//  Created by Carlos Ribeiro on 15/05/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SensorJSON.h"
#import "SensorOnOff.h"

@interface SensorOnOffJSON : SensorJSON


@end
