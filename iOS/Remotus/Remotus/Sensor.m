//
//  Sensor.m
//  RemotusProject
//
//  Created by Carlos Ribeiro on 12/05/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "Sensor.h"
#import "Action.h"
#import "Cartao.h"
#import "Cartoes.h"
#import "Modelo.h"
#import "EventBus.h"

@implementation Sensor
-(id) init{
    self = [super init];
    if (self){
        _myId = currentID++;
        self.listActions = [[NSMutableArray alloc] init];
        self.valueVisible = YES;
        self.value =@"";
        self.imageUrl=@"";
        self.unit=@"";
    }
    return self;
}

- (NSString *)simpleName{
    return @"Normal";
}

- (void)updateValue:(NSString *)value{
    NSString *old = [value copy];
    self.value=value;
    [self.card.cards.model.eventBus sensorChange:self oldValue:old newValue:value];
}

- (void) addAction:(Action*)action{
    action.card = self.card;
    [self.listActions addObject:action];
}

-(void) removeAction:(Action *)action{
    [self.listActions removeObject:action];
    [self.card.model cartaoChanged:self.card];
}

-(void) clear{
    [self.listActions removeAllObjects];
}
@end
