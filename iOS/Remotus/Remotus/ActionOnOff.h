//
//  ActionOnOff.h
//  RemotusProject
//
//  Created by Carlos Ribeiro on 12/05/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "Action.h"

@interface ActionOnOff : Action

@property NSString *on;
@property NSString *off;

- (BOOL)isOn;
-(void)toogle;
@end
