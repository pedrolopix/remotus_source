//
//  ActionEnumTableViewCell.m
//  Remotus
//
//  Created by Carlos Ribeiro on 02/07/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "ActionEnumTableViewCell.h"

@implementation ActionEnumTableViewCell

//invocado pela factory
-(id)initWithTableAndSensor:(UITableView *)table sensor:(Sensor *)sensor{
    static NSString *CellIdentifier = @"ActionEnumIdentifier";
    [table registerNib:[UINib nibWithNibName:@"ActionEnumTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:CellIdentifier];
    self = [table dequeueReusableCellWithIdentifier:CellIdentifier];
    self.action = (Action*)sensor;
    if (self){
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        [self config];
        [self update];
    }
    
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)config{
    ActionEnum *action = (ActionEnum*)self.action;
    self.lblName.text = action.name;
}

-(void)update{
    ActionEnum *action = (ActionEnum*)self.action;
    self.lblValue.text = [action nameOfValue];
}



@end
