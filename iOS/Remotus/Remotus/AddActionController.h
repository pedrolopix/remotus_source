//
//  AddActionController.h
//  Remotus
//
//  Created by Carlos Ribeiro on 26/06/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Sensor.h"
#import "Action.h"
#import "TooltipView.h"
#import "Validator.h"
#import "InvalidTooltipView.h"
#import "ValidTooltipView.h"
#import "Utils.h"

@interface AddActionController : UIViewController<UITextFieldDelegate,ValidatorDelegate>{
    TooltipView *_tooltipView;
}

@property Sensor *sensor;
@property Cartao *cartao;
@property Action *action;
@property Validator *validator;

@property (weak, nonatomic) IBOutlet UITextField *txtTitulo;
@property (weak, nonatomic) IBOutlet UITextField *txtTopicoIn;
@property (weak, nonatomic) IBOutlet UITextField *txtTopicoOut;
@property (weak, nonatomic) IBOutlet UITextField *txtQosOut;
@property (weak, nonatomic) IBOutlet UISwitch *swRetem;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;



- (void)fill;
- (void)save;

@end
