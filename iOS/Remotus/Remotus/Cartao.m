//
//  Cartao.m
//  RemotusProject
//
//  Created by Carlos Ribeiro on 06/05/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "Cartao.h"
#import "Cartoes.h"
#import "Sensor.h"
#import "Modelo.h"

@implementation Cartao

-(id)init{
    self = [super init];
    if (self){
        self.sensors = [[NSMutableArray alloc]init];
        self.imageUrl = @"";
    }
    return self;
}

-(void)addSensor:(Sensor*)sensor{
    sensor.card=self;
    if (sensor==nil)
        return;
    [self.sensors addObject:sensor];
}

-(NSInteger) countItemsOnCartao{
    return [self.sensors count];
}

-(void)clear{
    for(Sensor *s in self.sensors){
        [s clear];
    }
    [self.sensors removeAllObjects];
}

-(void) removeSensor:(Sensor *)sensor{
    [self.sensors removeObject:sensor];
    [self.model cartaoChanged:self];
}

-(void) removeAllSensor{
    [self.sensors removeAllObjects];
}

@end
