//
//  ActionOnOffTableViewCell.h
//  Remotus
//
//  Created by Carlos Ribeiro on 12/06/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "ActionTableViewCell.h"
#import "ActionOnOff.h"

@interface ActionOnOffTableViewCell : ActionTableViewCell
@property (weak, nonatomic) IBOutlet UISwitch *btSwitch;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblValue;

@end
