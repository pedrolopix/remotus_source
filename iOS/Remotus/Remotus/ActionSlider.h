//
//  ActionSlider.h
//  RemotusProject
//
//  Created by Carlos Ribeiro on 12/05/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "Action.h"

@interface ActionSlider : Action

@property NSInteger minValue;
@property NSInteger maxValue;
@property NSInteger incrementalValue;
@property (getter = isTriggerUp) BOOL triggerUp;

@end
