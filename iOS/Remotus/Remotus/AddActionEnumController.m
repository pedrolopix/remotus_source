//
//  AddActionEnumController.m
//  Remotus
//
//  Created by Carlos Ribeiro on 04/07/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "AddActionEnumController.h"

@interface AddActionEnumController ()

@end

@implementation AddActionEnumController

-(id)init {
    
    return [self initWithNibName:@"AddActionEnumView" bundle:nil];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)save{
    [super save];
    [self.validator validate];
}

-(void)fill{
    [super fill];
    ActionEnum *a = (ActionEnum *)self.action;
    [self.swGrelha setOn:[a isShowGrid]];
}

#pragma delegate textbox

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return [super textFieldShouldBeginEditing:textField];
}

#pragma ValidatorDelegate - Delegate methods

- (void) preValidation
{
    [super preValidation];
}

- (void)onSuccess
{
    [super onSuccess];
    ActionEnum *a = (ActionEnum *)self.action;
    [a setShowGrid:[self.swGrelha isOn]];
    [self.cartao.model cartaoChanged:self.cartao];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)onFailure:(Rule *)failedRule
{
    [super onFailure:failedRule];
}


@end
