//
//  SensonJSON.m
//  RemotusProject
//
//  Created by Carlos Ribeiro on 12/05/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "SensorJSON.h"

@interface SensorJSON()

//ficava privado o metodo
//-(void) fillSensor:(NSDictionary *)json sensor:(Sensor*)sensor;

@end

@implementation SensorJSON

-(NSMutableDictionary *)toJSON:(id)obj{
    Sensor *s = (Sensor *)obj;
    
    /* super
     JSONObject o = new JSONObject();
     o.put(TAG_CLASS,obj.getClass().getSimpleName());
     */
    
   /* NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                         NSStringFromClass([s class]), TAG_JSON_CLASS,
                         [s name], TAG_JSON_NAME,
                         [s topicIn], TAG_JSON_TOPIC_IN,
                         [s value], TAG_JSON_VALUE,
                         [s unit], TAG_JSON_UNIT,
                         [s qos], TAG_JSON_QOS,
                         [s isValueVisible], TAG_JSON_VALUE_VISIBLE,
                         [s imageUrl], TAG_JSON_IMAGE_URL,
                         nil]; */
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    [dic addEntriesFromDictionary:[NSDictionary dictionaryWithObjectsAndKeys:NSStringFromClass([s class]), TAG_JSON_CLASS, nil]];
    [dic addEntriesFromDictionary:[NSDictionary dictionaryWithObjectsAndKeys:[s name], TAG_JSON_NAME, nil]];
    [dic addEntriesFromDictionary:[NSDictionary dictionaryWithObjectsAndKeys:[s topicIn], TAG_JSON_TOPIC_IN, nil]];
    [dic addEntriesFromDictionary:[NSDictionary dictionaryWithObjectsAndKeys:[s value], TAG_JSON_VALUE, nil]];
    [dic addEntriesFromDictionary:[NSDictionary dictionaryWithObjectsAndKeys:[s unit], TAG_JSON_UNIT, nil]];
    [dic addEntriesFromDictionary:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:[s qos]], TAG_JSON_QOS, nil]];

    [dic addEntriesFromDictionary:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:[s isValueVisible]], TAG_JSON_VALUE_VISIBLE, nil]];
    [dic addEntriesFromDictionary:[NSDictionary dictionaryWithObjectsAndKeys:[s imageUrl], TAG_JSON_IMAGE_URL, nil]];
    
    return dic;
}

-(id) fromJSON:(NSDictionary *)json{
    Sensor *sensor = [[Sensor alloc]init];
    [self fillSensor:json sensor:sensor];
    NSLog(@"sensor normal : %@", sensor.name);
    return sensor;
}

-(void) fillSensor:(NSDictionary *)json sensor:(Sensor*)sensor{
    NSString *name = [json objectForKey:TAG_JSON_NAME];
    NSString *topicIn = [json objectForKey:TAG_JSON_TOPIC_IN];
    //NSString *value = [json objectForKey:TAG_JSON_VALUE];
    NSString *unit = [json objectForKey:TAG_JSON_UNIT];
    NSInteger qos =[[json objectForKey:TAG_JSON_UNIT]intValue];
    BOOL visible =[[json objectForKey:TAG_JSON_VALUE_VISIBLE]boolValue];
    NSString *imageUrl = [json objectForKey:TAG_JSON_IMAGE_URL];
    
    [sensor setName:name];
    [sensor setTopicIn:topicIn];
    //[sensor setValue:value];
    [sensor setUnit:unit];
    [sensor setQos:qos];
    [sensor setValueVisible:visible];
    [sensor setImageUrl:imageUrl];
}
@end
