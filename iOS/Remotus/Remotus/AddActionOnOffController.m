//
//  AddActionOnOffController.m
//  Remotus
//
//  Created by Carlos Ribeiro on 26/06/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "AddActionOnOffController.h"

@interface AddActionOnOffController ()

@end

@implementation AddActionOnOffController

-(id)init {
    return [self initWithNibName:@"AddActionOnOffView" bundle:nil];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.txtTextoOn setDelegate:self];
    [self.txtTextoOff setDelegate:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)save{
    [super save];
    [self.validator validate];
}

-(void)fill{
    [super fill];
    ActionOnOff *a = (ActionOnOff*)self.action;
    
    self.txtTextoOn.text= a.on;
    self.txtTextoOff.text= a.off;
}

#pragma delegate textbox

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return [super textFieldShouldBeginEditing:textField];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [super textFieldShouldReturn:textField];
    
    if ([textField isEqual:self.txtTextoOn]){
        [textField resignFirstResponder];
        [self.txtTextoOff becomeFirstResponder];
    } if ([textField isEqual:self.txtTextoOff]){
        [textField resignFirstResponder];
    }
    return YES;
}


#pragma ValidatorDelegate - Delegate methods

- (void) preValidation
{
    [super preValidation];
}

- (void)onSuccess
{
    [super onSuccess];
    ActionOnOff *a = (ActionOnOff *)self.action;
    [a setOn:self.txtTextoOn.text];
    [a setOff:self.txtTextoOff.text];
    [self.cartao.model cartaoChanged:self.cartao];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)onFailure:(Rule *)failedRule
{
    [super onFailure:failedRule];
}



@end
