//
//  AddActionController.m
//  Remotus
//
//  Created by Carlos Ribeiro on 26/06/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "AddActionController.h"

@interface AddActionController ()

@end

@implementation AddActionController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        UIBarButtonItem *savebutton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Save", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(save)];
        self.navigationItem.rightBarButtonItem = savebutton;
                    [self.swRetem setOn:YES animated:YES];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.txtTitulo setDelegate:self];
    [self.txtTopicoIn setDelegate:self];
    [self.txtTopicoOut setDelegate:self];
    [self.txtQosOut setDelegate:self];

    
    if (self.editing)
        [self.navigationItem setTitle:self.action.name];
    else
        [self.navigationItem setTitle:@"Nova"];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self fill];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)save {
    if (_tooltipView != nil)
    {
        [_tooltipView removeFromSuperview];
        _tooltipView = nil;
    }
    self.validator = [[Validator alloc] init];
    self.validator.delegate   = self;
    [self.validator putRule:[Rules minLength:1 withFailureString:NSLocalizedString(@"RequiredField", nil) forTextField:self.txtTitulo]];
    [self.validator putRule:[Rules minLength:1 withFailureString:NSLocalizedString(@"RequiredField", nil) forTextField:self.txtTopicoIn]];
    [self.validator putRule:[Rules checkRegEx:@"[0|1|2]" failureString:NSLocalizedString(@"QosInvalid", nil) forTextField:self.txtQosOut]];
}

-(void)fill{
    self.txtTitulo.text = self.action.name;
    self.txtTopicoIn.text = self.action.topicIn;
    self.txtTopicoOut.text = self.action.topicOut;
    [self.txtQosOut setText:[NSString stringWithFormat:@"%d", (int)self.action.qosOut]];
    [self.swRetem setOn:[self.action isRetained]];
    
}

#pragma delegate textbox

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    // Hides Tooltip
    if (_tooltipView != nil)
    {
        [_tooltipView removeFromSuperview];
        _tooltipView = nil;
    }
    return YES;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    // Hide tooltip
    if (_tooltipView != nil)
    {
        [_tooltipView removeFromSuperview];
        _tooltipView = nil;
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([textField isEqual:self.txtTitulo]){
        [textField resignFirstResponder];
        [self.txtTopicoIn becomeFirstResponder];
    }else if ([textField isEqual:self.txtTopicoIn]){
        [textField resignFirstResponder];
        [self.txtTopicoOut becomeFirstResponder];
    }else if ([textField isEqual:self.txtTopicoOut]){
        [textField resignFirstResponder];
        [self.txtQosOut becomeFirstResponder];
    } else if ([textField isEqual:self.txtQosOut]){
        [textField resignFirstResponder];
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [Utils positionOfScroll:self.scrollView textField:textField];
}


#pragma ValidatorDelegate - Delegate methods

- (void) preValidation
{
    self.txtTitulo.layer.borderWidth = 0;
    self.txtTopicoIn.layer.borderWidth = 0;
}

- (void)onSuccess
{
    NSLog(@"Success");
    if (_tooltipView != nil)
    {
        [_tooltipView removeFromSuperview];
        _tooltipView  = nil;
    }
    
    if (!self.editing){
        [self.sensor addAction:self.action];
    }
    [self.action setName:self.txtTitulo.text];
    [self.action setTopicIn:self.txtTopicoIn.text];
    [self.action setTopicOut:self.txtTopicoOut.text];
    if ([self.action.topicOut isEqualToString:@""])
        [self.action setTopicOut:[self.action topicIn]];
    [self.action setQosOut:[self.txtQosOut.text intValue]];
    [self.action setRetained:[self.swRetem isOn]];
}

- (void)onFailure:(Rule *)failedRule
{
    NSLog(@"Failed");
    failedRule.textField.layer.borderColor   = [[UIColor redColor] CGColor];
    failedRule.textField.layer.cornerRadius  = 5;
    failedRule.textField.layer.borderWidth   = 2;
    
    CGPoint point           = [failedRule.textField convertPoint:CGPointMake(0.0, failedRule.textField.frame.size.height - 4.0) toView:self.view];
    CGRect tooltipViewFrame = CGRectMake(6.0, point.y, 309.0, _tooltipView.frame.size.height);
    
    _tooltipView       = [[InvalidTooltipView alloc] init];
    _tooltipView.frame = tooltipViewFrame;
    _tooltipView.text  = [NSString stringWithFormat:@"%@",failedRule.failureMessage];
    _tooltipView.rule  = failedRule;
    [self.view addSubview:_tooltipView];
}

@end
