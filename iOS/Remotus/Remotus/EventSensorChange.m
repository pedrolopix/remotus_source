//
//  EventSensorChange.m
//  Remotus
//
//  Created by Carlos Ribeiro on 27/05/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "EventSensorChange.h"

@implementation EventSensorChange

-(id)initWithParms:(Sensor *)sensor theOldValue:(NSString*)theOldValue theNewdValue:(NSString*)theNewdValue{
    self = [super init];
    if (self){
        self.sensor = sensor;
        self.theOldValue = theOldValue;
        self.theNewdValue = theNewdValue;
    }
    return self;
}

+(NSString*)toString{
    return @"EventSensorChange";
}

@end
