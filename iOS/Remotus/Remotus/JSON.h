//
//  JSON1.h
//  RemotusProject
//
//  Created by Carlos Ribeiro on 11/05/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol JSON <NSObject>


-(NSMutableDictionary *) toJSON:(id)json;
-(id) fromJSON:(NSDictionary *)json;


@end

static const NSString * TAG_JSON_CLASS = @"class";
static const NSString * TAG_JSON_IMAGE_OFF_URL = @"imageOffUrl";
static const NSString * TAG_JSON_IMAGE_URL = @"imageurl";
static const NSString * TAG_JSON_NAME = @"name";
static const NSString * TAG_JSON_OFF = @"off";
static const NSString * TAG_JSON_ON = @"on";
static const NSString * TAG_JSON_TOPIC_IN = @"topicIn";
static const NSString * TAG_JSON_UNIT = @"unit";
static const NSString * TAG_JSON_QOS = @"qos";
static const NSString * TAG_JSON_VALUE_VISIBLE = @"valueVisible";
static const NSString * TAG_JSON_VALUE = @"value";
static const NSString * TAG_JSON_TOPIC_OUT = @"topicOut";
static const NSString * TAG_JSON_QOS_OUT = @"qosOut";
static const NSString * TAG_JSON_RETAINED = @"retained";
static const NSString * TAG_JSON_SLIDE_MINVALUE = @"min";
static const NSString * TAG_JSON_SLIDE_MAXVALUE = @"max";
static const NSString * TAG_JSON_SLIDE_INCREMENTALVALUE = @"incremental";
static const NSString * TAG_JSON_SLIDE_TRIGGERUP = @"triggerUp";
static const NSString * TAG_JSON_ENUM_ARRAY = @"enumArray";
static const NSString * TAG_JSON_ENUM_NAME = @"enumName";
static const NSString * TAG_JSON_ENUM_URL = @"enumUrl";
static const NSString * TAG_JSON_ENUM_VALUE = @"enumValue";
static const NSString * TAG_JSON_ENUM_SHOWGRID = @"enumShowGrid";