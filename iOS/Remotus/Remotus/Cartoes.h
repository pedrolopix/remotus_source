//
//  Cartoes.h
//  RemotusProject
//
//  Created by Carlos Ribeiro on 06/05/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Cartao;
@class Modelo;

@interface Cartoes : NSObject
@property (copy) NSString *topic;
@property (nonatomic, copy) NSString *nome;
@property (copy) NSString *idCartoes;
@property (nonatomic, strong) NSMutableArray *listCartao;
@property Modelo *model;

- (id)initWithModel:(Modelo *)model;
- (NSInteger) qos;
- (void) clear;
- (void) add:(Cartao*)cartao;
-(BOOL)isRetained;

@end
