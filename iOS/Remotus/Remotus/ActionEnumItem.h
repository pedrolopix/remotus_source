//
//  ActionEnumItem.h
//  Remotus
//
//  Created by Carlos Ribeiro on 02/07/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ActionEnumItem : NSObject
@property NSString *name;
@property NSString *imageUrl;
@property NSString *value;

@end
