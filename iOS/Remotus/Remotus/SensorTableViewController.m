//
//  SensorTableViewController.m
//  Remotus
//
//  Created by Carlos Ribeiro on 23/05/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "SensorTableViewController.h"
#import "SensorOnOffTableViewCell.h"
#import "Sensor.h"
#import "SensorTableViewCell.h"
#import "EventSensorChange.h"
#import "Cartao.h"
#import "CardFactory.h"
#import "ActionTableViewController.h"
#import "EventLoadCards.h"
#import "Modelo.h"
#import "Cartoes.h"
#import "AddSensorNormalController.h"
#import "EventLoadCardsLost.h"

@interface SensorTableViewController ()

@end


@implementation SensorTableViewController {
    @private
    Cartao *cartao;
    

}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadCards:) name:[EventLoadCards toString] object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sensorChange:) name:[EventSensorChange toString] object:nil];
    
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [self updateView];
}


-(void) viewWillAppear: (BOOL) animated {
    //return;
    [super viewWillAppear:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    NSLog(@"view did appear");
    [super viewDidAppear:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int count = [cartao.sensors count];
    if(self.editing) count++;
    return count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.isEditing && indexPath.row==[cartao.sensors count]){
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.textLabel.text = NSLocalizedString(@"NewSensor", nil);
        return cell;
    }
    SensorTableViewCell *cell = (SensorTableViewCell*)[CardFactory createSensorView:[cartao.sensors objectAtIndex:indexPath.row] tableView:tableView];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.editing && !self.editing)
    {
        [cell setEditing:NO animated:YES];
        return;
    }
    
    if (cell.editing){
        if (indexPath.row==[cartao.sensors count]){
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"ChoiceAction", nil)
                                                                     delegate:self
                                                            cancelButtonTitle:nil
                                                       destructiveButtonTitle:nil
                                                            otherButtonTitles: nil];
            
            for (NSString *str in [CardFactory listNameSensors]) {
                [actionSheet addButtonWithTitle:str];
            }
            [actionSheet addButtonWithTitle:NSLocalizedString(@"Cancel", nil)];
            [actionSheet setCancelButtonIndex:[CardFactory countSensors]];
            [actionSheet showInView:self.view];
            
        } else{
            //edit sensor
            Sensor *s = (Sensor*)[cartao.sensors objectAtIndex:indexPath.row];
            AddSensorController *add = [CardFactory createSensorAddBySensor:s];
            if (add!=nil){
                [add setSensor: s];
                [add setCartao:cartao];
                add.editing = YES;
                [self.navigationController pushViewController:add animated:YES];
            }
        }
    } else{
        [self performSegueWithIdentifier:@"segueAction" sender:self];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        Cartao *c = [self.model.cartoes.listCartao objectAtIndex:self.indexRowCartao];
        Sensor *s = [c.sensors objectAtIndex:indexPath.row];
        [c removeSensor:s];
    }
}


- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView.editing && indexPath.row == [cartao.sensors count]){
        return  UITableViewCellEditingStyleInsert;
    }
    
    return UITableViewCellEditingStyleDelete;
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath
                                                                  *)indexPath {
    if (indexPath.row == [cartao.sensors count]) // nao move a ultima linha
        return NO;
    return YES;
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
    NSInteger source = sourceIndexPath.row;
    NSInteger destination = destinationIndexPath.row;
    NSInteger moveDestination=0;
    NSInteger moveSource=0;
    if (destination==[cartao.sensors count]){
        [self updateTable];
        return;
    }
    
    if(source>destination)
        moveSource=1;
    else if (source<destination)
        moveDestination=1;
    
    if (moveSource==1 || moveDestination==1){
        [cartao.sensors insertObject:[cartao.sensors objectAtIndex:source] atIndex:destination+moveDestination];
        [cartao.sensors removeObjectAtIndex:source+moveSource];
        [cartao.model cartaoChanged:cartao];
    }
}


-(void)setEditing:(BOOL)editing animated:(BOOL) animated {
    [super setEditing:editing animated:animated];
    [self updateTable];
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    if ([[segue identifier] isEqualToString:@"segueAction"]){
        ActionTableViewController *dest = [segue destinationViewController];
        dest.indexRowCartao = self.indexRowCartao;
        dest.indexRowSensor =indexPath.row;
        dest.model = self.model;
    }
}

-(void)updateView{
    cartao = [self.model.cartoes.listCartao objectAtIndex:self.indexRowCartao];
    [self.navigationItem setTitle:cartao.name];
    [self updateTable];
}

-(void)loadCards:(NSNotification *)notification{
    [self updateView];
}

-(void)updateTable{
    [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
}

-(void)sensorChange:(NSNotification *)notification{
    EventSensorChange *event = [[notification userInfo] valueForKey:@"topic"];
    
    for (Sensor *item in cartao.sensors) {
        if ([event.sensor.topicIn isEqualToString:item.topicIn]){
            NSLog(@"updateSensor - topic %@ value %@", event.sensor.name, event.theNewdValue);
            [self updateTable];
            return;
        }
    }
}

#pragma delegate UIActionSheet
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    AddSensorController *add = [CardFactory createSensorAdd:buttonTitle];
    if (add!=nil){
        Sensor *s = [CardFactory createSensor:buttonTitle];
        [add setSensor:s];
        [add setCartao:cartao];
        add.editing = NO;
        [self.navigationController pushViewController:add animated:YES];
    }
}



@end
