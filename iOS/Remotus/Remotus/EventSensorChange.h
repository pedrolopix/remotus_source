//
//  EventSensorChange.h
//  Remotus
//
//  Created by Carlos Ribeiro on 27/05/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "Event.h"
#import "Sensor.h"
@class Sensor;

@interface EventSensorChange : Event

@property Sensor *sensor;
@property (readwrite, copy) NSString *theNewdValue;
@property (readwrite, copy) NSString *theOldValue;

-(id)initWithParms:(Sensor *)sensor theOldValue:(NSString*)theOldValue theNewdValue:(NSString*)theNewdValue;

+(NSString*)toString;

@end
