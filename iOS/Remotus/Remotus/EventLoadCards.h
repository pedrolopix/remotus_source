//
//  EventLoadCards.h
//  RemotusProject
//
//  Created by Carlos Ribeiro on 11/05/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "Event.h"

@interface EventLoadCards : Event
+(NSString*)toString;
@end
