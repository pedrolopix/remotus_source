//
//  ActionSliderJSON.m
//  Remotus
//
//  Created by Carlos Ribeiro on 09/06/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "ActionSliderJSON.h"
#import "ActionSlider.h"

@implementation ActionSliderJSON

-(NSMutableDictionary *)toJSON:(id)obj{
    ActionSlider *a = (ActionSlider *)obj;
    NSMutableDictionary *dic = [super toJSON:a];
    [dic addEntriesFromDictionary:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:[a minValue]], TAG_JSON_SLIDE_MINVALUE, nil]];
    [dic addEntriesFromDictionary:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:[a maxValue]], TAG_JSON_SLIDE_MAXVALUE, nil]];
    [dic addEntriesFromDictionary:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:[a incrementalValue]], TAG_JSON_SLIDE_INCREMENTALVALUE, nil]];
    [dic addEntriesFromDictionary:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:[a isTriggerUp]], TAG_JSON_SLIDE_TRIGGERUP, nil]];
    return dic;
}

-(id) fromJSON:(NSDictionary *)json{
    ActionSlider *action = [[ActionSlider alloc] init];
    [self fillSensor:json sensor:(Sensor *)action];
    return action;
}

-(void) fillSensor:(NSDictionary *)json sensor:(Sensor*)sensor{
    
    ActionSlider *a = (ActionSlider *)sensor;
    [super fillSensor:json sensor:sensor];
    
    NSInteger min =[[json objectForKey:TAG_JSON_SLIDE_MINVALUE]intValue];
    NSInteger max =[[json objectForKey:TAG_JSON_SLIDE_MAXVALUE]intValue];
    NSInteger inc =[[json objectForKey:TAG_JSON_SLIDE_INCREMENTALVALUE]intValue];
    BOOL up = [[json objectForKey:TAG_JSON_SLIDE_TRIGGERUP] boolValue];
    
    [a setMinValue:min];
    [a setMaxValue:max];
    [a setIncrementalValue:inc];
    [a setTriggerUp:up];

}

@end
