//
//  AddActionEnumController.h
//  Remotus
//
//  Created by Carlos Ribeiro on 04/07/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "AddActionController.h"
#import "ActionEnum.h"

@interface AddActionEnumController : AddActionController

@property (weak, nonatomic) IBOutlet UISwitch *swGrelha;

@end
