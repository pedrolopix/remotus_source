//
//  Action.h
//  Remotus
//
//  Created by Carlos Ribeiro on 09/06/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "Sensor.h"


@interface Action : Sensor{

}

@property (getter = isRetained) BOOL retained;
@property NSString *topicOut;
@property NSInteger qosOut;
@property Sensor *sensor;

-(void) sendValue:(NSString *)value;

@end
