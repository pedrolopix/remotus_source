//
//  EventMessageArrived.h
//  RemotusProject
//
//  Created by Carlos Ribeiro on 08/05/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "Event.h"
#import "MQTTKit.h"

@interface EventMessageArrived : Event
@property (copy) MQTTMessage *message;

-(id)init:(MQTTMessage *)message;
+(NSString*)toString;
@end
