//
//  AddActionSliderController.h
//  Remotus
//
//  Created by Carlos Ribeiro on 27/06/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "AddActionController.h"
#import "ActionSlider.h"

@interface AddActionSliderController : AddActionController

@property (weak, nonatomic) IBOutlet UITextField *txtMinimo;
@property (weak, nonatomic) IBOutlet UITextField *txtIncremento;
@property (weak, nonatomic) IBOutlet UITextField *txtMaximo;
@property (weak, nonatomic) IBOutlet UISwitch *swLiberta;



@end
