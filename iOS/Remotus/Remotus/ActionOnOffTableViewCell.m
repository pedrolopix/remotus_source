//
//  ActionOnOffTableViewCell.m
//  Remotus
//
//  Created by Carlos Ribeiro on 12/06/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "ActionOnOffTableViewCell.h"

@implementation ActionOnOffTableViewCell

//invocado pela factory
-(id)initWithTableAndSensor:(UITableView *)table sensor:(Sensor *)sensor{
    static NSString *CellIdentifier = @"ActionOnOffIdentifier";
    [table registerNib:[UINib nibWithNibName:@"ActionOnOffTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:CellIdentifier];
    self = [table dequeueReusableCellWithIdentifier:CellIdentifier];
    self.action = (Action*)sensor;
    if (self){
        self.accessoryType = UITableViewCellAccessoryNone;
        [self config];
        [self update];
    }

     [self.btSwitch addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void)config{
    ActionOnOff *action = (ActionOnOff*)self.action;
    self.lblName.text = action.name;
}

-(void)update{
    ActionOnOff *action = (ActionOnOff*)self.action;
    self.btSwitch.on = action.isOn;
    if (action.isOn)
        self.lblValue.text = action.on;
    else
        self.lblValue.text = action.off;
}

- (IBAction)changeSwitch:(id)sender{
    ActionOnOff *action = (ActionOnOff*)self.action;
    [action toogle];
}



@end
