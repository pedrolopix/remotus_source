//
//  EventLinkNotOk.h
//  Remotus
//
//  Created by Carlos Ribeiro on 13/07/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "Event.h"

@interface EventLinkNotOk : Event
+(NSString*)toString;
@end
