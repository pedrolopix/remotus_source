//
//  Cartao.h
//  RemotusProject
//
//  Created by Carlos Ribeiro on 06/05/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Cartoes;
@class Sensor;
@class Modelo;

@interface Cartao : NSObject

@property (readwrite, copy) NSString *name;
@property NSString *imageUrl;

@property NSMutableArray *sensors;
@property Cartoes *cards;
@property Modelo *model;

-(void)addSensor:(Sensor*)sensor;
-(NSInteger) countItemsOnCartao;
-(void)clear;
-(void) removeSensor:(Sensor *)sensor;
-(void) removeAllSensor;
@end
