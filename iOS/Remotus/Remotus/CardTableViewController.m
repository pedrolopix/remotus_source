//
//  CardTableViewController.m
//  Remotus
//
//  Created by Carlos Ribeiro on 21/05/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "CardTableViewController.h"
#import "Modelo.h"
#import "Cartao.h"
#import "Cartoes.h"
#import "EventLoadCards.h"
#import "EventLoadCardsLost.h"
#import "EventMqttConnectionLost.h"
#import "CartaoTableViewCell.h"
#import "SensorTableViewController.h"
#import "AddCardViewController.h"
#import "Mqtt.h"


@interface CardTableViewController ()

@end

@implementation CardTableViewController{
    @private
    Cartao *editCard;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.tableView registerNib:[UINib nibWithNibName:@"CartaoTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"CartaoIdentifier"];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(buildCards:) name:[EventLoadCards toString] object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(buildCardsLost:) name:[EventLoadCardsLost toString] object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(defaultsSettingsChanged:) name:NSUserDefaultsDidChangeNotification object:nil];

    _model = [[Modelo alloc] init];
    [_model start];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(networkChanged:) name:kReachabilityChangedNotification object:nil];
    self.reachability = [Reachability reachabilityWithHostName:[self.model.mqtt host]];
    [self.reachability startNotifier];
    
     self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void) viewWillAppear: (BOOL) animated {
    [super viewWillAppear:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int count = [self.model.cartoes.listCartao count];
    if(self.editing) count++;
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.isEditing && indexPath.row==[self.model.cartoes.listCartao count]){
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.textLabel.text = NSLocalizedString(@"NewCard", nil);
        return cell;
    }
    
    static NSString *CellId = @"CartaoIdentifier";
    CartaoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    [cell setupWithCartao:[self.model.cartoes.listCartao objectAtIndex:indexPath.row]];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.editing && !self.editing)
    {
        [cell setEditing:NO animated:YES];
        return;
    }
    
    if (cell.editing){
        if (indexPath.row==[self.model.cartoes.listCartao count])
            editCard = nil;
        else
            editCard = [self.model.cartoes.listCartao objectAtIndex:indexPath.row];
        AddCardViewController *dest = [[AddCardViewController alloc]initWithCardAndModel:editCard modelo:self.model];
        [self.navigationController pushViewController:dest animated:YES];
        
    } else{
        [self performSegueWithIdentifier:@"segueSensor" sender:self];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        Cartao *c = [self.model.cartoes.listCartao objectAtIndex:indexPath.row];
        [self.model removeCartao:c];
    }
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView.editing && indexPath.row == [self.model.cartoes.listCartao count]){
        return  UITableViewCellEditingStyleInsert;
    }

    return UITableViewCellEditingStyleDelete;
}

-(void)setEditing:(BOOL)editing animated:(BOOL) animated {
    
    [super setEditing:editing animated:animated];
    [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
    
    if(editing == YES)
    {
        // Your code for entering edit mode goes here
    } else {
        // Your code for exiting edit mode goes here
    }
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath
                                                                  *)indexPath {
    if (indexPath.row == [self.model.cartoes.listCartao count]) // nao move a ultima linha
        return NO;
    return YES;
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
    NSInteger source = sourceIndexPath.row;
    NSInteger destination = destinationIndexPath.row;
    NSInteger moveDestination=0;
    NSInteger moveSource=0;
    if (destination==[self.model.cartoes.listCartao count]){
        [self update];
        return;
    }
    if(source>destination)
        moveSource=1;
    else if (source<destination)
        moveDestination=1;
    
    if (moveSource==1 || moveDestination==1){
        [self.model.cartoes.listCartao insertObject:[self.model.cartoes.listCartao objectAtIndex:source] atIndex:destination+moveDestination];
        [self.model.cartoes.listCartao removeObjectAtIndex:source+moveSource];
        [self.model cartaoChanged:[self.model.cartoes.listCartao objectAtIndex:source]];
    }
}

-(void)update{
    [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
}

-(void) buildCards:(NSNotification *)notification{
    NSLog(@"buildCards");
    [self update];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    if ([[segue identifier] isEqualToString:@"segueSensor"]){
        SensorTableViewController *dest = [segue destinationViewController];
        dest.indexRowCartao = indexPath.row;
        dest.model = self.model;
        
    }
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    return NO;
}

-(void)defaultsSettingsChanged:(NSNotification *)notification{
    NSLog(@"notified change on bundle");
    [self.model clear];
    [self.navigationController popToRootViewControllerAnimated:YES];
    [self update];
    [self.model reload];
}

- (void)networkChanged:(NSNotification *)notification
{
    NSLog(@"Network changed");
    NetworkStatus remoteHostStatus = [self.reachability currentReachabilityStatus];
    if(remoteHostStatus == NotReachable) {
        NSLog(@"Not Reachable");
        [self.model stopFailOverNetwork];
        [self.navigationController popToRootViewControllerAnimated:YES];
        [self update];
    }
    else if (remoteHostStatus == ReachableViaWiFi || remoteHostStatus == ReachableViaWWAN)  {
        NSLog(@"Reachable");
        [self.model start];
    }
}

-(void) buildCardsLost:(NSNotification *)notification{
    NSLog(@"Received Notification - MqttConnectionLost");
    [[self navigationController] popToRootViewControllerAnimated:YES];
    [self update];
}

@end
