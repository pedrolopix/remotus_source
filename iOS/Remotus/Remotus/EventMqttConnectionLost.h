//
//  EventMqttConnectionLost.h
//  RemotusProject
//
//  Created by Carlos Ribeiro on 10/05/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "Event.h"

@interface EventMqttConnectionLost : Event
+(NSString*)toString;
@end
