//
//  Bus.h
//  RemotusProject
//
//  Created by Carlos Ribeiro on 07/05/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MQTTKit.h"
#import "EventMessageArrived.h"
#import "EventMqttConnected.h"
#import "EventMqttConnectionLost.h"
#import "EventLoadCards.h"
#import "EventSensorChange.h"
#import "Sensor.h"
#import "EventLoadCardsLost.h"
#import "EventLinkNotOk.h"

@class Sensor;
@interface EventBus : NSObject{
    @private
    NSNotificationCenter *notCenter;
}

@property NSNotificationCenter* notification;

-(id)init;
-(void) publish;
-(void) mqttMessageArrived:(MQTTMessage*)message;
-(void) mqttConnected:(BOOL)value;
-(void) mqttConnectionLost;
-(void) loadCards;
-(void) loadCardsLost;
-(void) sensorChange:(Sensor*)sensor oldValue:(NSString*)oldValue newValue:(NSString*)newValue;
-(void) linkNotOk;
@end
