//
//  CardJSON.m
//  RemotusProject
//
//  Created by Carlos Ribeiro on 08/05/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "CardJSON.h"

@implementation CardJSON
//-(NSDictionary *) toJSON:(id)json;
-(NSMutableDictionary *)toJSON:(id)obj{
    Cartao *c = (Cartao *)obj;
    /* super
     JSONObject o = new JSONObject();
     o.put(TAG_CLASS,obj.getClass().getSimpleName());
     */
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:NSStringFromClass([c class]), TAG_JSON_CLASS, c.name, TAG_JSON_NAME, c.imageUrl, TAG_JSON_IMAGE_URL, nil];
    return dic;
}

-(id) fromJSON:(NSDictionary *)json{
    Cartao *c = [[Cartao alloc]init];
    NSString *nome = [json objectForKey:TAG_JSON_NAME];
    NSString *imageUrl = [json objectForKey:TAG_JSON_IMAGE_URL];
    c.name = nome;
    c.imageUrl = imageUrl;
    return c;
}

@end
