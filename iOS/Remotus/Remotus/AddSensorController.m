//
//  AddSensorController.m
//  Remotus
//
//  Created by Carlos Ribeiro on 19/06/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "AddSensorController.h"
#import "EventLoadCards.h"

@interface AddSensorController ()

@end

@implementation AddSensorController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

        UIBarButtonItem *savebutton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Save", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(save)];
        self.navigationItem.rightBarButtonItem = savebutton;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.txtTitulo setDelegate:self];
    [self.txtTopico setDelegate:self];
    if (self.editing)
        [self.navigationItem setTitle:self.sensor.name];
    else
        [self.navigationItem setTitle:@"Novo"];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self fill];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)save {
    if (_tooltipView != nil)
    {
        [_tooltipView removeFromSuperview];
        _tooltipView = nil;
    }
    self.validator = [[Validator alloc] init];
    self.validator.delegate   = self;
    [self.validator putRule:[Rules minLength:1 withFailureString:NSLocalizedString(@"RequiredField", nil) forTextField:self.txtTitulo]];
    [self.validator putRule:[Rules minLength:1 withFailureString:NSLocalizedString(@"RequiredField", nil) forTextField:self.txtTopico]];
    
}

-(void)fill{
    self.txtTitulo.text = self.sensor.name;
    self.txtTopico.text = self.sensor.topicIn;
}

#pragma delegate textbox

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    // Hides Tooltip
    if (_tooltipView != nil)
    {
        [_tooltipView removeFromSuperview];
        _tooltipView = nil;
    }
    return YES;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    // Hide tooltip
    if (_tooltipView != nil)
    {
        [_tooltipView removeFromSuperview];
        _tooltipView = nil;
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([textField isEqual:self.txtTitulo]){
        [textField resignFirstResponder];
        [self.txtTopico becomeFirstResponder];
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [Utils positionOfScroll:self.scrollView textField:textField];
}


#pragma ValidatorDelegate - Delegate methods

- (void) preValidation
{
    self.txtTitulo.layer.borderWidth = 0;
    self.txtTopico.layer.borderWidth = 0;
}

- (void)onSuccess
{
    NSLog(@"Success");
    if (_tooltipView != nil)
    {
        [_tooltipView removeFromSuperview];
        _tooltipView  = nil;
    }
    if (!self.editing){
        [self.cartao addSensor:self.sensor];
        [self.sensor setUnit:@""];
    }
    [self.sensor setName:self.txtTitulo.text];
    [self.sensor setTopicIn:self.txtTopico.text];
}

- (void)onFailure:(Rule *)failedRule
{
    NSLog(@"Failed");
    failedRule.textField.layer.borderColor   = [[UIColor redColor] CGColor];
    failedRule.textField.layer.cornerRadius  = 5;
    failedRule.textField.layer.borderWidth   = 2;
    
    CGPoint point           = [failedRule.textField convertPoint:CGPointMake(0.0, failedRule.textField.frame.size.height - 4.0) toView:self.view];
    CGRect tooltipViewFrame = CGRectMake(6.0, point.y, 309.0, _tooltipView.frame.size.height);
    
    _tooltipView       = [[InvalidTooltipView alloc] init];
    _tooltipView.frame = tooltipViewFrame;
    _tooltipView.text  = [NSString stringWithFormat:@"%@",failedRule.failureMessage];
    _tooltipView.rule  = failedRule;
    [self.view addSubview:_tooltipView];
}




@end
