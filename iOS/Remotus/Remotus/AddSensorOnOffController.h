//
//  AddSensorOnOffController.h
//  Remotus
//
//  Created by Carlos Ribeiro on 19/06/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "AddSensorController.h"
#import "SensorOnOff.h"

@interface AddSensorOnOffController : AddSensorController

@property (weak, nonatomic) IBOutlet UITextField *txtTextoOn;
@property (weak, nonatomic) IBOutlet UITextField *txtTextoOff;
@property (weak, nonatomic) IBOutlet UITextField *txtUrlLigado;
@property (weak, nonatomic) IBOutlet UIImageView *imgLigado;
@property (weak, nonatomic) IBOutlet UITextField *txtUrlDesligado;
@property (weak, nonatomic) IBOutlet UIImageView *imgDesligado;

@end
