//
//  AddCartdViewController.h
//  Remotus
//
//  Created by Carlos Ribeiro on 06/07/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "TooltipView.h"
#import "Validator.h"
#import "InvalidTooltipView.h"
#import "ValidTooltipView.h"

@class Cartao;
@class Modelo;

@interface AddCardViewController : UIViewController<UITextFieldDelegate, ValidatorDelegate>{
    TooltipView    *_tooltipView;
}
@property (weak, nonatomic) IBOutlet UITextField *txtTitulo;
@property (weak, nonatomic) IBOutlet UITextField *txtImageUrl;
@property (weak, nonatomic) IBOutlet UIImageView *imgCard;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;


-(id)initWithCardAndModel:(Cartao*)cartao modelo:(Modelo*)modelo;

@end
