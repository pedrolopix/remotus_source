//
//  ActionOnOffJSON.m
//  Remotus
//
//  Created by Carlos Ribeiro on 09/06/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "ActionOnOffJSON.h"
#import "ActionOnOff.h"

@implementation ActionOnOffJSON

-(NSMutableDictionary *)toJSON:(id)obj{
    ActionOnOff *a = (ActionOnOff *)obj;
    
    NSMutableDictionary *dic = [super toJSON:a];
    [dic addEntriesFromDictionary:[NSDictionary dictionaryWithObjectsAndKeys:[a on], TAG_JSON_ON, nil]];
    [dic addEntriesFromDictionary:[NSDictionary dictionaryWithObjectsAndKeys:[a off], TAG_JSON_OFF, nil]];
    return dic;
}

-(id) fromJSON:(NSDictionary *)json{
    ActionOnOff *action = [[ActionOnOff alloc] init];
    [self fillSensor:json sensor:(Sensor *)action];
    return action;
}

-(void) fillSensor:(NSDictionary *)json sensor:(Sensor*)sensor{
    
    ActionOnOff *a = (ActionOnOff *)sensor;
    [super fillSensor:json sensor:sensor];
    
    NSString *on = [json objectForKey:TAG_JSON_ON];
    NSString *off = [json objectForKey:TAG_JSON_OFF];
    
    [a setOn:on];
    [a setOff:off];
    
    
}

@end
