//
//  ActionSliderTableViewCell.h
//  Remotus
//
//  Created by Carlos Ribeiro on 10/06/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActionTableViewCell.h"
#import "ActionSlider.h"

@interface ActionSliderTableViewCell : ActionTableViewCell

@property (weak, nonatomic) IBOutlet UISlider *controlSlider;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblValue;



@end
