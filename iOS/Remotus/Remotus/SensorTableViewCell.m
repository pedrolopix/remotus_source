//
//  SensorTableViewCell.m
//  Remotus
//
//  Created by Carlos Ribeiro on 23/05/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "SensorTableViewCell.h"
#import "EventSensorChange.h"


@implementation SensorTableViewCell 


-(id)initWithSensor:(Sensor *)sensor{
   
    self = [super init];
    if (self){
        self.sensor = sensor;
    }
    return self;
}

//invocado pela factory
-(id)initWithTableAndSensor:(UITableView *)table sensor:(Sensor *)sensor{
    static NSString *CellIdentifier = @"SensorIdentifier";
    [table registerNib:[UINib nibWithNibName:@"SensorTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:CellIdentifier];
    self = [table dequeueReusableCellWithIdentifier:CellIdentifier];
    //self = [self initWithSensor:sensor];
    //passar para o de cima...
    self.sensor = sensor;
    if (self){
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        [self update];
    }
    return self;
    
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)update{
    [self.lblName setText:[self.sensor name]];
    [self.lblValue setText:[self.sensor value]];
    [self.imgSensor setImageWithURL:[NSURL URLWithString:[self.sensor imageUrl]] placeholderImage:[UIImage imageNamed:@"desconhecido.png"]];
    
}



@end
