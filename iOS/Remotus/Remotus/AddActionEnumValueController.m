//
//  AddActionEnumValueController.m
//  Remotus
//
//  Created by Carlos Ribeiro on 08/07/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "AddActionEnumValueController.h"
#import "Modelo.h"
#import "ActionEnum.h"
#import "ActionEnumItem.h"
#import "Utils.h"

@interface AddActionEnumValueController ()

@end

@implementation AddActionEnumValueController{
    @private
    ActionEnumItem *actionEnumItem;
    ActionEnum *actionEnum;
}

-(id)initWithActionEnum:(ActionEnum*)action actionEnumItem:(ActionEnumItem*)item{
    self = [super init];
    if (self){
        UIBarButtonItem *savebutton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Save", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(save)];
        self.navigationItem.rightBarButtonItem = savebutton;
        actionEnum = action;
        actionEnumItem = item;
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.txtTitulo setDelegate:self];
    [self.txtValor setDelegate:self];
    [self.txtImageUrl setDelegate:self];
    
    if (actionEnumItem==nil)
        [self.navigationItem setTitle:NSLocalizedString(@"NewValue", nil)];
    else
        [self.navigationItem setTitle:actionEnumItem.name];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self fill];
}

- (void)save {
    if (_tooltipView != nil)
    {
        [_tooltipView removeFromSuperview];
        _tooltipView = nil;
    }
    Validator *validator = [[Validator alloc] init];
    validator.delegate   = self;
    [validator putRule:[Rules minLength:1 withFailureString:NSLocalizedString(@"RequiredField", nil) forTextField:self.txtTitulo]];
    [validator putRule:[Rules minLength:1 withFailureString:NSLocalizedString(@"RequiredField", nil) forTextField:self.txtValor]];
    [validator validate];
    
}

-(void)fill{
    self.txtTitulo.text = actionEnumItem.name;
    self.txtValor.text =actionEnumItem.value;
    self.txtImageUrl.text = actionEnumItem.imageUrl;
    [self.imgValue setImageWithURL:[NSURL URLWithString:actionEnumItem.imageUrl] placeholderImage:[UIImage imageNamed:@"desconhecido.png"]];
}


#pragma delegate textbox

-(void)textFieldDidEndEditing:(UITextField *)textField{
    if ([textField isEqual:self.txtImageUrl]){
        [self.imgValue setImageWithURL:[NSURL URLWithString:actionEnumItem.imageUrl] placeholderImage:[UIImage imageNamed:@"desconhecido.png"]];
    } 
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    // Hides Tooltip
    if (_tooltipView != nil)
    {
        [_tooltipView removeFromSuperview];
        _tooltipView = nil;
    }
    return YES;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    // Hide tooltip
    if (_tooltipView != nil)
    {
        [_tooltipView removeFromSuperview];
        _tooltipView = nil;
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([textField isEqual:self.txtTitulo]){
        [textField resignFirstResponder];
        [self.txtValor becomeFirstResponder];
    } else if ([textField isEqual:self.txtValor]){
        [textField resignFirstResponder];
        [self.txtImageUrl becomeFirstResponder];
    } else
        [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [Utils positionOfScroll:self.scrollView textField:textField];
}

#pragma ValidatorDelegate - Delegate methods

- (void) preValidation
{
    self.txtTitulo.layer.borderWidth = 0;
    self.txtImageUrl.layer.borderWidth = 0;
}

- (void)onSuccess
{
    NSLog(@"Success");
    if (_tooltipView != nil)
    {
        [_tooltipView removeFromSuperview];
        _tooltipView  = nil;
    }
    if (actionEnumItem==nil){
        actionEnumItem = [[ActionEnumItem alloc]init];
        [actionEnum.listValues addObject:actionEnumItem];
    }
    [actionEnumItem setName:self.txtTitulo.text];
    [actionEnumItem setValue:self.txtValor.text];
    [actionEnumItem setImageUrl:self.txtImageUrl.text];
    [actionEnum.card.model cartaoChanged:actionEnum.card];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)onFailure:(Rule *)failedRule
{
    NSLog(@"Failed");
    failedRule.textField.layer.borderColor   = [[UIColor redColor] CGColor];
    failedRule.textField.layer.cornerRadius  = 5;
    failedRule.textField.layer.borderWidth   = 2;
    
    CGPoint point           = [failedRule.textField convertPoint:CGPointMake(0.0, failedRule.textField.frame.size.height - 4.0) toView:self.view];
    CGRect tooltipViewFrame = CGRectMake(6.0, point.y, 309.0, _tooltipView.frame.size.height);
    
    _tooltipView       = [[InvalidTooltipView alloc] init];
    _tooltipView.frame = tooltipViewFrame;
    _tooltipView.text  = [NSString stringWithFormat:@"%@",failedRule.failureMessage];
    _tooltipView.rule  = failedRule;
    [self.view addSubview:_tooltipView];
}

@end
