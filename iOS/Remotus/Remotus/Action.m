//
//  Action.m
//  Remotus
//
//  Created by Carlos Ribeiro on 09/06/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "Action.h"

@implementation Action

-(id)init{
    self = [super init];
    if (self){
        [self setRetained:YES];
    }
    return  self;
}

-(void) sendValue:(NSString *)value{
    [self.card.model publish:self value:value];
}

@end
