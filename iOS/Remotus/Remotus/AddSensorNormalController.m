//
//  AddSensorNormalController.m
//  Remotus
//
//  Created by Carlos Ribeiro on 19/06/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "AddSensorNormalController.h"

@interface AddSensorNormalController ()

@end

@implementation AddSensorNormalController

-(id)init {
    return [self initWithNibName:@"AddSensorNormalView" bundle:nil];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
       
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.txtUrlImage setDelegate:self];
    [self.txtUrlImage setDelegate:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    return;
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

#pragma delegate button save

- (void)save{
    [super save];
    [self.validator validate];
}


#pragma fill data

-(void) fill{
    [super fill];
    self.txtUrlImage.text = self.sensor.imageUrl;
    [self.imgImage setImageWithURL:[NSURL URLWithString:self.sensor.imageUrl] placeholderImage:[UIImage imageNamed:@"desconhecido.png"]];
}

#pragma delegate textbox

-(void)textFieldDidEndEditing:(UITextField *)textField{
    if ([textField isEqual:self.txtUrlImage]){
        [self.imgImage setImageWithURL:[NSURL URLWithString:textField.text] placeholderImage:[UIImage imageNamed:@"desconhecido.png"]];
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return [super textFieldShouldBeginEditing:textField];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [super textFieldShouldReturn:textField];

    if ([textField isEqual:self.txtTopico]){
        [textField resignFirstResponder];
        [self.txtUrlImage becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    return YES;
}


#pragma ValidatorDelegate - Delegate methods

- (void) preValidation
{
    [super preValidation];
}

- (void)onSuccess
{
    [super onSuccess];
    
    [self.sensor setImageUrl:self.txtUrlImage.text];
    [self.cartao.model cartaoChanged:self.cartao];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)onFailure:(Rule *)failedRule
{
    [super onFailure:failedRule];
}


@end
