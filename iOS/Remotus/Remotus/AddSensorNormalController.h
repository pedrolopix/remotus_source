//
//  AddSensorNormalController.h
//  Remotus
//
//  Created by Carlos Ribeiro on 19/06/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "AddSensorController.h"

@interface AddSensorNormalController : AddSensorController


@property (weak, nonatomic) IBOutlet UITextField *txtUrlImage;
@property (weak, nonatomic) IBOutlet UIImageView *imgImage;

@end
