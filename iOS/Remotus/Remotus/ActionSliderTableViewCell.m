//
//  ActionSliderTableViewCell.m
//  Remotus
//
//  Created by Carlos Ribeiro on 10/06/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "ActionSliderTableViewCell.h"

@implementation ActionSliderTableViewCell{
    NSInteger oldValue;
}

//invocado pela factory
-(id)initWithTableAndSensor:(UITableView *)table sensor:(Sensor *)sensor{
    static NSString *CellIdentifier = @"ActionSliderIdentifier";
    [table registerNib:[UINib nibWithNibName:@"ActionSliderTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:CellIdentifier];
    self = [table dequeueReusableCellWithIdentifier:CellIdentifier];
    self.action = (Action*)sensor;
    if (self){
        self.accessoryType = UITableViewCellAccessoryNone;
        [self config];
        [self update];
    }
    [self.controlSlider addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)config{
    ActionSlider *actionSlider = (ActionSlider*)self.action;
    self.controlSlider.minimumValue = actionSlider.minValue;
    self.controlSlider.maximumValue = actionSlider.maxValue;
    self.controlSlider.continuous = !actionSlider.triggerUp;
    self.controlSlider.value = [actionSlider.value intValue];
    self.lblName.text = actionSlider.name;
}

-(void)update{
    ActionSlider *actionSlider = (ActionSlider*)self.action;
    self.lblValue.text= actionSlider.value;
    oldValue = [actionSlider.value intValue];
    [self.controlSlider setValue:oldValue animated:YES];
}

- (IBAction)sliderValueChanged:(UISlider *)sender {

    NSInteger v = [self RoundValue:sender.value];
    [sender setValue:v animated:YES];
    if (v!=oldValue){
        NSLog(@"Update value action: %@ value: %d", self.action.name, v);
        oldValue = v;
        [self.action sendValue:[NSString stringWithFormat:@"%d", v]];
    }
    
}

-(NSInteger) RoundValue:(float)value{
    ActionSlider *actionSlider = (ActionSlider*)self.action;
    return ((int)((value + (actionSlider.incrementalValue/2) ) / actionSlider.incrementalValue) * actionSlider.incrementalValue);
}




@end
