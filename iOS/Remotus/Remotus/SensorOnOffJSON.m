//
//  SensorOnOffJSON.m
//  RemotusProject
//
//  Created by Carlos Ribeiro on 15/05/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "SensorOnOffJSON.h"

@implementation SensorOnOffJSON

-(NSMutableDictionary *)toJSON:(id)obj{
    SensorOnOff *s = (SensorOnOff *)obj;
    NSMutableDictionary *dicS = [super toJSON:obj];
    NSMutableDictionary *dSOnOff = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                [s on], TAG_JSON_ON,
                                [s off], TAG_JSON_OFF,
                                [s imageOffUrl], TAG_JSON_IMAGE_OFF_URL,
                                nil];
    
    [dicS addEntriesFromDictionary:dSOnOff];
    
    return dicS;
}

-(id) fromJSON:(NSDictionary *)json{
    SensorOnOff *sensor = [[SensorOnOff alloc]init];
    [self fillSensor:json sensor:sensor];
    return sensor;
}

-(void) fillSensor:(NSDictionary *)json sensor:(Sensor*)sensor{
    
    [super fillSensor:json sensor:sensor];
    SensorOnOff *s = (SensorOnOff *)sensor;
    
    NSString *on = [json objectForKey:TAG_JSON_ON];
    NSString *off = [json objectForKey:TAG_JSON_OFF];
    NSString *imageOffUrl = [json objectForKey:TAG_JSON_IMAGE_OFF_URL];
    
    [s setOn:on];
    [s setOff:off];
    [s setImageOffUrl:imageOffUrl];
}

@end
