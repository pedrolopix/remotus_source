//
//  ActionTableViewController.h
//  Remotus
//
//  Created by Carlos Ribeiro on 10/06/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Modelo;

@interface ActionTableViewController : UITableViewController<UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate>

@property NSInteger indexRowCartao;
@property NSInteger indexRowSensor;
@property Modelo *model;

- (void)sensorChange:(NSNotification *)notification;

@end
