//
//  AddActionEnumValueController.h
//  Remotus
//
//  Created by Carlos Ribeiro on 08/07/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "TooltipView.h"
#import "Validator.h"
#import "InvalidTooltipView.h"
#import "ValidTooltipView.h"

@class ActionEnum;
@class ActionEnumItem;

@interface AddActionEnumValueController : UIViewController<UITextFieldDelegate, ValidatorDelegate>{
    TooltipView    *_tooltipView;
}
@property (weak, nonatomic) IBOutlet UITextField *txtTitulo;
@property (weak, nonatomic) IBOutlet UITextField *txtValor;
@property (weak, nonatomic) IBOutlet UITextField *txtImageUrl;
@property (weak, nonatomic) IBOutlet UIImageView *imgValue;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

-(id)initWithActionEnum:(ActionEnum*)action actionEnumItem:(ActionEnumItem*)item;
@end
