//
//  Mqtt.m
//  RemotusProject
//
//  Created by Carlos Ribeiro on 06/05/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "Mqtt.h"

@implementation Mqtt

-(id) init {
    self = [super init];
    return self;
}

- (id) initWithParameters:(NSString*)name hostname:(NSString*)host port:(NSString *)port username:(NSString*)user password:(NSString*)pass{
    self = [super init];
    if (self){
        self.host = host;
        self.port = port;
        self.name = name;
        self.username = user;
        self.password = pass;
    }
    return self;
}

@end
