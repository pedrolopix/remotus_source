//
//  MqttEngine.h
//  RemotusProject
//
//  Created by Carlos Ribeiro on 06/05/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import <Foundation/Foundation.h>
@class MQTTKit;
@class Modelo;
@class EventMessageArrived;
@class EventSensorChange;
@class Modelo;
@class MQTTClient;

@interface MqttEngine : NSObject

@property NSString *serverURI;
@property NSString *clientId;
@property NSString *username;
@property NSString *password;
@property NSString *port;
@property (nonatomic, strong) MQTTClient *client;
@property(getter = isConnected) BOOL connected;
@property (nonatomic, strong) Modelo *modelo;

- (id) initWithModel:(Modelo*)modelo;
- (void) connect;
- (void) dealloc;
- (void) disconnect;
-(void) forceDisconnect;
- (BOOL) subscribe:(NSString*)topicFilter qos:(NSInteger)qos;
- (void) unsubscribe:(NSString*)topicFilter;
- (void) publish:(NSString*)topic text:(NSString*)text retained:(BOOL)retained qos:(NSInteger)qos;

@end
