//
//  Utils.h
//  Remotus
//
//  Created by Carlos Ribeiro on 13/07/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utils : NSObject

+(void)positionOfScroll:(UIScrollView*)scroll textField:(UITextField*)textField;
@end
