//
//  Utils.m
//  Remotus
//
//  Created by Carlos Ribeiro on 13/07/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "Utils.h"

@implementation Utils

+(void)positionOfScroll:(UIScrollView*)scroll textField:(UITextField*)textField{
    CGPoint pt;
    CGRect rc = [textField bounds];
    rc = [textField convertRect:rc toView:scroll];
    pt = rc.origin;
    pt.x = 0;
    pt.y -= 80;
    [scroll setContentOffset:pt animated:YES];
}
@end
