//
//  SensorOnOffTableViewCell.m
//  Remotus
//
//  Created by Carlos Ribeiro on 04/06/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "SensorOnOffTableViewCell.h"

@implementation SensorOnOffTableViewCell{

}

-(id)initWithTableAndSensor:(UITableView *)table sensor:(Sensor *)sensor{
    static NSString *CellIdentifier = @"SensorIdentifier";
    [table registerNib:[UINib nibWithNibName:@"SensorOnOffTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:CellIdentifier]  ;
    self =  [table dequeueReusableCellWithIdentifier:CellIdentifier];
    //self = [super initWithSensor:sensor] ;
    if (self){
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        self.sensor = sensor;
        [self update];
    }
    return self;
    
}

-(void)update{
    
    [super update];
    SensorOnOff *s = (SensorOnOff*)self.sensor;
    if ([s isOn]){
        [[super lblValue]setText:s.on];
        [self.imgSensor setImageWithURL:[NSURL URLWithString:[s imageUrl]] placeholderImage:[UIImage imageNamed:@"desconhecido.png"]];

    } else {
        [[super lblValue]setText:s.off];
        [self.imgSensor setImageWithURL:[NSURL URLWithString:[s imageOffUrl]] placeholderImage:[UIImage imageNamed:@"desconhecido.png"]];
        
    }
    

}

@end
