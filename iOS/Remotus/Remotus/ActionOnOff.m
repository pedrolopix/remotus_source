//
//  ActionOnOff.m
//  RemotusProject
//
//  Created by Carlos Ribeiro on 12/05/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "ActionOnOff.h"

@implementation ActionOnOff

- (BOOL)isOn{
    return ![self.value isEqualToString:@"0"];
}

-(void)toogle{
    if ([self isOn]){
        [self sendValue:@"0"];
    } else{
        [self sendValue:@"1"];
    }
}

@end
