//
//  EventMessageArrived.m
//  RemotusProject
//
//  Created by Carlos Ribeiro on 08/05/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "EventMessageArrived.h"

@implementation EventMessageArrived

-(id)init:(MQTTMessage *)message{
    self = [super init];
    if (self){
        _message=message;
    }
    return self;
}

+(NSString*)toString{
    return @"EventMessageArrived";
}

@end
