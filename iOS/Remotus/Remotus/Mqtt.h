//
//  Mqtt.h
//  RemotusProject
//
//  Created by Carlos Ribeiro on 06/05/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Mqtt : NSObject
@property (readwrite, copy) NSString *host;
@property (readwrite, copy)  NSString *port;
@property (readwrite, copy)  NSString *name;
@property (readwrite, copy)  NSString *username;
@property (readwrite, copy)  NSString *password;

- (id) init;
- (id) initWithParameters:(NSString*)name hostname:(NSString*)host port:(NSString *)port username:(NSString*)user password:(NSString*)pass;

@end


