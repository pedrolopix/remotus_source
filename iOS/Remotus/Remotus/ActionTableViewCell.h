//
//  ActionTableViewCell.h
//  Remotus
//
//  Created by Carlos Ribeiro on 10/06/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Action.h"
#import "SensorTableViewCell.h"

@interface ActionTableViewCell : SensorTableViewCell
@property Action *action;


@end
