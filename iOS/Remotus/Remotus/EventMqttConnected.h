//
//  EventMqttConnected.h
//  RemotusProject
//
//  Created by Carlos Ribeiro on 08/05/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "Event.h"

@interface EventMqttConnected : Event
@property (getter = isConnected) BOOL connected;
+(NSString*)toString;
@end
