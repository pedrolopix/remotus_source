//
//  SensorTableViewController.h
//  Remotus
//
//  Created by Carlos Ribeiro on 23/05/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Cartao;
@class SensorOnOff;
@class Modelo;

@interface SensorTableViewController : UITableViewController<UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate>

@property NSInteger indexRowCartao;
@property Modelo *model;


@end
