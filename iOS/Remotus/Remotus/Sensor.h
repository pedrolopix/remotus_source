//
//  Sensor.h
//  RemotusProject
//
//  Created by Carlos Ribeiro on 12/05/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Cartao.h"
#import "Modelo.h"
//@class Cartao;
@class Action;

@interface Sensor : NSObject{

    
}
@property NSString *topicIn;
@property NSString *value;
@property Cartao *card;
@property NSString *unit;
@property NSString *name;
@property NSInteger qos;
@property NSInteger myId;
@property NSMutableArray *listActions;
@property(getter = isValueVisible) BOOL valueVisible;
@property NSString *imageUrl;
//static int mIdCount=0;


- (NSString *)simpleName;
- (void)updateValue:(NSString *)value;
- (void) addAction:(Action*)action;
-(void) removeAction:(Action *)action;
-(void) clear;

@end

static NSInteger currentID;
