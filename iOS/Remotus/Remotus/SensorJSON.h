//
//  SensonJSON.h
//  RemotusProject
//
//  Created by Carlos Ribeiro on 12/05/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JSON.h"
#import "Sensor.h"
#import "Action.h"

@interface SensorJSON : NSObject<JSON>

-(void) fillSensor:(NSDictionary *)json sensor:(Sensor*)sensor;

@end
