////
//  Model.m
//  RemotusProject
//
//  Created by Carlos Ribeiro on 06/05/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "Modelo.h"
#import "MQTTKit.h"
#import "MqttEngine.h"
#import "Cartoes.h"

#import "Mqtt.h"
#import "EventMqttConnected.h"
#import "EventMqttConnectionLost.h"
#import "EventLoadCardsLost.h"
#import "CardJSON.h"
#import "JSON.h"
#import "CardJSON.h"
#import "CardFactory.h"
#import "Sensor.h"
#import "EventBus.h"
#import "Action.h"

@implementation Modelo

static const NSString *TAG_ID =@"id";
static const NSString *TAG_NAME =@"name";
static const NSString *TAG_CARDS =@"cards";
static const NSString *TAG_SENSORS =@"sensors";
static const NSString *TAG_CLASS =@"class"; //deveria estar no json
static const NSString *TAG_ACTIONS=@"actions";

- (id) init {
    self = [super init];
    if (self){
        self.started = NO;
        self.connected = NO;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveMqttConnected:) name:[EventMqttConnected toString] object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(mqttMessageArrived:) name:[EventMessageArrived toString] object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(mqttConnectionLost:) name:[EventMqttConnectionLost toString] object:nil];
        //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(networkChanged:) name:kReachabilityChangedNotification object:nil];
    
        //self.reachability = [Reachability reachabilityForInternetConnection];
        

        
        self.eventBus =[[EventBus alloc]init];
        [self loadMqtt];
        [self loadCartoes];
        //self.cartoes = [[Cartoes alloc]init];
        //[self dadosTeste];
        
    }
    
    return self;
}

-(void) start{
    if(_started){
        NSLog(@"o modelo ja estava a trabalhar");
        return;
    }
    NSLog(@"start model");
    //_eventBus = [[EventBus alloc]init];
    [self startMqtt];
    _started = YES;
    NSLog(@"modelo iniciado");
}


-(void) stop{
    [self stopMqtt];
    _started = NO;
    _connected = NO;
    NSLog(@"stop model");
}

-(void) stopFailOverNetwork{
    //[self unSubscribeCards];
    [self clear];
    [self.mqttEngine forceDisconnect];
    self.mqttEngine = nil;
    _started = NO;
    _connected = NO;
    NSLog(@"stopFailOverNetwork");
}

-(void) startMqtt{
    [self stopMqtt];
    self.mqttEngine = [[MqttEngine alloc] initWithModel:self];
    self.mqttEngine.serverURI = _mqtt.host;
    self.mqttEngine.password = _mqtt.password;
    self.mqttEngine.username = _mqtt.username;
    self.mqttEngine.port = _mqtt.port;
    [self.mqttEngine connect];
}

-(void) stopMqtt{
    if (self.mqttEngine!=nil){
        [self.mqttEngine disconnect];
    }
}


-(void)subscribe:(NSString*)topic qos:(NSInteger)qos{
    NSLog(@"Subscribe: %@", topic);
    [self.mqttEngine subscribe:topic qos:qos];
}

-(void)unsubscribe:(NSString*)topic{
    NSLog(@"Unsubscribe: %@", topic);
    [self.mqttEngine unsubscribe:topic];
}

- (void) subscribeCards{
    for (Cartao *item in self.cartoes.listCartao) {
        for (Sensor *sensor in item.sensors) {
            [self subscribe:sensor.topicIn qos:sensor.qos];
            for(Sensor *action in  sensor.listActions){
                [self subscribe:action.topicIn qos:action.qos];
            }
        }
    }
    
    
}

-(void)unSubscribeCards{
    for (Cartao *item in self.cartoes.listCartao){
        for (Sensor *sensor in item.sensors) {
            [self unsubscribe:sensor.topicIn];
            for(Sensor *action in  sensor.listActions){
                [self unsubscribe:action.topicIn ];
            }
        }
    }
}

-(void)publish:(Action*)action value:(NSString *)value{
    //NSLog(@"publish: %@", action.name);
    [self.mqttEngine publish:action.topicOut text:value retained:action.retained qos:action.qosOut];
   
}

- (void)receiveMqttConnected:(NSNotification *)notification
{
    //subscreve o topico principal com as configurações
    if (_connected == YES)
        return;
    NSLog(@"Received Notification - MqttStarted");
    _connected = YES;
    [self subscribe:self.cartoes.topic qos:self.cartoes.qos];
    [self subscribeCards];
}


- (void)mqttMessageArrived:(NSNotification *)notification
{
    //quando recebe os dados do cartao de configuração carrega tudo o que vem lá
    EventMessageArrived *event = [[notification userInfo] valueForKey:@"topic"];
    //NSLog(@"mqttMessageArrived - topic %@", event.message.topic);
    if ([_cartoes.topic isEqualToString:event.message.topic]){
        [self loadCards:event.message.payloadString];
    }
    
    for (Cartao *item in self.cartoes.listCartao) {
        for (Sensor *sensor in item.sensors) {
            if ([sensor.topicIn isEqual:event.message.topic]){
                [sensor updateValue:event.message.payloadString];
                NSLog(@"mqttMessageArrived >>>> %@",event.message.payloadString);
            }
            
            for (Action *action in sensor.listActions) {
                if ([action.topicIn isEqualToString:event.message.topic]){
                    [action updateValue:event.message.payloadString];
                    NSLog(@"mqttMessageArrived # Action: %@ >>>> %@", action.topicIn, event.message.payloadString);
                }
            }
            
        }
    }
    
}

- (void)mqttConnectionLost:(NSNotification *)notification
{
    NSLog(@"Received Notification - MqttConnectionLost");
    [self clear];
    _connected = NO;
    [self.eventBus loadCardsLost];
}


-(void)cartaoChanged:(Cartao *)cartao{
    NSLog(@"cartaoChanged");
    [self unSubscribeCards];
    [self subscribeCards];
    if (cartao==nil)
        return;
    
    [self saveCards];
}

- (void) loadCards:(NSString *)json{
    NSLog(@"loadCards");
    NSData *jsonData = [json dataUsingEncoding:NSUTF8StringEncoding];
    
    NSError *error;
    NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
    
    id<JSON> cj = [[CardJSON alloc]init];
    NSString *nome = [jsonDic objectForKey:TAG_NAME];
    NSString *idCartoes = [jsonDic objectForKey:TAG_ID];
    
    if([idCartoes isEqualToString:_cartoes.idCartoes]){
        return;
    }
    
    [self unSubscribeCards];
    [self.cartoes clear];
    self.cartoes.model = self;
    self.cartoes.nome = nome;
    NSArray *arrayCartoes =[jsonDic objectForKey:TAG_CARDS];
    if ([arrayCartoes isKindOfClass:[NSArray class]]){
        for (NSDictionary *dic in arrayCartoes) {
            Cartao *card = (Cartao *)[cj fromJSON:dic];
            [self.cartoes add:card];
            card.model = self;
            //NSLog(@"cartao: %@", card.name);
            NSArray *arraySensores =[dic objectForKey:TAG_SENSORS];
            if ([arraySensores isKindOfClass:[NSArray class]]){
                for (NSDictionary *dicS in arraySensores) {
                    NSString *sensorClass = [dicS objectForKey:TAG_CLASS];
                    Item *iSensor =  [CardFactory sensor:sensorClass];
                    if (iSensor==nil)
                         continue;
                    Sensor *sensor = (Sensor*)[iSensor fromJSON:dicS];
                    [card addSensor:sensor];
                    //load actions
                    NSArray *arrayActions =[dicS objectForKey:TAG_ACTIONS];
                    //NSLog(@"Sensor: %@ # Count actions: %d", sensor.name, arrayActions.count);
                    
                    for (NSDictionary *dicA in arrayActions) {
                        NSString *actionClass = [dicA objectForKey:TAG_CLASS];
                        Item *iAction =  [CardFactory action:actionClass];
                        if (iAction==nil)
                            continue;
                        Action *action = (Action*)[iAction fromJSON:dicA];
                        [sensor addAction:action];
                        [action setSensor:sensor];
      
                    }
                 }
            }
        }
    }
    
    [self.eventBus loadCards];
    [self subscribeCards];
}

- (void) saveCards{
    NSLog(@"saveCards");
    
    NSMutableDictionary *ca = [[NSMutableDictionary alloc]init];
    NSString *uuid = [[NSUUID UUID] UUIDString];
    [self.cartoes setIdCartoes:uuid];
    
    //NSDictionary *jsonDic = [NSDictionary dictionaryWithObjectsAndKeys:TAG_NAME, @"Cartao", TAG_ID,self.cartoes.idCartoes];
    NSMutableDictionary *jsonDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    @"Cartoes", TAG_NAME, self.cartoes.idCartoes, TAG_ID, nil];
    [ca addEntriesFromDictionary:jsonDic];

    NSMutableArray *ac = [[NSMutableArray alloc]init];
    id<JSON> cj = [[CardJSON alloc]init];
    for (Cartao *card in self.cartoes.listCartao) {
        NSMutableDictionary *c = [cj toJSON:card];
        NSMutableArray *acs = [[NSMutableArray alloc]init];
        for (Sensor *sensor in card.sensors) {
            Item *iSensor = [CardFactory sensorObject:sensor];
            NSMutableDictionary *s = [iSensor toJSON:sensor];
            NSMutableArray *acsa = [[NSMutableArray alloc]init];
            for (Action *action in sensor.listActions) {
                Item *iAction = [CardFactory actionClass:action];
                NSMutableDictionary *a = [iAction toJSON:action];
                [acsa addObject:a];
            }
            if (acsa.count != 0){
                NSMutableDictionary *tmp = [NSMutableDictionary dictionaryWithObjectsAndKeys: acsa, TAG_ACTIONS, nil];
                [s addEntriesFromDictionary:tmp];
            }
            [acs addObject:s];
        }
        
        if (acs.count != 0){
            NSMutableDictionary *tmp = [NSMutableDictionary dictionaryWithObjectsAndKeys: acs, TAG_SENSORS, nil];
            [c addEntriesFromDictionary:tmp];
        }
        [ac addObject:c];
    }
    [ca setObject:ac forKey:TAG_CARDS];
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:ca options:0 error:nil];
    NSString *myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    [self.mqttEngine publish:self.cartoes.topic text:myString retained:self.cartoes.isRetained qos:self.cartoes.qos];
    [self.cartoes setIdCartoes:[[NSUUID UUID] UUIDString]];
}

-(void) dadosTeste{
   // _mqtt = [[Mqtt alloc] initWithParameters:@"nomemqtt" hostname:@"192.168.1.10" port:@"1883" username:@"carlos" password:@"password"];
   // _cartoes = [[Cartoes alloc] initWithModel:self];
   // _cartoes.topic = @"/remotus/ios/";
   // _cartoes.nome = @"Cartões";
}

-(void)loadMqtt{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *host =[defaults stringForKey:@"url_preference"];
    if (host==nil)
        [[NSUserDefaults standardUserDefaults] setValue:@"192.168.1.10" forKey:@"url_preference"];
    
    NSString *port=[defaults stringForKey:@"port_identifier"];
    if (port==nil)
        [[NSUserDefaults standardUserDefaults] setValue:@"1883" forKey:@"port_identifier"];
    
    NSString *user=[defaults stringForKey:@"user_identifier"];
    if (user==nil)
        [[NSUserDefaults standardUserDefaults] setValue:@"user1" forKey:@"user_identifier"];
    
    NSString *pass=[defaults stringForKey:@"password_identifier"];
    if (pass==nil)
        [[NSUserDefaults standardUserDefaults] setValue:@"pass1" forKey:@"password_identifier"];
    
    self.mqtt = [[Mqtt alloc] initWithParameters:@"namemqtt" hostname:host port:port username:user password:pass];
    
    NSLog(@"Endereço mqtt: %@", host);
}

-(void)loadCartoes{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *topicCard=[defaults stringForKey:@"topiccard_identifier"];
    if (topicCard==nil)
        [[NSUserDefaults standardUserDefaults] setValue:@"/remotus/card/1" forKey:@"topiccard_identifier"];
    
    self.cartoes = [[Cartoes alloc]initWithModel:self];
    [self.cartoes setNome:@"Cartoes"];
    [self.cartoes setTopic:topicCard];
}

-(void) removeCartao:(Cartao *)cartao{
    [cartao removeAllSensor];
    [self.cartoes.listCartao removeObject:cartao];
    [self cartaoChanged:cartao];
}

-(void) clear{
    [self.cartoes clear];
}

-(void)reload{
    [self stop];
    [self loadMqtt];
    [self loadCartoes];
    [self start];
}

/*- (void)networkChanged:(NSNotification *)notification
{
    NSLog(@"Network changed");
    NetworkStatus remoteHostStatus = [self.reachability currentReachabilityStatus];
    if(remoteHostStatus == NotReachable) {
        NSLog(@"Not Reachable");
        [self clear];
        //[self.eventBus linkNotOk];
        //[self.navigationController popToRootViewControllerAnimated:YES];
        //[self update];
    }
    else if (remoteHostStatus == ReachableViaWiFi || remoteHostStatus == ReachableViaWWAN)  {
        NSLog(@"Reachable");
        [self reload];
    }
}*/

@end
