//
//  ActionEnumTableViewController.h
//  Remotus
//
//  Created by Carlos Ribeiro on 04/07/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Modelo;

@interface ActionEnumTableViewController : UITableViewController<UITableViewDataSource, UITableViewDelegate>

@property NSInteger indexRowCartao;
@property NSInteger indexRowSensor;
@property NSInteger indexRowAcao;
@property Modelo *model;

@end
