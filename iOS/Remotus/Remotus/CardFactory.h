//
//  CardFactory.h
//  RemotusProject
//
//  Created by Carlos Ribeiro on 15/05/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSON.h"
#import "SensorJSON.h"
#import "SensorOnOffJSON.h"
#import "Sensor.h"
#import "SensorOnOff.h"
#import "SensorTableViewCell.h"
#import "SensorOnOffTableViewCell.h"
#import "Action.h"
#import "ActionSlider.h"
#import "ActionOnOff.h"
#import "ActionEnum.h"
#import "ActionJSON.h"
#import "ActionOnOffJSON.h"
#import "ActionSliderJSON.h"
#import "ActionEnumJSON.h"
#import "ActionTableViewCell.h"
#import "ActionSliderTableViewCell.h"
#import "ActionOnOffTableViewCell.h"
#import "ActionEnumTableViewCell.h"
#import "AddSensorNormalController.h"
#import "AddSensorOnOffController.h"
#import "AddSensorController.h"
#import "AddActionOnOffController.h"
#import "AddActionSliderController.h"
#import "AddActionEnumController.h"

@class ItemSensor;
@class ItemAction;
@class Item;


@interface CardFactory : NSObject
+(ItemSensor *)sensor:(NSString*)sensorClass;
+(ItemSensor *)sensorByName:(NSString*)sensorName;
+(ItemSensor *)sensorObject:(Sensor*)sensor;
+(ItemSensor *)sensorClass:(Sensor*)sensor;
+(NSMutableArray *)listNameSensors;
+(NSInteger)countSensors;
+(UITableViewCell *) createSensorView:(Sensor *)sensor tableView:(UITableView *)tableView;
+(AddSensorController *) createSensorAdd:(NSString *)sensorName;
+(AddSensorController *) createSensorAddBySensor:(Sensor *)sensor;
+(Sensor *) createSensor:(NSString *)sensorName;

+(ItemAction *)action:(NSString*)actionClass;
+(ItemAction *)actionObject:(Action*)action;
+(ItemAction *)actionClass:(Action*)action;
+(NSMutableArray *)listNameActions;
+(NSInteger)countActions;
+(AddActionController *) createActionAddByAction:(Action *)action;
+(AddActionController *) createActionAdd:(NSString *)actionName;
+(ActionTableViewCell *) createActionView:(Action *)action tableView:(UITableView *)tableView;
+(Action *) createAction:(NSString *)actionName;
@end

@interface Item : NSObject
@property NSString *name;
@property id<JSON> json;


-(id)initWithNameJSON:(NSString*)name json:(id<JSON>)json;
-(NSMutableDictionary *) toJSON:(id)json;
-(id) fromJSON:(NSDictionary *)json;


@end

@interface ItemSensor : Item
@property Class sensorClass;
@property Class sensorViewClass;
@property Class sensorAddClass;

-(id)initWithParameters:(NSString *)name sensorClass:(Class)sensorClass sensorViewClass:(Class)sensorViewClass sensorAddClass:(Class)sensorAddClass sensorJson:(id<JSON>)sensorJson;



@end

@interface ItemAction : Item
@property Class actionClass;
@property Class actionViewClass;
@property Class actionAddClass;
-(id)initWithParameters:(NSString *)name actionClass:(Class)actionClass actionViewClass:(Class)actionViewClass actionAddClass:(Class)actionAddClass actionJson:(id<JSON>)actionJson;
@end