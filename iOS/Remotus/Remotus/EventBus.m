//
//  Bus.m
//  RemotusProject
//
//  Created by Carlos Ribeiro on 07/05/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "EventBus.h"

@implementation EventBus

-(id)init{
    self = [super init];
    if (self){
        notCenter = [NSNotificationCenter defaultCenter];
    }
    return self;
}

-(void) publish{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"mqttStart" object:nil];
}

-(void) mqttMessageArrived:(MQTTMessage*)message{
    EventMessageArrived *event = [[EventMessageArrived alloc] init:message];
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:event forKey:@"topic"];
    [notCenter postNotificationName:[EventMessageArrived toString] object:self userInfo:userInfo];
 
}

-(void) mqttConnected:(BOOL)value{
    [notCenter postNotificationName:[EventMqttConnected toString] object:nil];
}

-(void) mqttConnectionLost{
    [notCenter postNotificationName:[EventMqttConnectionLost toString] object:nil];
}

-(void) loadCards{
    [notCenter postNotificationName:[EventLoadCards toString] object:nil];
}

-(void) loadCardsLost{
    [notCenter postNotificationName:[EventLoadCardsLost toString] object:nil];
}

-(void) sensorChange:(Sensor*)sensor oldValue:(NSString*)oldValue newValue:(NSString*)newValue{
    EventSensorChange *event = [[EventSensorChange alloc] initWithParms:sensor theOldValue:oldValue theNewdValue:newValue];
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:event forKey:@"topic"];
    [notCenter postNotificationName:[EventSensorChange toString] object:self userInfo:userInfo];
}

-(void) linkNotOk{
    [notCenter postNotificationName:[EventLoadCards toString] object:nil];
}


@end
