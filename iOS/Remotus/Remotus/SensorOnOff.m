//
//  SensorOnOff.m
//  Remotus
//
//  Created by Carlos Ribeiro on 03/06/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "SensorOnOff.h"

@implementation SensorOnOff

- (BOOL)isOn{
    return ![self.value isEqualToString:@"0"];
}

- (NSString *)simpleName{
    return @"SensorOnOff";
}
@end
