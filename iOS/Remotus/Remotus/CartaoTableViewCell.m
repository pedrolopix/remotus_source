//
//  CartaoTableViewCell.m
//  Remotus
//
//  Created by Carlos Ribeiro on 23/05/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "CartaoTableViewCell.h"

@implementation CartaoTableViewCell

- (void)awakeFromNib
{
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

-(void)setupWithCartao:(Cartao *)cartao{
    [self.lblName setText:[cartao name]];
    [self.imgCard setImageWithURL:[NSURL URLWithString:cartao.imageUrl] placeholderImage:[UIImage imageNamed:@"desconhecido.png"]];    
}




@end
