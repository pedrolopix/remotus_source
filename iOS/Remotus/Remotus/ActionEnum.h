//
//  ActionEnum.h
//  Remotus
//
//  Created by Carlos Ribeiro on 09/06/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "Action.h"
#import "ActionEnumItem.h"

@interface ActionEnum : Action

@property NSMutableArray *listValues;
@property (getter = isShowGrid) BOOL showGrid;

-(NSInteger)count;
-(void) addItem:(NSString*)name imageUrl:(NSString*)imageUrl value:(NSString*)value;
-(NSString *)nameOfValue;

@end

