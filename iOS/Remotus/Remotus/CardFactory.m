//
//  CardFactory.m
//  RemotusProject
//
//  Created by Carlos Ribeiro on 15/05/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "CardFactory.h"


static NSMutableArray *listSensors;
static NSMutableArray *listActions;

@implementation CardFactory

+(void)initialize{
    listSensors = [[NSMutableArray alloc] init];
    listActions = [[NSMutableArray alloc] init];
    
    [listSensors addObject:[[ItemSensor alloc]initWithParameters:@"Normal"
        sensorClass:[Sensor class]
        sensorViewClass:[SensorTableViewCell class]
        sensorAddClass:[AddSensorNormalController class]
        sensorJson:[[SensorJSON alloc]init]]];
    
    [listSensors addObject:[[ItemSensor alloc]initWithParameters:@"On/Off" sensorClass:[SensorOnOff class] sensorViewClass:[SensorOnOffTableViewCell class]
                            sensorAddClass:[AddSensorOnOffController class] sensorJson:[[SensorOnOffJSON alloc]init]]];
    
    
    [listActions addObject:[[ItemAction alloc]initWithParameters:@"On/Off" actionClass:[ActionOnOff class] actionViewClass:[ActionOnOffTableViewCell class]
                                                  actionAddClass:[AddActionOnOffController class] actionJson:[[ActionOnOffJSON alloc] init]]];
    [listActions addObject:[[ItemAction alloc]initWithParameters:@"Slider" actionClass:[ActionSlider class] actionViewClass:[ActionSliderTableViewCell class]
                                                       actionAddClass:[AddActionSliderController class] actionJson:[[ActionSliderJSON alloc] init]]];
    [listActions addObject:[[ItemAction alloc]initWithParameters:@"Enum" actionClass:[ActionEnum class] actionViewClass:[ActionEnumTableViewCell class]
                                                  actionAddClass:[AddActionEnumController class] actionJson:[[ActionEnumJSON alloc] init]]];
    

}

#pragma mark - Sensors
+(ItemSensor *)sensor:(NSString*)sensorClass {
    for (ItemSensor *item in listSensors) {
        if ([sensorClass isEqualToString:NSStringFromClass([item.sensorClass class])]){
            return item;
        }
    }
    return nil;
}

+(ItemSensor *)sensorByName:(NSString*)sensorName {
    for (ItemSensor *item in listSensors) {
        if ([sensorName isEqualToString:item.name]){
            return item;
        }
    }
    return nil;
}

+(ItemSensor *)sensorObject:(Sensor*)sensor{
    for (ItemSensor *item in listSensors) {
        if ([[sensor class] isEqual:[item sensorClass]]){
            return item;
        }
    }
    return nil;
}

+(ItemSensor *)sensorClass:(Sensor*)sensor {
    for (ItemSensor *item in listSensors) {
        if ([NSStringFromClass([sensor class]) isEqualToString:NSStringFromClass([item.sensorClass class]) ] )
        {
            return item;
        }
    }
    return nil;
}

+(NSMutableArray *)listNameSensors {
    NSMutableArray *array = [[NSMutableArray alloc] init];
    for (ItemSensor *item in listSensors) {
        [array addObject:item.name];
    }
    return array;
}

+(NSInteger)countSensors {
    return [listSensors count];
}


+(SensorTableViewCell *) createSensorView:(Sensor *)sensor tableView:(UITableView *)tableView {
    ItemSensor *item = (ItemSensor*)[CardFactory sensorClass:sensor];
    if (item==nil){
        NSLog(@"Não existe o sensor: %@", [sensor name]);
        return nil;
    }
    SensorTableViewCell *cell = [[[item.sensorViewClass class] alloc] initWithTableAndSensor:tableView sensor:sensor];
    return cell;
}

+(AddSensorController *) createSensorAdd:(NSString *)sensorName  {
    ItemSensor *item = (ItemSensor*)[CardFactory sensorByName:sensorName];
    if (item==nil){
        NSLog(@"Não existe o sensor: %@", sensorName);
        return nil;
    }
    return [[[item.sensorAddClass class] alloc]init];
}

+(AddSensorController *) createSensorAddBySensor:(Sensor *)sensor  {
    ItemSensor *item = (ItemSensor*)[CardFactory sensorClass:sensor];
    if (item==nil){
        NSLog(@"Não existe o sensor: %@", sensor.name);
        return nil;
    }
    return [[[item.sensorAddClass class] alloc]init];
}

+(Sensor *) createSensor:(NSString *)sensorName  {
    ItemSensor *item = (ItemSensor*)[CardFactory sensorByName:sensorName];
    if (item==nil){
        NSLog(@"Não existe o sensor: %@", sensorName);
        return nil;
    }
    return [[[item.sensorClass class] alloc]init];
}



#pragma mark - Actions
+(ItemAction *)action:(NSString*)actionClass {
    for (ItemAction *item in listActions) {
        if ([actionClass isEqualToString:NSStringFromClass([item.actionClass class])]){
            return item;
        }
    }
    return nil;
}

+(ItemAction *)actionByName:(NSString*)actionName {
    for (ItemAction *item in listActions) {
        if ([actionName isEqualToString:item.name]){
            return item;
        }
    }
    return nil;
}

+(ItemAction *)actionObject:(Action*)action{
    for (ItemAction *item in listActions) {
        if ([[action class] isEqual:[item actionClass]]){
            return item;
        }
    }
    return nil;
}

+(ItemAction *)actionClass:(Action*)action {
    for (ItemAction *item in listActions) {
        if ([NSStringFromClass([action class]) isEqualToString:NSStringFromClass([item.actionClass class]) ] )
        {
            return item;
        }
    }
    return nil;
}

+(NSMutableArray *)listNameActions {
    NSMutableArray *array = [[NSMutableArray alloc] init];
    for (ItemAction *item in listActions) {
        [array addObject:item.name];
    }
    return array;
}

+(NSInteger)countActions {
    return [listActions count];
}


+(ActionTableViewCell *) createActionView:(Action *)action tableView:(UITableView *)tableView {
    ItemAction *item = (ItemAction*)[CardFactory actionClass:action];
    if (item==nil){
        NSLog(@"Não existe a action: %@", [action name]);
        return nil;
    }
    ActionTableViewCell *cell = [[[item.actionViewClass class] alloc] initWithTableAndSensor:tableView sensor:action];
    return cell;
}

+(AddActionController *) createActionAddByAction:(Action *)action  {
    ItemAction *item = (ItemAction*)[CardFactory actionObject:action];
    if (item==nil){
        NSLog(@"Não existe a ação");
        return nil;
    }
    return [[[item.actionAddClass class] alloc]init];
}

+(AddActionController *) createActionAdd:(NSString *)actionName  {
    ItemAction *item = (ItemAction*)[CardFactory actionByName:actionName];
    if (item==nil){
        NSLog(@"Não existe a ação: %@", actionName);
        return nil;
    }
    return [[[item.actionAddClass class] alloc]init];
}

+(Action *) createAction:(NSString *)actionName  {
    ItemAction *item = (ItemAction*)[CardFactory actionByName:actionName];
    if (item==nil){
        NSLog(@"Não existe a ação: %@", actionName);
        return nil;
    }
    return [[[item.actionClass class] alloc]init];
}

@end


#pragma mark - Class Item
@implementation Item
-(id)initWithNameJSON:(NSString*)name json:(id<JSON>)json{
    self = [super init];
    if (self){
        self.name = [name copy];
        self.json = json;
    }
    return self;
}

-(NSMutableDictionary *) toJSON:(id)json{
    return [self.json toJSON:json];
}

-(id) fromJSON:(NSDictionary *)json{
    return [self.json fromJSON:json];
}

@end

@implementation ItemSensor

-(id)initWithParameters:(NSString *)name sensorClass:(Class)sensorClass sensorViewClass:(Class)sensorViewClass sensorAddClass:(Class)sensorAddClass sensorJson:(id<JSON>)sensorJson{
    self = [super init];
    if (self){
        self.name = [name copy];
        self.json = sensorJson;
        self.sensorClass = sensorClass;
        self.sensorViewClass = sensorViewClass;
        self.sensorAddClass = sensorAddClass;
    }
    return self;
}

@end

@implementation ItemAction

-(id)initWithParameters:(NSString *)name actionClass:(Class)actionClass actionViewClass:(Class)actionViewClass actionAddClass:(Class)actionAddClass actionJson:(id<JSON>)actionJson{
    self = [super init];
    if (self){
        self.name = [name copy];
        self.json = actionJson;
        self.actionClass = actionClass;
        self.actionViewClass = actionViewClass;
        self.actionAddClass = actionAddClass;
    }
    return self;
}

@end
