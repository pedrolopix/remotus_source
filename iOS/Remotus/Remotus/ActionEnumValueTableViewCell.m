//
//  ActionEnumValueTableViewCell.m
//  Remotus
//
//  Created by Carlos Ribeiro on 04/07/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "ActionEnumValueTableViewCell.h"
#import "EventSensorChange.h"

@implementation ActionEnumValueTableViewCell

-(void)setupWithActionEnumItem:(ActionEnumItem*)item ActionEnum:(ActionEnum*)action{
    self.item=item;
    self.action = action;
    [self config];
    [self updateAction];
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

-(void)config{
    self.lblName.text = self.item.name;
    [self.imgAction setImageWithURL:[NSURL URLWithString:self.item.imageUrl] placeholderImage:[UIImage imageNamed:@"desconhecido.png"]];
}

- (void)updateAction{
    if ([self.action.value isEqualToString:self.item.value]){
        [self.lblName setFont:[UIFont boldSystemFontOfSize:17.0f]];
    } else {
        [self.lblName setFont:[UIFont systemFontOfSize:17.0f]];
    }
}


@end
