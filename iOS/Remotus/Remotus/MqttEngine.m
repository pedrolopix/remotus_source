//
//  MqttEngine.m
//  RemotusProject
//
//  Created by Carlos Ribeiro on 06/05/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "MqttEngine.h"
#import "MQTTKit.h"
#import "Modelo.h"
#import "EventMessageArrived.h"
#import "EventSensorChange.h"
#import "EventBus.h"

@implementation MqttEngine

-(id) initWithModel:(Modelo*)modelo{
    self = [super init];
    if (self){
        _modelo = modelo;
    }
    return self;
}

- (void) connect {
    self.clientId = [UIDevice currentDevice].identifierForVendor.UUIDString;
    self.client = [[MQTTClient alloc] initWithClientId:self.clientId];
    _connected = NO;

    
    self.client.reconnectDelay = 0;
    self.client.reconnectDelayMax = 0;
    //self.client.reconnectExponentialBackoff = YES;
    [self.client setUsername:self.username];
    [self.client setPassword:self.password];
    [self.client setHost:self.serverURI];
    [self.client setPort:[self.port intValue]];
    
    
    [self.client setMessageHandler:^(MQTTMessage *message) {
        NSLog(@"MQTT receveid message on %@", message.topic);
        [self.modelo.eventBus mqttMessageArrived:message];
    }];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,
                                            (unsigned long)NULL), ^(void) {
        
        [self.client connectWithCompletionHandler:^(MQTTConnectionReturnCode code) {
            if (code == ConnectionAccepted) {
                NSLog(@"MQTT client is connected");
                _connected = YES;
                [_modelo.eventBus mqttConnected:_connected];
            }
            
        }];
        
    });

    
    [self.client disconnectWithCompletionHandler:^(NSUInteger code) {
        NSLog(@"MQTT is disconnected on normal");
        [_modelo.eventBus mqttConnectionLost];
    }];

}

- (void)dealloc
{
    [self.client disconnectWithCompletionHandler:^(NSUInteger code) {
        NSLog(@"MQTT is disconnected on dealloc");
    }];
}

- (void) disconnect{
    [self.client disconnectWithCompletionHandler:^(NSUInteger code) {
        NSLog(@"MQTT client is disconnected");
        _connected = NO;
        [_modelo.eventBus mqttConnectionLost];
    }];
    
}

-(void) forceDisconnect{
    NSLog(@"MQTT forceDisconnect");
    [self.client disconnectWithCompletionHandler:^(NSUInteger code) {
        NSLog(@"MQTT client is disconnected on forceDisconnect");
    }];
    
}

-(BOOL)subscribe:(NSString*)topicFilter qos:(NSInteger)qos{
   if (self.client==nil)
       return NO;
    
    if (topicFilter==nil)
        return NO;
    
    //NSLog(@"MQTT subscribe %@", topicFilter);
    [self.client subscribe:topicFilter withCompletionHandler:nil];
    
    return YES;
}

-(void)unsubscribe:(NSString*)topicFilter {
    if (self.client==nil)
        return;
    
    if (topicFilter==nil)
        return;

    [self.client unsubscribe:topicFilter withCompletionHandler:nil];

}

-(void)publish:(NSString*)topic text:(NSString*)text retained:(BOOL)retained qos:(NSInteger)qos{
    if (self.client==nil)
        return;
    
    [self.client publishString:text toTopic:topic withQos:qos retain:retained completionHandler:^(int mid) {
        
    }];
}


@end
