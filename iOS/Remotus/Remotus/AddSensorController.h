//
//  AddSensorController.h
//  Remotus
//
//  Created by Carlos Ribeiro on 19/06/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "TooltipView.h"
#import "Validator.h"
#import "InvalidTooltipView.h"
#import "ValidTooltipView.h"
#import "QuartzCore/QuartzCore.h"
#import "Sensor.h"
#import "Utils.h"

@interface AddSensorController : UIViewController<UITextFieldDelegate,ValidatorDelegate>{
    TooltipView *_tooltipView;
}

@property Sensor *sensor;
@property Cartao *cartao;
@property Validator *validator;

@property (nonatomic, retain) UIToolbar *keyboardToolbar;
@property (weak, nonatomic) IBOutlet UITextField *txtTitulo;
@property (weak, nonatomic) IBOutlet UITextField *txtTopico;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

- (void)fill;
- (void)save;

@end
