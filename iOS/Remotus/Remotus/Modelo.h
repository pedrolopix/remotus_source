//
//  Model.h
//  RemotusProject
//
//  Created by Carlos Ribeiro on 06/05/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "Reachability.h"
@class Mqtt;
@class EventMqttConnected;
@class EventMqttConnectionLost;
@class CardJSON;
@class JSON;
@class CardFactory;
@class Sensor;
@class EventBus;
@class MqttEngine;
@class Cartoes;
@class Action;
@class Cartao;


@interface Modelo : NSObject{

}

@property (nonatomic, strong) MqttEngine *mqttEngine;
@property (nonatomic, strong) Mqtt *mqtt;
@property (strong) Cartoes *cartoes;
@property (strong) EventBus *eventBus;
@property(getter = isConnected) BOOL connected;
@property(getter = isStarted) BOOL started;
//@property (nonatomic) Reachability *reachability;

-(id) init;
-(void) start;
-(void) stop;
-(void) stopFailOverNetwork;
-(void) startMqtt;
-(void) stopMqtt;
-(void) subscribe:(NSString*)topic qos:(NSInteger)qos;
-(void) unSubscribeCards;
-(void) receiveMqttConnected:(NSNotification *)notification;
-(void) mqttMessageArrived:(NSNotification *)notification;
-(void) mqttConnectionLost:(NSNotification *)notification;
-(void)cartaoChanged:(Cartao *)cartao;
-(void) loadCards:(NSString *)json;
-(void) saveCards;
-(void) subscribeCards;
-(void) dadosTeste;
-(void) publish:(Action*)action value:(NSString *)value;
-(void) removeCartao:(Cartao *)cartao;
-(void) clear;
-(void) reload;
//- (void)networkChanged:(NSNotification *)notification;

@end

