//
//  ActionEnumTableViewCell.h
//  Remotus
//
//  Created by Carlos Ribeiro on 02/07/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "ActionTableViewCell.h"
#import "ActionEnum.h"

@interface ActionEnumTableViewCell : ActionTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblValue;

@end
