//
//  SensorOnOff.h
//  Remotus
//
//  Created by Carlos Ribeiro on 03/06/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "Sensor.h"

@interface SensorOnOff : Sensor
@property NSString *on;
@property NSString *off;
@property NSString *imageOffUrl;


- (BOOL)isOn;
- (NSString *)simpleName;

@end
