//
//  CardTableViewController.h
//  Remotus
//
//  Created by Carlos Ribeiro on 21/05/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
@class Modelo;
@class Cartao;

@interface CardTableViewController : UITableViewController<UITableViewDataSource, UITableViewDelegate>{
    @private
    Cartao *cartao;
}

@property Modelo *model;
@property (nonatomic) Reachability *reachability;

-(void) buildCards:(NSNotification *)notification;
-(void) defaultsSettingsChanged:(NSNotification *)notification;
-(void) networkChanged:(NSNotification *)notification;
-(void) buildCardsLost:(NSNotification *)notification;
@end
