//
//  ActionEnumJSON.m
//  Remotus
//
//  Created by Carlos Ribeiro on 09/06/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "ActionEnumJSON.h"
#import "ActionEnum.h"
#import "ActionEnumItem.h"

@implementation ActionEnumJSON

-(NSMutableDictionary *)toJSON:(id)obj{
    
    if (![obj isKindOfClass:[ActionEnum class]]){
        return nil;
    }
    
    ActionEnum *a = (ActionEnum *)obj;
    NSMutableDictionary *dic = [super toJSON:a];
    [dic addEntriesFromDictionary:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:[a isShowGrid]], TAG_JSON_ENUM_SHOWGRID, nil]];
    
    NSMutableArray *array = [[NSMutableArray alloc]init];
    for (ActionEnumItem *item in a.listValues) {
        NSMutableDictionary *d = [[NSMutableDictionary alloc]init];
        [d addEntriesFromDictionary:[NSDictionary dictionaryWithObjectsAndKeys:[item name], TAG_JSON_ENUM_NAME, nil]];
        [d addEntriesFromDictionary:[NSDictionary dictionaryWithObjectsAndKeys:[item imageUrl], TAG_JSON_ENUM_URL, nil]];
        [d addEntriesFromDictionary:[NSDictionary dictionaryWithObjectsAndKeys:[item value], TAG_JSON_ENUM_VALUE, nil]];
        [array addObject:d];
    }
    if (array.count!=0){
        NSMutableDictionary *tmp = [NSMutableDictionary dictionaryWithObjectsAndKeys:array, TAG_JSON_ENUM_ARRAY, nil];
        [dic addEntriesFromDictionary:tmp];
    }
    return dic;
}

-(id) fromJSON:(NSDictionary *)json{
    ActionEnum *action = [[ActionEnum alloc]init];
    [self fillSensor:json sensor:(Sensor *)action];
    NSMutableArray *array = [json objectForKey:TAG_JSON_ENUM_ARRAY];
    for (NSDictionary *item in array) {
        NSString *name = [item objectForKey:TAG_JSON_ENUM_NAME];
        NSString *url = [item objectForKey:TAG_JSON_ENUM_URL];
        NSString *value = [item objectForKey:TAG_JSON_ENUM_VALUE];
        [action addItem:name imageUrl:url value:value];
    }
    
    return action;
}

-(void) fillSensor:(NSDictionary *)json sensor:(Sensor*)sensor{
    ActionEnum *a = (ActionEnum *)sensor;
    [super fillSensor:json sensor:sensor];
    BOOL show = [[json objectForKey:TAG_JSON_ENUM_SHOWGRID] boolValue];
    [a setShowGrid:show];
}
@end
