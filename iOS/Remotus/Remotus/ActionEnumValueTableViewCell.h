//
//  ActionEnumValueTableViewCell.h
//  Remotus
//
//  Created by Carlos Ribeiro on 04/07/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "ActionEnum.h"
#import "ActionEnumItem.h"

@interface ActionEnumValueTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UIImageView *imgAction;

@property ActionEnumItem *item;
@property ActionEnum *action;
-(void)setupWithActionEnumItem:(ActionEnumItem*)item ActionEnum:(ActionEnum*)action;
- (void)updateAction;
@end
