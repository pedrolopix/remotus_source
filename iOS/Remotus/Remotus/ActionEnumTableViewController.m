//
//  ActionEnumTableViewController.m
//  Remotus
//
//  Created by Carlos Ribeiro on 04/07/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "ActionEnumTableViewController.h"
#import "ActionEnum.h"
#import "Sensor.h"
#import "Modelo.h"
#import "Cartao.h"
#import "Cartoes.h"
#import "EventSensorChange.h"
#import "EventLoadCards.h"
#import "ActionEnumValueTableViewCell.h"
#import "AddActionEnumValueController.h"

@interface ActionEnumTableViewController ()

@end

@implementation ActionEnumTableViewController{
@private
    ActionEnum *action;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadCards:) name:[EventLoadCards toString] object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sensorChange:) name:[EventSensorChange toString] object:nil];
    [self updateView];
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int count = [action.listValues count];
    if (self.editing) count++;
    return count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.isEditing && indexPath.row==[action.listValues count]){
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.textLabel.text = NSLocalizedString(@"NewValue", nil);
        cell.accessoryType = UITableViewCellAccessoryNone;
        return cell;
    }
    static NSString *CellIdentifier = @"ActionEnumItemIdentifier";
    [tableView registerNib:[UINib nibWithNibName:@"ActionEnumValueTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:CellIdentifier];
    ActionEnumItem *item = [action.listValues objectAtIndex:indexPath.row];
    ActionEnumValueTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    [cell setupWithActionEnumItem:item ActionEnum:action];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.editing && !self.editing)
    {
        [cell setEditing:NO animated:YES];
        return;
    }
    
    if (cell.editing){
        AddActionEnumValueController *add;
        if (indexPath.row==[action.listValues count]){
            add = [[AddActionEnumValueController alloc]initWithActionEnum:action actionEnumItem:nil];
        } else{
            ActionEnumItem *item = (ActionEnumItem *)[action.listValues objectAtIndex:indexPath.row];
            add = [[AddActionEnumValueController alloc]initWithActionEnum:action actionEnumItem:item];            
        }
        [self.navigationController pushViewController:add animated:YES];
    } else {
        ActionEnumItem *item = [action.listValues objectAtIndex:indexPath.row];
        [action sendValue:item.value];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        Cartao *c = [self.model.cartoes.listCartao objectAtIndex:self.indexRowCartao];
        Sensor *s = [c.sensors objectAtIndex:self.indexRowSensor];
        Action *a = [s.listActions objectAtIndex:indexPath.row];
        [s removeAction:a];
    }
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView.editing && indexPath.row == [action.listValues count]){
        return  UITableViewCellEditingStyleInsert;
    }
    
    return UITableViewCellEditingStyleDelete;
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath
                                                                  *)indexPath {
    if (indexPath.row == [action.listValues count]) // nao move a ultima linha
        return NO;
    return YES;
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
    NSInteger source = sourceIndexPath.row;
    NSInteger destination = destinationIndexPath.row;
    NSInteger moveDestination=0;
    NSInteger moveSource=0;
    if (destination==[action.listValues count]){
        [self updateTable];
        return;
    }
    
    if(source>destination)
        moveSource=1;
    else if (source<destination)
        moveDestination=1;
    
    if (moveSource==1 || moveDestination==1){
        [action.listValues insertObject:[action.listValues objectAtIndex:source] atIndex:destination+moveDestination];
        [action.listValues removeObjectAtIndex:source+moveSource];
        Cartao *c = [self.model.cartoes.listCartao objectAtIndex:self.indexRowCartao];
        [self.model cartaoChanged:c];
    }
}


-(void)setEditing:(BOOL)editing animated:(BOOL) animated {
    [super setEditing:editing animated:animated];
    [self.tableView setEditing:editing animated:animated];
    [self updateTable];
}

-(void)updateView{
    Cartao *c =[self.model.cartoes.listCartao objectAtIndex:self.indexRowCartao];
    Sensor *s =[c.sensors objectAtIndex:self.indexRowSensor];
    action = (ActionEnum *)[s.listActions objectAtIndex:self.indexRowAcao];
    [self.navigationItem setTitle:action.name];
    [self updateTable];
}

-(void)loadCards:(NSNotification *)notification{
    [self updateView];
}

-(void)updateTable{
    [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
}

-(void)sensorChange:(NSNotification *)notification{
    EventSensorChange *event = [[notification userInfo] valueForKey:@"topic"];
    if ([event.sensor.topicIn isEqualToString:action.topicIn])
        [self updateTable];
}

@end
