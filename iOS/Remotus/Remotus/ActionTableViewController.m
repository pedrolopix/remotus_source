//
//  ActionTableViewController.m
//  Remotus
//
//  Created by Carlos Ribeiro on 10/06/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "ActionTableViewController.h"
#import "Action.h"
#import "ActionSlider.h"
#import "ActionSliderTableViewCell.h"
#import "CardFactory.h"
#import "EventSensorChange.h"
#import "EventLoadCards.h"
#import "Sensor.h"
#import "Modelo.h"
#import "Cartao.h"
#import "Cartoes.h"
#import "AddActionController.h"
#import "ActionEnumTableViewController.h"

@interface ActionTableViewController ()

@end

@implementation ActionTableViewController{
@private
    Sensor *sensor;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadCards:) name:[EventLoadCards toString] object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sensorChange:) name:[EventSensorChange toString] object:nil];
    [self updateView];
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void) viewWillAppear: (BOOL) animated {
    //return;
    [super viewWillAppear:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int count = [sensor.listActions count];
    if (self.editing) count++;
    return count;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.isEditing && indexPath.row==[sensor.listActions count]){
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.textLabel.text = NSLocalizedString(@"NewAction", nil);
        cell.accessoryType = UITableViewCellAccessoryNone;
        return cell;
    }
    ActionTableViewCell *cell = (ActionTableViewCell*) [CardFactory createActionView:[sensor.listActions objectAtIndex:indexPath.row] tableView:tableView];


    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.editing && !self.editing)
    {
        [cell setEditing:NO animated:YES];
        return;
    }
    
    if (cell.editing){
        if (indexPath.row==[sensor.listActions count]){
            NSLog(@"new sensor");
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"ChoiceAction", nil)
                                                                     delegate:self
                                                            cancelButtonTitle:nil
                                                       destructiveButtonTitle:nil
                                                            otherButtonTitles: nil];
            
            for (NSString *str in [CardFactory listNameActions]) {
                [actionSheet addButtonWithTitle:str];
            }
            [actionSheet addButtonWithTitle:NSLocalizedString(@"Cancel", nil)];
            [actionSheet setCancelButtonIndex:[CardFactory countActions]];
            [actionSheet showInView:self.view];
            
        } else{
            NSLog(@"edit sensor");
            Action *a = (Action *)[sensor.listActions objectAtIndex:indexPath.row];
            AddActionController *add = [CardFactory createActionAddByAction:a];
            if (add!=nil){
                Sensor *s = [a sensor];
                Cartao *c = [s card];
                [add setAction:a];
                [add setSensor:s];
                [add setCartao:c];
                
                add.editing = YES;
                [self.navigationController pushViewController:add animated:YES];
            }

        }
    }  else{
        Action *a = (Action *)[sensor.listActions objectAtIndex:indexPath.row];
        if ([a isKindOfClass:[ActionEnum class]]){
            [self performSegueWithIdentifier:@"segueActionEnum" sender:self];
        }
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        Cartao *c = [self.model.cartoes.listCartao objectAtIndex:self.indexRowCartao];
        Sensor *s = [c.sensors objectAtIndex:self.indexRowSensor];
        Action *a = [s.listActions objectAtIndex:indexPath.row];
        [s removeAction:a];
    }
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView.editing && indexPath.row == [sensor.listActions count]){
        return  UITableViewCellEditingStyleInsert;
    }
    
    return UITableViewCellEditingStyleDelete;
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath
                                                                  *)indexPath {
    if (indexPath.row == [sensor.listActions count]) // nao move a ultima linha
        return NO;
    return YES;
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
    NSInteger source = sourceIndexPath.row;
    NSInteger destination = destinationIndexPath.row;
    NSInteger moveDestination=0;
    NSInteger moveSource=0;
    if (destination==[sensor.listActions count]){
        [self updateTable];
        return;
    }
    
    if(source>destination)
        moveSource=1;
    else if (source<destination)
        moveDestination=1;
    
    if (moveSource==1 || moveDestination==1){
        [sensor.listActions insertObject:[sensor.listActions objectAtIndex:source] atIndex:destination+moveDestination];
        [sensor.listActions removeObjectAtIndex:source+moveSource];
        [sensor.card.model cartaoChanged:sensor.card];
    }
}


-(void)setEditing:(BOOL)editing animated:(BOOL) animated {
    [super setEditing:editing animated:animated];
    [self updateTable];
}

-(void)updateView{
    Cartao *c =[self.model.cartoes.listCartao objectAtIndex:self.indexRowCartao];
    sensor =[c.sensors objectAtIndex:self.indexRowSensor];
    [self.navigationItem setTitle:sensor.name];
    [self updateTable];
}

-(void)loadCards:(NSNotification *)notification{
    [self updateView];
}

-(void)updateTable{
    [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
}

-(void)sensorChange:(NSNotification *)notification{
    EventSensorChange *event = [[notification userInfo] valueForKey:@"topic"];
    for (Action *item in sensor.listActions) {
        if ([event.sensor.topicIn isEqualToString:item.topicIn]){
            [self updateTable];
            return;
        }
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    if ([[segue identifier] isEqualToString:@"segueActionEnum"]){
        ActionEnumTableViewController *dest = [segue destinationViewController];
        dest.indexRowCartao = self.indexRowCartao;
        dest.indexRowSensor = self.indexRowSensor;
        dest.indexRowAcao = indexPath.row;
        dest.model = self.model;
    }
}

#pragma delegate UIActionSheet
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    AddActionController *add = [CardFactory createActionAdd:buttonTitle];
    if (add!=nil){
        Action *a = [CardFactory createAction:buttonTitle];
        [add setCartao:sensor.card];
        [add setSensor:sensor];
        [add setAction:a];
        add.editing = NO;
        [self.navigationController pushViewController:add animated:YES];
    }
}


@end
