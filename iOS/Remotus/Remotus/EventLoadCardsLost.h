//
//  EventLoadCardsLost.h
//  Remotus
//
//  Created by Carlos Ribeiro on 13/07/14.
//  Copyright (c) 2014 ISEC. All rights reserved.
//

#import "Event.h"

@interface EventLoadCardsLost : Event
+(NSString*)toString;
@end
