#! /bin/sh
# /etc/init.d/arduino2mqtt.sh

### BEGIN INIT INFO
# Provides:          arduino2mqtt
# Required-Start:    $remote_fs $syslog
# Required-Stop:     $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Script para iniciar arduino2mqtt
# Description:       Script para iniciar arduino2mqtt durante o boot e parar durante o shutdown
### END INIT INFO


case "$1" in
  start)
    echo "A iniciar arduino2mqtt"
    # run application you want to start
    cd /home/pi/RemotusServer/
    sudo java -Djava.library.path=/usr/lib/jni -jar arduino2mqtt.jar -o arduino2mqtt.json &
    ;;
  debug)
    echo "A iniciar arduino2mqtt em modo de depuração"
    # run application you want to start
    cd /home/pi/RemotusServer/
    sudo java -Djava.library.path=/usr/lib/jni -jar arduino2mqtt.jar -o arduino2mqtt.json -d
    ;;
  stop)
    echo "A parar arduino2mqtt"
    # kill application you want to stop
    pkill -f 'java.*arduino2mqtt'
    echo "Feito"
    ;;
  restart)
    echo "A reinicar arduino2mqtt"
    pkill -f 'java.*arduino2mqtt'
    cd /home/pi/RemotusServer/
    sudo java -Djava.library.path=/usr/lib/jni -jar arduino2mqtt.jar -o arduino2mqtt.json &
    echo "Feito"
    ;;
  install)
    echo "A instalar arduino2mqtt"
    #copiar script para o arranque
    sudo cp arduino2mqtt.sh /etc/init.d/arduino2mqtt.sh
    #registar o script para ser executado no arranque
    sudo update-rc.d arduino2mqtt.sh defaults
    echo "Feito"
    ;;
  uninstall)
    echo "A remover arduino2mqtt"
    sudo update-rc.d -f arduino2mqtt.sh remove
    echo "Feito"
    ;;
  ports)
    cd /home/pi/RemotusServer/
    sudo java -Djava.library.path=/usr/lib/jni -jar arduino2mqtt.jar -p
    ;;
*)
    echo "Usage: arduino2mqtt.sh {start|stop|restart|install|uninstall|ports}"
    echo "  start      - inicia a aplicação"
    echo "  stop       - termina a aplicação"
    echo "  restart    - reinicia a aplicação"
    echo "  install    - instala a aplicação para iniciar quando o sistema arranca"
    echo "  uninstall  - remove a aplicação do iniciar quando o sistema arranca"
    echo "  ports      - mostra as portas disponiveis. Só funciona com a aplicação parada"
    echo "  debug      - inicia em modo de depuração"
    exit 1
    ;;
esac

exit 0