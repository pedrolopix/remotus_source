#! /bin/sh
# /etc/init.d/remotusService.sh

### BEGIN INIT INFO
# Provides:          remotusService
# Required-Start:    $remote_fs $syslog
# Required-Stop:     $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Script para iniciar remotusService
# Description:       Script para iniciar remotusService durante o boot e parar durante o shutdown
### END INIT INFO


case "$1" in
  start)
    echo "A iniciar remotusService"
    # run application you want to start
    cd /home/pi/RemotusServer/
    sudo java -Djava.library.path=/usr/lib/jni -jar remotusService.jar -o remotusService.json &
    ;;
  debug)
    echo "A iniciar remotusService em modo de depuração"
    # run application you want to start
    cd /home/pi/RemotusServer/
    sudo java -Djava.library.path=/usr/lib/jni -jar remotusService.jar -o remotusService.json -d
    ;;
  stop)
    echo "A parar remotusService"
    # kill application you want to stop
    pkill -f 'java.*remotusService'
    echo "Feito"
    ;;
  restart)
    echo "A reinicar remotusService"
    pkill -f 'java.*remotusService'
    cd /home/pi/RemotusServer/
    sudo java -Djava.library.path=/usr/lib/jni -jar remotusService.jar -o remotusService.json &
    echo "Feito"
    ;;
  install)
    echo "A instalar remotusService"
    #copiar script para o arranque
    sudo cp remotusService.sh /etc/init.d/remotusService.sh
    #registar o script para ser executado no arranque
    sudo update-rc.d remotusService.sh defaults
    echo "Feito"
    ;;
  uninstall)
    echo "A remover remotusService"
    sudo update-rc.d -f remotusService.sh remove
    echo "Feito"
    ;;
  ports)
    cd /home/pi/RemotusServer/
    sudo java -Djava.library.path=/usr/lib/jni -jar remotusService.jar -p
    ;;
*)
    echo "Usage: remotusService.sh {start|stop|restart|install|uninstall|ports|debug}"
    echo "  start      - inicia a aplicação"
    echo "  stop       - termina a aplicação"
    echo "  restart    - reinicia a aplicação"
    echo "  install    - instala a aplicação para iniciar quando o sistema arranca"
    echo "  uninstall  - remove a aplicação do iniciar quando o sistema arranca"
    echo "  ports      - mostra as portas disponiveis. Só funciona com a aplicação parada"
    echo "  debug      - inicia em modo de depuração"
    exit 1
    ;;
esac

exit 0