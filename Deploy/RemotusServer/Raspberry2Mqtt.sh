#! /bin/sh
# /etc/init.d/Raspberry2Mqtt.sh

### BEGIN INIT INFO
# Provides:          Raspberry2Mqtt
# Required-Start:    $remote_fs $syslog
# Required-Stop:     $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Script para iniciar Raspberry2Mqtt
# Description:       Script para iniciar Raspberry2Mqtt durante o boot e parar durante o shutdown
### END INIT INFO


case "$1" in
  start)
    echo "A iniciar Raspberry2Mqtt"
    # run application you want to start
    cd /home/pi/RemotusServer/
    sudo java -Djava.library.path=/usr/lib/jni -jar Raspberry2Mqtt.jar -o Raspberry2Mqtt.json &
    ;;
  debug)
    echo "A iniciar Raspberry2Mqtt em modo de depuração"
    # run application you want to start
    cd /home/pi/RemotusServer/
    sudo java -Djava.library.path=/usr/lib/jni -jar Raspberry2Mqtt.jar -d -o Raspberry2Mqtt.json
    ;;
  stop)
    echo "A parar Raspberry2Mqtt"
    # kill application you want to stop
    pkill -f 'java.*Raspberry2Mqtt'
    echo "Feito"
    ;;
  restart)
    echo "A reinicar Raspberry2Mqtt"
    pkill -f 'java.*Raspberry2Mqtt'
    cd /home/pi/RemotusServer/
    sudo java -Djava.library.path=/usr/lib/jni -jar Raspberry2Mqtt.jar -o Raspberry2Mqtt.json &
    echo "Feito"
    ;;
  install)
    echo "A instalar Raspberry2Mqtt"
    #copiar script para o arranque
    sudo cp Raspberry2Mqtt.sh /etc/init.d/Raspberry2Mqtt.sh
    #registar o script para ser executado no arranque
    sudo update-rc.d Raspberry2Mqtt.sh defaults
    echo "Feito"
    ;;
  uninstall)
    echo "A remover Raspberry2Mqtt"
    sudo update-rc.d -f Raspberry2Mqtt.sh remove
    echo "Feito"
    ;;
  ports)
    cd /home/pi/RemotusServer/
    sudo java -Djava.library.path=/usr/lib/jni -jar Raspberry2Mqtt.jar -p
    ;;
*)
    echo "Usage: Raspberry2Mqtt.sh {start|stop|restart|install|uninstall|ports|debug}"
    echo "  start      - inicia a aplicação"
    echo "  stop       - termina a aplicação"
    echo "  restart    - reinicia a aplicação"
    echo "  install    - instala a aplicação para iniciar quando o sistema arranca"
    echo "  uninstall  - remove a aplicação do iniciar quando o sistema arranca"
    echo "  ports      - mostra as portas disponiveis. Só funciona com a aplicação parada"
    echo "  debug      - inicia em modo de depuração"
    exit 1
    ;;
esac

exit 0