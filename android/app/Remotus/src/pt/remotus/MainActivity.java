
package pt.remotus;
import org.holoeverywhere.addon.AddonSlider;
import org.holoeverywhere.addon.AddonSlider.AddonSliderA;
import org.holoeverywhere.app.Activity;
import org.holoeverywhere.app.Activity.Addons;
import org.holoeverywhere.slider.SliderMenu;

import pt.remotus.fragments.AboutFragment;
import pt.remotus.fragments.CardsFragment;
import pt.remotus.fragments.SettingsFragment;
import pt.remotus.service.RemotusService;
import pt.remotus.service.RemotusService.LocalBinder;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import com.actionbarsherlock.app.ActionBar;

@Addons(Activity.ADDON_SLIDER)
public class MainActivity extends Activity {
	private static final String TAG = "MainActivity";
	public RemotusService mService;
	public AddonSliderA addonSlider() {
		return addon(AddonSlider.class);
	}


	private final ServiceConnection conn = new ServiceConnection() {


		@Override
		public void onServiceDisconnected(ComponentName name) {
		}

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			LocalBinder b = ((LocalBinder) service);
			mService = b.getService();
		}
	};



    @Override
    public void onCreate(Bundle savedInstanceState) {
    	Log.d(TAG,"onCreate");
        savedInstanceState = instanceState(savedInstanceState);
        super.onCreate(savedInstanceState);


        final ActionBar ab = getSupportActionBar();
        ab.setTitle(R.string.app_name);
		// setContentView(R.layout.activity_main);
		final SliderMenu sliderMenu = addonSlider().obtainDefaultSliderMenu(R.layout.navmenu);
//
		sliderMenu.add(R.string.cards, CardsFragment.class, SliderMenu.BLUE);
		sliderMenu.add(R.string.settings, SettingsFragment.class, SliderMenu.BLUE);
		sliderMenu.add(R.string.about, AboutFragment.class, SliderMenu.BLUE);

		addonSlider().setOverlayActionBar(false);

		// We are should provide activity to ThemePicker
		// ((NavBarThemePicker) findViewById(R.id.themePicker)).setActivity(this);
		Bundle extras = getIntent().getExtras();
		int id = -1;
		if (extras != null) {
			id = extras.getInt("notificationId");
			//mService.clearNotifications(id);
		}
        Intent intent= new Intent(this, RemotusService.class);
		intent.putExtra("notificationId", id);
		startService(intent);
        bindService(intent, conn, BIND_AUTO_CREATE);

    }


    @Override
    protected void onDestroy() {
    	Log.d(TAG,"Destroy");
    	//unbindService(conn);
		//Intent intent = new Intent(this, RemotusService.class);
		//stopService(intent);
        super.onDestroy();
    }




}
