package pt.remotus.cards;

import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.app.Activity;
import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.app.DialogFragment;
import org.holoeverywhere.app.Fragment;
import org.holoeverywhere.widget.EditText;
import org.holoeverywhere.widget.LinearLayout;
import org.holoeverywhere.widget.PopupMenu;
import org.holoeverywhere.widget.PopupMenu.OnMenuItemClickListener;
import org.holoeverywhere.widget.TextView;

import pt.remotus.R;
import pt.remotus.dialogs.AddSensor;
import pt.remotus.dialogs.AddSensorChoice;
import pt.remotus.model.Model;
import pt.remotus.model.action.Action;
import pt.remotus.model.sensor.Sensor;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.fima.cardsui.views.CardUI;


public class CardUi extends com.fima.cardsui.objects.Card implements OnMenuItemClickListener {

	private static final String TAG = "CardEntity";

	private static final int MNU_EDITAR_TITULO = 0;
	private static final int MNU_APAGAR = 1;
	private static final int MNU_ADD_SENSOR = 2;

	private final Model mModel;
	private LinearLayout mSensors;
	private TextView mTvtitle;
	private final pt.remotus.model.Cartao mCartao;
	private final CardUI mCardUI;
	private LinearLayout mLayoutActions;
	private Object mLastCardSensor;
	private ImageView mBtSettings;
	private PopupMenu mPopupMenu;
	private final Activity mActitvity;

	public CardUi(Activity activity,CardUI mCardView, pt.remotus.model.Cartao cartao, Model model) {
		this.mModel=model;
		this.mCartao=cartao;
		this.mCardUI=mCardView;
		this.mActitvity=activity;
	}

	@Override
	public View getCardContent(Context context) {
		View view = LayoutInflater.from(context).inflate(R.layout.card_entity, null);

		mTvtitle=((TextView) view.findViewById(R.id.title));
		mTvtitle.setText(mCartao.getName());
		mSensors=((LinearLayout) view.findViewById(R.id.sensors));
		mLayoutActions=(LinearLayout)view.findViewById(R.id.layoutActions);
		mLayoutActions.setVisibility(View.GONE);
		buildSensors(context,view);
	    mBtSettings = (ImageView)view.findViewById(R.id.btSettings);

		createEditmenu(view);
		prepateEdit();
		return view;
	}


	private void prepateEdit() {
		mTvtitle.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				editTitle();
			}
		});
	}

	private void editTitle() {
		AlertDialog.Builder alert = new AlertDialog.Builder(mCardUI.getContext());

		alert.setTitle("Titulo do cart??o");

		// Set an EditText view to get user input
		final EditText input = new EditText(mCardUI.getContext());
		input.setText(mCartao.getName());
		alert.setView(input);

		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int whichButton) {
		  String value = input.getText().toString();
		  mCartao.setName(value);
		  mModel.cartaoChanged(mCartao);
		  }
		});

		alert.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
		  @Override
		public void onClick(DialogInterface dialog, int whichButton) {
		    // Canceled.
		  }
		});

		alert.show();
	}


	private void buildSensors(Context context, View view) {
		for (Sensor item : mCartao.getSensors()) {
			View v= CardFactory.createCardSensor(context, this, mModel, item);
			mSensors.addView(v);
			buildActions(context,item);
		}
	}

	private void buildActions(Context context, Sensor sensor) {
		for (Action item : sensor.getActions()) {
			View v= CardFactory.createCardAction(context, this, mModel, item);
			if (v!=null) {
				v.setTag(item);
				mLayoutActions.addView(v);
			}
		}
	}

	public void toggleActionsVisibility(SensorUi sensorUI) {
		if (mLastCardSensor==sensorUI) {
			if(mLayoutActions.getVisibility()==View.VISIBLE) {
				mLayoutActions.setVisibility(View.GONE);
			} else {
				mLayoutActions.setVisibility(View.VISIBLE);
			}
		} else {
			mLayoutActions.setVisibility(View.VISIBLE);
		}

		mLastCardSensor=sensorUI;
		for(int i=0; i<mLayoutActions.getChildCount(); i++) {
			Action a=(Action)mLayoutActions.getChildAt(i).getTag();
			if (a==null) continue;
			if (a.getSensor()==sensorUI.sensor) {
				mLayoutActions.getChildAt(i).setVisibility(View.VISIBLE);
			} else {
				mLayoutActions.getChildAt(i).setVisibility(View.GONE);
			}
		}
	}


	private void createEditmenu(View view) {
		mPopupMenu = new PopupMenu(mCardUI.getContext(),mBtSettings);
		mPopupMenu.getMenu().add(Menu.NONE,MNU_EDITAR_TITULO,Menu.NONE,R.string.action_edit_title);
		mPopupMenu.getMenu().add(Menu.NONE,MNU_APAGAR,Menu.NONE,R.string.action_delete);
		mPopupMenu.getMenu().add(Menu.NONE,MNU_ADD_SENSOR,Menu.NONE,R.string.action_add_sensor);

		mPopupMenu.setOnMenuItemClickListener(this);
		mBtSettings.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mPopupMenu.show();
			}
		});
	}


	@Override
	public boolean onMenuItemClick(MenuItem item) {
		switch (item.getItemId()) {
		case MNU_EDITAR_TITULO:
			editTitle();
			break;
		case MNU_ADD_SENSOR:
			addSensor();
			break;
		case MNU_APAGAR:
			mModel.removeCartao(mCartao);
			mCardUI.removeCard(this, true);
			break;
		}
		return true;
	}


	private void addSensor() {
		Fragment fragment = Fragment.instantiate(AddSensorChoice.class);
		Bundle args= new Bundle();
		args.putInt(AddSensor.CARTAO_ID, mCartao.getId());
		fragment.setArguments(args);
		((DialogFragment) fragment).show(mActitvity);
	}

	public Activity getActivity() {
		return mActitvity;
	}

	public pt.remotus.model.Cartao getCartao() {
		return mCartao;
	}

	public CardUI getCardUI() {
		return mCardUI;
	}

	public void removeSensor(Sensor sensor) {
		mCartao.removeSensor(sensor);
		for (int i=0; i<mSensors.getChildCount(); i++) {
			if (mSensors.getChildAt(i).getTag()==sensor) {
				mSensors.removeViewAt(i);
				break;
			}
		}

		// TODO remover actions
		mModel.cartaoChanged(mCartao);
	}

	public void removeAction(Action action) {
		Sensor s=action.getSensor();
		s.removeAction(action);
		for(int i=0; i<mLayoutActions.getChildCount(); i++) {
			Action a=(Action)mLayoutActions.getChildAt(i).getTag();
			if (a==action) {
				mLayoutActions.removeViewAt(i);
				break;
			}
		}
		mModel.cartaoChanged(mCartao);
	}


}
