package pt.remotus.cards;

import pt.remotus.model.Model;
import pt.remotus.model.sensor.Sensor;
import pt.remotus.model.sensor.SensorOnOff;
import android.view.View;


public class SensorOnOffUi extends SensorUi{
	private SensorOnOff sensor;
	
	
	public SensorOnOffUi(Model model, CardUi cardEntity, Sensor sensor) {
		super(model, cardEntity, sensor);
		this.sensor=(SensorOnOff)sensor;
	}

	
	@Override
	protected void update() {
		if (tvinfo==null) return;
		if (image==null) return;
		tvValue.setVisibility(View.GONE);
		if (sensor.isOn()) {
			tvinfo.setText(sensor.getOn());
			// TODO Mudar imagem
			//image.setImageResource(R.drawable.lamp_on);	
			image.setImageUrl(sensor.getImageUrl(),mImageLoader);
		} else {
			tvinfo.setText(sensor.getOff());
			//image.setImageResource(R.drawable.lamp_off);	
			image.setImageUrl(sensor.getImageOffUrl(),mImageLoader);
		}
		super.update();
	}
	
	
}
