package pt.remotus.cards;

import org.holoeverywhere.app.DialogFragment;
import org.holoeverywhere.app.Fragment;
import org.holoeverywhere.widget.PopupMenu;
import org.holoeverywhere.widget.PopupMenu.OnMenuItemClickListener;

import pt.remotus.R;
import pt.remotus.RemotusApplication;
import pt.remotus.cards.CardFactory.ItemSensor;
import pt.remotus.dialogs.AddAction;
import pt.remotus.dialogs.AddActionChoice;
import pt.remotus.dialogs.AddSensor;
import pt.remotus.model.Model;
import pt.remotus.model.event.EventConnetionState;
import pt.remotus.model.event.EventSensorChange;
import pt.remotus.model.sensor.Sensor;
import pt.remotus.service.RemotusService.ConnectionStatus;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.widget.TextView;

import com.actionbarsherlock.view.MenuItem;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.squareup.otto.Subscribe;

public class SensorUi implements OnMenuItemClickListener {
	private static final int MNU_APAGAR = 1;
	private static final int MNU_EDIT_SENSOR = 2;
	private static final int MNU_ADD_ACCAO = 3;
	private static final String TAG = "SensorUi";
	protected String title;
	protected CardUi cardEntity;
	protected Sensor sensor;
	protected Model model;
	protected NetworkImageView image;
	protected TextView tvinfo;
	protected TextView tvValue;
	protected boolean connected;
	protected ImageLoader mImageLoader;
	private PopupMenu popupMenu;
	private boolean first=true;
	private class Subscribers {

		public Subscribers() {
			super();
			model.getBus().register(this);
		}

		@Subscribe
		public void onSensorChange(EventSensorChange event) {
			if (sensor!=null && event.getSensor()==sensor) {
				update();
			}
		}

		@Subscribe
		public void OnEventConnetionState(EventConnetionState event) {
			boolean c = event.getStatus() == ConnectionStatus.CONNECTED;
			if (c!=connected || first) {
				update();
			}
			connected = c;
			first=false;
		}
	};

	public SensorUi(Model model,CardUi cardEntity, Sensor sensor) {
		super();
		this.cardEntity = cardEntity;
		this.model = model;
		this.sensor = sensor;
		connected=model.isStarted();
		new Subscribers();
		mImageLoader= RemotusApplication.get().getImageLoader();
	}

	public View getViewContent(Context context) {
		View view = LayoutInflater.from(context).inflate(R.layout.card_sensor, null);
		image=(NetworkImageView) view.findViewById(R.id.image);
		tvinfo = ((TextView) view.findViewById(R.id.tvinfo));
		tvValue= ((TextView) view.findViewById(R.id.tvValue));
		tvinfo.setText(sensor.getName());
		if (sensor.isValueVisible()) {
			tvValue.setText(sensor.getValue());
		}
		view.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				viewClick();
			}
		});
		view.setOnLongClickListener(new OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				return viewLongClick();
			}
		});
		image.setImageUrl(sensor.getImageUrl(), mImageLoader);
		createEditmenu(view);
		return view;
	}


	protected boolean viewLongClick() {
		popupMenu.show();
		return true;
	}

	protected void viewClick(){
		cardEntity.toggleActionsVisibility(this);
	}

	protected void update() {
		if (tvValue==null) {
			return;
		}
		if (image==null) {
			return;
		}
		if (sensor.isValueVisible()) {
			tvValue.setText(sensor.getValue());
		}
//		if (!connected) {
//			image.setAlpha((float)0.1);
//			tvValue.setAlpha((float)0.1);
//		} else {
//			image.setAlpha((float)1);
//			tvValue.setAlpha(1);
//		}
	}


	@Override
	protected void finalize() throws Throwable {
		Model.getInstance().getBus().unregister(this);
		super.finalize();
	}

	private void createEditmenu(View view) {
		popupMenu = new PopupMenu(cardEntity.getActivity(),view);
		popupMenu.getMenu().add(Menu.NONE,MNU_APAGAR,Menu.NONE,R.string.action_delete);
		popupMenu.getMenu().add(Menu.NONE,MNU_EDIT_SENSOR,Menu.NONE,R.string.action_edit_sensor);
		popupMenu.getMenu().add(Menu.NONE,MNU_ADD_ACCAO,Menu.NONE,R.string.action_adicionar_acao);

		popupMenu.setOnMenuItemClickListener(this);

	}

	@Override
	public boolean onMenuItemClick(MenuItem item) {
		switch (item.getItemId()) {
		case MNU_APAGAR:
			cardEntity.removeSensor(sensor);
			break;
		case MNU_EDIT_SENSOR:
			editSensor();
			break;
		case MNU_ADD_ACCAO:
			addAction();
			break;
		}
		return false;
	}

	private void editSensor() {
        ItemSensor item = CardFactory.getSensor(sensor);
        if (item==null) {
			Log.d(TAG, "configuração não encontrada para:" + sensor.getName());
        	return; // TODO registar erro
        }

		Intent intent = new Intent(cardEntity.getActivity(), item.getAddClass());
		intent.putExtra(AddSensor.CARTAO_ID, cardEntity.getCartao().getId());
		intent.putExtra(AddSensor.SENSOR_NAME, sensor.getName());
		intent.putExtra(AddSensor.SENSOR_ID, sensor.getId());
		cardEntity.getActivity().startActivity(intent);
	}

	private void addAction() {
		Fragment fragment = Fragment.instantiate(AddActionChoice.class);
		Bundle args= new Bundle();
		args.putInt(AddAction.CARTAO_ID, cardEntity.getCartao().getId());
		args.putInt(AddAction.SENSOR_ID, sensor.getId());
		args.putString(AddAction.SENSOR_NAME, sensor.getName());
		fragment.setArguments(args);
		((DialogFragment) fragment).show(cardEntity.getActivity());
	}

}
