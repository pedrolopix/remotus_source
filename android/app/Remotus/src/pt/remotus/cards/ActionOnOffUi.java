package pt.remotus.cards;

import org.holoeverywhere.widget.Switch;

import pt.remotus.model.Model;
import pt.remotus.model.action.Action;
import pt.remotus.model.action.ActionOnOff;
import android.content.Context;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

public class ActionOnOffUi extends ActionUi {

	private Switch bt;
	private boolean isUpdating;

	public ActionOnOffUi(Model model, CardUi cardEntity, Action action) {
		super(model, cardEntity, action);
	}

	@Override
	protected void viewClick() {
		if (mAction instanceof ActionOnOff) {
			((ActionOnOff) sensor).toggle();
		}
	}



	@Override
	protected View getActionView(Context context) {
	   // LinearLayout l= new LinearLayout(context);
		bt= new Switch(context);
		//bt.setBackgroundResource(R.drawable.selectable_background_cardbank);
		//l.addView(bt);
		if (mAction instanceof ActionOnOff) {
			final ActionOnOff a=((ActionOnOff) sensor);
			bt.setTextOn(a.getOn());
			bt.setTextOff(a.getOff());
		
			bt.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					if (isUpdating) return;
					a.toggle();
				}
			});
		}
		return bt;
	}

	@Override
	protected void update() {
		isUpdating=true;
		if (bt!=null && sensor instanceof ActionOnOff) {
			final ActionOnOff a=((ActionOnOff) sensor);
			
			bt.setChecked(a.isOn());
			
		}
		super.update();
		
		isUpdating=false;
	}



	

}
