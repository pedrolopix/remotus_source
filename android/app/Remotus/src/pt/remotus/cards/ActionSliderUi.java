package pt.remotus.cards;


import org.holoeverywhere.widget.TextView;

import pt.remotus.model.Model;
import pt.remotus.model.action.Action;
import pt.remotus.model.action.ActionSlider;
import pt.remotus.model.sensor.Sensor;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class ActionSliderUi extends ActionUi {

	private boolean isUpdating;
	private SeekBar seekBar;
	private TextView textView;
	
	public ActionSliderUi(Model model, CardUi cardEntity, Action action) {
		super(model, cardEntity, action);
	}

	@Override
	protected void viewClick() {
		
	}

	@Override
	protected View getActionView(Context context) {
		LinearLayout ll = new LinearLayout(context);
		ll.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		ll.setOrientation(android.widget.LinearLayout.VERTICAL);
		
		seekBar = new SeekBar(context);
		textView = new TextView(context);
		textView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		textView.setGravity(Gravity.CENTER_HORIZONTAL);
		
		if (mAction instanceof ActionSlider) {
			final ActionSlider a = ((ActionSlider) sensor);
			seekBar.setMax(getMax(a));		
			seekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {
					if (!a.isTriggerUp()) return;
					if (!isUpdating){
						int v = translateSeekBarToValue(seekBar.getProgress(), a);
						mAction.setValue(String.valueOf(v));
						updateText(v);
					}
				}

				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {

				}

				@Override
				public void onProgressChanged(SeekBar seekBar, int progress,
						boolean fromUser) {
					int v = translateSeekBarToValue(progress, a);
					if (!a.isTriggerUp()) {
						if (!isUpdating && fromUser) {
							mAction.setValue(String.valueOf(v));
						}
					}
					updateText(v);

				}
			});
		}
		
		ll.addView(seekBar);
		ll.addView(textView);
		return ll;
	}

	@Override
	protected void update() {
		isUpdating = true;
		try {
			int v = translateValueToSeekBar(
					Integer.parseInt(sensor.getValue()), sensor);
			seekBar.setProgress(v);
			updateText(Integer.parseInt(sensor.getValue()));
		} catch (Exception e) {
			//
		}
		super.update();

		isUpdating = false;
	}
	
	private void updateText(int value){
		textView.setText(String.valueOf(value));
	}
	
	private int translateValueToSeekBar(int value, final Sensor sensor){
		int v=0;
		final ActionSlider a = (ActionSlider)sensor;
		v= (value-a.getMin())/a.getIncremental();
		return v;
	}
	
	private int translateSeekBarToValue(int value, final ActionSlider a){
		int v=0;
		v= value*a.getIncremental()+a.getMin();
		return v;
	}
	
	private int getMax(final ActionSlider a){
		int v=0;
		if (a.getIncremental()==0) return 0;
		v= (a.getMax()-a.getMin())/a.getIncremental();
		return v;
	}
	
}



