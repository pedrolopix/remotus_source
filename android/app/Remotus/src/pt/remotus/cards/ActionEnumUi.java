package pt.remotus.cards;

import java.util.ArrayList;
import java.util.List;

import pt.remotus.R;
import pt.remotus.adapter.EnumMainAdapter;
import pt.remotus.model.Model;
import pt.remotus.model.action.Action;
import pt.remotus.model.action.ActionEnum;
import pt.remotus.widget.ExpandableHeightGridView;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;


public class ActionEnumUi extends ActionUi {

	private boolean isUpdating;
	private List<TextView> listTvAction;
	private EnumMainAdapter enumImage;
	public ActionEnumUi(Model model, CardUi cardEntity, Action action) {
		super(model, cardEntity, action);
		listTvAction = new ArrayList<TextView>();
	}

	@Override
	protected void viewClick() {
		
	}

	@Override
	protected View getActionView(Context context) {
		final ActionEnum a = ((ActionEnum) sensor);
		View v = null;
		if (a.isShowGrid()) {
			v = createGridView(context, a);
		} else {
			v = createListView(context, a);
		}
		return v;
	}

	private View createGridView(Context context, final ActionEnum a) {
		enumImage = new EnumMainAdapter(cardEntity.getActivity(),a.getActionList(), mImageLoader, a);
		LayoutInflater inflater = cardEntity.getActivity().getLayoutInflater();
		View rowView =inflater.inflate(R.layout.card_action_grid, null);
		ExpandableHeightGridView gv = (ExpandableHeightGridView) rowView.findViewById(R.id.gridCardAction);
		gv.setAdapter(enumImage);
		gv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View v, int position,
					long id) {

				if (isUpdating) return;
				a.setValue(a.getActionOfList(position).getValue());
				
			}
		});
		
		return rowView;
	}

	private View createListView(Context context, final ActionEnum a) {
		View v;
		HorizontalScrollView hsv = new HorizontalScrollView(context);
		LinearLayout main= new LinearLayout(context);
		main.setClickable(true);
		for (final ActionEnum.Item e : a.getActionList()){
			View view = LayoutInflater.from(context).inflate(R.layout.card_sensor, null);

			image=(NetworkImageView) view.findViewById(R.id.image);
			tvinfo = ((TextView) view.findViewById(R.id.tvinfo));
			tvValue= ((TextView) view.findViewById(R.id.tvValue));
			tvinfo.setText(e.getName());

			image.setImageUrl(e.getImageUrl(), mImageLoader);
			tvValue.setVisibility(View.GONE);
			main.addView(view);
			listTvAction.add(tvinfo);
			view.setOnClickListener(new View.OnClickListener() {
				ActionEnum.Item item = e;
				
				@Override
				public void onClick(View v) {
					if (isUpdating) return;
					a.setValue(item.getValue());
				}
			});
			
		}
		hsv.addView(main);
		v=hsv;
		return v;
	}

	@Override
	protected void update() {
		isUpdating = true;
		if (listTvAction != null && sensor instanceof ActionEnum) {
			final ActionEnum a = ((ActionEnum) sensor);
			if (a.isShowGrid())
				updateGridView(a);
			else
				updateListView(a);
		}
		super.update();
		isUpdating = false;
	}

	private void updateListView(final ActionEnum a) {
		for (int i = 0; i < a.getCount(); i++) {

			if (a.getActionOfList(i).getValue().equals(a.getValue())) {
				listTvAction.get(i).setTypeface(null, Typeface.BOLD);
			} else {
				listTvAction.get(i).setTypeface(null, Typeface.NORMAL);
			}
		}
	}
	
	private void updateGridView(final ActionEnum a) {
		if (enumImage!=null)
			enumImage.notifyDataSetChanged();
	}
	

	
}
