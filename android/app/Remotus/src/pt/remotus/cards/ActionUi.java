package pt.remotus.cards;

import org.holoeverywhere.widget.LinearLayout;
import org.holoeverywhere.widget.PopupMenu;
import org.holoeverywhere.widget.PopupMenu.OnMenuItemClickListener;

import pt.remotus.R;
import pt.remotus.cards.CardFactory.ItemAction;
import pt.remotus.dialogs.AddAction;
import pt.remotus.model.Model;
import pt.remotus.model.action.Action;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.TextView;

import com.actionbarsherlock.view.MenuItem;

/**
 * @author Pedro Lopes
 * 
 *
 */
public class ActionUi extends SensorUi {
	private static final int MNU_EDITAR = 1;
	private static final int MNU_APAGAR = 2;
	private static final String TAG = "ActionUi";
	protected Action mAction;
	private PopupMenu mPopupMenu;

	public ActionUi(Model model, CardUi cardEntity, Action action) {
		super(model, cardEntity, action);
		this.mAction=action;
	}

	@Override
	protected void viewClick() {
		
	}

	
	/* (non-Javadoc)
	 * @see pt.a21200565.remotus.cards.SensorUi#getViewContent(android.content.Context)
	 * Cria vista para a action caso exista
	 */
	@Override
	public View getViewContent(Context context) {
		View actionUi=getActionView(context);
		if (actionUi==null) return null;
		
		LinearLayout main= new LinearLayout(context);
		main.setClickable(true);
		
		//main.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,(int)context.getResources().getDimension(R.dimen.action_height)));
		
		main.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		
		main.setOrientation(android.widget.LinearLayout.HORIZONTAL);
		main.setBackgroundResource(R.drawable.selectable_background_cardbank);
		
		tvinfo = new TextView(context);
		tvinfo.setLayoutParams(new LayoutParams((int)context.getResources().getDimension(R.dimen.action_label_width), LayoutParams.WRAP_CONTENT));	
		main.addView(tvinfo);
		actionUi.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));	
		main.addView(actionUi);
		
		
		//main.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,actionUi.getMeasuredHeight()));
		
		tvinfo.setText(sensor.getName()+" "+sensor.getUnit());
		createEditmenu(context,main);
		return main;
	}
	
//	@Override
//	public View getViewContent(Context context) {
//		View actionUi=getActionView(context);
//		if (actionUi==null) return null;
//		
//		LayoutInflater inflater = cardEntity.getActivity().getLayoutInflater();
//		LinearLayout main =(LinearLayout) inflater.inflate(R.layout.card_action, null);
//		tvinfo = (TextView) main.findViewById(R.id.tvinfo);
//		
//		actionUi.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));	
//		main.addView(actionUi);
//		
//		tvinfo.setText(sensor.getName()+" "+sensor.getUnit());
//		createEditmenu(context,main);
//		return main;
//	}
//

	protected View getActionView(Context context) {
		return null;
	}

	private void createEditmenu(Context context, View view) {
		mPopupMenu = new PopupMenu(context,view);
		mPopupMenu.getMenu().add(Menu.NONE,MNU_EDITAR,Menu.NONE,R.string.action_edit_action);
		mPopupMenu.getMenu().add(Menu.NONE,MNU_APAGAR,Menu.NONE,R.string.action_delete);
		
		mPopupMenu.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				menuClick(item);
				return true;
			}
		});
		view.setOnLongClickListener(new OnLongClickListener() {
			
			@Override
			public boolean onLongClick(View v) {
				mPopupMenu.show();
				return true;
			}
		});
	}

	protected void menuClick(MenuItem item) {
		switch (item.getItemId()) {
		case MNU_EDITAR:
			editAction();
			break;
		case MNU_APAGAR:
			cardEntity.removeAction(mAction);
			break;
		}		
	}

	private void editAction() {
        ItemAction item = CardFactory.getAction(mAction);
        if (item==null) {
        	Log.d(TAG,"configuração não encontrada para:"+mAction.getName());
        	return; // TODO registar erro
        }
        sensor=mAction.getSensor();
		
		Intent intent = new Intent(cardEntity.getActivity(), item.getAddClass());
		intent.putExtra(AddAction.CARTAO_ID, cardEntity.getCartao().getId());
		intent.putExtra(AddAction.ACTION_NAME, mAction.getName());
		intent.putExtra(AddAction.SENSOR_NAME, sensor.getName());
		intent.putExtra(AddAction.SENSOR_ID, sensor.getId());
		intent.putExtra(AddAction.ACTION_ID, mAction.getId());
		cardEntity.getActivity().startActivity(intent);    
	}
	

}
