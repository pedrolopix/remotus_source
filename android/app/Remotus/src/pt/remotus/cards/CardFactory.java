package pt.remotus.cards;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.view.View;
import pt.remotus.dialogs.AddAction;
import pt.remotus.dialogs.AddActionEnum;
import pt.remotus.dialogs.AddActionOnOff;
import pt.remotus.dialogs.AddActionSlider;
import pt.remotus.dialogs.AddSensor;
import pt.remotus.dialogs.AddSensorNormal;
import pt.remotus.dialogs.AddSensorOnOff;
import pt.remotus.model.Model;
import pt.remotus.model.action.Action;
import pt.remotus.model.action.ActionEnum;
import pt.remotus.model.action.ActionOnOff;
import pt.remotus.model.action.ActionSlider;
import pt.remotus.model.json.ActionEnumJSON;
import pt.remotus.model.json.ActionOnOffJSON;
import pt.remotus.model.json.ActionSliderJSON;
import pt.remotus.model.json.JSON;
import pt.remotus.model.json.SensorJSON;
import pt.remotus.model.json.SensorOnOffJSON;
import pt.remotus.model.sensor.Sensor;
import pt.remotus.model.sensor.SensorOnOff;

public class CardFactory {
	private static final String TAG = "CardFactory";
	public static List<ItemSensor> mListSensors;
	public static List<ItemAction> mListActions;
	public static class Item {
		String mName;
		JSON mJson;
		public Item(String mName, JSON mJson) {
			super();
			this.mName = mName;
			this.mJson = mJson;
		}
		public JSONObject ToJSON(Object obj) throws JSONException {
			return mJson.toJSON(obj);
		}
		public Object fromJSON(JSONObject json) throws JSONException {
			return mJson.fromJSON(json);
		}
		
		public String getName() {
			return mName;
		}
	}
	
	public static class ItemSensor extends Item{
		Class<? extends Sensor> mSensorClass;
		Class<? extends SensorUi> mSensorUiClass;
		Class<? extends AddSensor> mAddClass;

		public ItemSensor(String mName, JSON mJson,
				Class<? extends Sensor> mSensorClass,
				Class<? extends SensorUi> mSensorUiClass,
				Class<? extends AddSensor> mAddClass) {
			super(mName, mJson);
			this.mSensorClass = mSensorClass;
			this.mSensorUiClass = mSensorUiClass;
			this.mAddClass = mAddClass;
		}
		
		public Class<? extends AddSensor> getAddClass() {
			return mAddClass;
		}

		public Class<? extends SensorUi> getUi() {
			return mSensorUiClass;
		}
	}
	
	public static class ItemAction extends Item{
		Class<? extends Action> mActionClass;
		Class<? extends ActionUi> mActionUiClass;
		Class<? extends AddAction> mAddClass;
		
		public ItemAction(String mName, JSON mJson,
				Class<? extends Action> mSensorClass,
				Class<? extends ActionUi> mActionUiClass,
				Class<? extends AddAction> mAddClass) {
			super(mName, mJson);
			this.mActionClass = mSensorClass;
			this.mActionUiClass = mActionUiClass;
			this.mAddClass = mAddClass;
		}

		public Class<? extends AddAction> getAddClass() {
			return mAddClass;
		}
		
		public Class<? extends ActionUi> getUi() {
			return mActionUiClass;
		}


	}
	
	
	 static {
		 mListSensors= new ArrayList<ItemSensor>();
		 mListActions= new ArrayList<ItemAction>();
		 
		//Registar sensor
		addSensor("Normal",Sensor.class,SensorUi.class, AddSensorNormal.class, new SensorJSON());
		addSensor("On/off",SensorOnOff.class,SensorOnOffUi.class, AddSensorOnOff.class, new SensorOnOffJSON());
	
		//Registar Actions
		addAction("On/Off",ActionOnOff.class,ActionOnOffUi.class,AddActionOnOff.class, new ActionOnOffJSON());
		addAction("Slider",ActionSlider.class,ActionSliderUi.class,AddActionSlider.class, new ActionSliderJSON());
		addAction("Enum",ActionEnum.class,ActionEnumUi.class,AddActionEnum.class, new ActionEnumJSON());
		
	 
	 }

	private static void addSensor(String name, Class<? extends Sensor> sensorClass,
			Class<? extends SensorUi> sensorUiClass, Class<? extends AddSensor> addClass,
			JSON sensorJSon) {
		mListSensors.add(new ItemSensor(name, sensorJSon, sensorClass, sensorUiClass, addClass));
	}
	
	private static void addAction(String name, Class<? extends Action> actionClass,
			Class<? extends ActionUi> actionUiClass, Class<? extends AddAction> addClass,
			JSON sensorJSon) {
		mListActions.add(new ItemAction(name, sensorJSon, actionClass, actionUiClass, addClass));
	}

	public static ItemSensor getSensor(Sensor sensor) {
		for(ItemSensor item: mListSensors) {
			if (sensor.getClass().equals(item.mSensorClass)) return item;
		}
		return null;
	}

	public static ItemSensor getSensor(String sensorClass) {
		for(ItemSensor item: mListSensors) {
			if (sensorClass.equals(item.mSensorClass.getSimpleName().toString())) return item;
		}
		return null;
	}
	public static ItemSensor getSensorByName(String name) {
		for(ItemSensor item: mListSensors) {
			if (name.equals(item.mName)) return item;
		}
		return null;
	}

	public static ItemAction getAction(Action action) {
		for(ItemAction item: mListActions) {
			if (action.getClass().equals(item.mActionClass)) return item;
		}
		return null;
	}
	
	public static ItemAction getActionbyName(String name) {
		for(ItemAction item: mListActions) {
			if (name.equals(item.mName)) return item;
		}
		return null;
	}

	public static Sensor createSensor(String clazz) {
		ItemSensor item= getSensor(clazz);
		return createSensor(item);
	}
	
	public static Sensor createSensor(ItemSensor sensor) {
		if (sensor==null) return null;
		Constructor<?> ctor;
		try {
			ctor = sensor.mSensorClass.getConstructor();
			Sensor cs= (Sensor)ctor.newInstance();
			return cs;
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static ItemAction getAction(String actionClass) {
		for(ItemAction item: mListActions) {
			if (actionClass.equals(item.mActionClass.getSimpleName().toString())) return item;
		}
		return null;
	}

	public static List<String> getActions() {
		List<String> list = new ArrayList<String>();
		for(ItemAction item: mListActions) {
			list.add(item.getName());
		}
		return list;
	}

	public static List<String> getSensors() {
		List<String> list = new ArrayList<String>();
		for(ItemSensor item: mListSensors) {
			list.add(item.getName());
		}
		return list;
	}
	
	public static View createCardSensor(Context context,CardUi cartao, Model model, Sensor sensor) {
		View v=null;
		ItemSensor item = getSensor(sensor);
		if (item==null) {
			Log.e(TAG, "não existe o sensor: "+sensor.getName());
			return v;
		}
		Constructor<?> ctor;
		try {
			ctor = item.getUi().getConstructor(Model.class,CardUi.class, Sensor.class);
			SensorUi cs= (SensorUi)ctor.newInstance(model,cartao,sensor);		
			v=cs.getViewContent(context);
			v.setTag(sensor);
			cs.update();
			return v;
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		return v;

	}
	
	
	public static View createCardAction(Context context,CardUi cartao, Model model, Action action) {
		View v=null;
		ItemAction item = getAction(action);
		if (item==null) {
			Log.e(TAG, "não existe a acão: "+action.getName());
			return v;
		}
		Constructor<?> ctor;
		try {
			ctor = item.getUi().getConstructor(Model.class,CardUi.class, Action.class);
			SensorUi cs= (SensorUi)ctor.newInstance(model,cartao,action);		
			v=cs.getViewContent(context);
			cs.update();
			return v;
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return v;

	}
	
	
}
