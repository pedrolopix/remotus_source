
package pt.remotus.fragments;

import org.holoeverywhere.preference.EditTextPreference;
import org.holoeverywhere.preference.PreferenceFragment;
import org.holoeverywhere.preference.PreferenceManager;
import org.holoeverywhere.preference.SharedPreferences;
import org.holoeverywhere.preference.SharedPreferences.OnSharedPreferenceChangeListener;

import pt.remotus.R;
import android.os.Bundle;

public class SettingsFragment extends PreferenceFragment implements OnSharedPreferenceChangeListener  {
    private EditTextPreference mqtthost;
	private EditTextPreference mqttport;
	private EditTextPreference mqttusername;
	private EditTextPreference mqttpassword;
	private EditTextPreference cfgCardTopic;

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        mqtthost = (EditTextPreference) findPreference(R.id.mqtthost);
        mqttport = (EditTextPreference) findPreference(R.id.mqttport);
        mqttusername = (EditTextPreference) findPreference(R.id.mqttusername);
        mqttpassword = (EditTextPreference) findPreference(R.id.mqttpassword);
        cfgCardTopic = (EditTextPreference) findPreference(R.id.cfgCardTopic);
        
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        
    }

    @Override
    public void onResume() {
        super.onResume();
        getSupportActionBar().setSubtitle(R.string.settings);
        
        fillSummary();
    }

	private void fillSummary() {
		SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
		fillSummary(sharedPrefs);
        
	}

	private void fillSummary(SharedPreferences sharedPrefs) {
        String host=sharedPrefs.getString(R.id.mqtthost,"");
        String port=sharedPrefs.getString(R.id.mqttport,"");
        String username=sharedPrefs.getString(R.id.mqttusername,"");
        String password=sharedPrefs.getString(R.id.mqttpassword,"");
        String topic = sharedPrefs.getString(R.id.cfgCardTopic,"");
        mqtthost.setSummary(String.format(getSupportActivity().getString(R.string.mqtt_url)+"\n%s",host));
        mqttport.setSummary(String.format(getSupportActivity().getString(R.string.mqtt_port)+"\n%s",port));
        mqttusername.setSummary(String.format(getSupportActivity().getString(R.string.user)+"\n%s",username));
        mqttpassword.setSummary(R.string.password);
        cfgCardTopic.setSummary(String.format(getSupportActivity().getString(R.string.summary_endereco_do_cartao)+"\n%s",topic));
	}


	@Override
	public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
		fillSummary(prefs);
	}


}
