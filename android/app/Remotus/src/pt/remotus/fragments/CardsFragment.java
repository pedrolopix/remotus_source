package pt.remotus.fragments;

import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.app.Activity;
import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.app.Fragment;
import org.holoeverywhere.widget.Button;

import pt.remotus.MainActivity;
import pt.remotus.R;
import pt.remotus.cards.CardUi;
import pt.remotus.model.Cartao;
import pt.remotus.model.Model;
import pt.remotus.model.event.EventConnetionState;
import pt.remotus.model.event.EventLoadCards;
import pt.remotus.service.RemotusService.ConnectionStatus;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.fima.cardsui.views.CardUI;
import com.squareup.otto.Subscribe;

public class CardsFragment extends Fragment{
	private CardUI cardUi;
	protected boolean mConnected;
	private MenuItem mActionAddCard;
	private MenuItem mActionConnect;
	private Button ivDisconnect;
	private Animation rotate;
	private ImageView refreshView;
	private boolean mIsConnecting=false;
	private ConnectionStatus mStatus=ConnectionStatus.INITIAL;
	private ConnectionStatus mLastStatus=ConnectionStatus.INITIAL;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mConnected = false;
		mIsConnecting = false;
		connect();
	}

	private void initView() {
		LayoutInflater inflater = (LayoutInflater) getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		refreshView = (ImageView) inflater.inflate(R.drawable.menu_refresh, null);
		// refreshView = (ImageView) inflater.inflate(android.R.drawable.ic_menu_rotate, null);

		rotate = AnimationUtils.loadAnimation(getSupportActivity(), R.anim.anim_refresh);
		rotate.setAnimationListener(rotateListener);
	}
	


	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		initView();
		View view = inflater.inflate(R.layout.cards);
		cardUi = (CardUI)view.findViewById(R.id.cardsview);
		ivDisconnect = (Button) view.findViewById(R.id.ivDisconnect);
		ivDisconnect.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				connect();
			}
		});
		cardUi.setSwipeable(false);
		setHasOptionsMenu(true);

		rotate = AnimationUtils.loadAnimation(getSupportActivity(), R.anim.anim_refresh);
		rotate.setAnimationListener(rotateListener);
		return view;
    }

	private final AnimationListener rotateListener = new AnimationListener() {
		@Override
		public void onAnimationStart(Animation animation) {
		}

		@Override
		public void onAnimationRepeat(Animation animation) {
		}

		@Override
		public void onAnimationEnd(Animation animation) {

				if (refreshView != null) {
					refreshView.startAnimation(rotate);
				}

		}
	};



	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
	}

	@Override
	public void onDestroy() {

		super.onDestroy();
	}

    @Subscribe
	public void buildCards(EventLoadCards event) {
//  	    handler.post(new Runnable() {
//			@Override
//			public void run() {
				Model model = Model.getInstance();
				cardUi.setSwipeable(false);
				cardUi.clearCards();
				for (Cartao item : model.getEntities()) {
					cardUi.addCard(new CardUi(getSupportActivity(), cardUi,item,model));
				}
				cardUi.refresh();
//			}
//		});
	}

	@Override
    public void onResume() {
        super.onResume();
        getSupportActionBar().setSubtitle(R.string.cards);
		mConnected = Model.getInstance().isConnected();
        update(true);
    }



	@Override
	public void onStart() {
		Model.getInstance().getBus().register(this);
		buildCards(null);
		super.onStart();
	}

	@Override
	public void onStop() {
		refreshView.clearAnimation();
		mActionConnect.setActionView(null);
		mIsConnecting = false;
		Model.getInstance().getBus().unregister(this);
		super.onStop();
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
	}

	@Override
	public void onDetach() {
		super.onDetach();
	}

	@Subscribe
	public void OnEventConnetionState(EventConnetionState event) {
		mConnected = event.getStatus() == ConnectionStatus.CONNECTED;
		mStatus=event.getStatus();
		update(false);
	}

	synchronized private void update(boolean force) {
		if (cardUi==null) return;
		if (ivDisconnect==null) return;
		if (mActionAddCard==null) return;
		if (mActionConnect==null) return;
		if (!force && mLastStatus==mStatus) return;
		mLastStatus=mStatus;
		
		if (mConnected) {
			cardUi.setVisibility(View.VISIBLE);
		} else {
			cardUi.setVisibility(View.GONE);
			ivDisconnect.setVisibility(View.VISIBLE);
		}
		mActionAddCard.setVisible(mConnected);
		mActionConnect.setVisible(!mConnected);
		
		
		if (mStatus == ConnectionStatus.CONNECTING) {

			if (!mIsConnecting) {
				mIsConnecting=true;
				mActionConnect.setActionView(refreshView);
				refreshView.startAnimation(rotate);
				
			}
		}
		if (mStatus == ConnectionStatus.NOTCONNECTED_DATADISABLED || mStatus == ConnectionStatus.NOTCONNECTED_UNKNOWNREASON
				|| mStatus == ConnectionStatus.NOTCONNECTED_MQTTERROR || mStatus == ConnectionStatus.CONNECTED) {
			refreshView.clearAnimation();
			mActionConnect.setActionView(null);
			mIsConnecting = false;
		}
		
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.cards, menu);
	    mActionAddCard = menu.findItem(R.id.action_addcard);
		mActionConnect= menu.findItem(R.id.action_connect);
		update(true);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
    	case R.id.action_connect:
			connect();
    		return true;
    	case R.id.action_addcard:
			addCard();
			return true;
		default:
			break;
		}
    	return super.onContextItemSelected(item);
    }

	private void connect() {
		if ( getSupportActivity() instanceof MainActivity) {
			MainActivity a=((MainActivity)getSupportActivity());
			if (a.mService != null) {
				a.mService.connect();
			}
		}
	}

	private void addCard() {

		AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());

		alert.setTitle(R.string.title_add_card);

		// Set an EditText view to get user input
		final EditText input = new EditText(getActivity());
		alert.setView(input);

		alert.setPositiveButton(R.string.action_ok, new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int whichButton) {
		  Model model = Model.getInstance();
		  String value = input.getText().toString();
		  Cartao cartao = new Cartao();
		  cartao.setName(value);
		  model.getCartoes().add(cartao);
		  cardUi.addCard(new CardUi(getSupportActivity(), cardUi,cartao,Model.getInstance()));
		  model.cartaoChanged(cartao);
		  }
		});

		alert.setNegativeButton(R.string.action_cancelar, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int whichButton) {
			    // Canceled.
			  }
			});

		alert.show();
	}

}
