package pt.remotus.dialogs;

import org.holoeverywhere.widget.EditText;

import pt.remotus.R;
import pt.remotus.model.Mqtt;
import pt.remotus.model.sensor.Sensor;
import android.net.Uri;
import android.text.Editable;
import android.text.TextWatcher;

import com.android.volley.toolbox.NetworkImageView;
import com.throrinstudio.android.common.libs.validator.Validate;
import com.throrinstudio.android.common.libs.validator.validator.UrlValidator;

public class AddSensorNormal extends AddSensor {
	EditText mEdImagem1;
	NetworkImageView mImage1;
	
	@Override
	protected void init() {
		setContentView(R.layout.activity_add_sensor_normal);
		super.init();
		
		mEdImagem1=(EditText)findViewById(R.id.edImagem1);
		mImage1=(NetworkImageView)findViewById(R.id.image1);
		mEdImagem1.addTextChangedListener(edImagem1Changed);
		
	    Validate edImagem1urlValidator = new Validate(mEdImagem1);
	    edImagem1urlValidator.addValidator(new UrlValidator(this));

	    mForm.addValidates(edImagem1urlValidator);
	    if (!mEditing) {
	    	mSensor=new Sensor();
	    }
	}
	
	private TextWatcher edImagem1Changed= new TextWatcher() {

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
		}
		
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,	int after) {
			
		}
		
		@Override
		public void afterTextChanged(Editable s) {
			if (mImageLoader==null) return;
			if (s.toString().isEmpty()) return;
			if (Uri.parse(s.toString()).getHost()==null) return;
			mImage1.setImageUrl(s.toString(), mImageLoader);
		}
	};
	
	@Override
	protected void fillActivity(Sensor sensor) {
		super.fillActivity(sensor);
		//image1.setImageUrl(sensor.getImageUrl(), mImageLoader);
		mEdImagem1.setText(sensor.getImageUrl());
	}
	
	@Override
	protected void okClick() {
		
		Mqtt mqtt = mModel.getMqtt();
		String topicIn = mEdTopico.getText().toString();
		int qos = 0;
		String name = mEdTitulo.getText().toString();
		String unit = "";
		String imageurl = mEdImagem1.getText().toString();
		
		mSensor.set(mCartao, mqtt, topicIn, qos, name, unit, imageurl);
		if (!mEditing) mCartao.addSensor(mSensor);
		mModel.cartaoChanged(mCartao);
		finish();
	}
	
}
