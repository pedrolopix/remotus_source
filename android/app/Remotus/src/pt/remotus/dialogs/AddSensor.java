package pt.remotus.dialogs;

import org.holoeverywhere.app.Activity;
import org.holoeverywhere.widget.EditText;

import pt.remotus.R;
import pt.remotus.RemotusApplication;
import pt.remotus.model.Cartao;
import pt.remotus.model.Model;
import pt.remotus.model.sensor.Sensor;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.android.volley.toolbox.ImageLoader;
import com.throrinstudio.android.common.libs.validator.Form;
import com.throrinstudio.android.common.libs.validator.Validate;
import com.throrinstudio.android.common.libs.validator.validator.NotEmptyValidator;

public abstract class AddSensor extends Activity{
	protected static final String TAG = "AddSensorActivity";
	public static final String CARTAO_ID = "CartaoId";
	public static final String SENSOR_NAME = "SensorName";
	public static final String SENSOR_ID = "SensorId";	
	Model mModel;
	Cartao mCartao;
	String mSensorName;
	int mSendorId;
	EditText mEdTitulo;
	EditText mEdTopico;
	int mAction;
	boolean mEditing;
	Form mForm = new Form();
	ImageLoader mImageLoader= RemotusApplication.get().getImageLoader();
	Sensor mSensor;
	ImageButton mImgBtnTopic;
	
	@Override
	protected void onCreate(Bundle sSavedInstanceState) {
		super.onCreate(sSavedInstanceState);
		int id=getIntent().getIntExtra(CARTAO_ID, -1);
		if (id==-1) {
			finish();
		}
		mSensorName=getIntent().getStringExtra(SENSOR_NAME);
		mSendorId=getIntent().getIntExtra(SENSOR_ID, -1);
		mModel=Model.getInstance();
		mCartao=mModel.getCartao(id);	
		if (mCartao==null) {
			finish();
		}
		setTitle(String.format(getString(R.string.add_sensor_to),mCartao.getName(),mSensorName));
		mSensor = mCartao.getSensor(mSendorId);
		mEditing=mSensor!=null;
		init();
		if (mEditing) {
			fillActivity(mSensor);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getSupportMenuInflater().inflate(R.menu.ok_cancel, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_ok:
			if (validate()) {
				okClick();
			}
			return true;
		case R.id.action_cancel:
			finish();
			return true;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	protected void init() {
		mEdTitulo=(EditText)findViewById(R.id.edTitulo);
		mEdTopico=(EditText)findViewById(R.id.edTopico);
		mImgBtnTopic=(ImageButton)findViewById(R.id.imgBtnListaTopicos);
		
		mImgBtnTopic.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				AlertTopics dialog = new AlertTopics(getContext(), mModel);
				dialog.showAlert(mEdTopico);
			}

		});
		
		
		
	    Validate edTituloValidator = new Validate(mEdTitulo);
	    Validate edTopicoValidator = new Validate(mEdTopico);
	    
	    edTituloValidator.addValidator(new NotEmptyValidator(this));
	    edTopicoValidator.addValidator(new NotEmptyValidator(this));
		
	    mForm.addValidates(edTituloValidator);
	    mForm.addValidates(edTopicoValidator);
	}
	
	
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	}
	
	protected OnClickListener btOkClick = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (validate()) {
					okClick();
				}
			}
		};
	protected OnClickListener btCancelClick = new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		};
	
	
	@Override
	protected void onResume() {
		super.onResume();
	}
	
	protected void fillActivity(Sensor sensor) {
		mEdTitulo.setText(sensor.getName());
		mEdTopico.setText(sensor.getTopicIn());
	}

	private boolean validate() {
		if(mForm.validate()){
			//Toast.makeText(getApplicationContext(), "Valid Form", Toast.LENGTH_LONG).show();
			return true;
		}
		return false;
	}
	
	
	protected void okClick() {
		
	}
	
	private Context getContext(){
		return this;
	}

}
