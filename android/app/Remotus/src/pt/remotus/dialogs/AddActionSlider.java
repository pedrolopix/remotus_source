package pt.remotus.dialogs;

import org.holoeverywhere.widget.CheckBox;
import org.holoeverywhere.widget.EditText;

import pt.remotus.R;
import pt.remotus.model.Mqtt;
import pt.remotus.model.action.Action;
import pt.remotus.model.action.ActionSlider;

import com.throrinstudio.android.common.libs.validator.Validate;
import com.throrinstudio.android.common.libs.validator.validate.ConfirmMinMax;
import com.throrinstudio.android.common.libs.validator.validator.NotEmptyValidator;

public class AddActionSlider extends AddAction {
	
	private EditText edMin;
	private EditText edMax;
	private EditText edInc;
	private CheckBox chkTriggerUp;
	
	
	@Override
	protected void init() {
		setContentView(R.layout.activity_add_action_slider);
		edMin=(EditText)findViewById(R.id.edMinValue);
		edMax=(EditText)findViewById(R.id.edMaxValue);
		edInc=(EditText)findViewById(R.id.edIncValue);
		chkTriggerUp=(CheckBox)findViewById(R.id.chkTriggerUp);
		super.init();
			
		ConfirmMinMax edMinMaxValidor = new ConfirmMinMax(edMin, edMax);
		
		Validate edIncValidator = new Validate(edInc);
		edIncValidator.addValidator(new NotEmptyValidator(this));
		
	    mForm.addValidates(edIncValidator);
	    mForm.addValidates(edMinMaxValidor);
	    
		if (!mEditing) {
	    	mAction=new ActionSlider();
	    }
	}
	
	@Override
	protected void fillActivity(Action action) {
		super.fillActivity(mAction);
		ActionSlider a= (ActionSlider)action;
		edMin.setText(""+a.getMin());
		edMax.setText(""+a.getMax());
		edInc.setText(""+a.getIncremental());
		chkTriggerUp.setChecked(a.isTriggerUp());
	}
	
	@Override
	protected void okClick() {
		Mqtt mqtt = mModel.getMqtt();
		String topicIn = mEdTopicoIn.getText().toString();
		String topicOut = mEdTopicoOut.getText().toString();
		int qos = Integer.parseInt(mEdQosOut.getText().toString());
		String name = mEdTitulo.getText().toString();
		boolean retained= mChkRetained.isChecked();
		
		int min = Integer.parseInt(edMin.getText().toString());
		int max = Integer.parseInt(edMax.getText().toString());
		int inc = Integer.parseInt(edInc.getText().toString());
		boolean trigger= chkTriggerUp.isChecked();
		
		ActionSlider action= (ActionSlider) mAction;
		action.set(mSensor, mqtt, topicIn, 0, topicOut, qos, retained, name,"");
		action.setMin(min);
		action.setMax(max);
		action.setIncremental(inc);
		action.setTriggerUp(trigger);
		if(!mEditing) mSensor.addAction(action);
		mModel.cartaoChanged(mCartao);
		finish();
	}
	
}
