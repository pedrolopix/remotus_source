package pt.remotus.dialogs;

import org.holoeverywhere.widget.EditText;

import pt.remotus.R;
import pt.remotus.model.Model;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;


public class AlertTopics {
	
	private Model mModel;
	private Context mContext;
	private int position;
	
	protected AlertTopics(Context context, Model model) {
		this.mContext = context;
		this.mModel = model;
		this.position = -1;
	}

	public void showAlert(final EditText edittext) {
		position = mModel.getPositionTopicListByTopic(edittext.getText().toString());
		AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
		builder.setTitle(R.string.lbl_lista_topicos);
		builder.setSingleChoiceItems(mModel.getListTopicToArray(), position,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						position = which;
					}
				});
		builder.setPositiveButton(R.string.action_ok, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				edittext.setText(mModel.getListTopic().get(position));
			}
		});
		builder.setNegativeButton(R.string.action_cancelar,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
					}
				});
		
		builder.create();
		builder.show();
	}

}
