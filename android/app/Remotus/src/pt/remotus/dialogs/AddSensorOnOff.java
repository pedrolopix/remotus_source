package pt.remotus.dialogs;

import org.holoeverywhere.widget.EditText;

import pt.remotus.R;
import pt.remotus.model.Mqtt;
import pt.remotus.model.sensor.Sensor;
import pt.remotus.model.sensor.SensorOnOff;
import android.net.Uri;
import android.text.Editable;
import android.text.TextWatcher;

import com.android.volley.toolbox.NetworkImageView;
import com.throrinstudio.android.common.libs.validator.Validate;
import com.throrinstudio.android.common.libs.validator.validator.UrlValidator;

public class AddSensorOnOff extends AddSensor {
	EditText mEdImagem1;
	NetworkImageView mImage1;
	EditText mEdImagem2;
	NetworkImageView mImage2;
	private EditText mEdOn;
	private EditText mEdOff;
	
	
	@Override
	protected void init() {
		setContentView(R.layout.activity_add_sensor_onoff);
		super.init();
		
		mEdImagem1=(EditText)findViewById(R.id.edImagem1);
		mImage1=(NetworkImageView)findViewById(R.id.image1);
		mEdImagem1.addTextChangedListener(edImagem1Changed);
	    Validate edImagem1urlValidator = new Validate(mEdImagem1);
	    edImagem1urlValidator.addValidator(new UrlValidator(this));
	    mForm.addValidates(edImagem1urlValidator);
	    
	    
		mEdImagem2=(EditText)findViewById(R.id.edImagem2);
		mImage2=(NetworkImageView)findViewById(R.id.image2);
		mEdImagem2.addTextChangedListener(edImagem2Changed);
	    Validate edImagem2urlValidator = new Validate(mEdImagem2);
	    edImagem2urlValidator.addValidator(new UrlValidator(this));
	    mForm.addValidates(edImagem2urlValidator);
	    
	    mEdOn=(EditText)findViewById(R.id.edOn);
	    mEdOff=(EditText)findViewById(R.id.edOff);
	    if (!mEditing) {
	    	mSensor=new SensorOnOff();
	    }
	}

	private TextWatcher edImagem1Changed= new TextWatcher() {

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
		}
		
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,	int after) {
			
		}
		
		@Override
		public void afterTextChanged(Editable s) {
			if (mImageLoader==null) return;
			if (s.toString().isEmpty()) return;
			if (Uri.parse(s.toString()).getHost()==null) return;
			mImage1.setImageUrl(s.toString(), mImageLoader);
		}
	};
	
	private TextWatcher edImagem2Changed= new TextWatcher() {

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
		}
		
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,	int after) {
			
		}
		
		@Override
		public void afterTextChanged(Editable s) {
			if (mImageLoader==null) return;
			if (s.toString().isEmpty()) return;
			if (Uri.parse(s.toString()).getHost()==null) return;
			mImage2.setImageUrl(s.toString(), mImageLoader);
		}
	};
	
	
	@Override
	protected void fillActivity(Sensor sensor) {
		super.fillActivity(sensor);
		SensorOnOff s=(SensorOnOff)sensor;
		mEdImagem1.setText(s.getImageUrl());
		mEdImagem2.setText(s.getImageOffUrl());
		mEdOn.setText(s.getOn());
		mEdOff.setText(s.getOff());
	}
	
	@Override
	protected void okClick() {
		Mqtt mqtt = mModel.getMqtt();
		String topicIn = mEdTopico.getText().toString();
		int qos = 0;
		String name = mEdTitulo.getText().toString();
		String imageurl1 = mEdImagem1.getText().toString();
		String imageurl2 = mEdImagem2.getText().toString();
		String on= mEdOn.getText().toString();
		String off= mEdOff.getText().toString();
		SensorOnOff  s=(SensorOnOff)mSensor;
		s.set(mCartao, mqtt, topicIn, qos, name, on, off, imageurl1, imageurl2);
		if (!mEditing) mCartao.addSensor(s);
		mModel.cartaoChanged(mCartao);
		finish();
	}
	
	
}
