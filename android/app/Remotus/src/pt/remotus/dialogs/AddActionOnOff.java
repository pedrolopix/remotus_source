package pt.remotus.dialogs;

import org.holoeverywhere.widget.EditText;

import pt.remotus.R;
import pt.remotus.model.Mqtt;
import pt.remotus.model.action.Action;
import pt.remotus.model.action.ActionOnOff;

import com.android.volley.toolbox.NetworkImageView;

public class AddActionOnOff extends AddAction {
	EditText edImagem1;
	NetworkImageView image1;
	EditText edImagem2;
	NetworkImageView image2;
	private EditText edOn;
	private EditText edOff;
	
	
	@Override
	protected void init() {
		setContentView(R.layout.activity_add_action_onoff);
	    
	    edOn=(EditText)findViewById(R.id.edOn);
	    edOff=(EditText)findViewById(R.id.edOff);
	    super.init();
	    if (!mEditing) {
	    	mAction=new ActionOnOff();
	    }
	}

	@Override
	protected void fillActivity(Action action) {
		super.fillActivity(mAction);
		ActionOnOff a= (ActionOnOff)action;
		edOn.setText(a.getOn());
		edOff.setText(a.getOff());
	}
	
	@Override
	protected void okClick() {
		Mqtt mqtt = mModel.getMqtt();
		String topicIn = mEdTopicoIn.getText().toString();
		String topicOut = mEdTopicoOut.getText().toString();
		int qos = Integer.parseInt(mEdQosOut.getText().toString());
		String name = mEdTitulo.getText().toString();
		String on = edOn.getText().toString();
		String off = edOff.getText().toString();
		boolean retained= mChkRetained.isChecked();

		ActionOnOff action= (ActionOnOff) mAction;
		action.set(mSensor, mqtt, topicIn, 0, topicOut, qos, retained, name, on, off);
		if(!mEditing) mSensor.addAction(action); 
		mModel.cartaoChanged(mCartao);
		finish();
	}
	
	
}
