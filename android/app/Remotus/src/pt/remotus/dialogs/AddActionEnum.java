package pt.remotus.dialogs;

import org.holoeverywhere.widget.CheckBox;
import org.holoeverywhere.widget.ListView;

import pt.remotus.R;
import pt.remotus.adapter.EnumAdapter;
import pt.remotus.model.Mqtt;
import pt.remotus.model.action.ActionEnum;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageButton;
import android.widget.ListAdapter;

public class AddActionEnum extends AddAction {
	private ListView myList;
	private EnumAdapter myAdapter;
	private ImageButton btnAddItem;
	private ActionEnum action;
	private CheckBox mChkShowGrid;

	public static void setListViewHeightBasedOnChildren(ListView listView) {
		ListAdapter listAdapter = listView.getAdapter();
		if (listAdapter == null)
			return;

		int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(),
				MeasureSpec.UNSPECIFIED);
		int totalHeight = 0;
		View view = null;
		for (int i = 0; i < listAdapter.getCount(); i++) {
			view = listAdapter.getView(i, view, listView);
			if (i == 0)
				view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth,
						LayoutParams.WRAP_CONTENT));

			view.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
			totalHeight += view.getMeasuredHeight();
		}
		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight
				+ (listView.getDividerHeight() * (listAdapter.getCount() - 1));
		listView.setLayoutParams(params);
		listView.requestLayout();
	}

	@Override
	protected void init() {
		setContentView(R.layout.activity_add_action_enum);
		myList = (ListView) findViewById(R.id.lstActionEnumRow);
		btnAddItem = (ImageButton) findViewById(R.id.btnAddNewItem);
		mChkShowGrid = (CheckBox) findViewById(R.id.chkGrid);
		mChkShowGrid.setChecked(false);

		super.init();

		if (!mEditing) {
			mAction = new ActionEnum();
			action = (ActionEnum) mAction;
		} else {
			action = (ActionEnum) mAction;
			action.startEdit();
			mChkShowGrid.setChecked(action.isShowGrid());
		}

		myList.setItemsCanFocus(true);
		myAdapter = new EnumAdapter(getContext(), action.getActionList(),
				mImageLoader);

		myList.setAdapter(myAdapter);
		myAdapter.notifyDataSetChanged();
		setListViewHeightBasedOnChildren(myList);
		btnAddItem.setOnClickListener(new AddActionEnumItem(getContext(),
				action, myAdapter, mImageLoader, myList));

	}

	@Override
	protected void okClick() {
		action.finnishEdit();
		Mqtt mqtt = mModel.getMqtt();
		String topicIn = mEdTopicoIn.getText().toString();
		String topicOut = mEdTopicoOut.getText().toString();
		int qos = Integer.parseInt(mEdQosOut.getText().toString());
		String name = mEdTitulo.getText().toString();
		boolean retained = mChkRetained.isChecked();
		boolean showGrid = mChkShowGrid.isChecked();

		ActionEnum action = (ActionEnum) mAction;
		action.set(mSensor, mqtt, topicIn, 0, topicOut, qos, retained, name, "");
		action.setShowGrid(showGrid);
		;
		if (!mEditing)
			mSensor.addAction(action);
		mModel.cartaoChanged(mCartao);
		finish();
	}

	@Override
	protected void cancelClick() {
		action.cancelEdit();
		super.cancelClick();
	}

}
