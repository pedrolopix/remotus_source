package pt.remotus.dialogs;

import java.util.List;

import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.app.AlertDialog.Builder;
import org.holoeverywhere.app.DialogFragment;
import org.holoeverywhere.widget.ArrayAdapter;

import pt.remotus.R;
import pt.remotus.cards.CardFactory;
import pt.remotus.cards.CardFactory.ItemSensor;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class AddSensorChoice extends DialogFragment{

    protected static final String TAG = "AddSensorChoice";
	private List<String> list;
	private int cartaoId;

	public AddSensorChoice() {
        setDialogType(DialogType.AlertDialog);
    }

    @Override
    public AlertDialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getSupportActivity());
        builder.setTitle(R.string.title_choose_sensor);
        //builder.setIcon(R.drawable.ic_launcher);
        prepareBuilder(builder);
        cartaoId=getArguments().getInt(AddSensor.CARTAO_ID);
        Log.d(TAG,"cartaoID: "+cartaoId);
        return builder.create();
    }

	private final OnClickListener clickListener = new OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            Log.d(TAG,"escolhido: "+list.get(which));
            ItemSensor item = CardFactory.getSensorByName(list.get(which));
            if (item==null) {
				Log.d(TAG, "configura????o n??o encontrada para:" + list.get(which));
            	return; // TODO registar erro
            }

			Intent intent = new Intent(getActivity(), item.getAddClass());
			intent.putExtra(AddSensor.CARTAO_ID, cartaoId);
			intent.putExtra(AddSensor.SENSOR_NAME, list.get(which));
			getActivity().startActivity(intent);

        	dialog.dismiss();
        }
    };

    protected void prepareBuilder(Builder builder) {
    	list = CardFactory.getSensors();
    	ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1, list);
        builder.setAdapter(dataAdapter, clickListener);
    }

}
