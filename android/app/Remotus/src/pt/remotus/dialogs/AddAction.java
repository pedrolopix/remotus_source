package pt.remotus.dialogs;

import org.holoeverywhere.app.Activity;
import org.holoeverywhere.widget.CheckBox;
import org.holoeverywhere.widget.EditText;

import pt.remotus.R;
import pt.remotus.RemotusApplication;
import pt.remotus.model.Cartao;
import pt.remotus.model.Model;
import pt.remotus.model.action.Action;
import pt.remotus.model.sensor.Sensor;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.android.volley.toolbox.ImageLoader;
import com.throrinstudio.android.common.libs.validator.Form;
import com.throrinstudio.android.common.libs.validator.Validate;
import com.throrinstudio.android.common.libs.validator.validator.NotEmptyValidator;
import com.throrinstudio.android.common.libs.validator.validator.RegExpValidator;

public class AddAction extends Activity{
	protected static final String TAG = "AddAction";
	public static final String CARTAO_ID = "CartaoId";
	public static final String ACTION_NAME = "ActionName";
	public static final String SENSOR_ID = "SensorId";
	public static final String SENSOR_NAME = "SensorName";
	public static final String ACTION_ID = "ActionID";
	protected Model mModel;
	protected Cartao mCartao;
	protected Sensor mSensor;
	protected String mActionName;
	protected int mSensorId;
	protected EditText mEdTitulo;
	protected EditText mEdTopicoIn;
	protected EditText mEdTopicoOut;
	protected ImageButton mImgBtnTopicIn;
	protected ImageButton mImgBtnTopicOut;
	protected String mSensorName;
	protected Form mForm = new Form();
	protected ImageLoader mImageLoader= RemotusApplication.get().getImageLoader();
	protected EditText mEdQosOut;
	protected CheckBox mChkRetained;
	protected int mActionId;
	protected Action mAction;
	protected boolean mEditing;
	@Override
	protected void onCreate(Bundle sSavedInstanceState) {
		super.onCreate(sSavedInstanceState);
		init();
	}

	protected void init() {
		mEdTitulo=(EditText)findViewById(R.id.edTitulo);
		mEdTopicoIn=(EditText)findViewById(R.id.edTopicoIn);
		mEdTopicoOut=(EditText)findViewById(R.id.edTopicoOut);
		mEdQosOut=(EditText)findViewById(R.id.edQosOut);
		mChkRetained=(CheckBox)findViewById(R.id.chkRetained);
		mImgBtnTopicIn=(ImageButton)findViewById(R.id.imgBtnListaTopicosIn);
		mImgBtnTopicOut=(ImageButton)findViewById(R.id.imgBtnListaTopicosOut);
		
		mImgBtnTopicIn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AlertTopics dialog = new AlertTopics(getContext(), mModel);
				dialog.showAlert(mEdTopicoIn);
			}

		});
		
		mImgBtnTopicOut.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AlertTopics dialog = new AlertTopics(getContext(), mModel);
				dialog.showAlert(mEdTopicoOut);
			}

		});
		
		mEdQosOut.setText("0");
		mChkRetained.setChecked(true);
		
	    Validate edTituloValidator = new Validate(mEdTitulo);
	    Validate edTopicoValidator = new Validate(mEdTopicoIn);
	    
	    edTituloValidator.addValidator(new NotEmptyValidator(this));
	    edTopicoValidator.addValidator(new NotEmptyValidator(this));
		
	    mForm.addValidates(edTituloValidator);
	    mForm.addValidates(edTopicoValidator);
	    
	    int id=getIntent().getIntExtra(CARTAO_ID, -1);
		if (id==-1) {
			finish();
		}
		mSensorName=getIntent().getStringExtra(SENSOR_NAME);
		mActionName=getIntent().getStringExtra(ACTION_NAME);
		mSensorId=getIntent().getIntExtra(SENSOR_ID, 0);
		mActionId=getIntent().getIntExtra(ACTION_ID, 0);
		mModel=Model.getInstance();
		mCartao=mModel.getCartao(id);		
		if (mCartao==null) finish();
		
		mSensor= mCartao.getSensor(mSensorId);
		if (mSensor==null) finish();
		mAction=mSensor.getAction(mActionId);
		mEditing=mAction!=null;
		
		if (mEditing) {
			fillActivity(mAction);
		}
		setTitle(String.format(getString(R.string.title_add_action_to),mActionName,mSensor.getName()));
		
		Validate edQosValidator = new Validate(mEdQosOut);
		edQosValidator.addValidator(new RegExpValidator(this, "[0|1|2]",R.string.qos_validor));
		edQosValidator.addValidator(new NotEmptyValidator(this));
		
		Validate mEdTopicoInValidate= new Validate(mEdTopicoIn);
		mEdTopicoInValidate.addValidator(new NotEmptyValidator(this));
		
		mForm.addValidates(edQosValidator);
		mForm.addValidates(mEdTopicoInValidate);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getSupportMenuInflater().inflate(R.menu.ok_cancel, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_ok:
			if (validate()) okClick();
			return true;
		case R.id.action_cancel:
			//finish();
			cancelClick();
			return true;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}	
	
	protected void fillActivity(Action action) {
		mEdTitulo.setText(action.getName());
		mEdQosOut.setText(""+action.getQosOut());
		mEdTopicoIn.setText(action.getTopicIn());
		mEdTopicoOut.setText(action.getTopicOut());
		mChkRetained.setChecked(action.isRetained());
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	}
	

	
	@Override
	protected void onResume() {
		super.onResume();
	}
	
	
	private boolean validate() {
		if(mForm.validate()){
			//Toast.makeText(getApplicationContext(), "Valid Form", Toast.LENGTH_LONG).show();
			return true;
		}
		return false;
	}
	
	
	protected void okClick() {
		
	}
	
	protected void cancelClick() {
		finish();
	}
	
	protected Context getContext(){
		return this;
	}

}
