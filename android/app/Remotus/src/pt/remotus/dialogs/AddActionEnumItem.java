package pt.remotus.dialogs;

import org.holoeverywhere.widget.ListView;

import pt.remotus.R;
import pt.remotus.adapter.EnumAdapter;
import pt.remotus.model.action.ActionEnum;
import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.throrinstudio.android.common.libs.validator.Form;
import com.throrinstudio.android.common.libs.validator.Validate;
import com.throrinstudio.android.common.libs.validator.validator.NotEmptyValidator;
import com.throrinstudio.android.common.libs.validator.validator.UrlValidator;

public class AddActionEnumItem implements OnClickListener {
	private ActionEnum.Item mActionItem;
	private ActionEnum mAction;
	private NetworkImageView mImage1;
	private EnumAdapter mAdapter;
	private Context mContext;
	private ImageLoader mImageLoad;
	private ListView myList;
	private Form mForm = new Form();
	
	public AddActionEnumItem(Context context, ActionEnum action, EnumAdapter adapter, ImageLoader imageLoad, ListView list){
		this.mActionItem=null;
		this.mAction = action;
		this.mAdapter = adapter;
		this.mContext= context;
		this.mImageLoad = imageLoad;
		this.myList = list;
	}
	
	public AddActionEnumItem(Context context, ActionEnum.Item item, EnumAdapter adapter, ImageLoader imageLoad){
		this.mContext = context;
		this.mActionItem = item;
		this.mAdapter = adapter;
		this.mImageLoad = imageLoad;
	}
	
	@Override
	public void onClick(View v) {
    	
    	final Dialog dialog = new Dialog(mContext);
        dialog.setContentView(R.layout.activity_add_action_enum_row);
        dialog.setTitle("Item...");
        
        Button dialogButtonOk = (Button) dialog.findViewById(R.id.btnItemOk);
        Button dialogButtonCancel = (Button) dialog.findViewById(R.id.btnItemCancelar);
        final EditText edNome =(EditText)dialog.findViewById(R.id.edEnumRowName);
        final EditText edUrl =(EditText)dialog.findViewById(R.id.edEnumRowUrl);
        final EditText edValor =(EditText)dialog.findViewById(R.id.edEnumRowValue);
        
        mImage1 = (NetworkImageView)dialog.findViewById(R.id.imageItemRow);

		edUrl.addTextChangedListener(edImagem1Changed);

        Validate edImagem1urlValidator = new Validate(edUrl);
	    edImagem1urlValidator.addValidator(new UrlValidator(mContext));
	    
	    
	    Validate mEdNomeValidator = new Validate(edNome);
	    mEdNomeValidator.addValidator(new NotEmptyValidator(mContext));
	    
	    Validate mEdValorValidator = new Validate(edValor);
	    mEdValorValidator.addValidator(new NotEmptyValidator(mContext));
	    
	    mForm.addValidates(edImagem1urlValidator);
	    mForm.addValidates(mEdNomeValidator);
	    mForm.addValidates(mEdValorValidator);
	    
	    
	    if (mActionItem!=null){
	    	edNome.setText(mActionItem.getName());
	    	edUrl.setText(mActionItem.getImageUrl());
	    	edValor.setText(mActionItem.getValue());
	    }
	    
	    dialogButtonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            	if (!validate())return;
            	if (mActionItem==null)
            		mAction.addItem(edNome.getText().toString(), edUrl.getText().toString(), edValor.getText().toString());
            	else{
            		mActionItem.setName(edNome.getText().toString());
            		mActionItem.setValue(edValor.getText().toString());
            		mActionItem.setImageUrl(edUrl.getText().toString());
            	}
            	mAdapter.notifyDataSetChanged();
            	dialog.dismiss();
            	if (myList!=null)
            		AddActionEnum.setListViewHeightBasedOnChildren(myList);
            }
        });
	    
	    dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            	dialog.dismiss();
            }
        });

        dialog.show();
    }
    
    private TextWatcher edImagem1Changed= new TextWatcher() {
    	
		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
		}
		
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,	int after) {
			
		}
		
		@Override
		public void afterTextChanged(Editable s) {
			if (mImageLoad==null) return;
			if (s.toString().isEmpty()) return;
			if (Uri.parse(s.toString()).getHost()==null) return;
			mImage1.setImageUrl(s.toString(), mImageLoad);
		}
	};
	
	private boolean validate() {
		if(mForm.validate()){
			return true;
		}
		return false;
	}
}