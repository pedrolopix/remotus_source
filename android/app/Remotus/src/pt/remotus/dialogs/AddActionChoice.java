package pt.remotus.dialogs;

import java.util.List;

import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.app.AlertDialog.Builder;
import org.holoeverywhere.app.DialogFragment;
import org.holoeverywhere.widget.ArrayAdapter;

import pt.remotus.R;
import pt.remotus.cards.CardFactory;
import pt.remotus.cards.CardFactory.ItemAction;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class AddActionChoice extends DialogFragment{

    protected static final String TAG = "AddActionChoice";
	private List<String> list;
	private int cartaoId;
	private String sensorName;
	private int sensorId;

	public AddActionChoice() {
        setDialogType(DialogType.AlertDialog);
    }

    @Override
    public AlertDialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getSupportActivity());
        builder.setTitle(R.string.title_escolher_acao);
        //builder.setIcon(R.drawable.ic_launcher);
        prepareBuilder(builder);
        cartaoId=getArguments().getInt(AddAction.CARTAO_ID);
        sensorName=getArguments().getString(AddAction.SENSOR_NAME);
        sensorId=getArguments().getInt(AddAction.SENSOR_ID);
        Log.d(TAG,"cartaoID: "+cartaoId);
        return builder.create();
    }

	private final OnClickListener clickListener = new OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            Log.d(TAG,"escolhido: "+list.get(which));
            ItemAction item = CardFactory.getActionbyName(list.get(which));
            if (item==null) {
				Log.d(TAG, "configuração não encontrada para:" + list.get(which));
            	return; // TODO registar erro
            }

			Intent intent = new Intent(getActivity(), item.getAddClass());
			intent.putExtra(AddAction.CARTAO_ID, cartaoId);
			intent.putExtra(AddAction.ACTION_NAME, list.get(which));
			intent.putExtra(AddAction.SENSOR_NAME, sensorName);
			intent.putExtra(AddAction.SENSOR_ID, sensorId);
			getActivity().startActivity(intent);

        	dialog.dismiss();
        }
    };

    protected void prepareBuilder(Builder builder) {
    	list = CardFactory.getActions();
    	ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1, list);
        builder.setAdapter(dataAdapter, clickListener);
    }

}
