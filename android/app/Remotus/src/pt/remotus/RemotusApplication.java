package pt.remotus;

import java.io.File;
import org.holoeverywhere.HoloEverywhere;
import org.holoeverywhere.HoloEverywhere.PreferenceImpl;
import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.ThemeManager;
import org.holoeverywhere.app.Application;
import pt.remotus.R;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.os.Environment;
import android.support.v4.util.LruCache;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageLoader.ImageCache;
import com.android.volley.toolbox.Volley;

public class RemotusApplication extends Application {

	private static final String PACKAGE = RemotusApplication.class.getPackage().getName();
	private static final String DISK_CACHE_SUBDIR = "imageCache";
	private ImageLoader mImageLoader;
	private static RemotusApplication sInstance;

	private final LruCache<String, Bitmap> mImageCache = new LruCache<String, Bitmap>(20);

	public static RemotusApplication get() {
		return sInstance;
	}

	static {
		HoloEverywhere.DEBUG = true;
		HoloEverywhere.PREFERENCE_IMPL = PreferenceImpl.JSON;

		LayoutInflater.registerPackage(PACKAGE + ".widget");

		ThemeManager.setDefaultTheme(ThemeManager.MIXED);

		// Android 2.* incorrect process FULLSCREEN flag when we are modify
		// DecorView of Window. This hack using HoloEverywhere Slider
		if (VERSION.SDK_INT >= VERSION_CODES.HONEYCOMB) {
			// ThemeManager.modify(ThemeManager.FULLSCREEN);
		}

		ThemeManager.map(ThemeManager.DARK, R.style.Holo_Remotus_Theme);
		ThemeManager.map(ThemeManager.LIGHT, R.style.Holo_Remotus_Theme_Light);
		ThemeManager.map(ThemeManager.MIXED, R.style.Holo_Remotus_Theme_Light_DarkActionBar);
//
//		ThemeManager.map(ThemeManager.DARK | ThemeManager.FULLSCREEN, R.style.Holo_Demo_Theme_Fullscreen);
//		ThemeManager.map(ThemeManager.LIGHT | ThemeManager.FULLSCREEN, R.style.Holo_Demo_Theme_Light_Fullscreen);
//		ThemeManager.map(ThemeManager.MIXED | ThemeManager.FULLSCREEN, R.style.Holo_Demo_Theme_Light_DarkActionBar_Fullscreen);
	}

	@Override
	public void onCreate() {
		super.onCreate();
		sInstance = this;

		RequestQueue queue = Volley.newRequestQueue(this);

		ImageCache imageCache = new ImageCache() {

			@Override
			public void putBitmap(String key, Bitmap value) {
				mImageCache.put(key, value);
			}

			@Override
			public Bitmap getBitmap(String key) {
				return mImageCache.get(key);
			}
		};

		// File cacheDir = getDiskCacheDir(this, DISK_CACHE_SUBDIR);

		mImageLoader = new ImageLoader(queue, imageCache);
	}

	// Creates a unique subdirectory of the designated app cache directory. Tries to use external
	// but if not mounted, falls back on internal storage.
	// http://developer.android.com/training/displaying-bitmaps/cache-bitmap.html
	public static File getDiskCacheDir(Context context, String uniqueName) {
		// Check if media is mounted or storage is built-in, if so, try and use external cache dir
		// otherwise use internal cache dir
		final String cachePath = Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState()) || !Environment.isExternalStorageRemovable()
				? Environment.getDownloadCacheDirectory().getPath()
				: context.getCacheDir().getPath();

		return new File(cachePath + File.separator + uniqueName);
	}

	public ImageLoader getImageLoader() {
		return mImageLoader;
	}



}
