package pt.remotus.model;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pt.remotus.cards.CardFactory;
import pt.remotus.cards.CardFactory.Item;
import pt.remotus.model.action.Action;
import pt.remotus.model.action.ActionOnOff;
import pt.remotus.model.action.ActionSlider;
import pt.remotus.model.event.EventLoadCards;
import pt.remotus.model.event.EventMqttConnected;
import pt.remotus.model.event.EventMqttConnectionLost;
import pt.remotus.model.event.EventMqttMessageArrived;
import pt.remotus.model.json.CardJSON;
import pt.remotus.model.json.JSON;
import pt.remotus.model.notification.Notification;
import pt.remotus.model.notification.Notifications;
import pt.remotus.model.sensor.Sensor;
import pt.remotus.model.sensor.SensorOnOff;
import android.util.Log;

import com.squareup.otto.Subscribe;

public class Model {
	private static final String TAG_SENSORS = "sensors";
	private static final String TAG_ACTIONS = "actions";
	private static final String TAG_CARDS = "cards";
	private static final String TAG_NAME = "name";
	private static final String TAG_ID = "id";
	private static final String TAG_TOPICS = "topics";
	private static final String TAG_TOPIC = "topic";
	protected static final String TAG = "Model";
	private final Cartoes mCartoes;
	private final MqttEngine mMqttEngine;
	private final EventBus mBus;
	private final RemotusLog mLog;
	private boolean mIsStarted;
	private static Model mInstance;
	private Mqtt mMqtt;
	private boolean mIsConnected;
	private Notifications mNotifications;
	private String mTopicOfList;
	private List<String> mListTopic;

	public static Model getInstance() {
		if (mInstance==null) {
			mInstance= new Model();
		}
		return mInstance;
	}

	public Model() {
		super();
		mIsStarted=false;
		mIsConnected = false;
		mBus= new EventBus();
		mLog= new RemotusLog();
		mCartoes= new Cartoes(this);
		mNotifications = new Notifications(this);
		mListTopic = new ArrayList<String>();
		mBus.register(this);
		mMqttEngine= new MqttEngine(Model.this);
		mMqtt= new Mqtt();
		//dadosTeste();
		//mCartoes.setTopic("/remotus/config/pedro");
		dadosTeste1();
	}

	public void start() {
		if (mIsStarted) {
			return;
		}
		mIsStarted=true;
		startMqtt();
	}


	public void stop() {
		stopMqtt();
		mIsStarted = false;
		mIsConnected = false;
	}


	private void stopMqtt() {
		if (mMqttEngine!=null){
			mMqttEngine.disconnect();
		}
   }

	synchronized private void startMqtt() {
		Thread t= new Thread(new Runnable() {

			@Override
			public void run() {
				stopMqtt();
				if (mMqtt.getHost().isEmpty()) {
					mIsStarted = false;
					mIsConnected = false;
					return;
				}
				mLog.d(TAG,"Ligar a "+mMqtt.getHost());
				mMqttEngine.setServerURI(mMqtt.getHost());
				mMqttEngine.setPassword(mMqtt.getPassWord());
				mMqttEngine.setUserName(mMqtt.getUserName());
				mMqttEngine.connect();
				if (!mMqttEngine.isConnected()) {
					mLog.d(TAG,"Erro a ligar a "+mMqtt.getHost());
					mIsStarted=false;
					mIsConnected = false;
					return;
				}

				subscribe(mCartoes.getTopic(),mCartoes.GetQos());
				subscribeCards();
				subscribeNotifications();
			}


		});
		t.start();
	}
	
	synchronized private void subscribeNotifications() {
		for (Notification item : mNotifications.getNotifications()) {
			subscribe(item.getTopic(), item.getQos());
		}
	}
	
	public synchronized void unSubscribeNotifications() {
		for (Notification item : mNotifications.getNotifications()) {
			unsubscribe(item.getTopic());
		}
	}

	synchronized private void subscribeCards() {
		// Registar subscri????es mqtt

		for (Cartao item : mCartoes.getCartoes()) {
			for (Sensor sensor : item.getSensors()) {
				subscribe(sensor.getTopicIn(), sensor.getQos());
				for (Sensor action : sensor.getActions()) {
					subscribe(action.getTopicIn(), sensor.getQos());
				}
			}
		}
	}

	public synchronized void unSubscribeCards() {
		// Remover subscri????es mqtt
		for (Cartao item : mCartoes.getCartoes()) {
			for (Sensor sensor : item.getSensors()) {
				unsubscribe(sensor.getTopicIn());
				for (Sensor action : sensor.getActions()) {
					unsubscribe(action.getTopicIn());
				}
			}
		}
	}


	private void dadosTeste() {
		mCartoes.setTopic("/remotus/config/pedro");
		mCartoes.setNome("Cart??es");

	    mMqtt=new Mqtt("pedrolopes","tcp://pedrolopes.no-ip.com:1883","Pedro","ze");


		Cartao card=new Cartao();
		card.setName("Sala");
		Sensor s= new Sensor();
		s.set(card, mMqtt, "/arduino/1/rgb",0,"RGB","","http://192.168.10.2/remotus/imagens/sensores/rgb.png");
		s.addAction(new ActionSlider().set(s, mMqtt, "/arduino/1/rgb/r",0,"",0,true,"RGB","Red"));
		s.addAction(new ActionSlider().set(s, mMqtt, "/arduino/1/rgb/b",0,"",0,true,"RGB","green"));
		s.addAction(new ActionSlider().set(s, mMqtt, "/arduino/1/rgb/g",0,"",0,true,"RGB","blue"));
		card.addSensor(s);
		s= new Sensor();
		s.set(card, mMqtt, "/arduino/1/lightsensor",0,"Sensor","valor","http://192.168.10.2/remotus/imagens/sensores/sun.png");
		card.addSensor(s);
		s = new Sensor();
		s.set(card, mMqtt, "/arduino/1/resistor",0,"resistor","valor","http://192.168.10.2/remotus/imagens/sensores/volume.png");
		card.addSensor(s);
		mCartoes.add(card);

		card=new Cartao();
		card.setName("Quarto");
		SensorOnOff so= new SensorOnOff();
		so.set(card, mMqtt, "/arduino/1/luz",0,"Luz","ON","OFF",
				"http://192.168.10.2/remotus/imagens/sensores/lamp_on.png",
				"http://192.168.10.2/remotus/imagens/sensores/lamp_off.png");
		so.addAction(new ActionOnOff().set(s, mMqtt, "/arduino/1/luz",0,"",0,true,"On/Off","Ligado","Desligado"));
		card.addSensor(so);
		mCartoes.add(card);

		mBus.postAsync(new EventLoadCards());

		try {
			saveCards();
		} catch (JSONException e) {
			e.printStackTrace();
		}

	}
	
	private void dadosTeste1() {

		Notification n = new Notification();
		n.set(mNotifications, "/carlos/n/1");
		mNotifications.addNotification(n);
		
		Notification n2 = new Notification();
		n2.set(mNotifications, "/carlos/n/2");
		mNotifications.addNotification(n2);
		
		Notification n3 = new Notification();
		n3.set(mNotifications, "/carlos/n/3");
		mNotifications.addNotification(n3);
		
	}

	@Subscribe
	public void onMqttConnectionLost(EventMqttConnectionLost event) {
		mIsStarted=false;
		mIsConnected = false;
	}

	@Subscribe
	public void onMqttConnection(EventMqttConnected event) {
		mIsConnected = true;
	}

	@Subscribe
	synchronized public void onReceiveMqtt(EventMqttMessageArrived event) {
		if (mCartoes.getTopic().equals(event.getTopic())) {
			try {
				loadCards(event.getMessage().toString());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		for (Cartao item : mCartoes.getCartoes()) {
			for (Sensor sensor : item.getSensors()) {
				if (sensor.getTopicIn().equals(event.getTopic())) {
					sensor.updateValue(event.getMessage().toString());
				}
				for (Action action : sensor.getActions()) {
					if (action.getTopicIn().equals(event.getTopic())) {
						action.updateValue(event.getMessage().toString());
					}
				}
			}
		}

		for (Notification item : mNotifications.getNotifications()) {
			if (item.getTopic().equals(event.getTopic())) {
				item.updateValue(event.getMessage().toString());
			}	
		}
		
		if (mTopicOfList.equals(event.getTopic())){
			try {
				updateTopicOfList(event.getMessage().toString());
			} catch (JSONException e) {
				getLog().d(TAG, "Ocorreu um erro ao atualizar o tópico da lista de tópicos. " + e.getMessage());
			}
		}

	}

	synchronized public void addEntity(Cartao card) {
		mCartoes.add(card);
	}

	public EventBus getBus() {
		return mBus;
	}

	public RemotusLog getLog() {
		return mLog;
	}


	synchronized public List<Cartao> getEntities() {
		return mCartoes.getCartoes();
	}


	public Mqtt getMqtt() {
		return mMqtt;
	}

	public void publish(Action action, String value) {
		mMqttEngine.publish(action.getTopicOut(), value, action.isRetained(), action.getQosOut());
	}

	public boolean isStarted() {
		return mIsStarted;
	}

	synchronized public void removeCartao(Cartao cartao) {
		mCartoes.remove(cartao);
		cartaoChanged(cartao);
	}

	synchronized public Cartao getCartao(int id) {
		return mCartoes.getCartao(id);
	}

	synchronized public Sensor getSensor(int cartaoId, int sensorId) {
		Cartao cartao= getCartao(cartaoId);
		if (cartao==null) {
			return null;
		}
		return cartao.getSensor(sensorId);
	}

	synchronized public void cartaoChanged(Cartao mCartao) {
		//mBus.postAsync(new EventLoadCards());
		//startMqtt();
		Log.d(TAG,"cartaoChanged");
		unSubscribeCards();
		subscribeCards();
		if (mCartao==null) {
			return;
		}
		try {
			saveCards();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	synchronized public void loadCards(String json) throws JSONException {
		Log.d(TAG,"loadCards");
		JSONObject o= new JSONObject(json);
		JSON cj= new CardJSON();
		String nome=o.getString(TAG_NAME);
		String id=o.optString(TAG_ID);
		// se for o mesmo cart??o pode ser ignorado
		if (!id.isEmpty() &&  id.equals(mCartoes.getId())) {
			Log.d(TAG,"loadCards, same id discarted");
			return;
		}
		unSubscribeCards();
		mCartoes.clear();
		mCartoes.setNome(nome);
		JSONArray ca = o.getJSONArray(TAG_CARDS);
		if (ca!=null) {
			for(int i=0; i<ca.length(); i++) {
				JSONObject ci= ca.getJSONObject(i);
				Cartao card=(Cartao)cj.fromJSON(ci);
				mCartoes.add(card);
				//sensores
				JSONArray as = null;
				try{ // este try tem a ver com o json que é construido pelo ios, pois n trás o array sensors
					as=ci.getJSONArray(TAG_SENSORS);
					if (as==null) {
						continue;
					}
				} catch (JSONException e) {
					continue;
				}
				for(int j=0; j<as.length(); j++) {
					JSONObject s= as.getJSONObject(j);
					String sensorClass = s.getString(JSON.TAG_CLASS);
					Item iSensor=CardFactory.getSensor(sensorClass);
					if (iSensor==null) {
						continue;
					}
					Sensor sensor=(Sensor)iSensor.fromJSON(s);
					card.addSensor(sensor);
					JSONArray aa;
					try{ // este try tem a ver com o json que é construido pelo ios, pois n trás o array sensors
						aa=s.getJSONArray(TAG_ACTIONS);
					} catch (JSONException e){
						continue;
					}
					for(int n=0; n<aa.length(); n++) {
						JSONObject a= aa.getJSONObject(n);
						String actionClass = a.getString(JSON.TAG_CLASS);
						Item iAction=CardFactory.getAction(actionClass);
						if (iAction==null) {
							continue;
						}
						Action action=(Action)iAction.fromJSON(a);
						sensor.addAction(action);
					}
				}
			}
		}

		mBus.post(new EventLoadCards());
		subscribeCards();
	}

	synchronized public void saveCards() throws JSONException {
		Log.d(TAG,"SaveCards");
		JSONObject ca= new JSONObject();
		//gerar novo id
		mCartoes.setId(UUID.randomUUID().toString());
		ca.put(TAG_NAME, mCartoes.getNome());
		ca.put(TAG_ID, mCartoes.getId());
		JSONArray ac = new JSONArray();
		JSON cj= new CardJSON();
		for (Cartao card : mCartoes.getCartoes()) {
			JSONObject c=cj.toJSON(card);
			JSONArray acs = new JSONArray();
			for (Sensor sensor:card.getSensors()) {
				Item iSensor=CardFactory.getSensor(sensor);
				JSONObject s=iSensor.ToJSON(sensor);
				JSONArray acsa = new JSONArray();
				for(Action action: sensor.getActions()) {
					Item iAction=CardFactory.getAction(action);
					JSONObject a=iAction.ToJSON(action);
					acsa.put(a);
				}
				s.put(TAG_ACTIONS, acsa);
				acs.put(s);
			}
			c.put(TAG_SENSORS, acs);
			ac.put(c);
		}
		ca.put(TAG_CARDS, ac);
		//Log.d(TAG,ac.toString());


		mMqttEngine.publish(mCartoes.getTopic(),ca.toString(),mCartoes.isRetained(),mCartoes.GetQos());
		mCartoes.setId(UUID.randomUUID().toString());
	}

	public Cartoes getCartoes() {
		return mCartoes;
	}

	public boolean isConfigured() {
		return !mCartoes.getTopic().isEmpty() || !mMqtt.getHost().isEmpty();
	}

	public void unsubscribe(String topic) {
		mMqttEngine.unsubscribe(topic);
	}

	public void subscribe(String topic, int getQos) {
		mMqttEngine.subscribe(topic, getQos);
	}

	public boolean isConnected() {
		return mIsConnected;
	}

	public void setTopicOfList(String topic){
		this.mTopicOfList = topic;
	}

	public String getTopicOfList(){
		return mTopicOfList;
	}
	
	private void updateTopicOfList(String message) throws JSONException {
		Log.d(TAG,"updateTopicOfList");
		mListTopic.clear();
		JSONObject o= new JSONObject(message);
		JSONArray na = o.getJSONArray(TAG_TOPICS);
		if (na!=null) {
			for(int i=0; i<na.length(); i++) {
				JSONObject ni= na.getJSONObject(i);
				String topic=ni.getString(TAG_TOPIC);
				mListTopic.add(topic);
				Log.d(TAG,"updateTopicOfList: "+topic);
			}
		}
	}
	
	public List<String> getListTopic(){
		return mListTopic;
	}
	
	public CharSequence[] getListTopicToArray(){
		CharSequence[] cs =mListTopic.toArray(new CharSequence[mListTopic.size()]);
		return cs;
	}
	
	public int getPositionTopicListByTopic(String topic){
		int position=-1;
		if (topic==null) return position;
		for(int i=0; i<mListTopic.size(); i++)
			if (mListTopic.get(i).equals(topic))
				position = i;
		return position;
	}
}
