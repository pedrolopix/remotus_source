package pt.remotus.model.json;

import org.json.JSONException;
import org.json.JSONObject;

import pt.remotus.model.action.ActionOnOff;
import pt.remotus.model.sensor.Sensor;

public class ActionOnOffJSON extends ActionJSON {

	@Override
	public JSONObject toJSON(Object obj) throws JSONException {
		if (!(obj instanceof ActionOnOff)) {
			notSuported(obj);
			return null;
		}
		ActionOnOff a=(ActionOnOff)obj;
		JSONObject o = super.toJSON(a);
		o.put(TAG_ON,a.getOn());
		o.put(TAG_OFF,a.getOff());
		return o;
	}

	@Override
	public Object fromJSON(JSONObject json) throws JSONException {
		ActionOnOff action=new ActionOnOff();
		fillSensor(json, action);
		return action;
	}
	
	
	@Override
	protected void fillSensor(JSONObject json, Sensor sensor) throws JSONException {
		ActionOnOff a= (ActionOnOff)sensor;
		super.fillSensor(json, sensor);
		String on=json.getString(TAG_ON);
		String off=json.getString(TAG_OFF);
		a.setOn(on);
		a.setOff(off);
	}
	

}
