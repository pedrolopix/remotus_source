package pt.remotus.model.json;

import org.json.JSONException;
import org.json.JSONObject;

public abstract class JSON {
	
	public static final String TAG_CLASS = "class";
	public static final String TAG_IMAGE_OFF_URL = "imageOffUrl";
	public static final String TAG_IMAGE_URL = "imageurl";
	public static final String TAG_NAME = "name";
	public static final String TAG_OFF = "off";
	public static final String TAG_ON = "on";
	public static final String TAG_QOS = "qos";
	public static final String TAG_TOPIC_IN = "topicIn";
	public static final String TAG_UNIT = "unit";
	public static final String TAG_VALUE = "value";
	public static final String TAG_VALUE_VISIBLE = "valueVisible";
	public static final String TAG_TOPIC = "topic";
	protected static final String TAG_RETAINED = "retained";
	protected static final String TAG_QOS_OUT = "qosOut";
	protected static final String TAG_TOPIC_OUT = "topicOut";
	protected static final String TAG_ENUM_ARRAY = "enumArray";
	protected static final String TAG_ENUM_NAME = "enumName";
	protected static final String TAG_ENUM_URL = "enumUrl";
	protected static final String TAG_ENUM_VALUE = "enumValue";
	protected static final String TAG_ENUM_SHOWGRID = "enumShowGrid";
	protected static final String TAG_SLIDE_MINVALUE = "min";
	protected static final String TAG_SLIDE_MAXVALUE = "max";
	protected static final String TAG_SLIDE_INCREMENTALVALUE = "incremental";
	protected static final String TAG_SLIDE_TRIGGERUP = "triggerUp";
	
	
	public abstract Object fromJSON(JSONObject json)throws JSONException;;
	
	void notSuported(Object obj) throws JSONException {
		throw new JSONException("Objecto nãoo suportado: "+obj.getClass());
	}

	public JSONObject toJSON(Object obj) throws JSONException {
		JSONObject o = new JSONObject();
		o.put(TAG_CLASS,obj.getClass().getSimpleName());
		return o;
	}
	
}
