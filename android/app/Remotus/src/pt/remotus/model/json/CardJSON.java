package pt.remotus.model.json;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;
import pt.remotus.model.Cartao;

public class CardJSON  extends JSON{

	@Override
	public JSONObject toJSON(Object obj) throws JSONException {
		if (!(obj instanceof Cartao)) {
			notSuported(obj);
			return null;
		}
		Cartao c=(Cartao)obj;
		JSONObject o= super.toJSON(c);
		o.put(TAG_NAME,c.getName());
		o.put(TAG_IMAGE_URL, c.getImageUrl());
		return o;
	}


	@Override
	public Object fromJSON(JSONObject json) throws JSONException {
		Cartao card=new Cartao();
		String nome= json.getString(TAG_NAME);
		Log.d("Nome+++++", nome);
		String imageUrl;
		try{
			imageUrl= json.getString(TAG_IMAGE_URL);
		} catch(JSONException ex){
			imageUrl = "";
		}
		Log.d("image+++++", nome);
		card.setName(nome);
		card.setImageUrl(imageUrl);
		return card;
	}


}
