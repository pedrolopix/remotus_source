package pt.remotus.model.json;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pt.remotus.model.action.ActionEnum;
import pt.remotus.model.sensor.Sensor;

public class ActionEnumJSON  extends ActionJSON{

	@Override
	public JSONObject toJSON(Object obj) throws JSONException {
		if (!(obj instanceof ActionEnum)) {
			notSuported(obj);
			return null;
		}
		ActionEnum a=(ActionEnum)obj;
		JSONObject o = super.toJSON(a);
		o.put(TAG_ENUM_SHOWGRID,a.isShowGrid());
		
		JSONArray arr = new JSONArray();
		for(int i=0; i<a.getCount();i++){
			JSONObject ob = new JSONObject();
			ob.put(TAG_ENUM_NAME, a.getActionOfList(i).getName());
			ob.put(TAG_ENUM_URL, a.getActionOfList(i).getImageUrl());
			ob.put(TAG_ENUM_VALUE,a.getActionOfList(i).getValue());
			arr.put(ob);
		}
		o.put(TAG_ENUM_ARRAY, arr);
		
		return o;
	}

	@Override
	public Object fromJSON(JSONObject json) throws JSONException {
		ActionEnum action=new ActionEnum();
		fillSensor(json, action);
		try{
			JSONArray arr = json.getJSONArray(TAG_ENUM_ARRAY);
			for (int i=0; i<arr.length(); i++){
				JSONObject o = (JSONObject)arr.get(i);
				action.addItem(o.getString(TAG_ENUM_NAME), o.getString(TAG_ENUM_URL), o.getString(TAG_ENUM_VALUE));
			}
		} catch(JSONException ex){
			
		}
		return action;
	}
	
	@Override
	protected void fillSensor(JSONObject json, Sensor sensor) throws JSONException {
		ActionEnum a= (ActionEnum)sensor;
		super.fillSensor(json, sensor);
		boolean show = false;
		try {
			show=json.getBoolean(TAG_ENUM_SHOWGRID);
		} catch (Exception e){
		}
		a.setShowGrid(show);
	}

}
