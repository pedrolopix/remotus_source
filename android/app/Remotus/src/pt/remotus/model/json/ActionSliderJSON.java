package pt.remotus.model.json;

import org.json.JSONException;
import org.json.JSONObject;

import pt.remotus.model.action.ActionSlider;
import pt.remotus.model.sensor.Sensor;

public class ActionSliderJSON  extends ActionJSON{

	@Override
	public JSONObject toJSON(Object obj) throws JSONException {
		if (!(obj instanceof ActionSlider)) {
			notSuported(obj);
			return null;
		}
		ActionSlider a=(ActionSlider)obj;
		JSONObject o = super.toJSON(a);
		o.put(TAG_SLIDE_MINVALUE,a.getMin());
		o.put(TAG_SLIDE_MAXVALUE,a.getMax());
		o.put(TAG_SLIDE_INCREMENTALVALUE,a.getIncremental());
		o.put(TAG_SLIDE_TRIGGERUP, a.isTriggerUp());
		return o;
	}

	@Override
	public Object fromJSON(JSONObject json) throws JSONException {
		ActionSlider action=new ActionSlider();
		fillSensor(json, action);
		return action;
	}

	@Override
	protected void fillSensor(JSONObject json, Sensor sensor) throws JSONException {
		ActionSlider a= (ActionSlider)sensor;
		super.fillSensor(json, sensor);
		try{
			int min=json.getInt(TAG_SLIDE_MINVALUE);
			int max=json.getInt(TAG_SLIDE_MAXVALUE);
			int inc=json.getInt(TAG_SLIDE_INCREMENTALVALUE);
			boolean up = json.getBoolean(TAG_SLIDE_TRIGGERUP);
			a.setMin(min);
			a.setMax(max);
			a.setIncremental(inc);
			a.setTriggerUp(up);
		} catch (JSONException ex){
			
		}
	}
	
	

}
