package pt.remotus.model.json;

import org.json.JSONException;
import org.json.JSONObject;

import pt.remotus.model.sensor.Sensor;
import pt.remotus.model.sensor.SensorOnOff;

public class SensorOnOffJSON  extends SensorJSON{

	@Override
	public JSONObject toJSON(Object obj) throws JSONException {
		if (!(obj instanceof SensorOnOff)) {
			notSuported(obj);
			return null;
		}
		SensorOnOff s=(SensorOnOff)obj;
		JSONObject o= super.toJSON(s);
		o.put(TAG_ON,s.getOn());
		o.put(TAG_OFF,s.getOff());
		o.put(TAG_IMAGE_OFF_URL,s.getImageOffUrl());
		return o;
	}

	@Override
	public Object fromJSON(JSONObject json) throws JSONException {
		SensorOnOff sensor=new SensorOnOff();
		fillSensor(json, sensor);
		return sensor;
	}
	
	@Override
	protected void fillSensor(JSONObject json, Sensor sensor) throws JSONException {
		SensorOnOff s= (SensorOnOff)sensor;
		super.fillSensor(json, sensor);
		String on=json.getString(TAG_ON);
		String off=json.getString(TAG_OFF);
		String imageOffUrl=json.getString(TAG_IMAGE_OFF_URL);
		
		s.setOn(on);
		s.setOff(off);
		s.setImageOffUrl(imageOffUrl);
		
	}

}
