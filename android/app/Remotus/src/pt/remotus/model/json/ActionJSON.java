package pt.remotus.model.json;

import org.json.JSONException;
import org.json.JSONObject;

import pt.remotus.model.action.Action;
import pt.remotus.model.sensor.Sensor;

public class ActionJSON extends SensorJSON {

	@Override
	public JSONObject toJSON(Object obj) throws JSONException {
		if (!(obj instanceof Action)) {
			notSuported(obj);
			return null;
		}
		Action a=(Action)obj;
		JSONObject o = super.toJSON(a);
		
		o.put(TAG_TOPIC_OUT,a.getTopicOut());
		o.put(TAG_QOS_OUT,a.getQosOut());
		o.put(TAG_RETAINED,a.isRetained());
		
		return o;
	}

	@Override
	protected void fillSensor(JSONObject json, Sensor sensor) throws JSONException {
		Action a= (Action)sensor;
		super.fillSensor(json, sensor);
		String topicOut=json.getString(TAG_TOPIC_OUT);
		int qosOut=json.getInt(TAG_QOS_OUT);
		boolean retained=json.getBoolean(TAG_RETAINED);
		a.setTopicOut(topicOut);
		a.setQosOut(qosOut);
		a.setRetained(retained);
	}

}
