package pt.remotus.model.json;

import org.json.JSONException;
import org.json.JSONObject;

import pt.remotus.model.sensor.Sensor;

public class SensorJSON  extends JSON{




	@Override
	public JSONObject toJSON(Object obj) throws JSONException {
		if (!(obj instanceof Sensor)) {
			notSuported(obj);
			return null;
		}
		Sensor s=(Sensor)obj;
		JSONObject o= super.toJSON(s);
		o.put(TAG_NAME, s.getName());
		o.put(TAG_TOPIC_IN,s.getTopicIn());
		o.put(TAG_VALUE,s.getValue());
		o.put(TAG_UNIT,s.getUnit());
		o.put(TAG_QOS, s.getQos());
		o.put(TAG_VALUE_VISIBLE, s.isValueVisible());
		o.put(TAG_IMAGE_URL, s.getImageUrl());
		return o;
	}
	
	@Override
	public Object fromJSON(JSONObject json) throws JSONException {
		Sensor sensor=new Sensor();
		fillSensor(json, sensor);
		return sensor;
	}

	protected void fillSensor(JSONObject json, Sensor sensor) throws JSONException {
		String nome=json.getString(TAG_NAME);
		String topicIn=json.getString(TAG_TOPIC_IN);
		//String value=json.getString(TAG_VALUE);
		String unit=json.getString(TAG_UNIT);
		int qos=json.getInt(TAG_QOS);
		boolean visible=json.getBoolean(TAG_VALUE_VISIBLE);
		String imageUrl=json.getString(TAG_IMAGE_URL);
		
		sensor.setName(nome);
		sensor.setTopicIn(topicIn);
		sensor.setUnit(unit);
		sensor.setQos(qos);
		sensor.setValueVisible(visible);
		sensor.setImageUrl(imageUrl);
	}

}
