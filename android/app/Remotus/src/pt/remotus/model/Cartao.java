package pt.remotus.model;

import java.util.ArrayList;
import java.util.List;

import pt.remotus.model.sensor.Sensor;

public class Cartao {
	private static int last=0;
	private int id;
	private String name;
	private String mImageUrl;
	private List<Sensor> sensors;
	private Cartoes mCards;
	
	public Cartao() {
		super();
		id=last++;
		sensors = new ArrayList<Sensor>();
	}
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getImageUrl() {
		return mImageUrl;
	}
	
	public void setImageUrl(String imageUrl) {
		this.mImageUrl = imageUrl;
	}

	public List<Sensor> getSensors() {
		return sensors;
	}
	
	public void addSensor(Sensor sensor) {
		sensor.setCard(this);
		sensors.add(sensor);
	}
	
	public Model getModel() {
		return mCards.getModel();
	}

	public int getId() {
		return id;
	}

	public Sensor getSensor(int sensorId) {
		for (Sensor item:sensors) {
			if (item.getId()==sensorId) return item;
		}
		return null;
	}

	public void removeSensor(Sensor sensor) {
		sensors.remove(sensor);
	}


	public void setCards(Cartoes cards) {
		mCards=cards;
	}
	

}
