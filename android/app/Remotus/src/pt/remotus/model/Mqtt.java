package pt.remotus.model;

public class Mqtt {
	private String host;
	private String name;
	private String userName;
	private String passWord;
	
	public Mqtt() {
		this.name = "";
		this.host = "";
		this.userName="";
		this.passWord="";	
	}
	
	public Mqtt(String name, String host, String userName, String passWord) {
		super();
		this.name = name;
		this.host = host;
		this.userName=userName;
		this.passWord=passWord;
	}
	
	public String getHost() {
		return host;
	}
	
	public void setHost(String host) {
		this.host = host;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassWord() {
		return passWord;
	}

	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}
	
}
