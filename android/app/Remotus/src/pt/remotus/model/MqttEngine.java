package pt.remotus.model;

import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;


public class MqttEngine {
	private static final String TAG = "MqttEngine";
	MqttClient client;
	private String serverURI;
	private String clientId;
	private String userName;
	private String password;
	private final Model model;

	private final MqttCallback callback=new MqttCallback() {
        @Override
        public void connectionLost(Throwable cause) {
            model.getBus().mqttConnectionLost(cause);
        }

        @Override
        public void messageArrived(String topic, MqttMessage message) throws Exception {
            model.getBus().mqttMessageArrived(topic,message);
        }

        @Override
        public void deliveryComplete(IMqttDeliveryToken token) {
            model.getBus().mqttDeliveryComplete(token);
        }

	};
	private boolean isConnected;


	public MqttEngine(Model model) {
		this.model=model;
	}

	public void unsubscribe(String topicFilter) {
		if (client==null) {
			return;
		}
		if (topicFilter==null || topicFilter.isEmpty()) {
			return;
		}
		model.getLog().i(TAG, "unsubscribe "+topicFilter);
		try {
			client.unsubscribe(topicFilter);
		} catch (MqttException e) {
			// o unsubscribe se der erro ?? porque est?? desligado...
		}
	}


	public boolean subscribe(String topicFilter, int qos) {
		if (client==null) {
			return false;
		}
		if (topicFilter==null) {
			return false;
		}
		if (topicFilter=="") {
			return false;
		}
		try {
			client.subscribe(topicFilter, qos);
			model.getLog().i(TAG, "subscribe "+topicFilter);
		} catch (MqttSecurityException e) {
			model.getLog().e(TAG,e);
			return false;
		} catch (MqttException e) {
			model.getLog().e(TAG,e);
			return false;
		}
		return true;
	}

	public void connect() {
		try {
			clientId = MqttClient.generateClientId(); //"remotus_android"; //
			client= new MqttClient(getServerURI(), clientId, new MemoryPersistence());
			MqttConnectOptions options= new MqttConnectOptions();
			options.setUserName(getUserName());
			options.setPassword(getPassword().toCharArray());
			options.setConnectionTimeout(500);
			client.setTimeToWait(500);
			client.setCallback(callback);
			client.connect(options);
			model.getLog().i(TAG, "ligado a "+getServerURI().toString());
			isConnected=true;
			model.getBus().mqttConnected(true);
		} catch (Exception e) {
			isConnected=false;
			model.getLog().e(TAG, e);
			model.getBus().mqttConnected(false);
		}
	}

	public void disconnect() {
		if (client!=null) {
			try {
				if (client.isConnected())
				    client.disconnect();
			} catch (MqttException e) {
				//model.getLog().e(TAG, e);
			}
		}
	}

	public void stop() {
		disconnect();
	}

	public String getServerURI() {
		return serverURI;
	}

	public void setServerURI(String serverURI) {
		this.serverURI = serverURI;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void publish(String topic, int value, boolean retained, int qos) {
		publish(topic, String.valueOf(value), retained, qos);
	}

	public void publish(final String topic, final String text, final boolean retained, final int qos) {
		if (client==null) {
			return;
		}
		Thread t= new Thread(new Runnable() {

			@Override
			public void run() {

				MqttTopic t = client.getTopic(topic);
				try {
					if (!client.isConnected()) {
						client.connect();
					}
					MqttMessage mqttMessage = new MqttMessage(text.getBytes());
					mqttMessage.setRetained(retained);
					mqttMessage.setQos(qos);
					t.publish(mqttMessage);
				} catch (MqttPersistenceException e) {
					model.getLog().e(TAG,e);
					model.getBus().mqttConnectionLost(e);
				} catch (MqttException e) {
					model.getLog().e(TAG,e);
					model.getBus().mqttConnectionLost(e);
				}
			}
		});
		t.start();
	}


	public boolean isConnected() {
		return isConnected;
	}


	public void setConnected(boolean isConnected) {
		this.isConnected = isConnected;
	}




}
