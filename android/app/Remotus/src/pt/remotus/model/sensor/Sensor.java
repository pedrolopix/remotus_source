package pt.remotus.model.sensor;

import java.util.ArrayList;
import java.util.List;

import pt.remotus.model.Cartao;
import pt.remotus.model.Mqtt;
import pt.remotus.model.action.Action;
import android.util.Log;


public class Sensor {
	private static final String TAG = "Sensor";
	protected String mTopicIn;
	protected String mValue="";
	protected Cartao mCard;
	protected String mUnit;
	protected String mName;
	protected int mQos;
	protected Mqtt mMqtt;
	protected int mId;
	protected List<Action> mActions;
	protected boolean mValueVisible=true; 
	protected String mImageUrl;
	static int mIdCount=0;

	public Sensor() {
		super();
		mId=mIdCount++;
		Log.d(TAG,"novo sensor: "+this.getClass().getSimpleName()+" "+mId);
	    mActions= new ArrayList<Action>();
	}
	
	public void set(Cartao card, Mqtt mqtt, String topicIn, int qos, String name, String unit, String imageUrl) {
		this.mCard=card;
		this.mMqtt=mqtt;
		this.mTopicIn = topicIn;
		this.mUnit = unit;
		this.mName = name;
		this.mValue="";
		this.mValueVisible=true;
		this.mQos=qos;
		this.mImageUrl=imageUrl;
	}

	
	public  String getDescricao() {
		return "Normal";
	}
	
	public void addAction(Action action) {
		action.setSensor(this);
		mActions.add(action);
	}
	
	
	public List<Action> getActions() {
		return mActions;
	}


	public String getTopicIn() {
		return mTopicIn;
	}
	

	public void setTopicIn(String topicIn) {
		this.mTopicIn = topicIn;
	}
	
	public String getValue() {
		return mValue;
	}
	
//	public void setValue(String value) {
//		entity.getModel().publish(this, value);
//	}

	public String getName() {
		return mName;
	}

	public void setName(String name) {
		this.mName = name;
	}

	public String getUnit() {
		return mUnit;
	}

	public void setUnit(String unit) {
		this.mUnit = unit;
	}

	public Mqtt getMqtt() {
		return mMqtt;
	}

	public void setMqtt(Mqtt mqtt) {
		this.mMqtt = mqtt;
	}


	
	public int getQos() {
		return mQos;
	}

	public void setQos(int qos) {
		this.mQos = qos;
	}

	public void updateValue(String value) {
		String old=this.mValue;
		this.mValue=value;
		mCard.getModel().getBus().sensorChange(this,old,value);
	}

	public boolean isValueVisible() {
		return mValueVisible;
	}

	public void setValueVisible(boolean valueVisible) {
		this.mValueVisible = valueVisible;
	}

	public String getImageUrl() {
		return mImageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.mImageUrl = imageUrl;
	}

	public int getId() {
		return mId;
	}

	public void removeAction(Action action) {
		mActions.remove(action);
	}

	public Cartao getCartao() {
		return mCard;
	}

	public Action getAction(int mActionId) {
		for (int i=0; i<mActions.size();i++) {
			if (mActionId==mActions.get(i).getId()) {
				return mActions.get(i);
			}
		}
		return null;
	}

	public void setCard(Cartao card) {
		mCard=card;
	}	
	
	
	
	
	
	
}
