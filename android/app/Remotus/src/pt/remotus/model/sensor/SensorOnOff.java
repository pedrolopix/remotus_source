package pt.remotus.model.sensor;

import pt.remotus.model.Cartao;
import pt.remotus.model.Mqtt;

public class SensorOnOff extends Sensor {
	private String on;
	private String off;
	private String imageOffUrl;
	
	
	public SensorOnOff() {
		super();
	}

	public void set(Cartao entity, Mqtt mqtt, String topicIn, int qos,
			String name, String on, String off, String imageOnUrl, String imageOffUrl) {
		super.set(entity, mqtt, topicIn, qos, name, "", imageOnUrl);
		this.on=on;
		this.off=off;
		this.imageOffUrl=imageOffUrl;
	}


	@Override
	public String getDescricao() {
		return "on/off";
	}
	

	public String getOn() {
		return on;
	}


	public void setOn(String on) {
		this.on = on;
	}


	public String getOff() {
		return off;
	}


	public void setOff(String off) {
		this.off = off;
	}

	public boolean isOn(){
		return !getValue().equals("0");
	}
	

	public String getImageOffUrl() {
		return imageOffUrl;
	}


	public void setImageOffUrl(String imageOffUrl) {
		this.imageOffUrl = imageOffUrl;
	}
	
}
