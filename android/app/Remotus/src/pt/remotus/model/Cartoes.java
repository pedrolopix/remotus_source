package pt.remotus.model;

import java.util.ArrayList;
import java.util.List;

public class Cartoes {
	private List<Cartao> mCartoes;
	private String mTopic;
	private String mNome;
	private String mId;
	private Model mModel;
	
	public Cartoes(Model model) {
		super();
		mModel=model;
		mTopic="";
		mNome="";
		mId="";
		mCartoes= new ArrayList<Cartao>();
	}

	public List<Cartao> getCartoes() {
		return mCartoes;
	}
	
	public String getTopic() {
		return mTopic;
	}
	public void setTopic(String topic) {
		this.mTopic = topic;
	}
	public String getNome() {
		return mNome;
	}
	public void setNome(String nome) {
		this.mNome = nome;
	}
	
	public String getId() {
		return mId;
	}

	public void setId(String id) {
		this.mId = id;
	}

	public void add(Cartao cartao) {
		cartao.setCards(this);
		mCartoes.add(cartao);
	}

	public void remove(Cartao cartao) {
		mCartoes.remove(cartao);
		
	}

	public Cartao getCartao(int id) {
		for (Cartao item : mCartoes) {
			if (item.getId()==id) return item;
		}
		return null;
	}

	public int GetQos() {
		return 0;
	}

	public boolean isRetained() {
		return true;
	}

	public void clear() {
		mCartoes.clear();
	}

	public Model getModel() {
		return mModel;
	}
	
}
