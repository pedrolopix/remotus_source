package pt.remotus.model.notification;

import java.util.ArrayList;
import java.util.List;

import pt.remotus.model.Model;

public class Notifications {
	
	private static final String TAG = "Notification";
	private List<Notification> mNotifications;
	private Model mModel;
	
	public Notifications(Model model) {
		super();
		this.mModel = model;
		this.mNotifications = new ArrayList<Notification>();
	}

	public List<Notification> getNotifications() {
		return mNotifications;
	}
	
	public void addNotification(Notification notification) {
		this.mNotifications.add(notification);
	}
	
	public void clearNotifications(){
		if (mNotifications == null) return;
		mNotifications.clear();
	}
	
	public Model getModel(){
		return this.mModel;
	}
	
}
