package pt.remotus.model.notification;



public class Notification {
	

	private int mId;
	static int mIdCount=0;
	private String mTopic;
	private int mQos;
	private String mValue;
	private Notifications mNotifications;
	
	public Notification() {
		super();
		mId=mIdCount++;
		mQos=0;
		mValue="";
	}
	
	public void set(Notifications notifications, String topic){
		this.mNotifications = notifications;
		this.mTopic=topic;
	}
	
	public void set(String topic){
		this.mTopic=topic;
	}
	
	public void set(Notifications notifications){
		this.mNotifications = notifications;
	}
	
	public String getTopic() {
		return mTopic;
	}

	public int getQos() {
		return mQos;
	}
	
	public String getValue() {
		return mValue;
	}

	public void updateValue(String value) {
		String old=this.mValue;
		this.mValue=value;
		mNotifications.getModel().getBus().notificationChange(this,old,value);
	}
	
}
