package pt.remotus.model;


import android.util.Log;

public class RemotusLog {

	public void i(String tag, String msg) {
		Log.i(tag,msg);
	}

	public void e(String tag, String msg, Throwable e) {
		Log.e(tag,msg,e);
	}

	public void e(String tag, Throwable e) {
		e.printStackTrace();
		//Log.e(tag,e.toString(),e);
	}

	public void d(String tag, String msg) {
		Log.d(tag,msg);
	}

}
