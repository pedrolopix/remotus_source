package pt.remotus.model.action;

import java.util.ArrayList;
import java.util.List;

import pt.remotus.model.Mqtt;
import pt.remotus.model.sensor.Sensor;


public class ActionEnum extends Action {
	
	public class Item{
	  String name;
	  String imageUrl;
	  String value;
	  boolean delete;
	  
	  
	  public Item(String name, String imageUrl, String value){
		  this.name=name;
		  this.imageUrl=imageUrl;
		  this.value=value;
		  this.delete = false;
	  }
	  
	  public String getName(){
		 return name;
	  }
	  
	  public String getValue(){
			 return value;
		  }
	  
	  public String getImageUrl(){
			 return imageUrl;
		  }
	  
	  public void setDelete(){
		  this.delete = true;
	  }
	  
	  public boolean isDelete(){
		  return this.delete;
	  }
	  
	  public void setName(String name){
		  this.name=name;
	  }
	  
	  public void setImageUrl(String imageUrl){
		  this.imageUrl = imageUrl;
	  }
	  
	  public void setValue(String value){
		  this.value=value;
	  }
	  
	}
	
	
	private List<Item> mValues;
	private List<Item> mValuesOriginal;
	private boolean mShowGrid;
	
	public ActionEnum() {
		super();
		mValues = new ArrayList<Item>();
	}

	@Override
	public ActionEnum set(Sensor sensor, Mqtt mqtt, String topicIn, int qosIn,
			String topicOut, int qosOut, boolean retained, String name,
			String unit) {
		super.set(sensor, mqtt, topicIn, qosIn, topicOut, qosOut, retained, name, unit);
		this.mShowGrid = false;
		return this;
	}
	
	@Override
	public  String getDescricao() {
		return "Enum";
	}

	public int getCount(){
		return mValues.size();
	}
	
	public Item getActionOfList(int position){
		return mValues.get(position);
	}
	
	public List<Item> getActionList(){
		return mValues;
	}
	
	public void addItem(String name, String imageUrl, String value){
		Item i = new Item(name, imageUrl, value);
		mValues.add(i);
	}
	
	
	public void removeItem(int position){
		mValues.remove(position);
	}
	
	public void setShowGrid(boolean value){
		this.mShowGrid = value;
	}
	 
	public boolean isShowGrid(){
		return mShowGrid;
	}

	public void startEdit() {
		mValuesOriginal = new ArrayList<Item>();
		for(Item i: mValues){
			mValuesOriginal.add(new Item(i.getName(), i.getImageUrl(), i.getValue()));
		}
	}
	
	public void finnishEdit(){
		if (mValues!=null)
			if (mValuesOriginal!=null)
				mValuesOriginal.clear();
	}
	
	public void cancelEdit(){
		mValues = mValuesOriginal;
	}
	
}
