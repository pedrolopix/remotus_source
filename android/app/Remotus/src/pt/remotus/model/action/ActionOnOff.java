package pt.remotus.model.action;

import pt.remotus.model.Mqtt;
import pt.remotus.model.sensor.Sensor;

public class ActionOnOff extends Action {
	private String on;
	private String off;
	
	public ActionOnOff() {
		super();
	}
	
	public ActionOnOff set(Sensor sensor, Mqtt mqtt, String topicIn, int qosIn,
			String topicOut, int qosOut, boolean retained, String name, String on, String off) {
		set(sensor, mqtt, topicIn, qosIn, topicOut, qosOut, retained, name, "");
		this.on=on;
		this.off=off;
		return this;
	}


	public String getOn() {
		return on;
	}


	public void setOn(String on) {
		this.on = on;
	}


	public String getOff() {
		return off;
	}


	public void setOff(String off) {
		this.off = off;
	}

	public boolean isOn(){
		return !getValue().equals("0");
	}
	
	public void toggle() {
		if (isOn()) {
			setValue("0");
		} else {
			setValue("1");
		}
		
	}

	@Override
	public  String getDescricao() {
		return "on/off";
	}
	
}
