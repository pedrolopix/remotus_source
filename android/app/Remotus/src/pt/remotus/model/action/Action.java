package pt.remotus.model.action;

import pt.remotus.model.Mqtt;
import pt.remotus.model.sensor.Sensor;

public class Action extends Sensor {
	private boolean mRetained;
	private String mTopicOut;	
	private int mQosOut;
	private Sensor mSensor;
	
	public Action() {
		super();
	}

	public Action set(Sensor sensor, Mqtt mqtt, String topicIn, int qosIn, String topicOut, int qosOut, boolean retained, String name,
			String unit) {
		set(sensor.getCartao(), mqtt, topicIn, qosIn, name, unit, "");
		this.mSensor=sensor;
		this.mTopicOut=topicOut;
		this.mQosOut=qosOut;
		this.mRetained=retained;
		return this;
	}
	

	public void setSensor(Sensor sensor) {
		mSensor=sensor;
		setCard(sensor.getCartao());
	}
	
	public boolean isRetained() {
		return mRetained;
	}


	public void setRetained(boolean retained) {
		this.mRetained = retained;
	}


	public String getTopicOut() {
		if (mTopicOut==null || mTopicOut.isEmpty()) return mTopicIn;
		return mTopicOut;
	}


	public void setTopicOut(String topicOut) {
		this.mTopicOut = topicOut;
	}


	public int getQosOut() {
		return mQosOut;
	}


	public void setQosOut(int qosOut) {
		this.mQosOut = qosOut;
	}

	public void setValue(String value) {
		mCard.getModel().publish(this, value);
	}


	public Sensor getSensor() {
		return mSensor;
	}
	
	

}
