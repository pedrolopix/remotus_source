package pt.remotus.model.action;

import pt.remotus.model.Mqtt;
import pt.remotus.model.sensor.Sensor;


public class ActionSlider extends Action {

	private int mMinValue;
	private int mMaxValue;
	private int mIncrementalValue;
	private boolean mTriggerUp;
	
	public ActionSlider() {
		super();
		mMinValue = 0;
		mMaxValue = 100;
		mIncrementalValue = 1;
		mTriggerUp = false;
	}

	@Override
	public ActionSlider set(Sensor sensor, Mqtt mqtt, String topicIn, int qosIn,
			String topicOut, int qosOut, boolean retained, String name,
			String unit) {
		super.set(sensor, mqtt, topicIn, qosIn, topicOut, qosOut, retained, name, unit);
		return this;
	}
	
	@Override
	public  String getDescricao() {
		return "Slider";
	}
	
	public int getMin() {
		return mMinValue;
	}

	public void setMin(int value) {
		this.mMinValue = value;
	}

	public int getMax() {
		return mMaxValue;
	}

	public void setMax(int value) {
		this.mMaxValue = value;
	}
	
	public int getIncremental() {
		return mIncrementalValue;
	}

	public void setIncremental(int value) {
		this.mIncrementalValue = value;
	}
	
	public boolean isTriggerUp() {
		return mTriggerUp;
	}

	public void setTriggerUp(boolean value) {
		this.mTriggerUp = value;
	}
}
