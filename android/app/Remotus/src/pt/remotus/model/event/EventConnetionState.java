package pt.remotus.model.event;

import pt.remotus.service.RemotusService.ConnectionStatus;

public class EventConnetionState extends Event {

	private ConnectionStatus status;

	public EventConnetionState(ConnectionStatus status) {
		this.status=status;
	}

	public ConnectionStatus getStatus() {
		return status;
	}
	
	

}
