package pt.remotus.model.event;

import pt.remotus.model.notification.Notification;

public class EventNotificationChange {
	private Notification notification;
	private String oldValue;
	private String newValue;
	
	
	public EventNotificationChange(Notification notification, String oldValue, String newValue) {
		this.notification=notification;
		this.oldValue=oldValue;
		this.newValue=newValue;
	}


	public Notification getNotification() {
		return notification;
	}


	public void setSensor(Notification notification) {
		this.notification = notification;
	}


	public String getOldValue() {
		return oldValue;
	}


	public void setOldValue(String oldValue) {
		this.oldValue = oldValue;
	}


	public String getNewValue() {
		return newValue;
	}


	public void setNewValue(String newValue) {
		this.newValue = newValue;
	}

	
	
}
