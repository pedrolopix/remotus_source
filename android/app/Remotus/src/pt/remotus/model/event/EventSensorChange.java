package pt.remotus.model.event;

import pt.remotus.model.sensor.Sensor;

public class EventSensorChange {
	private Sensor sensor;
	private String oldValue;
	private String newValue;
	
	
	public EventSensorChange(Sensor sensor, String oldValue, String newValue) {
		this.sensor=sensor;
		this.oldValue=oldValue;
		this.newValue=newValue;
	}


	public Sensor getSensor() {
		return sensor;
	}


	public void setSensor(Sensor sensor) {
		this.sensor = sensor;
	}


	public String getOldValue() {
		return oldValue;
	}


	public void setOldValue(String oldValue) {
		this.oldValue = oldValue;
	}


	public String getNewValue() {
		return newValue;
	}


	public void setNewValue(String newValue) {
		this.newValue = newValue;
	}

	
	
}
