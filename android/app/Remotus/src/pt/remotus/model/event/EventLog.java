package pt.remotus.model.event;


public class EventLog extends Event{
	private String tag;
	private String log;

	public EventLog(String tag, String log) {
		super();
		this.tag = tag;
		this.log=log;
	}

	public String getLog() {
		return log;
	}

	public void setLog(String log) {
		this.log = log;
	}

	@Override
	public String toString() {
		return "["+tag+"] "+log;
	}
}
