package pt.remotus.model.event;

public class EventMqttConnected {

	private boolean connected;

	public EventMqttConnected(boolean connected) {
		this.setConnected(connected);
	}

	public boolean isConnected() {
		return connected;
	}

	public void setConnected(boolean connected) {
		this.connected = connected;
	}

}
