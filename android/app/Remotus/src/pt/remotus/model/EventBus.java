package pt.remotus.model;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttTopic;

import pt.remotus.model.event.EventMqttConnected;
import pt.remotus.model.event.EventMqttConnectionLost;
import pt.remotus.model.event.EventMqttMessageArrived;
import pt.remotus.model.event.EventNotificationChange;
import pt.remotus.model.event.EventSensorChange;
import pt.remotus.model.notification.Notification;
import pt.remotus.model.sensor.Sensor;
import android.os.Handler;

import com.squareup.otto.Bus;


public class EventBus extends Bus {
	private Handler handler= new Handler();

	public void postAsync(final Object obj) {
		handler.post(new Runnable() {
			@Override
			public void run() {
				post(obj);
			}
		});
	}
	
	public void mqttMessageArrived(final String topic, final MqttMessage message) {
		postAsync(new EventMqttMessageArrived(topic,message));
	}

	public void mqttDeliveryComplete(IMqttDeliveryToken token) {
		// TODO Auto-generated method stub
		
	}

	public void mqttConnectionLost(Throwable throwable) {
		postAsync(new EventMqttConnectionLost(throwable));
		
	}

	public void sensorChange(Sensor sensor, String oldValue, String newValue) {
		postAsync(new EventSensorChange(sensor,oldValue,newValue));
	}
	
	public void notificationChange(Notification notification, String oldValue, String newValue) {
		postAsync(new EventNotificationChange(notification,oldValue,newValue));
	}

	@Override
	public void unregister(final Object obj) {
		handler.post(new Runnable() {
			@Override
			public void run() {
				EventBus.super.unregister(obj);
			}
		});
		
	}

	public void mqttConnected(boolean connected) {
		postAsync(new EventMqttConnected(connected));
	}

}
