
package pt.remotus.widget;

import org.holoeverywhere.FontLoader;
import pt.remotus.R;
import android.view.View;
import android.view.ViewGroup;

public class NavMenuItem {
	public CharSequence label;
    public View lastView;
    public boolean longClickable = false;
    public int selectionHandlerColor = -1;
    public boolean selectionHandlerVisible = false;

	public NavMenuItem() {

    }

    public int getItemViewType() {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
		NavMenuListRowView view = makeView(convertView, parent);
        view.setLabel(label);
        view.setSelectionHandlerVisiblity(selectionHandlerVisible);
        if (selectionHandlerVisible) {
            if (selectionHandlerColor > 0) {
                view.setSelectionHandlerColor(selectionHandlerColor);
            } else {
                view.setSelectionHandlerColorResource(R.color.transparent);
            }
        }
        return view;
    }

	protected NavMenuListRowView makeView(View convertView, ViewGroup parent) {
        if (convertView == null) {
			return FontLoader.applyDefaultFont(new NavMenuListRowView(parent.getContext()));
        } else {
			return (NavMenuListRowView) convertView;
        }
    }

    public void onClick() {

    }

    public boolean onLongClick() {
        return false;
    }
}
