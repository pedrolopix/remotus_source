package pt.remotus.adapter;

import java.util.List;

import pt.remotus.R;
import pt.remotus.dialogs.AddActionEnumItem;
import pt.remotus.model.action.ActionEnum.Item;
import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

public class EnumAdapter extends ArrayAdapter<Item> {

	private Context mContext;
	private ImageLoader mImageLoader;

	public EnumAdapter(Context context, List<Item> items, ImageLoader imageLoad) {
		super(context, R.layout.activity_add_action_enum_row_view, items);
		this.mContext = context;
		this.mImageLoader = imageLoad;
	}
	
	@Override
	public int getCount() {
		return super.getCount();
	}

	@Override
	public Item getItem(int position) {
		return super.getItem(position);
	}
	
	private EnumAdapter getAdapter(){
		return this;
	}

	@Override
    public View getView(int position, View convertView, ViewGroup parent) {
		View rowView = convertView;
		final Item it = getItem(position);    
		ViewHolder viewHolder;
       if (rowView == null) {
    	   viewHolder = new ViewHolder();
    	   LayoutInflater inflater = LayoutInflater.from(getContext());
    	   rowView = inflater.inflate(R.layout.activity_add_action_enum_row_view, null);
           viewHolder.name = (TextView) rowView.findViewById(R.id.tvItemRowNome);
			viewHolder.image = (NetworkImageView) rowView.findViewById(R.id.imageItemRowView);
			viewHolder.value = (TextView) rowView.findViewById(R.id.tvItemRowValor);
			viewHolder.delete = (ImageButton) rowView.findViewById(R.id.imageBtItemDelete);
			viewHolder.edit = (ImageButton) rowView.findViewById(R.id.imageBtItemEdit);
			rowView.setTag(viewHolder);
       } else {
           viewHolder = (ViewHolder) convertView.getTag();
       }
      
       viewHolder.name.setText(it.getName());
       viewHolder.value.setText(it.getValue());
       loadImage(viewHolder, it.getImageUrl());
       
       viewHolder.delete.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				remove(it);
				notifyDataSetChanged();
			}
		});
       
       viewHolder.edit.setOnClickListener(new AddActionEnumItem(mContext, it, getAdapter(), mImageLoader));
       
       return rowView;
   }
	
	private void loadImage(ViewHolder s, String url) {
		//s.image.setImageURI(null);
		s.image.setImageUrl(null, mImageLoader);
		if (mImageLoader==null) return;
		if (url.toString().isEmpty()) return;
		if (Uri.parse(url.toString()).getHost()==null) return;
		s.image.setImageUrl(url.toString(), mImageLoader);
	}

	class ViewHolder {
		public TextView name;
		public NetworkImageView image;
		public TextView value;
		public ImageButton delete;
		public ImageButton edit;
	}

}
