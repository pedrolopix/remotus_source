package pt.remotus.adapter;

import java.util.List;

import pt.remotus.R;
import pt.remotus.model.action.ActionEnum;
import pt.remotus.model.action.ActionEnum.Item;
import android.app.Activity;
import android.graphics.Typeface;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

public class EnumMainAdapter extends ArrayAdapter<Item> {

	private ImageLoader mImageLoader;
	private ActionEnum mAction;
	
	public EnumMainAdapter(Activity context, List<Item> items, ImageLoader imageLoad, ActionEnum mAction) {
		super(context, R.layout.card_sensor, items);
		this.mImageLoader = imageLoad;
		this.mAction = mAction;
	}
	
	@Override
	public int getCount() {
		return super.getCount();
	}

	@Override
	public Item getItem(int position) {
		return super.getItem(position);
	}

	@Override
    public View getView(int position, View convertView, ViewGroup parent) {
		View rowView = convertView;
		final Item it = getItem(position);    
		ViewHolder viewHolder = null;
       if (rowView == null) {
    	   viewHolder = new ViewHolder();
    	   LayoutInflater inflater = LayoutInflater.from(getContext());
    	   rowView = inflater.inflate(R.layout.card_sensor, null);
			
			viewHolder.image=(NetworkImageView) rowView.findViewById(R.id.image);
			viewHolder.name = ((TextView) rowView.findViewById(R.id.tvinfo));
			viewHolder.value= ((TextView) rowView.findViewById(R.id.tvValue));
			
			rowView.setTag(viewHolder);
       } else {
           viewHolder = (ViewHolder) convertView.getTag();
       }
      
       viewHolder.name.setText(it.getName());
       if (mAction.getValue().equals(it.getValue()))
    	   viewHolder.name.setTypeface(null, Typeface.BOLD);
       else
    	   viewHolder.name.setTypeface(null, Typeface.NORMAL);
       
       loadImage(viewHolder, it.getImageUrl());
       viewHolder.value.setVisibility(View.GONE);
       
    
      
       return rowView;
   }
	
	private void loadImage(ViewHolder s, String url) {
		s.image.setImageUrl(null, mImageLoader);
		if (mImageLoader==null) return;
		if (url.toString().isEmpty()) return;
		if (Uri.parse(url.toString()).getHost()==null) return;
		s.image.setImageUrl(url.toString(), mImageLoader);
	}

	class ViewHolder {
		public TextView name;
		public NetworkImageView image;
		public TextView value;
	}

}
