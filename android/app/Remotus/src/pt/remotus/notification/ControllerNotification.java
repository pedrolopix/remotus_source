package pt.remotus.notification;

import pt.remotus.model.Model;
import pt.remotus.model.event.EventNotificationChange;
import pt.remotus.model.notification.Notification;
import android.content.Context;

import com.squareup.otto.Subscribe;

public class ControllerNotification {
	private Model mModel;
	protected boolean connected;
	private Context mContext;
	private NotificationUI mNotificationUI;
	
	private class Subscribers {

		public Subscribers() {
			super();
			mModel.getBus().register(this);
		}

		@Subscribe
		public void onNotificationChange(EventNotificationChange event) {
			update(event.getNotification());
		}

	};
	
	public ControllerNotification(Model model, Context context) {
		super();
		this.mModel=model;
		this.mContext=context;
		connected=model.isStarted();
		mNotificationUI = new NotificationUI(mContext, "Remotus");
		new Subscribers();
	}
	
	 private void update(Notification notification) {
		mNotificationUI.show(notification.getTopic(), notification.getValue());
	}
	
	public void clearNotificafions(){
		mNotificationUI = new NotificationUI(mContext, "Remotus");
	}
	
	
	
	
}
