package pt.remotus.notification;

import pt.remotus.MainActivity;
import pt.remotus.R;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

public class NotificationUI {
	private static final int ID = 100;
	private Context mContext;
	private String mTitle;
	private String mTopic;
	private String mValue;
	
	private int mId;
	static int mIdCount=0;
	private int mCountNotification;
	private NotificationCompat.InboxStyle mInboxStyle;
	private NotificationCompat.Builder mBuilder;
	private NotificationManager mNotificationManager;
	
	public NotificationUI(Context context, String title) {
		super();
		mId=mIdCount++;
		this.mContext = context;
		this.mTitle = title;
		this.mCountNotification=0;
		init();
	}

	private void init(){  
		mBuilder = new NotificationCompat.Builder(mContext);      
		mBuilder.setSmallIcon(R.drawable.ic_launcher);
		mBuilder.setContentTitle(mTitle);
		mBuilder.setContentText(mTopic + " | " + mValue);
		//mBuilder.setContentInfo("Info");
		Intent resultIntent = new Intent(mContext, MainActivity.class);
		resultIntent.putExtra("notificationId", mId);

		TaskStackBuilder stackBuilder = TaskStackBuilder.create(mContext);
		stackBuilder.addParentStack(MainActivity.class);
		stackBuilder.addNextIntent(resultIntent);
		PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,
				PendingIntent.FLAG_UPDATE_CURRENT);
		mBuilder.setContentIntent(resultPendingIntent);
		
		mInboxStyle = new NotificationCompat.InboxStyle();
		mInboxStyle.setBigContentTitle(mTitle);
		mBuilder.setStyle(mInboxStyle);
		
		mBuilder.setAutoCancel(true);
		mBuilder.setDefaults(Notification.DEFAULT_ALL);		
		mNotificationManager =  (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
	}

	public void show(String topic, String value) {
		mInboxStyle.setSummaryText(++mCountNotification + " alerta(s)");
		mInboxStyle.addLine(topic + " | " + value);
		mNotificationManager.notify(mId, mBuilder.build());
	}
	
}
