package pt.remotus.service;

import org.holoeverywhere.preference.PreferenceManager;
import org.holoeverywhere.preference.SharedPreferences;
import org.holoeverywhere.preference.SharedPreferences.OnSharedPreferenceChangeListener;
import org.holoeverywhere.widget.Toast;

import pt.remotus.R;
import pt.remotus.model.Model;
import pt.remotus.model.event.EventConnetionState;
import pt.remotus.model.event.EventMqttConnected;
import pt.remotus.model.event.EventMqttConnectionLost;
import pt.remotus.notification.ControllerNotification;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Binder;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.Log;

import com.squareup.otto.Produce;
import com.squareup.otto.Subscribe;

public class RemotusService extends Service implements OnSharedPreferenceChangeListener {

	 public enum ConnectionStatus {
        INITIAL,                            // initial status
        CONNECTING,                         // attempting to connect
        CONNECTED,                          // connected
        NOTCONNECTED_WAITINGFORINTERNET,    // can't connect because the phone does not have Internet access
        NOTCONNECTED_USERDISCONNECT,        // user has explicitly requested disconnection
        NOTCONNECTED_DATADISABLED,          // can't connect because the use  has disabled data access
        NOTCONNECTED_UNKNOWNREASON,         // failed to connect for some reason
        NOTCONNECTED_MQTTERROR              // erro a ligar ao mqtt server
    }

	/************************************************************************/
    /*    Constantes                                                        */
    /************************************************************************/


	private static final String TAG = "RemotusService";


	/************************************************************************/
    /*    VARIABLES used to maintain state                                  */
    /************************************************************************/

    // status of client connection
    private ConnectionStatus connectionStatus = ConnectionStatus.INITIAL;
	private NetworkConnectionIntentReceiver netConnReceiver;
	private BackgroundDataChangeIntentReceiver dataEnabledReceiver;
	private ControllerNotification mControllerNot;
	
	/************************************************************************/
	/*    METHODS - core Service lifecycle methods                          */
    /************************************************************************/

    // see http://developer.android.com/guide/topics/fundamentals.html#lcycles

    @Override
    public void onCreate()
    {
        super.onCreate();
        
        // reset status variable to initial state
        connectionStatus = ConnectionStatus.INITIAL;

		// register to be notified whenever the user changes their preferences
        //  relating to background data use - so that we can respect the current
        //  preference
        dataEnabledReceiver = new BackgroundDataChangeIntentReceiver();
		registerReceiver(dataEnabledReceiver, new IntentFilter(ConnectivityManager.ACTION_BACKGROUND_DATA_SETTING_CHANGED));

        Context ctx = getApplicationContext();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        prefs.registerOnSharedPreferenceChangeListener(this);
        onSharedPreferenceChanged(prefs,null);
        Log.d(TAG,"Created");
		Model.getInstance().getBus().register(this);
		mControllerNot = new ControllerNotification(Model.getInstance(), ctx);
    }

    @Override
    public void onStart(final Intent intent, final int startId)
    {
        // This is the old onStart method that will be called on the pre-2.0
        // platform.  On 2.0 or later we override onStartCommand() so this
        // method will not be called.
    	handleStart(intent, startId);
    }


    @Override
    public int onStartCommand(final Intent intent, int flags, final int startId)
    {

        handleStart(intent, startId);


        // return START_NOT_STICKY - we want this Service to be left running
        //  unless explicitly stopped, and it's process is killed, we want it to
        //  be restarted
        return START_NOT_STICKY;
    }

    synchronized void handleStart(Intent intent, int startId)
    {
    	// before we start - check for a couple of reasons why we should stop
    	ConnectivityManager cm = (ConnectivityManager)getSystemService(CONNECTIVITY_SERVICE);
        if (cm.getBackgroundDataSetting() == false) // respect the user's request not to use data!
        {
            // user has disabled background data
            changeState(ConnectionStatus.NOTCONNECTED_DATADISABLED);
            // we have a listener running that will notify us when this
            //   preference changes, and will call handleStart again when it
            //   is - letting us pick up where we leave off now
            return;
        }

        connect();

        // changes to the phone's network - such as bouncing between WiFi
        //  and mobile data networks - can break the MQTT connection
        // the MQTT connectionLost can be a bit slow to notice, so we use
        //  Android's inbuilt notification system to be informed of
        //  network changes - so we can reconnect immediately, without
        //  haing to wait for the MQTT timeout
        if (netConnReceiver == null)
        {
            netConnReceiver = new NetworkConnectionIntentReceiver();
            registerReceiver(netConnReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

        }
        if (intent.getIntExtra("notificationId", -1)!=-1){
        	clearNotifications();
        }
    }

	public void disconnect() {
		connectionStatus = ConnectionStatus.INITIAL;
		Model.getInstance().stop();
	}

	public void connect() {
		Model model=Model.getInstance();
		if (model.isConnected()) {
			return;
		}
        if (!model.isStarted()) {
        	changeState(ConnectionStatus.CONNECTING);
        }
        if (isOnline()) {
	        model.start();
        } else {
        	changeState(ConnectionStatus.NOTCONNECTED_WAITINGFORINTERNET);
        }
	}

	
	@Produce 
	public EventConnetionState procedueEventConnetionState() {
		return new EventConnetionState(connectionStatus);
	}
	
	@Subscribe
	public void onMqttConnected(EventMqttConnected event) {
        if (event.isConnected()) {
			changeState(ConnectionStatus.CONNECTED);
			Context ctx = getApplicationContext();
			SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctx);
			String topic = prefs.getString(R.id.cfgCardTopic, "/remotus/cards/1");
			
			Model model = Model.getInstance();
			model.unsubscribe(model.getCartoes().getTopic());
			model.unSubscribeCards();
			model.getCartoes().setTopic(topic);
			model.subscribe(model.getCartoes().getTopic(), model.getCartoes().GetQos());
			
			// ##################
			String topicLog = prefs.getString(R.id.cfgTopicList, "/remotus/topics/1");
			model.unsubscribe(model.getTopicOfList());
			model.setTopicOfList(topicLog);
			model.subscribe(model.getTopicOfList(), 0);
			
        } else {
           changeState(ConnectionStatus.NOTCONNECTED_MQTTERROR);
           Toast.makeText(getApplicationContext(), R.string.msg_erro_a_ligar_ao_servidor_mqtt, Toast.LENGTH_SHORT).show();
        }
	}


    @Subscribe
    public void onMqttConnectionLost(EventMqttConnectionLost event) {
    	// we protect against the phone switching off while we're doing this
        //  by requesting a wake lock - we request the minimum possible wake
        //  lock - just enough to keep the CPU running until we've finished
        PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
        WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);
        wl.acquire();

        if (isOnline() == false)
        {
            changeState(ConnectionStatus.NOTCONNECTED_WAITINGFORINTERNET);
			Toast.makeText(getApplicationContext(), R.string.msg_ligacao_internet_indisponivel, Toast.LENGTH_SHORT).show();

        } else
        {
            //
            // we are still online
            //   the most likely reason for this connectionLost is that we've
            //   switched from wifi to cell, or vice versa
            //   so we try to reconnect immediately
            //
        	changeState(ConnectionStatus.NOTCONNECTED_UNKNOWNREASON);
            // try to reconnect
			disconnect();
        	connect();
        }

        // we're finished - if the phone is switched off, it's okay for the CPU
        //  to sleep now
        wl.release();
    }

    @Override
    public void onDestroy()
    {
    	Log.d(TAG,"onDestroy");
    	if (netConnReceiver != null)
        {
            unregisterReceiver(netConnReceiver);
        }
		if (dataEnabledReceiver != null) {
			unregisterReceiver(dataEnabledReceiver);
		}

    	Model.getInstance().getBus().unregister(this);
		disconnect();
    	super.onDestroy();
    }


    private void changeState(ConnectionStatus newState) {
		if (connectionStatus != newState) {
			connectionStatus = newState;
			Log.d(TAG, connectionStatus.toString());
			Model.getInstance().getBus().post(new EventConnetionState(connectionStatus));
		}
	}

	@Override
    public IBinder onBind(Intent intent)
    {
		return new LocalBinder(this);
    }





	@Override
	public boolean stopService(Intent name) {
		disconnect();
		return super.stopService(name);
	}


    private boolean isOnline()
    {
        ConnectivityManager cm = (ConnectivityManager)getSystemService(CONNECTIVITY_SERVICE);
        if(cm.getActiveNetworkInfo() != null &&
           cm.getActiveNetworkInfo().isAvailable() &&
           cm.getActiveNetworkInfo().isConnected())
        {
            return true;
        }

        return false;
    }

	public class LocalBinder extends Binder
    {

		private RemotusService mService;

		public LocalBinder(RemotusService service)
        {
			mService = service;
        }

		public RemotusService getService()
        {
			return mService;
        }
        public void close()
        {
            mService = null;
        }
    }


    private class BackgroundDataChangeIntentReceiver extends BroadcastReceiver
    {
        @Override
        public void onReceive(Context ctx, Intent intent)
        {
            // we protect against the phone switching off while we're doing this
            //  by requesting a wake lock - we request the minimum possible wake
            //  lock - just enough to keep the CPU running until we've finished
            PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
            WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);
            wl.acquire();

            ConnectivityManager cm = (ConnectivityManager)getSystemService(CONNECTIVITY_SERVICE);
            if (cm.getBackgroundDataSetting())
            {
                // user has allowed background data - we start again - picking
                // up where we left off in handleStart before
                handleStart(intent, 0);
            }
            else
            {
                // user has disabled background data
            	changeState(ConnectionStatus.NOTCONNECTED_DATADISABLED);
				disconnect();
           }

            // we're finished - if the phone is switched off, it's okay for the CPU
            // to sleep now
            wl.release();
        }
    }


    /*
     * Called in response to a change in network connection - after losing a
     *  connection to the server, this allows us to wait until we have a usable
     *  data connection again
     */
    private class NetworkConnectionIntentReceiver extends BroadcastReceiver
    {
        @Override
        public void onReceive(Context ctx, Intent intent)
        {
            // we protect against the phone switching off while we're doing this
            //  by requesting a wake lock - we request the minimum possible wake
            //  lock - just enough to keep the CPU running until we've finished
            PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
            WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MQTT");
            wl.acquire();

            if (isOnline())
            {
                // we have an internet connection - have another try at connecting
            	connect();
			} else {
				Toast.makeText(RemotusService.this, R.string.msg_ligacao_internet_indisponivel, Toast.LENGTH_SHORT).show();
            }

            // we're finished - if the phone is switched off, it's okay for the CPU
            //  to sleep now
            wl.release();
        }
    }

	@Override
	public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
		String host = prefs.getString(R.id.mqtthost, "192.168.10.3");
        String port=prefs.getString(R.id.mqttport,"1883");
        String userName=prefs.getString(R.id.mqttusername,"user");
        String passWord=prefs.getString(R.id.mqttpassword,"pass");

        Model model= Model.getInstance();
		disconnect();
        model.getMqtt().setHost("tcp://"+host+":"+port);
        model.getMqtt().setUserName(userName);
        model.getMqtt().setPassWord(passWord);

		connect();
	}
	
	public void clearNotifications(){
		mControllerNot.clearNotificafions();
	}
}
