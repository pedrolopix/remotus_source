package pt.remotus.programer;

import pt.remotus.programer.model.Model;
import android.app.Application;

public class RemotusApplicartion extends Application {
	@Override
	public void onCreate() {
		super.onCreate();

		Model.getInstance().load(this);
	}
}
