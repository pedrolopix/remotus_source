package pt.remotus.programer.asynctask;

import org.eclipse.paho.client.mqttv3.*;

import pt.remotus.programer.R;
import pt.remotus.programer.model.Model;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;


public class MqttSubscribeTask extends AsyncTask<Void, Void, Integer> {
	private static final String TAG = "MqttSubscribeTask";
	Context mContext;
	ProgressDialog mPd;
	private boolean mRecebido;

	public MqttSubscribeTask(Context context, ProgressDialog pd) {
		mContext = context;
		mPd = pd;
		mRecebido = false;
	}

	private final MqttCallback mCallback = new MqttCallback() {
		@Override
		public void connectionLost(Throwable cause) {

		}

		@Override
		public void messageArrived(String topic, MqttMessage message) throws Exception {
			Model.getInstance().setJSON(mContext, message.toString());
			mRecebido = true;
		}

		@Override
		public void deliveryComplete(IMqttDeliveryToken token) {

		}


	};

	@Override
	protected Integer doInBackground(Void... v) {
		try {
			SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mContext);
			String mqtthost = preferences.getString("mqtt_host", "");
			String mqttport = preferences.getString("mqtt_port", "");
			String mqttusername = preferences.getString("mqtt_username", "");
			String mqttpassword = preferences.getString("mqtt_password", "");
			String mqtttopic = preferences.getString("mqtt_topic", "");

			MqttEngine mqtt = new MqttEngine();
			mqtt.setServerURI("tcp://" + mqtthost + ":" + mqttport);
			mqtt.setUserName(mqttusername);
			mqtt.setPassword(mqttpassword);
			mqtt.connect(mCallback);
			mqtt.subscribe(mqtttopic, 0);
			Thread.sleep(1000);
			mqtt.unsubscribe(mqtttopic);
			mqtt.disconnect();
			if (mRecebido) {
				return R.string.msg_mqtt_subscrive_ok;
			} else {
				return R.string.msg_mqtt_subscrive_error;
			}
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			return R.string.msg_mqtt_connection_error;
		}
	}

	@Override
	protected void onPostExecute(Integer result) {
		Toast.makeText(mContext, result, Toast.LENGTH_SHORT).show();
		mPd.dismiss();
	}
}