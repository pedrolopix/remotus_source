package pt.remotus.programer.asynctask;

import pt.remotus.programer.R;
import pt.remotus.programer.model.Model;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;


public class MqttPublishTask extends AsyncTask<Void, Void, Integer> {
	private static final String TAG = "MqttPublishTask";
	Context mContext;
	ProgressDialog mPd;

	public MqttPublishTask(Context context, ProgressDialog pd) {
		mContext = context;
		mPd = pd;
	}

	@Override
	protected Integer doInBackground(Void... v) {
		String message = Model.getInstance().getJSON();
		try {
			SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mContext);
			String mqtthost = preferences.getString("mqtt_host", "");
			String mqttport = preferences.getString("mqtt_port", "");
			String mqttusername = preferences.getString("mqtt_username", "");
			String mqttpassword = preferences.getString("mqtt_password", "");
			String mqtttopic = preferences.getString("mqtt_topic", "");

			MqttEngine mqtt = new MqttEngine();
			mqtt.setServerURI("tcp://" + mqtthost + ":" + mqttport);
			mqtt.setUserName(mqttusername);
			mqtt.setPassword(mqttpassword);
			mqtt.connect(null);
			if (!mqtt.isConnected()) {
				return R.string.msg_mqtt_connection_error;
			}
			;
			if (!mqtt.publish(mqtttopic, message, true, 0)) {
				return R.string.msg_mqtt_publish_error;
			}
			mqtt.disconnect();
			return R.string.msg_mqtt_publish_ok;
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			return R.string.msg_mqtt_connection_error;
		}
	}

	@Override
	protected void onPostExecute(Integer result) {
		Toast.makeText(mContext, result, Toast.LENGTH_SHORT).show();
		mPd.dismiss();
	}
}