package pt.remotus.programer.asynctask;

import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.eclipse.paho.client.mqttv3.MqttSecurityException;
import org.eclipse.paho.client.mqttv3.MqttTopic;
import android.util.Log;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;


public class MqttEngine {
	private static final String TAG = "MqttEngine";
	private MqttClient client;
	private String serverURI;
	private String clientId;
	private String userName;
	private String password;


	private boolean isConnected;


	public MqttEngine() {
	}

	public void unsubscribe(String topicFilter) {
		if (client==null) {
			return;
		}
		try {
			client.unsubscribe(topicFilter);
		} catch (MqttException e) {
			// o unsubscribe se der erro ?? porque est?? desligado...
		}
	}


	public boolean subscribe(String topicFilter, int qos) {
		if (client==null) {
			return false;
		}
		if (topicFilter==null) {
			return false;
		}
		try {
			client.subscribe(topicFilter, qos);
			Log.i(TAG, "subscribe " + topicFilter);
		} catch (MqttSecurityException e) {
			Log.e(TAG, e.getMessage());
			return false;
		} catch (MqttException e) {
			Log.e(TAG, e.getMessage());
			return false;
		}
		return true;
	}

	public void connect(MqttCallback callback) {
		try {
			Log.i(TAG, "ligar a " + getServerURI().toString());
			clientId = MqttClient.generateClientId(); //"remotus_android"; //
			client= new MqttClient(getServerURI(), clientId, new MemoryPersistence());
			MqttConnectOptions options= new MqttConnectOptions();
			options.setUserName(getUserName());
			options.setPassword(getPassword().toCharArray());
			options.setConnectionTimeout(100);
			if (callback != null) {
				client.setCallback(callback);
			}
			client.connect(options);
			Log.i(TAG, "ligado a " + getServerURI().toString());
			isConnected=true;
		} catch (Exception e) {
			isConnected=false;
			Log.e(TAG, e.getMessage());
		}
	}

	public void disconnect() {
		if (client!=null) {
			try {
				client.disconnect();
			} catch (MqttException e) {
				//model.getLog().e(TAG, e);
			}
		}
	}

	public void stop() {
		disconnect();
	}

	public String getServerURI() {
		return serverURI;
	}

	public void setServerURI(String serverURI) {
		this.serverURI = serverURI;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void publish(String topic, int value, boolean retained, int qos) {
		publish(topic, String.valueOf(value), retained, qos);
	}

	public boolean publish(final String topic, final String text, final boolean retained, final int qos) {
		if (client==null) {
			return false;
		}

		MqttTopic t = client.getTopic(topic);
		try {
			if (!client.isConnected()) {
				client.connect();
			}
			MqttMessage mqttMessage = new MqttMessage(text.getBytes());
			mqttMessage.setRetained(retained);
			mqttMessage.setQos(qos);
			t.publish(mqttMessage);
			return true;
		} catch (MqttPersistenceException e) {
			Log.e(TAG, e.getMessage());
			return false;
		} catch (MqttException e) {
			Log.e(TAG, e.getMessage());
			return false;
		}
	}


	public boolean isConnected() {
		return isConnected;
	}


	public void setConnected(boolean isConnected) {
		this.isConnected = isConnected;
	}




}
