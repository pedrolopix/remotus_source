package pt.remotus.programer.bus;

import android.os.Handler;
import com.squareup.otto.Bus;


public class EventBus extends Bus {
	private final Handler handler= new Handler();

	private static EventBus _instance;

	public static EventBus getInstance() {
		if (_instance == null) {
			_instance = new EventBus();
		}
		return _instance;
	}

	public void postAsync(final Object obj) {
		handler.post(new Runnable() {
			@Override
			public void run() {
				post(obj);
			}
		});
	}


	@Override
	public void unregister(final Object obj) {
		handler.post(new Runnable() {
			@Override
			public void run() {
				EventBus.super.unregister(obj);
			}
		});

	}


}
