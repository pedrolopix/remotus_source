package pt.remotus.programer;

import pt.remotus.programer.activities.EditActionActivity;
import pt.remotus.programer.activities.EditMqttMessageActivity;
import pt.remotus.programer.activities.EditTopicAliasActivity;
import pt.remotus.programer.activities.EditVariableActivity;
import pt.remotus.programer.activities.SettingsActivity;
import pt.remotus.programer.rules.EditRulesActivity;
import android.content.Context;
import android.content.Intent;

public class Dialogs {

	public static void editVariable(Context context, String mKey) {
		Intent intent = new Intent(context, EditVariableActivity.class);
		intent.putExtra(EditVariableActivity.TAG_ACTION, EditVariableActivity.TAG_ACTION_EDIT);
		intent.putExtra(EditVariableActivity.TAG_KEY, mKey);
		context.startActivity(intent);
	}

	public static void addVariable(Context context) {
		Intent intent = new Intent(context, EditVariableActivity.class);
		intent.putExtra(EditVariableActivity.TAG_ACTION, EditVariableActivity.TAG_ACTION_ADD);
		context.startActivity(intent);
	}

	public static void addTopicAlias(Context context) {
		Intent intent = new Intent(context, EditTopicAliasActivity.class);
		intent.putExtra(EditVariableActivity.TAG_ACTION, EditTopicAliasActivity.TAG_ACTION_ADD);
		context.startActivity(intent);

	}

	public static void editTopicAlias(Context context, String mKey) {
		Intent intent = new Intent(context, EditTopicAliasActivity.class);
		intent.putExtra(EditVariableActivity.TAG_ACTION, EditTopicAliasActivity.TAG_ACTION_EDIT);
		intent.putExtra(EditVariableActivity.TAG_KEY, mKey);
		context.startActivity(intent);
	}

	public static void editRule(Context context, int ruleID) {
		Intent intent = new Intent(context, EditRulesActivity.class);
		intent.putExtra(EditRulesActivity.TAG_ACTION, EditRulesActivity.TAG_ACTION_EDIT);
		intent.putExtra(EditRulesActivity.TAG_RULEID, ruleID);
		context.startActivity(intent);
	}

	public static void addRuleTopic(Context context) {
		Intent intent = new Intent(context, EditRulesActivity.class);
		intent.putExtra(EditRulesActivity.TAG_ACTION, EditRulesActivity.TAG_ADD_RULE_TOPIC);
		context.startActivity(intent);
	}

	public static void addRuleTimer(Context context) {
		Intent intent = new Intent(context, EditRulesActivity.class);
		intent.putExtra(EditRulesActivity.TAG_ACTION, EditRulesActivity.TAG_ADD_RULE_TIMER);
		context.startActivity(intent);

	}

	public static void editAction(Context context, int ruleId, int actionId) {
		Intent intent = new Intent(context, EditActionActivity.class);
		intent.putExtra(EditActionActivity.TAG_ACTION, EditActionActivity.TAG_ACTION_EDIT);
		intent.putExtra(EditActionActivity.TAG_RULE_ID, ruleId);
		intent.putExtra(EditActionActivity.TAG_ACTION_ID, actionId);
		context.startActivity(intent);
	}

	public static void addAction(Context context, int ruleId, int actionId) {
		Intent intent = new Intent(context, EditActionActivity.class);
		intent.putExtra(EditActionActivity.TAG_ACTION, EditActionActivity.TAG_ACTION_ADD);
		intent.putExtra(EditActionActivity.TAG_RULE_ID, ruleId);
		intent.putExtra(EditActionActivity.TAG_ACTION_ID, actionId);
		context.startActivity(intent);
	}

	public static void mqttStart(Context context) {
		Intent intent = new Intent(context, EditMqttMessageActivity.class);
		intent.putExtra(EditMqttMessageActivity.TAG_ACTION, EditMqttMessageActivity.TAG_START);
		context.startActivity(intent);
	}

	public static void mqttWill(Context context) {
		Intent intent = new Intent(context, EditMqttMessageActivity.class);
		intent.putExtra(EditMqttMessageActivity.TAG_ACTION, EditMqttMessageActivity.TAG_WILL);
		context.startActivity(intent);
	}

	public static void settings(Context context) {
		Intent intent = new Intent(context, SettingsActivity.class);
		context.startActivity(intent);
	}

}
