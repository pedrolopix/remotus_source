package pt.remotus.programer.mainfragments;

import pt.remotus.programer.R;
import pt.remotus.programer.Dialogs;
import pt.remotus.programer.FragmentHelperIntf;
import pt.remotus.programer.bus.EventBus;
import pt.remotus.programer.bus.EventRefresh;
import pt.remotus.programer.bus.EventVariable;
import pt.remotus.programer.model.Model;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;

import com.squareup.otto.Subscribe;


/**
 * Fragment that shows a list of variables.
 */
public class VariablesListFragment extends ListFragment implements OnItemLongClickListener, FragmentHelperIntf
{

	private String[] mValues;
	private String mKey;
	private ArrayAdapter<String> mAdapter;
	private ActionMode mActionMode;

	public VariablesListFragment()
    {
        // Obligatory empty constructor.
    }

	@Override
	public void onStart() {
		super.onStart();
		registerForContextMenu(getListView());
		setEmptyText(getResources().getString(R.string.variables_list_empty_text));
		getListView().setOnItemLongClickListener(this);
	}


	private void fillAdpater() {
		mValues = Model.getInstance().getRules().getVariables().variablesToStringArray();
		mAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, mValues);
		setListAdapter(mAdapter);
		mAdapter.notifyDataSetChanged();
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
		String item = mValues[position];
		String[] elemts = item.split("=");
		if (elemts.length == 0) {
			return false;
		}
		mKey = elemts[0].trim();

		// Start the CAB using the ActionMode.Callback defined above
		mActionMode = getActivity().startActionMode(mActionModeCallback);
		parent.requestFocusFromTouch(); // IMPORTANT!
		parent.setSelected(true);
		return true;
	}

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override
	public void onPause() {
		EventBus.getInstance().unregister(this);
		super.onPause();
	}

	@Override
	public void onResume() {
		EventBus.getInstance().register(this);
		fillAdpater();
		super.onResume();
	}

	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
			case R.id.action_add:
				Dialogs.addVariable(getActivity());
				return true;
    	}
    	return super.onOptionsItemSelected(item);
    }


	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.variables, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Subscribe
	public void doEventRefresh(EventRefresh event) {
		fillAdpater();
	}

	@Subscribe
	public void doEventVariable(EventVariable event) {
		fillAdpater();
	}

	private final ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

		// Called when the action mode is created; startActionMode() was called
		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			// Inflate a menu resource providing context menu items
			MenuInflater inflater = mode.getMenuInflater();
			// Assumes that you have "contexual.xml" menu resources
			inflater.inflate(R.menu.variable_edit_rowselection, menu);
			return true;
		}

		// Called each time the action mode is shown. Always called after
		// onCreateActionMode, but
		// may be called multiple times if the mode is invalidated.
		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			return false; // Return false if nothing is done
		}

		// Called when the user selects a contextual menu item
		@Override
		public boolean onActionItemClicked(final ActionMode mode, MenuItem item) {
			switch (item.getItemId()) {
				case R.id.action_delete:
					new AlertDialog.Builder(getActivity()).setTitle(R.string.msg_delete_variable).setMessage(String.format(getActivity().getString(R.string.msg_delete_variable_s), mKey))
							.setPositiveButton(R.string.action_ok, new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									Model.getInstance().deleteVariable(mKey);
									mode.finish();
								}
							}).setNegativeButton(R.string.action_cancel, new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									mode.finish();
								}
							}).show();

					return true;
				case R.id.action_edit:
					Dialogs.editVariable(getActivity(), mKey);
					mode.finish();
					return true;
				default:
					return false;
			}
		}

		// Called when the user exits the action mode
		@Override
		public void onDestroyActionMode(ActionMode mode) {
			mActionMode = null;
			mKey = null;
		}
	};

	@Override
	public void onTabUnselected() {
		if (mActionMode != null) {
			mActionMode.finish();
		}

	}

	@Override
	public boolean canExit() {
		return true;
	}


}
