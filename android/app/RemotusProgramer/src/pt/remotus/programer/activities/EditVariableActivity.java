package pt.remotus.programer.activities;

import pt.remotus.programer.R;
import pt.remotus.programer.model.Model;
import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.throrinstudio.android.common.libs.validator.Form;
import com.throrinstudio.android.common.libs.validator.Validate;
import com.throrinstudio.android.common.libs.validator.validator.NotEmptyValidator;

public class EditVariableActivity extends Activity {

	public static final String TAG_ACTION = "Action";
	public static final String TAG_ACTION_ADD = "add";
	public static final String TAG_ACTION_EDIT = "edit";
	public static final String TAG_KEY = "Key";
	private String mAction;
	private EditText edVarKey;
	private EditText edVarValue;
	private String mKey;
	private String mValue;
	private int mChangeRevision;
	private final Form mForm = new Form();


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		setContentView(R.layout.edit_variable);
		edVarKey = (EditText) findViewById(R.id.edVarKey);
		edVarValue = (EditText) findViewById(R.id.edVarValue);
		if (getIntent().getExtras()!=null) {
			mAction = getIntent().getExtras().getString(TAG_ACTION);
			mKey = getIntent().getExtras().getString(TAG_KEY);
			mValue = Model.getInstance().getRules().getVariables().getVariableValue(mKey);

			edVarKey.setText(mKey);
			edVarValue.setText(mValue);
		}

		Validate edVarKeyValidate = new Validate(edVarKey);
		edVarKeyValidate.addValidator(new NotEmptyValidator(this));
		mForm.addValidates(edVarKeyValidate);

		Validate edVarValueValidate = new Validate(edVarValue);
		edVarValueValidate.addValidator(new NotEmptyValidator(this));
		mForm.addValidates(edVarValueValidate);

		edVarKey.addTextChangedListener(textWatcher);
		edVarValue.addTextChangedListener(textWatcher);
	}

	private final TextWatcher textWatcher = new TextWatcher() {

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		}

		@Override
		public void afterTextChanged(Editable s) {
			mChangeRevision++;
		}
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.variable_edit, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				onBackPressed();
				return true;
			case R.id.action_save:
				save();
				break;
			case R.id.action_cancel:
				finish();
				break;
			default:
				break;
		}
		return super.onOptionsItemSelected(item);
	}

	private boolean save() {
		if (!mForm.validate()) {
			return false;
		}

		String key = edVarKey.getText().toString();
		String value = edVarValue.getText().toString();
		if (mAction.equals(TAG_ACTION_ADD)) {
			Model.getInstance().addVariable(key, value);
			finish();
		}
		if (mAction.equals(TAG_ACTION_EDIT)) {
			Model.getInstance().editVariable(mKey, key, value);
			finish();
		}

		Model.getInstance().save(this);

		return true;
	}

	@Override
	public void onBackPressed() {
		if (mChangeRevision == 0) {

		} else {
			if (!save()) {
				return;
			}
		}
		super.onBackPressed();
	}

}
