package pt.remotus.programer.activities;

import pt.remotus.programer.R;
import pt.remotus.programer.model.Model;
import android.app.ActionBar;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;

import com.throrinstudio.android.common.libs.validator.Form;
import com.throrinstudio.android.common.libs.validator.Validate;
import com.throrinstudio.android.common.libs.validator.validator.NotEmptyValidator;
import com.throrinstudio.android.common.libs.validator.validator.RegExpValidator;

public class EditMqttMessageActivity extends FragmentActivity {
	public static final String TAG_ACTION = "action";
	public static final int TAG_START = 1;
	public static final int TAG_WILL = 2;
	private EditText edTopic;
	private CheckBox chkRetainded;
	private EditText edValue;
	private EditText edQOS;
	private int mChangeRevision;
	private final Form mForm = new Form();
	private remotus.rules.MqttMyMessage mMqttMyMessage;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.edit_topic_message);
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		edTopic = (EditText) findViewById(R.id.edTopic);
		chkRetainded = (CheckBox) findViewById(R.id.chkRetainded);
		edValue = (EditText) findViewById(R.id.edValue);
		edQOS = (EditText) findViewById(R.id.edQOS);

		int a = getIntent().getIntExtra(TAG_ACTION, -1);
		switch (a) {
			case TAG_START:
				setTitle(R.string.action_mqtt_start);
				mMqttMyMessage = Model.getInstance().getMqttConfig().getStartMsg();
				break;
			case TAG_WILL:
				setTitle(R.string.action_mqtt_will);
				mMqttMyMessage = Model.getInstance().getMqttConfig().getWillMsg();
				break;

			default:
				break;
		}



		Validate edTopicValidate = new Validate(edTopic);
		edTopicValidate.addValidator(new NotEmptyValidator(this));

		Validate edValueValidate = new Validate(edValue);
		edValueValidate.addValidator(new NotEmptyValidator(this));

		Validate edQOSValidate = new Validate(edQOS);
		edQOSValidate.addValidator(new RegExpValidator(this, "[0|1|2]", R.string.qos_validor));

		mForm.addValidates(edTopicValidate);
		mForm.addValidates(edQOSValidate);
		mForm.addValidates(edValueValidate);

		fill();
		addListners();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.action_edit, menu);
		return true;
	}

	private void addListners() {
		edTopic.addTextChangedListener(textWatcher);
		chkRetainded.setOnCheckedChangeListener(checkedWatcher);
		edValue.addTextChangedListener(textWatcher);
		edQOS.addTextChangedListener(textWatcher);
	}

	private final TextWatcher textWatcher = new TextWatcher() {

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		}

		@Override
		public void afterTextChanged(Editable s) {
			mChangeRevision++;
		}
	};

	private final OnCheckedChangeListener checkedWatcher = new OnCheckedChangeListener() {

		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			mChangeRevision++;
		}
	};

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				onBackPressed();
				return true;
			case R.id.action_save:
				if (save()) {
					finish();
				}
				return true;
			case R.id.action_cancel:
				cancel();
				finish();
				return true;
			default:
				break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {
		if (mChangeRevision == 0) {
			cancel();
		} else {
			if (!save()) {
				return;
			}
		}
		super.onBackPressed();
	}

	private void fill() {
		edTopic.setText(mMqttMyMessage.topic);
		chkRetainded.setChecked(mMqttMyMessage.retained);
		edQOS.setText("" + mMqttMyMessage.qos);
		edValue.setText(mMqttMyMessage.message);
		mChangeRevision = 0;
	}

	private boolean save() {
		if (!mForm.validate()) {
			return false;
		}
		mMqttMyMessage.topic = edTopic.getText().toString();
		mMqttMyMessage.retained = chkRetainded.isChecked();
		try {
			mMqttMyMessage.qos = Integer.parseInt(edQOS.getText().toString());
		} catch (Exception e) {
			mMqttMyMessage.qos =0;
		}
		mMqttMyMessage.message = edValue.getText().toString();
		Model.getInstance().save(this);
		return true;
	}

	private void cancel() {
		finish();
	}

}
