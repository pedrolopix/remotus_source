package pt.remotus.programer.activities;

import pt.remotus.programer.R;
import pt.remotus.programer.bus.EventBus;
import pt.remotus.programer.bus.EventRuleAction;
import pt.remotus.programer.model.Model;
import pt.remotus.programer.rules.EditRulesActivity;
import remotus.rules.actions.Action;
import remotus.rules.actions.ActionBasePublish;
import remotus.rules.actions.ActionPublish;
import remotus.rules.conditions.Condition;
import android.app.ActionBar;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.throrinstudio.android.common.libs.validator.Form;
import com.throrinstudio.android.common.libs.validator.Validate;
import com.throrinstudio.android.common.libs.validator.validator.NotEmptyValidator;
import com.throrinstudio.android.common.libs.validator.validator.RegExpValidator;

public class EditActionActivity extends FragmentActivity {
	public static final String TAG_ACTION = "Action";
	public static final String TAG_RULE_ID = "RuleId";
	public static final String TAG_ACTION_ID = "ActionId";
	public static final int TAG_ACTION_EDIT = 1;
	public static final int TAG_ACTION_ADD = 2;
	private CheckBox chkActive;
	private EditText edName;
	private EditText edTopic;
	private CheckBox chkRetainded;
	private EditText edValue;
	private EditText edDelay;
	private EditText edQOS;
	private int mRuleId;
	private int mActionId;
	private int mAction;
	private Condition mRule;
	private Action mTheAction;
	private int mChangeRevision;
	private LinearLayout llValue;
	private final Form mForm = new Form();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.edit_action);
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		chkActive = (CheckBox) findViewById(R.id.chkActive);
		edName = (EditText) findViewById(R.id.edName);
		edTopic = (EditText) findViewById(R.id.edTopic);
		chkRetainded = (CheckBox) findViewById(R.id.chkRetainded);
		edValue = (EditText) findViewById(R.id.edValue);
		edDelay = (EditText) findViewById(R.id.edDelay);
		edQOS = (EditText) findViewById(R.id.edQOS);
		llValue = (LinearLayout) findViewById(R.id.llValue);

		if (getIntent() != null) {
			mRuleId = getIntent().getIntExtra(TAG_RULE_ID, -1);
			mActionId = getIntent().getIntExtra(TAG_ACTION_ID, -1);
			mAction = getIntent().getIntExtra(TAG_ACTION, -1);
			fill();
		}

		Validate edNameValidator = new Validate(edName);
		edNameValidator.addValidator(new NotEmptyValidator(this));
		Validate edTopicValidate = new Validate(edTopic);
		edTopicValidate.addValidator(new NotEmptyValidator(this));

		Validate edQOSValidate = new Validate(edQOS);
		edQOSValidate.addValidator(new RegExpValidator(this, "[0|1|2]", R.string.qos_validor));

		mForm.addValidates(edNameValidator);
		mForm.addValidates(edTopicValidate);
		mForm.addValidates(edQOSValidate);

		if (mTheAction instanceof ActionPublish) {
			Validate edValueValidate = new Validate(edValue);
			edValueValidate.addValidator(new NotEmptyValidator(this));
			mForm.addValidates(edValueValidate);
		}

		addListners();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.action_edit, menu);
		return true;
	}

	private void addListners() {
		chkActive.setOnCheckedChangeListener(checkedWatcher);
		edName.addTextChangedListener(textWatcher);
		edTopic.addTextChangedListener(textWatcher);
		chkRetainded.setOnCheckedChangeListener(checkedWatcher);
		edValue.addTextChangedListener(textWatcher);
		edDelay.addTextChangedListener(textWatcher);
		edQOS.addTextChangedListener(textWatcher);
	}

	private final OnCheckedChangeListener checkedWatcher = new OnCheckedChangeListener() {

		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			mChangeRevision++;
		}
	};

	private final TextWatcher textWatcher = new TextWatcher() {

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		}

		@Override
		public void afterTextChanged(Editable s) {
			mChangeRevision++;
		}
	};

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				onBackPressed();
				return true;
			case R.id.action_save:
				if (save()) {
					finish();
				}
				return true;
			case R.id.action_cancel:
				cancel();
				finish();
				return true;
			default:
				break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {
		if (mChangeRevision == 0) {
			cancel();
		} else {
			if (!save()) {
				return;
			}
		}
		super.onBackPressed();
	}

	private void fill() {
		mRule = Model.getInstance().getRules().getConditionId(mRuleId);
		if (mRule == null) {
			return;
		}
		mTheAction = mRule.getActionID(mActionId);
		if (mTheAction == null) {
			return;
		}
		chkActive.setChecked(mTheAction.isActive());
		edName.setText(mTheAction.getName());
		if (mTheAction instanceof ActionBasePublish) {
			ActionBasePublish a = (ActionBasePublish) mTheAction;
			edTopic.setText(a.getTopic());
			chkRetainded.setChecked(a.isRetained());
			edDelay.setText("" + a.getDelay());
			edQOS.setText("" + a.getQos());
		}
		if (mTheAction instanceof ActionPublish) {
			ActionPublish a = (ActionPublish) mTheAction;
			edValue.setText(a.getValue());
			llValue.setVisibility(View.VISIBLE);
		} else {
			llValue.setVisibility(View.GONE);
		}
		mChangeRevision = 0;
	}

	private boolean save() {
		if (mTheAction == null) {
			return false;
		}

		if (!mForm.validate()) {
			// Toast.makeText(this, "", Toast.LENGTH_LONG);
			return false;
		}
		mTheAction.setActive(chkActive.isChecked());
		mTheAction.setName(edName.getText().toString());
		if (mTheAction instanceof ActionBasePublish) {
			ActionBasePublish a = (ActionBasePublish) mTheAction;
			a.setTopic(edTopic.getText().toString());
			a.setRetained(chkRetainded.isChecked());
			try {
				a.setDelay(Integer.parseInt(edDelay.getText().toString()));
			} catch (Exception e) {
				a.setDelay(0);
			}
			try {
				a.setQos(Integer.parseInt(edQOS.getText().toString()));
			} catch (Exception e) {
				a.setQos(0);
			}
		}
		if (mTheAction instanceof ActionPublish) {
			ActionPublish a = (ActionPublish) mTheAction;
			a.setValue(edValue.getText().toString());
		}
		Model.getInstance().save(this);
		EventBus.getInstance().postAsync(new EventRuleAction());
		return true;
	}

	private void cancel() {
		if (mAction != EditRulesActivity.TAG_ACTION_EDIT) {
			if (mRule != null) {
				mRule.getActions().remove(mTheAction);
				EventBus.getInstance().postAsync(new EventRuleAction());
			}
		}

	}

}
