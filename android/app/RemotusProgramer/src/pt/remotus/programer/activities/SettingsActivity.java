package pt.remotus.programer.activities;

import pt.remotus.programer.R;
import android.app.ActionBar;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.view.MenuItem;


public class SettingsActivity extends PreferenceActivity {

	// private EditTextPreference mqtthost;

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		addPreferencesFromResource(R.xml.preferences);

//		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
//
//		mqtthost = (EditTextPreference) getPreferenceScreen().findPreference("mqtt_host");


	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				onBackPressed();
				return true;
			default:
				break;
		}
		return super.onOptionsItemSelected(item);
	}



}
