package pt.remotus.programer;

import java.util.Locale;

import pt.remotus.programer.R;
import pt.remotus.programer.asynctask.MqttPublishTask;
import pt.remotus.programer.asynctask.MqttSubscribeTask;
import pt.remotus.programer.mainfragments.RulesListFragment;
import pt.remotus.programer.mainfragments.TopicAliasListFragment;
import pt.remotus.programer.mainfragments.VariablesListFragment;
import pt.remotus.programer.model.Model;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.ActionBar.TabListener;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;


public class MainActivity extends  FragmentActivity implements TabListener {

	private SectionsPagerAdapter mSectionsPagerAdapter;
	private ViewPager mViewPager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pager);
		 // Set up the action bar.
        final ActionBar actionBar = getActionBar();
        assert actionBar != null;
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

     // Create the adapter that will return a fragment for each of the three
        // primary sections of the app.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        // When swiping between different sections, select the corresponding
        // tab. We can also use ActionBar.Tab#select() to do this if we have
        // a reference to the Tab.
        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener()
        {
            @Override
            public void onPageSelected(int position)
            {
                actionBar.setSelectedNavigationItem(position);
            }
        });

        // For each of the sections in the app, add a tab to the action bar.
        for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++)
        {
            // Create a tab with text corresponding to the page title defined by
            // the adapter. Also specify this Activity object, which implements
            // the TabListener interface, as the callback (listener) for when
            // this tab is selected.
            actionBar.addTab(actionBar.newTab()
            		.setText(mSectionsPagerAdapter
            		.getPageTitle(i))
                    .setTabListener(this));
        }



	}

	@Override
	protected void onPause() {
		Model.getInstance().save(this);
		super.onPause();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_mqtt_start:
				Dialogs.mqttStart(this);
				break;
			case R.id.action_mqtt_will:
				Dialogs.mqttWill(this);
				break;
			case R.id.action_settings:
				Dialogs.settings(this);
				break;
			case R.id.action_clear:
				new AlertDialog.Builder(this).setTitle(R.string.msg_clear_all)
						.setPositiveButton(R.string.action_clear, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								Model.getInstance().clearRules();
							}
						}).setNegativeButton(R.string.action_cancel, null).show();
				break;
			case R.id.action_upload:
				ProgressDialog pd = ProgressDialog.show(this, null, getString(R.string.msg_sending), true, false);
				MqttPublishTask task = new MqttPublishTask(this, pd);
				task.execute();
				break;
			case R.id.action_download:
				new AlertDialog.Builder(this).setTitle(R.string.msg_download_confirmation)
						.setPositiveButton(R.string.action_download, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								ProgressDialog pd1 = ProgressDialog.show(MainActivity.this, null, getString(R.string.msg_receiving), true, false);
								MqttSubscribeTask task1 = new MqttSubscribeTask(MainActivity.this, pd1);
								task1.execute();
							}
						}).setNegativeButton(R.string.action_cancel, null).show();


				break;
			default:
				break;
		}
		return super.onOptionsItemSelected(item);
	}


	 public class SectionsPagerAdapter extends FragmentPagerAdapter
	    {
		Fragment frgment[] = new Fragment[3];

		public SectionsPagerAdapter(FragmentManager fm)
	        {
	            super(fm);
			frgment[0] = new RulesListFragment();
			frgment[1] = new VariablesListFragment();
			frgment[2] = new TopicAliasListFragment();
	        }

	        @Override
	        public Fragment getItem(int position)
	        {
			return frgment[position];
	        }

	        @Override
	        public int getCount()
	        {
			return frgment.length;
	        }

	        @Override
	        public CharSequence getPageTitle(int position)
	        {
	            Locale l = Locale.getDefault();
	            switch (position)
	            {
	            case 0:
	                return getString(R.string.title_rules).toUpperCase(l);
	            case 1:
	                return getString(R.string.title_variables).toUpperCase(l);
	            case 2:
	                return getString(R.string.title_alias).toUpperCase(l);
	            }
	            return null;
	        }
	    }

	@Override
	public void onTabReselected(Tab tab, android.app.FragmentTransaction ft) {
		mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabSelected(Tab tab, android.app.FragmentTransaction ft) {
		mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(Tab tab, android.app.FragmentTransaction ft) {
		Fragment f = mSectionsPagerAdapter.getItem(tab.getPosition());
		if (f instanceof FragmentHelperIntf) {
			((FragmentHelperIntf) f).onTabUnselected();
		}
	}


}
