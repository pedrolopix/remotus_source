package pt.remotus.programer;

public interface FragmentHelperIntf {
	public void onTabUnselected();

	public boolean canExit();
}
