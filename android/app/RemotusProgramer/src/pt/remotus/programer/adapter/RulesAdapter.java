package pt.remotus.programer.adapter;

import pt.remotus.programer.R;
import remotus.rules.Rules;
import remotus.rules.conditions.Condition;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

public class RulesAdapter extends BaseAdapter {

	private final Rules mRules;
	private final Activity mContext;

	public RulesAdapter(Activity context, Rules rules) {
		mRules = rules;
		mContext = context;
	}

	@Override
	public int getCount() {
		return mRules.getRules().size();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View rowView = convertView;
		final Condition r = mRules.getRules().get(position);
		if (rowView == null) {
			LayoutInflater inflater = mContext.getLayoutInflater();
			rowView = inflater.inflate(R.layout.row_checkbox, null);
			// rowView.setClickable(true);
			final ViewHolder viewHolder = new ViewHolder();
			viewHolder.text = (TextView) rowView.findViewById(R.id.textview);
			viewHolder.checkbox = (CheckBox) rowView.findViewById(R.id.checkbox);
			rowView.setTag(viewHolder);

			viewHolder.checkbox.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					r.setActive(viewHolder.checkbox.isChecked());
				}
			});
		}

		ViewHolder holder = (ViewHolder) rowView.getTag();

		holder.text.setText(r.getName());
		holder.checkbox.setChecked(r.isActive());


		return rowView;
	}




	@Override
	public Object getItem(int position) {
		return mRules.getRules().get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	class ViewHolder {

		public CheckBox checkbox;
		public TextView text;

	}

}