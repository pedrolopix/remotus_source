package pt.remotus.programer.model;

import java.io.File;
import java.io.IOException;

import pt.remotus.programer.bus.EventBus;
import pt.remotus.programer.bus.EventRefresh;
import pt.remotus.programer.bus.EventTopicAlias;
import pt.remotus.programer.bus.EventVariable;
import remotus.rules.ExpressionIntf;
import remotus.rules.IEngine;
import remotus.rules.JSONLoad;
import remotus.rules.JSONSave;
import remotus.rules.LogIntf;
import remotus.rules.MqttConfig;
import remotus.rules.Rules;
import remotus.rules.SchedulerTask;
import remotus.rules.actions.ActionPublish;
import remotus.rules.actions.ActionToggle;
import remotus.rules.conditions.Condition;
import remotus.rules.conditions.ConditionTime;
import remotus.rules.conditions.ConditionTopic;
import android.content.Context;

public class Model implements IEngine {

	private static Model _instance;

	public static Model getInstance()
    {
        if (_instance == null)
        {
            _instance = new Model();
        }
        return _instance;
    }

	private Rules mRules;
	private final Expression mExpression;
	private MqttConfig mMqttConfig;
	private final AndroidLog mLog;

	public Model() {
		super();
		mExpression= new Expression();
		mRules= new Rules(this);
		mMqttConfig = new MqttConfig();
		mLog = new AndroidLog();
		// teste();
	}


	public void teste() {
		//teste
		mRules.addTopicAlias("ZeLuz","/casa/quarto/ze/luz");
		mRules.addTopicAlias("ZeSwitch","/casa/quarto/ze/switch");
		ConditionTopic ct=new ConditionTopic();
		ct.setName("Rule topic");
		ct.addTopic("ZeLuz1");
		ct.addTopic("ZeLuz2");
		ct.setCondition("value>10");
		ct.setSchedulingPattern("* * * * *"); // por minuto
		ActionPublish ap= new ActionPublish();
		ap.set(true, "action active", "/arduino/ze/luz", "=!/arduino/ze/luz", 0, true, 0);
		ActionToggle at= new ActionToggle();
		at.set(false, "action desactive", "/arduino/ana/luz", "", 0, true, 1000);
		ct.addAction(at);

		at= new ActionToggle();
		at.set(true, "", "/arduino/ana/luz", "", 0, true, 2000);
		ct.addAction(at);

		ct.addAction(ap);

		mRules.addRule(ct);

		ConditionTime ct1 = new ConditionTime();
		ct1.setName("Rule timer");
		ct1.setActive(true);
		mRules.addRule(ct1);

		mRules.getVariables().setVariableValue("var1", "123");
		mRules.getVariables().setVariableValue("var2", "345");
	}

	public String getJSON() {
		JSONSave jsave = new JSONSave(mLog);
		return jsave.getJSON(mRules, mMqttConfig);
	}

	public void setJSON(Context context, String json) {
		mRules = new Rules(this);
		mMqttConfig = new MqttConfig();
		JSONLoad jload = new JSONLoad(mLog);
		jload.setJSON(json, mRules, mMqttConfig);
		save(context);
		EventBus.getInstance().postAsync(new EventRefresh());

	}

	public void save(Context context) {

		File file = new File(context.getExternalFilesDir(null) + File.separator + "remotusservice.json");
		JSONSave jsave = new JSONSave(mLog);
		try {
			file.createNewFile();
			jsave.save(file, mRules, mMqttConfig);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void load(Context context) {
		mRules = new Rules(this);
		mMqttConfig = new MqttConfig();
		File file = new File(context.getExternalFilesDir(null) + File.separator + "remotusservice.json");
		if (!file.exists()) {
			return;
		}
		JSONLoad jload = new JSONLoad(mLog);
		try {
			jload.load(file, mRules, mMqttConfig);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Rules getRules() {
		return mRules;
	}

	public MqttConfig getMqttConfig() {
		return mMqttConfig;
	}

	@Override
	public boolean subscrive(String topic) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void unsubscrive(String topicFilter) {
		// TODO Auto-generated method stub
	}

	@Override
	public void publish(String topic, String value, boolean retained, int qos) {
		// TODO Auto-generated method stub
	}



	@Override
	public ExpressionIntf GetExpression() {
		return mExpression;
	}

	public void addVariable(String key, String value) {
		mRules.setVariableValue(key, value);
		EventBus.getInstance().postAsync(new EventVariable());
	}

	public void deleteVariable(String key) {
		mRules.deleteVariable(key);
		EventBus.getInstance().postAsync(new EventVariable());
	}

	public void editVariable(String oldKey, String newKey, String value) {
		mRules.deleteVariable(oldKey);
		mRules.setVariableValue(newKey, value);
		EventBus.getInstance().postAsync(new EventVariable());
	}

	public void addAlias(String alias, String topic) {
		mRules.addTopicAlias(alias, topic);
		EventBus.getInstance().postAsync(new EventTopicAlias());
	}

	public void editAlias(String oldAlias, String alias, String topic) {
		mRules.deleteTopic(oldAlias);
		mRules.addTopicAlias(alias, topic);
		EventBus.getInstance().postAsync(new EventTopicAlias());
	}

	public void deleteTopicAlias(String alias) {
		mRules.deleteTopic(alias);
		EventBus.getInstance().postAsync(new EventTopicAlias());
	}

	public void deleteRule(Condition rule) {
		mRules.deleteRule(rule);
	}

	public void addRule(Condition rule) {
		mRules.addRule(rule);
	}


	public void clearRules() {
		mRules = new Rules(this);
		mMqttConfig = new MqttConfig();
		EventBus.getInstance().postAsync(new EventRefresh());
	}


	@Override
	public LogIntf getLog() {
		return mLog;
	}

	@Override
	public void schedule(String schedulingPattern, SchedulerTask task) {
		// TODO Auto-generated method stub
	}

	@Override
	public void unschedule(SchedulerTask task) {
		// TODO Auto-generated method stub
	}

}
