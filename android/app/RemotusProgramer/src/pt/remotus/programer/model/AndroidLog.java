package pt.remotus.programer.model;

import android.util.Log;
import remotus.rules.LogIntf;

public class AndroidLog implements LogIntf {

	@Override
	public void e(String tag, Throwable e) {
		Log.e(tag, e.getMessage());
	}

	@Override
	public void e(String tag, String msg) {
		Log.e(tag,msg);
	}

	@Override
	public void d(String tag, String msg) {
		Log.d(tag,msg);
	}

	@Override
	public void i(String tag, String msg) {
		Log.i(tag,msg);
	}

	@Override
	public void e(String tag, String msg, Throwable e) {
		Log.e(tag,msg,e);
	}

}
