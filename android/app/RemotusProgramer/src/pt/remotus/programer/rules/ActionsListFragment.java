package pt.remotus.programer.rules;

import pt.remotus.programer.R;
import pt.remotus.programer.Dialogs;
import pt.remotus.programer.FragmentHelperIntf;
import pt.remotus.programer.adapter.ActionsAdapter;
import pt.remotus.programer.bus.EventBus;
import pt.remotus.programer.bus.EventRuleAction;
import pt.remotus.programer.model.Model;
import remotus.rules.actions.Action;
import remotus.rules.actions.ActionPublish;
import remotus.rules.actions.ActionToggle;
import remotus.rules.conditions.Condition;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;

import com.squareup.otto.Subscribe;


public class ActionsListFragment extends ListFragment implements FragmentHelperIntf, OnItemLongClickListener
{

	private ActionsAdapter mAdapter;
	private int mAction;
	private int mId;
	private Condition mRule;
	private ActionMode mActionMode;
	private Action mKey;


	public ActionsListFragment() {

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);

		Bundle bundle = this.getArguments();
		if (bundle != null) {
			mId = bundle.getInt(EditRulesActivity.TAG_RULEID);
			mAction = bundle.getInt(EditRulesActivity.TAG_ACTION);
			mRule = Model.getInstance().getRules().getConditionId(mId);
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.actions_list, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_publish_value:
				ActionPublish ap = new ActionPublish();
				mRule.addAction(ap);
				Dialogs.addAction(getActivity(), mRule.getId(), ap.getId());
				break;
			case R.id.action_publish_toggle:
				ActionToggle at = new ActionToggle();
				mRule.addAction(at);
				Dialogs.addAction(getActivity(), mRule.getId(), at.getId());
				break;
			default:
				break;
		}
		return super.onOptionsItemSelected(item);
	}

	private void fillAdpater() {
		mAdapter = new ActionsAdapter(getActivity(), mRule);
		setListAdapter(mAdapter);
		mAdapter.notifyDataSetChanged();
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
		mKey = mRule.getActions().get(position);
		// Start the CAB using the ActionMode.Callback defined above
		mActionMode = getActivity().startActionMode(mActionModeCallback);
		parent.requestFocusFromTouch(); // IMPORTANT!
		parent.setSelected(true);
		return true;
	}

	@Override
	public void onStart() {
		super.onStart();
		registerForContextMenu(getListView());
		setEmptyText(getResources().getString(R.string.actions_list_empty_text));
		getListView().setOnItemLongClickListener(this);
	}

	@Override
	public void onPause() {
		if (mActionMode != null) {
			mActionMode.finish();
		}
		EventBus.getInstance().unregister(this);
		super.onPause();
	}

	@Override
	public void onResume() {
		EventBus.getInstance().register(this);
		fillAdpater();
		super.onResume();
	}

	@Subscribe
	public void OnEventRuleAction(EventRuleAction event) {
		fillAdpater();
	}

	@Override
	public boolean canExit() {
		return true;
	}

	@Override
	public void onTabUnselected() {
		if (mActionMode != null) {
			mActionMode.finish();
		}
	}

	private final ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

		// Called when the action mode is created; startActionMode() was called
		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			// Inflate a menu resource providing context menu items
			MenuInflater inflater = mode.getMenuInflater();
			// Assumes that you have "contexual.xml" menu resources
			inflater.inflate(R.menu.variable_edit_rowselection, menu);
			return true;
		}

		// Called each time the action mode is shown. Always called after
		// onCreateActionMode, but
		// may be called multiple times if the mode is invalidated.
		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			return false; // Return false if nothing is done
		}

		// Called when the user selects a contextual menu item
		@Override
		public boolean onActionItemClicked(final ActionMode mode, MenuItem item) {
			switch (item.getItemId()) {
				case R.id.action_delete:
					new AlertDialog.Builder(getActivity()).setTitle(R.string.msg_delete_action).setMessage(String.format(getActivity().getString(R.string.msg_delete_action_s), mKey))
							.setPositiveButton(R.string.action_ok, new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									mRule.getActions().remove(mKey);
									mode.finish();
									fillAdpater();
								}
							}).setNegativeButton(R.string.action_cancel, new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									mode.finish();
								}
							}).show();

					return true;
				case R.id.action_edit:
					Dialogs.editAction(getActivity(), mRule.getId(), mKey.getId());
					mode.finish();
					return true;
				default:
					return false;
			}
		}

		// Called when the user exits the action mode
		@Override
		public void onDestroyActionMode(ActionMode mode) {
			mActionMode = null;
			mKey = null;
		}
	};

}
