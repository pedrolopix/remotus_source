package pt.remotus.programer.rules;

import java.util.Locale;

import pt.remotus.programer.R;
import pt.remotus.programer.FragmentHelperIntf;
import pt.remotus.programer.model.Model;
import remotus.rules.conditions.Condition;
import remotus.rules.conditions.ConditionTime;
import remotus.rules.conditions.ConditionTopic;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.ActionBar.TabListener;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;

public class EditRulesActivity extends FragmentActivity implements TabListener {

	public static final String TAG_ACTION = "Action";
	public static final String TAG_RULEID = "RuleID";
	public static final int TAG_ACTION_EDIT = 1;
	public static final int TAG_ADD_RULE_TIMER = 2;
	public static final int TAG_ADD_RULE_TOPIC = 3;


	private SectionsPagerAdapter mSectionsPagerAdapter;
	private ViewPager mViewPager;
	private int mRuleID;
	private int mAction;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pager);
		 // Set up the action bar.
        final ActionBar actionBar = getActionBar();
        assert actionBar != null;
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		actionBar.setDisplayHomeAsUpEnabled(true);
		mRuleID = -1;
		mAction = -1;
		if (getIntent().getExtras() != null) {
			// mAction = getIntent().getExtras().getString(TAG_ACTION);

			mAction = getIntent().getExtras().getInt(TAG_ACTION);
			switch (mAction) {
				case TAG_ADD_RULE_TIMER:
					ConditionTime ct1 = new ConditionTime();
					Model.getInstance().addRule(ct1);
					mRuleID = ct1.getId();
					break;
				case TAG_ADD_RULE_TOPIC:
					ConditionTopic ct2 = new ConditionTopic();
					Model.getInstance().addRule(ct2);
					mRuleID = ct2.getId();

					break;
				case TAG_ACTION_EDIT:
					mRuleID = getIntent().getExtras().getInt(TAG_RULEID, mRuleID);
					break;
				default:
					break;
			}

			Condition c = Model.getInstance().getRules().getConditionId(mRuleID);
			if (c instanceof ConditionTime) {
				setTitle(R.string.title_edit_timer_rule);
			}
			if (c instanceof ConditionTopic) {
				setTitle(R.string.title_edit_topic_rule);
			} 
			
		}

     // Create the adapter that will return a fragment for each of the three
        // primary sections of the app.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        // When swiping between different sections, select the corresponding
        // tab. We can also use ActionBar.Tab#select() to do this if we have
        // a reference to the Tab.
        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener()
        {
            @Override
            public void onPageSelected(int position)
            {
                actionBar.setSelectedNavigationItem(position);
            }
        });

        // For each of the sections in the app, add a tab to the action bar.
        for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++)
        {
            // Create a tab with text corresponding to the page title defined by
            // the adapter. Also specify this Activity object, which implements
            // the TabListener interface, as the callback (listener) for when
            // this tab is selected.
            actionBar.addTab(actionBar.newTab()
            		.setText(mSectionsPagerAdapter
            		.getPageTitle(i))
                    .setTabListener(this));
        }


	}

	public boolean canExit() {
		for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
			Fragment fragment = mSectionsPagerAdapter.getItem(i);
			if (fragment instanceof FragmentHelperIntf) {
				if (!((FragmentHelperIntf) fragment).canExit()) {
					mViewPager.setCurrentItem(i);
					return false;
				}
			}
		}
		return true;
	}

	@Override
	public void onBackPressed() {
		if (!canExit()) {
			return;
		}
		super.onBackPressed();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.

		getMenuInflater().inflate(R.menu.rules_edit, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				onBackPressed();
				return true;
			case R.id.action_cancel:
				finish();
				return true;
			case R.id.action_save:
				if (canExit()) {
					finish();
				}
				return true;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}


	 public class SectionsPagerAdapter extends FragmentPagerAdapter
	    {

		Fragment frgment[] = new Fragment[2];

		public SectionsPagerAdapter(FragmentManager fm)
	        {
			super(fm);
			Bundle b = new Bundle();
			b.putInt(TAG_RULEID, mRuleID);
			b.putInt(TAG_ACTION, mAction);
			frgment[0] = new RuleFragment();
			frgment[0].setArguments(b);
			frgment[1] = new ActionsListFragment();
			frgment[1].setArguments(b);
	        }

	        @Override
	        public Fragment getItem(int position)
	        {
			return frgment[position];
	        }

	        @Override
	        public int getCount()
	        {
			return frgment.length;
	        }

	        @Override
	        public CharSequence getPageTitle(int position)
	        {
	            Locale l = Locale.getDefault();
	            switch (position)
	            {
	            case 0:
					return getString(R.string.title_condition).toUpperCase(l);
	            case 1:
					return getString(R.string.title_action).toUpperCase(l);
	            }
	            return null;
	        }
	    }

	@Override
	public void onTabReselected(Tab tab, android.app.FragmentTransaction ft) {
		mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabSelected(Tab tab, android.app.FragmentTransaction ft) {
		mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(Tab tab, android.app.FragmentTransaction ft) {
		Fragment f = mSectionsPagerAdapter.getItem(tab.getPosition());
		if (f instanceof FragmentHelperIntf) {
			((FragmentHelperIntf) f).onTabUnselected();
		}
	}

}
