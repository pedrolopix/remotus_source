package pt.remotus.programer.rules;

import pt.remotus.programer.R;
import pt.crontabpicker.Crontab;
import pt.remotus.programer.FragmentHelperIntf;
import pt.remotus.programer.model.Model;
import remotus.rules.conditions.Condition;
import remotus.rules.conditions.ConditionTime;
import remotus.rules.conditions.ConditionTopic;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.throrinstudio.android.common.libs.validator.Form;
import com.throrinstudio.android.common.libs.validator.Validate;
import com.throrinstudio.android.common.libs.validator.validator.NotEmptyValidator;


/**
 * Fragment that shows a list of variables.
 */
public class RuleFragment extends Fragment implements FragmentHelperIntf
{
	private static final int CRONTAB_RESULT = 0x0001;
	private Button mBtnAddNewItem;
	private View mConditionRow;
	private LinearLayout mLlTopics;
	private CheckBox chkActive;
	private EditText edName;
	private EditText edCondition;
	private EditText edShedulerPattern;
	private LinearLayout llCondition;
	private LinearLayout llShedulerPattern;
	private LinearLayout llTopics;
	private int mChangeRevision;
	private int mAction;
	private int mId;
	private final Form mForm = new Form();
	private Button btnCron;

	public RuleFragment() {

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.condition, container, false);
		mLlTopics = (LinearLayout) view.findViewById(R.id.llTopics);
		mBtnAddNewItem = (Button) view.findViewById(R.id.btnAddNewItem);
		mBtnAddNewItem.setOnClickListener(btnHandler);

		chkActive = (CheckBox) view.findViewById(R.id.chkActive);
		edName = (EditText) view.findViewById(R.id.edName);
		edCondition = (EditText) view.findViewById(R.id.edCondition);
		edShedulerPattern = (EditText) view.findViewById(R.id.edShedulerPattern);
		btnCron= (Button) view.findViewById(R.id.btnCron);

		llCondition = (LinearLayout) view.findViewById(R.id.llCondition);
		llShedulerPattern = (LinearLayout) view.findViewById(R.id.llShedulerPattern);
		llTopics = (LinearLayout) view.findViewById(R.id.llTopics);

		Bundle bundle = this.getArguments();
		if (bundle != null) {
			mId = bundle.getInt(EditRulesActivity.TAG_RULEID);
			mAction = bundle.getInt(EditRulesActivity.TAG_ACTION);
			fill(mId);
		}
		addListners();

		Validate edNameValidate = new Validate(edName);
		edNameValidate.addValidator(new NotEmptyValidator(getActivity()));
		mForm.addValidates(edNameValidate);

		if (mRule instanceof ConditionTopic) {
			Validate edConditionValidate = new Validate(edCondition);
			edConditionValidate.addValidator(new NotEmptyValidator(getActivity()));
			mForm.addValidates(edConditionValidate);
		}

		return view;
	}



	private void addListners() {
		edName.addTextChangedListener(textWatcher);
		edCondition.addTextChangedListener(textWatcher);
		edShedulerPattern.addTextChangedListener(textWatcher);
		chkActive.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				mChangeRevision++;
			}
		});
		btnCron.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getActivity(), Crontab.class);
				startActivityForResult(i, CRONTAB_RESULT);
			}
		});
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if (resultCode == Activity.RESULT_OK && requestCode == CRONTAB_RESULT) {

			String str = data.getStringExtra("key_crontab_string");

			if (str != null && !TextUtils.isEmpty(str)) {
				edShedulerPattern.setText(str);
			}
		}
	
		
	}
	
	
	private final TextWatcher textWatcher = new TextWatcher() {

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			// TODO Auto-generated method stub

		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			// TODO Auto-generated method stub

		}

		@Override
		public void afterTextChanged(Editable s) {
			mChangeRevision++;
		}
	};
	private Condition mRule;

	private boolean save() {
		if (mRule == null) {
			return true;
		}
		if (!mForm.validate()) {
			Toast.makeText(getActivity(), R.string.msg_please_verify_condition_values, Toast.LENGTH_SHORT).show();
			return false;
		}
		mRule.setActive(chkActive.isChecked());
		mRule.setName(edName.getText().toString());
		if (mRule instanceof ConditionTime) {
			ConditionTime timer = (ConditionTime) mRule;
			timer.setSchedulingPattern(edShedulerPattern.getText().toString());
		}
		if (mRule instanceof ConditionTopic) {
			ConditionTopic topic = (ConditionTopic) mRule;
			topic.setCondition(edCondition.getText().toString());
			topic.getTopics().clear();
			for (int i = 0; i < mLlTopics.getChildCount(); i++) {
				View v = mLlTopics.getChildAt(i);
				if (v instanceof LinearLayout) {
					LinearLayout ll = (LinearLayout) v;
					for (int j = 0; j < ((LinearLayout) v).getChildCount(); j++) {
						View v1 = ll.getChildAt(j);
						if (v1 instanceof EditText) {
							EditText ed = (EditText) v1;
							String s = ed.getText().toString();
							if (s != null && (ed.getTag() != null) && !s.isEmpty() && ((Boolean) ed.getTag())) {
								topic.getTopics().add(s);
							}
						}
					}
				}

			}
		}
		Model.getInstance().save(getActivity());
		return true;
	}

	private void cancel() {
		if (mAction != EditRulesActivity.TAG_ACTION_EDIT) {
			Model.getInstance().deleteRule(mRule);
		}
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_save:
				save();
				break;
			default:
				break;
		}
		return super.onOptionsItemSelected(item);
	}

	private void fill(int id) {
		mRule = Model.getInstance().getRules().getConditionId(id);
		if (mRule == null) {
			return;
		}
		chkActive.setChecked(mRule.isActive());
		edName.setText(mRule.getName());

		if (mRule instanceof ConditionTime) {
			llShedulerPattern.setVisibility(View.VISIBLE);
			ConditionTime ct = (ConditionTime) mRule;
			edShedulerPattern.setText(ct.getSchedulingPattern());
		} else {
			llShedulerPattern.setVisibility(View.GONE);
		}

		if (mRule instanceof ConditionTopic) {
			llTopics.setVisibility(View.VISIBLE);
			llCondition.setVisibility(View.VISIBLE);
			ConditionTopic ct = (ConditionTopic) mRule;
			edCondition.setText(ct.getCondition());
			for (String item : ct.getTopics()) {
				inflateEditRow(item);
			}
		} else {
			llTopics.setVisibility(View.GONE);
			llCondition.setVisibility(View.GONE);
		}
		mChangeRevision = 0;
	}

	@Override
	public void onStart() {
		super.onStart();
	}

	private final OnClickListener btnHandler = new OnClickListener() {
		@Override
		public void onClick(View v) {

			switch (v.getId()) {
				case R.id.btnAddNewItem:
					inflateEditRow(null);
					v.setVisibility(View.GONE);
					break;

				case R.id.btnRemove:
					View v1 = (View) v.getParent();
					EditText editText = (EditText) v1.findViewById(R.id.etTopic);
					editText.setTag(false);
					mLlTopics.removeView(v1);
					break;

				default:
					break;
			}
		}
	};

	private void inflateEditRow(String name) {

		LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		final View rowView = inflater.inflate(R.layout.condition_row, null);

		final EditText editText = (EditText) rowView.findViewById(R.id.etTopic);
		final ImageButton deleteButton = (ImageButton) rowView.findViewById(R.id.btnRemove);
		deleteButton.setOnClickListener(btnHandler);
		if (name != null && !name.isEmpty()) {
			editText.setText(name);
			editText.setTag(true);
		} else {
			mConditionRow = rowView;
			deleteButton.setVisibility(View.INVISIBLE);
			editText.requestFocus();
		}

		editText.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable s) {
				mChangeRevision++;
				editText.setTag(true);
				if (s.toString().isEmpty()) {
					mBtnAddNewItem.setVisibility(View.GONE);
					deleteButton.setVisibility(View.INVISIBLE);

					if (mConditionRow != null && mConditionRow != rowView) {
						mLlTopics.removeView(mConditionRow);
					}
					mConditionRow = rowView;
				} else {

					if (mConditionRow == rowView) {
						mConditionRow = null;
					}

					mBtnAddNewItem.setVisibility(View.VISIBLE);
					deleteButton.setVisibility(View.VISIBLE);
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
		});

		// Inflate at the end of all rows but before the "Add new" button
		mLlTopics.addView(rowView, mLlTopics.getChildCount() - 1);
	}

	@Override
	public boolean canExit() {
		if (mChangeRevision == 0) {
			cancel();
		} else {
			return save();
		}
		return true;
	}

	@Override
	public void onTabUnselected() {
		// TODO Auto-generated method stub

	}

}
