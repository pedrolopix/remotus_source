package com.throrinstudio.android.common.libs.validator.validate;

import android.content.Context;
import android.widget.TextView;

import com.throrinstudio.android.common.libs.validator.AbstractValidate;
import com.throrinstudio.android.common.libs.validator.AbstractValidator;
import com.throrinstudio.android.common.libs.validator.Validate;
import com.throrinstudio.android.common.libs.validator.validator.NotEmptyValidator;
import com.throrinstudio.android.example.validator.R;

public class ConfirmMinMax extends AbstractValidate {

	private TextView _field1;
	private TextView _field2;
	private Context mContext;
	private TextView source;
	private int _myErrorMessage = R.string.validator_minmax;
	private String _errorMessage;
	
	public ConfirmMinMax(TextView field1, TextView field2){
		this._field1 = field1;
		this._field2 = field2;
		source = _field1;
		mContext = field1.getContext();
	}

	@Override
	public boolean isValid(String value) {	
		
		Validate edMinValidator = new Validate(_field1);
		edMinValidator.addValidator(new NotEmptyValidator(mContext));
		
		if (!edMinValidator.isValid(_field1.getText().toString())){
			source = _field1;
			_errorMessage = edMinValidator.getMessages();
			return false;
		}
		
		Validate edMaxValidator = new Validate(_field2);
		edMaxValidator.addValidator(new NotEmptyValidator(mContext));
		
		if (!edMaxValidator.isValid(_field2.getText().toString())){
			source = _field2;
			_errorMessage = edMaxValidator.getMessages();
			return false;
		}
		source = _field2;
		if (edMinValidator.isValid(_field1.getText().toString()) && edMaxValidator.isValid(_field2.getText().toString()))
		{
			int v1 = Integer.parseInt(_field1.getText().toString());
			int v2 = Integer.parseInt(_field2.getText().toString());

			if (v1 < v2)
				return true;
				
		}
		_errorMessage = mContext.getString(_myErrorMessage);
		return false;
	}


	@Override
	public String getMessages() {
		return _errorMessage;
	}


	@Override
	public void addValidator(AbstractValidator validator) {
	}

	@Override
	public TextView getSource() {
		return source;
	}
	
	
}
