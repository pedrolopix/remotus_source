package pt.crontabpicker;

public class GridObject {

	private String mName;
	private boolean mState = false;

	public GridObject(String name) {
		mName = name;
	}

	public String getName() {
		return mName;
	}

	public void setName(String name) {
		mName = name;
	}

	public boolean getState() {
		return mState;
	}

	public void toggleState() {
		mState = !mState;
	}
}