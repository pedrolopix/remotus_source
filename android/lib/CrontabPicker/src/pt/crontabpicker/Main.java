package pt.crontabpicker;

import pt.crontabpicker.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class Main extends Activity {

	private static final int CRONTAB_RESULT = 0x0001;
	private TextView tvStatus;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		Button btnCrontab = (Button) findViewById(R.id.btnCrontab);
		btnCrontab.setOnClickListener(btnCrontabListener);

		tvStatus = (TextView) findViewById(R.id.tvStatus);
	}

	OnClickListener btnCrontabListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			try {
				Intent i = new Intent(getApplicationContext(), Crontab.class);
				startActivityForResult(i, CRONTAB_RESULT);

			} catch (Exception e) {
			}
		}
	};

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK && requestCode == CRONTAB_RESULT) {

			String str = data.getStringExtra("key_crontab_string");

			if (str != null && !TextUtils.isEmpty(str)) {
				tvStatus.setText(str);
			}
		}
	}

}