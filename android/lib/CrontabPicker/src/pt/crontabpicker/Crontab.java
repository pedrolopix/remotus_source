package pt.crontabpicker;

import java.util.ArrayList;
import java.util.Locale;

import pt.crontabpicker.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

public class Crontab extends Activity implements OnClickListener, OnCheckedChangeListener, OnItemClickListener, OnSeekBarChangeListener {

	private static int SIZE = 5;
	private ArrayList<GridObject> mMinutesGrid;
	private ArrayList<GridObject> mHoursGrid;
	private ArrayList<GridObject> mDayOfMonthGrid;
	private ArrayList<GridObject> mMonthGrid;
	private ArrayList<GridObject> mDayOfWeekGrid;

	private Animation inAnimation;
	private Animation outAnimation;

	private int mHeadersIdList[] = {
			R.id.header1, R.id.header2, R.id.header3, R.id.header4, R.id.header5 };

	private int mOptionsIdList[] = {
			R.id.options1, R.id.options2, R.id.options3, R.id.options4, R.id.options5 };

	private String mHeadersString[];

	private ArrayList<LinearLayout> headersLayout;
	private ArrayList<RadioGroup> radioGroupLayout;
	private ArrayList<SeekBar> seekBars;
	private ArrayList<ExpandableHeightGridView> gridViews;
	private ArrayList<GridAdapter> gridAdapters;
	private ArrayList<RadioButton> radioButtonOption1;
	private ArrayList<RadioButton> radioButtonOption2;
	private ArrayList<RadioButton> radioButtonOption3;
	private ScrollView scrollView;
	private TextView tvStatus;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.crontab_list);

		mHeadersString = getResources().getStringArray(R.array.headers);

		scrollView = (ScrollView) findViewById(R.id.scrollView);
		tvStatus = (TextView) findViewById(R.id.tvStatus);

		getLayoutsId();
		getRadioButtonIds();

		setupSeekBars();

		setRadioButtonsText();

		setHeadersText();

		setupGridAdapters();

		setupListeners();

		initAnimations();
		
		buildCronString();
	}

	private void setupGridAdapters() {
		mMinutesGrid = initNumericGridView(0, 60, true); // 60 minutes
		mHoursGrid = initNumericGridView(0, 24, false); // 24 hours
		mDayOfMonthGrid = initNumericGridView(1, 31, false); // 31 days
		mMonthGrid = initMonthGridView(); // 12 months
		mDayOfWeekGrid = initDaysGridView(); // 7 days of the week

		gridAdapters = new ArrayList<GridAdapter>();
		gridAdapters.add(new GridAdapter(this, mMinutesGrid));
		gridAdapters.add(new GridAdapter(this, mHoursGrid));
		gridAdapters.add(new GridAdapter(this, mDayOfMonthGrid));
		gridAdapters.add(new GridAdapter(this, mMonthGrid));
		gridAdapters.add(new GridAdapter(this, mDayOfWeekGrid));

		gridViews = new ArrayList<ExpandableHeightGridView>();
		for (int i = 0; i < SIZE; i++) {
			ExpandableHeightGridView gridView = (ExpandableHeightGridView) radioGroupLayout.get(i).findViewById(R.id.gridView);
			gridView.setExpanded(true);
			gridView.setAdapter(gridAdapters.get(i));
			gridViews.add(gridView);
		}

		gridViews.get(3).setNumColumns(4); // months
		gridViews.get(4).setNumColumns(4); // days of the week
	}

	private void setupSeekBars() {
		seekBars = new ArrayList<SeekBar>();

		SeekBar seekbarMinutes = (SeekBar) radioGroupLayout.get(0).findViewById(R.id.seekBar);
		seekbarMinutes.setMax(58); // 59-1
		seekbarMinutes.setProgress(1);
		seekBars.add(seekbarMinutes);

		SeekBar seekbarHours = (SeekBar) radioGroupLayout.get(1).findViewById(R.id.seekBar);
		seekbarHours.setMax(22); // 23-1
		seekbarHours.setProgress(1);
		seekBars.add(seekbarHours);
	}

	private void setHeadersText() {
		for (int i = 0; i < mHeadersString.length; i++) {
			((TextView) headersLayout.get(i).findViewById(R.id.tvTitle)).setText(mHeadersString[i]);
		}
	}

//	private void setSubTitleText(String str) {
//		for (int i = 0; i < mHeadersString.length; i++) {
//			((TextView) headersLayout.get(i).findViewById(R.id.tvSubTitle)).setText("(" + str + ")");
//		}
//	}

	private void getRadioButtonIds() {
		radioButtonOption1 = new ArrayList<RadioButton>();
		for (int i = 0; i < SIZE; i++) {
			RadioButton radioButton = (RadioButton) radioGroupLayout.get(i).findViewById(R.id.rbOption1);
			radioButtonOption1.add(radioButton);
		}

		radioButtonOption2 = new ArrayList<RadioButton>();
		for (int i = 0; i < SIZE; i++) {
			RadioButton radioButton = (RadioButton) radioGroupLayout.get(i).findViewById(R.id.rbOption2);
			radioButtonOption2.add(radioButton);
		}

		radioButtonOption3 = new ArrayList<RadioButton>();
		for (int i = 0; i < SIZE; i++) {
			RadioButton radioButton = (RadioButton) radioGroupLayout.get(i).findViewById(R.id.rbOption3);
			radioButtonOption3.add(radioButton);
		}
	}

	private void setRadioButtonsText() {

		// TODO colocar estas strings em strings.xml

		for (int i = 0; i < radioButtonOption1.size(); i++) {
			radioButtonOption1.get(i).setText(getString(R.string.every)+" "+ mHeadersString[i].toLowerCase(Locale.US));
		}

		radioButtonOption2.get(0).setText(
				String.format(Locale.US, getString(R.string.every_d)+" " + mHeadersString[0].toLowerCase(Locale.US) + "s", seekBars.get(0).getProgress()));
		radioButtonOption2.get(1).setText(
				String.format(Locale.US, getString(R.string.every_d)+" " + mHeadersString[1].toLowerCase(Locale.US) + "s", seekBars.get(1).getProgress()));
		for (int i = 2; i < radioButtonOption2.size(); i++) {
			radioButtonOption2.get(i).setVisibility(View.GONE);
		}

		for (int i = 0; i < radioButtonOption3.size(); i++) {
			radioButtonOption3.get(i).setText(getString(R.string.each_selected ,mHeadersString[i].toLowerCase(Locale.US)));
		}

	}

	private void setupListeners() {

		// TODO isto para bem devia-se registar apenas os listeners para as vistas que est???o vis???veis

		for (int i = 0; i < headersLayout.size(); i++) {

			// headers
			headersLayout.get(i).setOnClickListener(this);

			// radio groups
			radioGroupLayout.get(i).setOnCheckedChangeListener(this);

			// grid views
			gridViews.get(i).setOnItemClickListener(this);
		}

		// seek bars
		seekBars.get(0).setOnSeekBarChangeListener(this);
		seekBars.get(1).setOnSeekBarChangeListener(this);
	}

	private void getLayoutsId() {
		headersLayout = new ArrayList<LinearLayout>();
		radioGroupLayout = new ArrayList<RadioGroup>();

		for (int i = 0; i < mHeadersIdList.length; i++) {
			headersLayout.add((LinearLayout) findViewById(mHeadersIdList[i]));
			radioGroupLayout.add((RadioGroup) findViewById(mOptionsIdList[i]));
		}
	}

	@Override
	public void onClick(View v) {

		for (int i = 0; i < mHeadersIdList.length; i++) {

			if (v.getId() == mHeadersIdList[i]) {

				boolean opened = toogleVisibility(radioGroupLayout.get(i));

				// TODO
				if (opened && (i == 3 || i == 4)) {
					scrollView.post(new Runnable() {
						@Override
						public void run() {
							scrollView.fullScroll(ScrollView.FOCUS_DOWN);
						}
					});
				}

				// TODO isto s??o apenas testes

				// scrollView.fullScroll(ScrollView.FOCUS_DOWN);

				// Display display = getWindowManager().getDefaultDisplay();
				// int height = display.getHeight();

				// int h = radioGroupLayout.get(i).getHeight();

				// Toast.makeText(this, "h: " + h + "\nh: " + height, Toast.LENGTH_LONG).show();

				// scrollView.smoothScrollTo(0, headersLayout.get(i).getTop());

				// scrollView.smoothScrollTo(0, radioGroupLayout.get(i).getBottom());

				break;
			}
		}
		buildCronString();
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		// checkedId is the RadioButton selected

		for (int i = 0; i < radioGroupLayout.size(); i++) {

			if (group.equals(radioGroupLayout.get(i))) {

				if (checkedId==R.id.rbOption1) {

						if (i == 0 || i == 1) {
							hide(seekBars.get(i), false);
						}
						hide(gridViews.get(i), false);
						
				}
				if (checkedId==R.id.rbOption2) {
						if (i == 0 || i == 1) {
							show(seekBars.get(i), false);
						}
						hide(gridViews.get(i), false);
						
				}
				if (checkedId== R.id.rbOption3) {
						if (i == 0 || i == 1) {
							hide(seekBars.get(i), false);
						}

						boolean opened = show(gridViews.get(i), false);

						// se est?? a ser aberto && se for o ??ltimo ou pen??ltimo;
						if (opened && (i == 3 || i == 4)) {
							scrollView.post(new Runnable() {
								@Override
								public void run() {
									// fazer scroll at?? ao fim da vista/lista
									scrollView.fullScroll(ScrollView.FOCUS_DOWN);
								}
							});
						}
					
				}
			}
		}
		buildCronString();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if (item.getItemId()==R.id.done) {
				Intent intent = Crontab.this.getIntent();
				intent.putExtra("key_crontab_string", tvStatus.getText().toString());
				Crontab.this.setResult(RESULT_OK, intent);
				Crontab.this.finish();
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
		if (seekBar.equals(seekBars.get(0))) {
			radioButtonOption2.get(0).setText(String.format(Locale.US, getString(R.string.every_d) + mHeadersString[0].toLowerCase(Locale.US) + "s", progress + 1));

		} else if (seekBar.equals(seekBars.get(1))) {
			radioButtonOption2.get(1).setText(String.format(Locale.US, getString(R.string.every_d) + mHeadersString[1].toLowerCase(Locale.US) + "s", progress + 1));
		}
		buildCronString();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

		for (int i = 0; i < gridViews.size(); i++) {
			if (gridViews.get(i).equals(parent)) {

				switch (i) {
					case 0:
						mMinutesGrid.get(position).toggleState();
						break;
					case 1:
						mHoursGrid.get(position).toggleState();
						break;
					case 2:
						mDayOfMonthGrid.get(position).toggleState();
						break;
					case 3:
						mMonthGrid.get(position).toggleState();
						break;
					case 4:
						mDayOfWeekGrid.get(position).toggleState();
						break;

					default:
						break;
				}
				gridAdapters.get(i).notifyDataSetChanged();
				break;
			}
		}
		buildCronString();
	}

	private ArrayList<GridObject> initNumericGridView(int firstNumber, int size, boolean doubleDigit) {
		ArrayList<GridObject> mGridItems = new ArrayList<GridObject>();
		for (int i = firstNumber; i < size; i++) {
			if (doubleDigit) {
				mGridItems.add(new GridObject(String.format(Locale.US, "%02d", i)));
			} else {
				mGridItems.add(new GridObject("" + i));
			}
		}
		return mGridItems;
	}

	private ArrayList<GridObject> initMonthGridView() {

		String[] months = getResources().getStringArray(R.array.months);

		ArrayList<GridObject> mGridItems = new ArrayList<GridObject>();
		for (int i = 0; i < months.length; i++) {
			mGridItems.add(new GridObject(months[i]));
		}
		return mGridItems;
	}

	private ArrayList<GridObject> initDaysGridView() {

		String[] daysOfTheWeek = getResources().getStringArray(R.array.days_of_the_week);

		ArrayList<GridObject> mGridItems = new ArrayList<GridObject>();
		for (int i = 0; i < daysOfTheWeek.length; i++) {
			mGridItems.add(new GridObject(daysOfTheWeek[i]));
		}
		return mGridItems;
	}

	private void initAnimations() {
		inAnimation = AnimationUtils.loadAnimation(Crontab.this, R.anim.in_animation);
		outAnimation = AnimationUtils.loadAnimation(Crontab.this, R.anim.out_animation);
	}

	private boolean show(View view, boolean withAnimation) {
		if (isVisible(view)) {
			return false;
		}
		if (withAnimation) {
			view.startAnimation(inAnimation);
		}
		view.setVisibility(View.VISIBLE);
		return true;
	}

	private void hide(View view, boolean withAnimation) {
		if (isGone(view)) {
			return;
		}
		if (withAnimation) {
			view.startAnimation(outAnimation);
		}
		view.setVisibility(View.GONE);
	}

	private boolean toogleVisibility(View view) {
		if (isVisible(view)) {
			view.setVisibility(View.GONE);
			return false;
		} else {
			view.setVisibility(View.VISIBLE);
			return true;
		}
	}

	private boolean isVisible(View view) {
		return (view.getVisibility() == View.VISIBLE);
	}

	private boolean isGone(View view) {
		return (view.getVisibility() == View.GONE);
	}

	private void buildCronString() {
		String cron="";
		// minutos
		if (radioButtonOption1.get(0).isChecked()) {
			// todos os minutus
			cron="* ";
		};
		if (radioButtonOption2.get(0).isChecked()) {
			// every x minutes
			SeekBar seekbarMinutes = (SeekBar) radioGroupLayout.get(0).findViewById(R.id.seekBar);
			cron="*/"+(seekbarMinutes.getProgress()+1)+ " ";
		};
		if (radioButtonOption3.get(0).isChecked()) {
			String s="";
			for(int i=0; i<mMinutesGrid.size(); i++) {
				if (mMinutesGrid.get(i).getState()) {
					if (!s.isEmpty()) s+=",";
					s+=i;
				}
			}
			if (s.isEmpty()) s="*";
			cron=s+" "; 
		}
		// horas
		if (radioButtonOption1.get(1).isChecked()) {
			// todos os minutus
			cron+="* ";
		};
		if (radioButtonOption2.get(1).isChecked()) {
			// every x minutes
			SeekBar seekbarMinutes = (SeekBar) radioGroupLayout.get(1).findViewById(R.id.seekBar);
			cron+="*/"+(seekbarMinutes.getProgress()+1)+ " ";
		};
		if (radioButtonOption3.get(1).isChecked()) {
			String s="";
			for(int i=0; i<mHoursGrid.size(); i++) {
				if (mHoursGrid.get(i).getState()) {
					if (!s.isEmpty()) s+=",";
					s+=i;
				}
			}
			if (s.isEmpty()) s="*";
			cron+=s+" "; 
		}
		// Dias do m??s
		if (radioButtonOption1.get(2).isChecked()) {
			// todos os minutus
			cron+="* ";
		};
		if (radioButtonOption3.get(2).isChecked()) {
			String s="";
			for(int i=0; i<mDayOfMonthGrid.size(); i++) {
				if (mDayOfMonthGrid.get(i).getState()) {
					if (!s.isEmpty()) s+=",";
					s+=i;
				}
			}
			if (s.isEmpty()) s="*";
			cron+=s+" "; 
		}
		// m??s
		if (radioButtonOption1.get(3).isChecked()) {
			// todos os minutus
			cron+="* ";
		};
		if (radioButtonOption3.get(3).isChecked()) {
			String s="";
			for(int i=0; i<mMonthGrid.size(); i++) {
				if (mMonthGrid.get(i).getState()) {
					if (!s.isEmpty()) s+=",";
					s+=(i+1);
				}
			}
			if (s.isEmpty()) s="*";
			cron+=s+" "; 
		}
		
		// dia semana
		if (radioButtonOption1.get(4).isChecked()) {
			// todos os minutus
			cron+="* ";
		};
		if (radioButtonOption3.get(4).isChecked()) {
			String s="";
			for(int i=0; i<mDayOfWeekGrid.size(); i++) {
				if (mDayOfWeekGrid.get(i).getState()) {
					if (!s.isEmpty()) s+=",";
					s+=i;
				}
			}
			if (s.isEmpty()) s="*";
			cron+=s+" "; 
		}
		
		
		tvStatus.setText(cron);
	}
	
	
	// TODO para testes
//	private int measureCellWidth(Context context, View cell) {
//		// We need a fake parent
//		FrameLayout buffer = new FrameLayout(context);
//		android.widget.AbsListView.LayoutParams layoutParams = new android.widget.AbsListView.LayoutParams(LayoutParams.WRAP_CONTENT,
//				LayoutParams.WRAP_CONTENT);
//		buffer.addView(cell, layoutParams);
//
//		cell.forceLayout();
//		cell.measure(1000, 1000);
//
//		int width = cell.getMeasuredWidth();
//
//		buffer.removeAllViews();
//
//		return width;
//	}
}