package pt.crontabpicker;

import java.util.ArrayList;

import pt.crontabpicker.R;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class GridAdapter extends BaseAdapter {

	private LayoutInflater mInflater;
	private ArrayList<GridObject> mGridItems;

	public GridAdapter(Context context, ArrayList<GridObject> gridItems) {
		mInflater = LayoutInflater.from(context);
		mGridItems = gridItems;
	}

	@Override
	public int getCount() {
		return mGridItems.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		GridObject object = mGridItems.get(position);
		ViewHolder holder;

		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.list_item_icon_text, null);
			holder = new ViewHolder();
			holder.text = (TextView) convertView.findViewById(R.id.text);
			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.text.setText(object.getName());

		if (object.getState()) {
			holder.text.setBackgroundColor(Color.CYAN);
		} else {
			holder.text.setBackgroundColor(Color.GRAY);
		}

		return convertView;
	}

	static class ViewHolder {
		TextView text;
	}
}