package model.rules;


public class Pin {
	private int threshold=255;
	private int id;
	private String topic="";
	private boolean retained = true;
	private int qos = 0;
	private boolean active=false;
	private PinType pinType;
	

	public Pin(int id) {
		super();
		this.id = id;
	}

	public Pin(int id, String topic, boolean retained, int qos) {
		super();
		this.id = id;
		this.topic = topic;
		this.retained = retained;
		this.qos = qos;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public int getId() {
		return id;
	}
	
	public void setId(int value) {
		id=value;
	}
	
	public  boolean isInput() {
		return pinType==PinType.DIGITAL_INPUT || pinType==PinType.ANALOG_INPUT;
	}

	public boolean isOutput() {
		return !isInput();
	}
	
	public boolean getRetained() {
		return retained;
	}

	public int getQos() {
		return qos;
	}

	public boolean isActive() {
		return active;
	}

	public void setRetained(boolean retained) {
		this.retained = retained;
	}

	public void setQos(int qos) {
		this.qos = qos;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	@Override
	public String toString() {
		return "#" + id + "=>" + topic;
	}

	public PinType getType() {
		return pinType;
	}

	public int getThreshold() {
		return threshold;
	}

	public void setThreshold(int threshold) {
		this.threshold=threshold;
	}

	public void setType(PinType pinType) {
		this.pinType=pinType;
		
	}
	
}
