package model.rules;

import java.util.Collection;
import java.util.HashMap;

import mqtt.MqttMyMessage;

public class Rules {
	private MqttMyMessage willMsg;
	private MqttMyMessage startMsg;
	private HashMap<Integer,Pin> pins;

	
	public Rules() {
		super();
		willMsg= new MqttMyMessage();
		startMsg= new MqttMyMessage();
		pins= new HashMap<Integer,Pin>();
	}

	
	public Collection<Pin> getPins() {
		return pins.values();
	}
	
//	public void defineType(int pinNumer, PinType type, String topic) {
//		
//		
//	}
	

	@Override
	public String toString() {
		return pins.toString();
	}

	public MqttMyMessage getWillMsg() {
		return willMsg;
	}
	
	public MqttMyMessage getStartMsg() {
		return startMsg;
	}

	public void setWillMsg(MqttMyMessage msg) {
		this.willMsg=msg;
	}

	public void setStartMsg(MqttMyMessage msg) {
		this.startMsg=msg;
	}


	public Pin getPin(int pin) {
	    return pins.get(pin);
	}


	public void addPin(Pin pin) {
		pins.put(pin.getId(), pin);	
	}
	
}
