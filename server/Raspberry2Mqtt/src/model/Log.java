package model;

public class Log {
	private Model model;

	public Log(Model model) {
		super();
		this.model = model;
	}

	public void e(String tag, Exception e) {
		model.getEventBus().logError(tag, e.getMessage());
		e.printStackTrace();
	}
	
	public void e(String tag, String msg) {
		model.getEventBus().logError(tag, msg);
	}
	
	public void d(String tag, String msg) {
		model.getEventBus().logError(tag, msg);
	}

	public void i(String tag, String msg) {
		model.getEventBus().logError(tag, msg);
	}


	
}
