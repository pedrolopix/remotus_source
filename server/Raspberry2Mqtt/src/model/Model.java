package model;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import model.rules.Pin;
import model.rules.PinType;
import model.rules.Rules;
import mqtt.MqttEngine;
import mqtt.MqttMyMessage;
import net.engio.mbassy.BusConfiguration;
import net.engio.mbassy.listener.Listener;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttSecurityException;
import org.json.JSONException;

import raspberry.Raspberry;
import event.EventBus;
import event.EventFileOpen;
import event.EventMqttMessageArrived;
import event.EventRaspberryPinChange;

public class Model {
	public enum State {START, MQTT_CONNECTING, MQTT_CONNECTED, RASPBERRY_CONNECTING, 
			RASPBERRY_CONNECTED, RUNNING, ERROR, RASPBERRY_ERROR, CLOSED}
	private static final String TAG = "Model";
	private EventBus eventBus;
	private Rules rules;
	private Raspberry raspbery;
	private MqttEngine mqttEngine;
	private Log log;
	private State state;
	
	
	public Model() {
		super();
		log= new Log(this);
		eventBus=  new EventBus(BusConfiguration.Default());
		raspbery= new Raspberry(this);
		mqttEngine=new MqttEngine(this);
    	rules=new Rules();
    	//teste();
    	state=State.START;
    	getEventBus().subscribe(this);
	}
	
	private void teste() {
		Pin p=new Pin(1);
		p.setActive(true);
		p.setTopic("/arduino/1/luz");
		p.setType(PinType.DIGITAL_OUTPUT);
		p.setRetained(true);
		rules.addPin(p);
		
		Pin p1=new Pin(2);
		p1.setActive(true);
		p1.setTopic("/arduino/1/switch");
		p1.setType(PinType.DIGITAL_INPUT);
		p1.setRetained(true);
		rules.addPin(p1);
		//mqttEngine.setServerURI("tcp://127.0.0.1:1883");
		mqttEngine.setServerURI("tcp://192.168.1.10:1883");
		mqttEngine.setUserName("pedro");
		mqttEngine.setPassword("ze");
		
	}

	public void close(){
		raspbery.stop();
		getEventBus().unsubscribe(this);
		mqttEngine.stop();
		setState(State.CLOSED);
	}

	public void setState(State state) {
		log.d("mqttstate", this.state.toString()+"->"+state.toString());
		this.state=state;
	}

	public void start() {
		if (state!=State.START) {
			close();
		}
		
		while (true) {
			switch (state) {
			case START:
				connectMqtt();
				break;
			case MQTT_CONNECTING:
				break;
			case MQTT_CONNECTED:
				raspberryStart();
				break;
			case RASPBERRY_CONNECTING:
				break;
			case RASPBERRY_CONNECTED:
				setState(State.RUNNING);
				initPins();
				break;
			case RASPBERRY_ERROR:
				setState(State.ERROR);
				break;
			case RUNNING:
				try {
					
					Thread.sleep(100);
				} catch (InterruptedException e) {
					setState(State.ERROR);
				}
				break;
			case ERROR:	
				close();
				break;
			case CLOSED:
				return;
			default:
				break;
			}	
		}
	}
	
	private void initPins(){
		this.getLog().d(TAG,"pin count=>"+rules.getPins().size());
		for (Pin pin: rules.getPins()) {
			if (!pin.isActive()) {
				this.getLog().d(TAG,"pin "+pin.getId()+"=> disable");
				continue;
			}
			this.pinMode(pin.getId(), pin.getType());
			if (pin.isOutput()) {
				this.getLog().d(TAG,"Subcribe pin "+pin.getId()+"=>"+pin.getTopic());
				this.subscrive(pin.getTopic());
			}
		}
	}
	
	@Listener
	public void onReceiveMqttMessage(final EventMqttMessageArrived event) {	
		String name= event.getTopic();
		getLog().d(TAG, "onReceiveMqttMessage "+name);
		List<Pin> pins=getOutputPinTopic(name);
		int value = Integer.parseInt(event.getMessage().toString());
		for (Pin pin : pins) {
			getLog().d(TAG, "onReceiveMqttMessage send to pin "+pin);
			sendPinValue(pin,value);
		}
	}
	
	@Listener
	public void onReceivePin(EventRaspberryPinChange data) {
		Pin pin=getPin(data.getPin());
		if (pin==null) return;
		
		RaspberryToMQtt(pin, data.getValue());
	}
	
	private void RaspberryToMQtt(Pin pin, int value) {
		if (pin.isActive() && pin.isInput() && pin.getTopic().length()!=0) {
			publish(pin.getTopic(),value,pin.getRetained(), pin.getQos());
		}
	}
	
	public EventBus getEventBus() {
		return eventBus;
	}

	public void connectMqtt() {
		setState(State.MQTT_CONNECTING);
		try {
			mqttEngine.connect();
			setState(State.MQTT_CONNECTED);
		} catch (Exception e) {
			log.e(TAG, e);
			setState(State.ERROR);
		}
	}
	
	public void connectMqtt(String host, String userName, String password) throws MqttSecurityException, MqttException {
		mqttEngine.setServerURI(host);
		mqttEngine.setUserName(userName);
		mqttEngine.setPassword(password);
		mqttEngine.connect();
	}

	public void publish(String topic, int value, boolean retained, int qos) {
		mqttEngine.publish(topic, value, retained, qos);
		
	}
	public void publish(String topic, String value, boolean retained, int qos) {
		mqttEngine.publish(topic, value, retained, qos);
	}
	
	public void subscrive(String topic) {
		mqttEngine.subscrive(topic);
	}
	
	public void raspberryStart() {
		
		setState(State.RASPBERRY_CONNECTING);
		raspbery.start();
		setState(State.RASPBERRY_CONNECTED);
		
	}
	
	public Log getLog() {
		return log;
	}


	public Pin getPin(int pin) {
		return rules.getPin(pin);
	}


	public void saveTo(File file) throws JSONException, IOException {
		JsonParse jp= new JsonParse();
		jp.save(mqttEngine,rules, file);
	}
	
	public void loadfile(File file) throws IOException, JSONException, JsonParseException {
		rules = new Rules();
		JsonParse jp= new JsonParse();
		jp.load(mqttEngine,rules, file);
		eventBus.publishAsync(new EventFileOpen(file));
		getLog().d(TAG, "loadfile: " + file.toString());
	}

	public Rules getRules() {
		return rules;
	}

	public MqttEngine getMqttEngine() {
		return mqttEngine;
	}

	public List<Pin> getOutputPinTopic(String topic) {
		List<Pin> ret= new ArrayList<Pin>();
		for (Pin pin : rules.getPins()) {
			if (pin.isOutput() && topic.equals(pin.getTopic())) ret.add(pin);
		}
		return ret;
	}

	public void sendPinValue(Pin pin, int value) {
		raspbery.pinWrite(pin.getId(), pin.getType(), value);
	}

	public void pinMode(int pin, PinType input) {
		raspbery.pinMode(pin, input);
	}

	public Raspberry getRaspberry() {
		return raspbery;
	}

	public void setAnalogThreshold(int pin, int analogThreshold) {
		raspbery.setAnalogThreshold(pin, analogThreshold);
		
	}

	public void setWillMsg(MqttMyMessage msg) {
		mqttEngine.setWillMsg(msg);
	}

	public void setStartMsg(MqttMyMessage msg) {
		mqttEngine.setWillMsg(msg);
	}
	

}
