package model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import model.rules.Pin;
import model.rules.PinType;
import model.rules.Rules;
import mqtt.MqttEngine;
import mqtt.MqttMyMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JsonParse {

	private static final String WILL = "will";
	private static final String START = "start";
	private static final String THRESHOLD = "threshold";
	private static final String ACTIVE = "active";
	private static final String RETAINED = "retained";
	private static final String QOS = "qos";
	private static final String TOPIC = "topic";
	private static final String PIN = "pin";
	private static final String PIN_TYPE = "type";
	private static final String PASSWORD = "password";
	private static final String USER = "user";
	private static final String URI = "uri";
	private static final String PINS = "pins";
	private static final String MQTT = "mqtt";
	private static final String VERSION = "version";
	private static final String MESSAGE = "message";


	private void pinToJSON(Pin pin, JSONObject json) throws JSONException {
		json.put(PIN,pin.getId());
		json.put(TOPIC,pin.getTopic());
		json.put(QOS,pin.getQos());
		json.put(RETAINED,pin.getRetained());
		json.put(ACTIVE,pin.isActive());
		json.put(PIN_TYPE, pin.getType().toString());
		json.put(THRESHOLD, pin.getThreshold());
	}
	
	private void JSONToPin(Rules rules, JSONObject json) throws JSONException {		
		int pinNum=json.getInt(PIN);
		Pin pin = rules.getPin(pinNum);
		if (pin==null) {
			pin= new Pin(pinNum);
		}
		pin.setType(PinType.valueOf(json.getString(PIN_TYPE)));
		pin.setThreshold(json.getInt(THRESHOLD));
		pin.setId(json.getInt(PIN));
		pin.setTopic(json.getString(TOPIC));
		pin.setQos(json.getInt(QOS));
		pin.setRetained(json.getBoolean(RETAINED));
		pin.setActive(json.getBoolean(ACTIVE));
		rules.addPin(pin);
	}
	
	
	public void rulesToJSON(Rules rules, JSONObject json) throws JSONException {
		JSONArray arr;
		arr= new JSONArray();
		for (Pin pin : rules.getPins()) {
			JSONObject value=new JSONObject();
			pinToJSON(pin,value);
			arr.put(value);
		}
		json.put(PINS, arr);
		
		JSONObject value=new JSONObject();
		MessageToJson(rules.getWillMsg(),value);
		json.put(WILL, value);
		value=new JSONObject();
		MessageToJson(rules.getStartMsg(),value);
		json.put(START, value);
		
	}

	private void MessageToJson(MqttMyMessage msg, JSONObject json) throws JSONException {
		json.put(TOPIC, msg.topic);
		json.put(MESSAGE, msg.message);
		json.put(QOS, msg.qos);
		json.put(RETAINED, msg.retained);
	}

	private void JsonToMessage(MqttMyMessage msg, JSONObject json) throws JSONException {
		msg.topic=json.getString(TOPIC);
		msg.message=json.getString(MESSAGE);
		msg.qos=json.getInt(QOS);
		msg.retained=json.getBoolean(RETAINED);
	}
	
	
	private void jsonToRules(Rules rules, JSONObject json) throws JSONException {
		JSONArray arr;
		arr=json.optJSONArray(PINS);
		if (arr!=null) {
			for (int i = 0; i < arr.length(); i++) {
				JSONToPin(rules,arr.getJSONObject(i));
			}
		}
		JSONObject o;
		o=json.getJSONObject(WILL);
		JsonToMessage(rules.getWillMsg(),o);
		o=json.getJSONObject(START);
		JsonToMessage(rules.getStartMsg(),o);
	}

	
	private void mqttToJson(MqttEngine mqtt, JSONObject json) throws JSONException {
		json.put(URI, mqtt.getServerURI());
		json.put(USER, mqtt.getUserName());
		json.put(PASSWORD, mqtt.getPassword());
	}
	
	private void jsonToMqtt(MqttEngine mqtt, JSONObject json) throws JSONException {
		mqtt.setServerURI(json.getString(URI));
		mqtt.setUserName(json.getString(USER));
		mqtt.setPassword(json.getString(PASSWORD));
	}

		
	public void save( MqttEngine mqtt, Rules rules, File file) throws JSONException, IOException {
		
		rules.setWillMsg(mqtt.getWillMsg());
		rules.setStartMsg(mqtt.getStartMsg());
		
		JSONObject json= new JSONObject();
		json.put(VERSION, 1);
		JSONObject aux = new  JSONObject();

		
		aux = new  JSONObject();
		json.put(MQTT, aux);
		mqttToJson(mqtt,aux);
		
		rulesToJSON(rules,json);
		
        FileWriter fw = new FileWriter(file);
        fw.write(json.toString());
        fw.close();
	}


	public void load(MqttEngine mqtt, Rules rules, File file) throws IOException, JSONException, JsonParseException {
		StringBuffer fileData = new StringBuffer();
        BufferedReader reader = new BufferedReader(new FileReader(file));
        char[] buf = new char[1024];
        int numRead=0;
        while((numRead=reader.read(buf)) != -1){
            String readData = String.valueOf(buf, 0, numRead);
            fileData.append(readData);
        }
        reader.close();
		JSONObject json= new JSONObject(fileData.toString());
		//verifica versão
		//JSONObject json1= json.getJSONObject(VERSION);
		int version= json.getInt(VERSION);
		if (version!=1) throw new JsonParseException("Versão do ficheiro não suportada!");
		
		jsonToMqtt(mqtt,json.getJSONObject(MQTT));
		jsonToRules(rules,json);
		
		mqtt.setStartMsg(rules.getStartMsg());
		mqtt.setWillMsg(rules.getWillMsg());
		
	}

}
