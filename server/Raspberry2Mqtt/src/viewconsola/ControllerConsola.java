package viewconsola;

import model.Model;
import net.engio.mbassy.listener.Listener;
import event.Event;


public class ControllerConsola {
	private static ControllerConsola instance;
	private boolean debug;
	public static ControllerConsola getInstance() {
		if (instance == null)
			instance = new ControllerConsola();
		return instance;
	}

	public void start(final Model model, Boolean wait, boolean debug) {
		this.debug=debug;
		model.getEventBus().subscribe(this);
		
		model.start();
		
		if (wait) {
	 		System.console().readLine();
		}
	}
	
	@Listener
    public void onLog(final Event event) {
		if (debug) {
			System.out.println(event.toString());
		}
	}
	
}
