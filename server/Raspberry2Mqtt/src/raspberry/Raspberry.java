package raspberry;

import java.util.EnumSet;
import java.util.HashMap;

import model.Model;
import model.rules.PinType;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPin;
import com.pi4j.io.gpio.GpioPinAnalogOutput;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinMode;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiGpioProvider;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListener;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;
import com.pi4j.io.gpio.impl.PinImpl;

import event.EventRaspberryPinChange;

public class Raspberry {
	private static final String TAG = "Raspberry";
	private GpioController gpio;
	private HashMap<Integer, GpioPin> pins;
	private Model model;
	private GpioPinListener pinListner = new GpioPinListenerDigital() {

		@Override
		public void handleGpioPinDigitalStateChangeEvent(
				GpioPinDigitalStateChangeEvent event) {
			model.getLog().d(TAG,
					"handleGpioPinDigitalStateChangeEvent => press " + event.getPin().getPin()
					.getAddress() + " on state " + event.getState());
			
			model.getEventBus().publishAsync(
					new EventRaspberryPinChange(event.getPin().getPin()
							.getAddress(),
							event.getState() == PinState.HIGH ? 1 : 0));
		}
	};

	public Raspberry(Model model) {
		super();
		pins = new HashMap<>();
		this.model = model;
	}

	public void stop() {
		gpio.shutdown();
		pins.clear();
	}

	public void start() {
		if (gpio != null)
			return;

		gpio = GpioFactory.getInstance();

	}

	public void pinWrite(int pinNumer, PinType type, int value) {
		if (gpio == null)
			return;
		GpioPin p = pins.get(pinNumer);
		if (p == null) {
			model.getLog().d(TAG,
					"pin " + pinNumer + " not exist in " + pins.size());
			return;
		}
		if (p instanceof GpioPinDigitalOutput) {
			GpioPinDigitalOutput pd = (GpioPinDigitalOutput) p;
			model.getLog().d(TAG,
					"send to digital pin " + pinNumer + "=>" + (value == 1));
			pd.setState(value == 1);
		} else if (p instanceof GpioPinAnalogOutput) {
			GpioPinAnalogOutput pa = (GpioPinAnalogOutput) p;
			model.getLog().d(TAG,
					"send to analog pin " + pinNumer + "=>" + value);
			pa.setValue((int) value);
		} else {
			model.getLog().d(TAG, "instance of pin not reconigzed");
		}
	}

	public void pinMode(int pinNumer, PinType type) {
		if (gpio == null)
			return;
		GpioPin pinIO = null;
		Pin pin = null;
		switch (type) {
		case PWM_OUTPUT:
		case DIGITAL_INPUT:
			pin = createDigitalPin(pinNumer);
			pinIO = gpio.provisionDigitalInputPin(pin,PinPullResistance.PULL_DOWN);
			pinIO.addListener(pinListner);
			break;
		case DIGITAL_OUTPUT:
			pin = createDigitalPin(pinNumer);
			pinIO = gpio.provisionDigitalOutputPin(pin);
			break;
		case ANALOG_INPUT:
			pin = createAnalogPin(pinNumer);
			pinIO = gpio.provisionAnalogInputPin(pin);
			break;
		case ANALOG_OUTPUT:
			pin = createAnalogPin(pinNumer);
			pinIO = gpio.provisionAnalogOutputPin(pin);
			break;
		default:
			// TODO add to log error
			break;
		}
		if (pinIO == null)
			return;
		GpioPin p = pins.get(pinNumer);
		if (p == null) {
			model.getLog().d(TAG, "add pin " + pinNumer);
			pinIO.setShutdownOptions(true, PinState.LOW, PinPullResistance.OFF);
			pins.put(pinNumer, pinIO);
		}
	}

	private Pin createAnalogPin(int pinNumer) {
		// TODO Auto-generated method stub
		return null;
	}

	private Pin createDigitalPin(int pinNumer) {
		if (pinNumer == 1) {
			return new PinImpl(RaspiGpioProvider.NAME, pinNumer, "GPIO "
					+ pinNumer, EnumSet.of(PinMode.DIGITAL_INPUT,
					PinMode.DIGITAL_OUTPUT, PinMode.PWM_OUTPUT),
					PinPullResistance.all());
		} else {
			return new PinImpl(RaspiGpioProvider.NAME, pinNumer, "GPIO "
					+ pinNumer, EnumSet.of(PinMode.DIGITAL_INPUT,
					PinMode.DIGITAL_OUTPUT), PinPullResistance.all());
		}
	}

	public void setAnalogThreshold(int pin, int analogThreshold) {
		// TODO Auto-generated method stub
	}

}
