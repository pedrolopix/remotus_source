package main;

import java.io.File;
import java.io.IOException;

import model.JsonParseException;
import model.Model;

import org.json.JSONException;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;

import viewconsola.ControllerConsola;



public class Raspberry2Mqtt {
	/**
	 * Launch the application.
	 */
	private Model model;

	public Raspberry2Mqtt() {
		super();
		model = new Model();
	}

	private void start(String[] args) {
		
		boolean startconsole=false;
		boolean debug=false;
		if (args.length > 0) {
			for (int i = 0; i < args.length; i++) {
				if (args[i].equals("-o"))
					try {
						if (args.length>i + 1) {
							model.loadfile(new File(args[i + 1]));
							startconsole=true;
						}
					} catch (IOException | JSONException | JsonParseException e) {
						e.printStackTrace();
					}
				else if (args[i].equals("-d")) {
					debug=true;
					startconsole=true;
				}
			}
			if (startconsole ) {
				System.out.println("Raspberry2Mqtt - a iniciar...");
				ControllerConsola.getInstance().start(model, true, debug);
				System.out.println("Raspberry2Mqtt - terminado");
			}
		} else {
			ajuda();
		}		
	}


	private void ajuda() {
		System.out.println("Raspberry2Mqtt");
		System.out.println("");
		System.out.println("opções: ");
		System.out.println(" -o <NomeFicheiro> lê ficheiro de configurações");
		System.out.println(" -d modo de debug");
		System.out.println("");
		System.out.println("Exemplo: Raspberry2Mqtt -d -o config.json");
		System.out.println("");
	}

	public static void main(String[] args) {
		Raspberry2Mqtt app = new Raspberry2Mqtt();
		app.start(args);
       
	}

}
