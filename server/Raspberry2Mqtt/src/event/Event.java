package event;


public class Event {
	protected int id=0;
	private static int idCound=0;
	public Event() {
		super();
		synchronized (this) {
			id=idCound;
			idCound++;
		}	
	}
	@Override
	public String toString() {
		return id+". "+this.getClass().getSimpleName();
	}
	
	
}
