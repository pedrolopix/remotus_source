package event;

import model.rules.Pin;

public class EventSendRaspberry extends Event{

	private Pin pin;
	private int value;

	public EventSendRaspberry(Pin pin, int value) {
		this.pin=pin;
		this.value=value;
	}

	public Pin getPin() {
		return pin;
	}

	public void setPin(Pin pin) {
		this.pin = pin;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return super.toString()+" pin=" + pin + ", value=" + value;
	}

	
	
}