package event;

public class EventRaspberryPinChange extends Event {
	private int pin;
	private int value;

	public EventRaspberryPinChange(int pin, int value) {
		super();
		this.pin = pin;
		this.value = value;
	}

	public int getPin() {
		return pin;
	}

	public int getValue() {
		return value;
	}

	@Override
	public String toString() {
		return "[pin=" + pin + ", value=" + value+ "]";
	}
	
	
	
}
