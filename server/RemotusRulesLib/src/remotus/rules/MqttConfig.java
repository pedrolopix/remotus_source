package remotus.rules;


public class MqttConfig {


	private final MqttMyMessage mWillMsg = new MqttMyMessage();
	private final MqttMyMessage mStartMsg = new MqttMyMessage();
	private final MqttMyMessage mConfigMsg = new MqttMyMessage();
	private String mServerURI;
	private String mPassword;
	private String mUserName;
	private String mid;

	public void setServerURI(String value) {
		mServerURI=value;
	}

	public void setUserName(String value) {
		mUserName=value;
	}


	public void setPassword(String value) {
		mPassword=value;
	}

	public MqttMyMessage getStartMsg() {
		return mStartMsg;
	}

	public MqttMyMessage getWillMsg() {
		return mWillMsg;
	}

	public String getUserName() {
		return mUserName;
	}

	public String getPassword() {
		return mPassword;
	}

	public String getServerURI() {
		return mServerURI;
	}

	public MqttMyMessage getConfigMsg() {
		return mConfigMsg;
	}

	public String getId() {
		return mid;
	}

	public void setId(String mid) {
		this.mid = mid;
	}


}
