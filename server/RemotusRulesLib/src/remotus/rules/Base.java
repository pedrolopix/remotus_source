package remotus.rules;

public class Base {
	public LogIntf log;

	public Base(LogIntf log) {
		super();
		this.log = log;
	}

	public LogIntf getLog() {
		return log;
	}
}
