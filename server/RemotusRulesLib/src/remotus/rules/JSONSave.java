package remotus.rules;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import remotus.rules.actions.ActionBasePublish;
import remotus.rules.actions.ActionPublish;
import remotus.rules.actions.ActionToggle;
import remotus.rules.conditions.ConditionTime;
import remotus.rules.conditions.ConditionTopic;

public class JSONSave extends Base implements Visit, JSONTAGS{

	public JSONSave(LogIntf log) {
		super(log);
	}


	private static final String TAG = "JSONSave";
	private JSONArray mActions;
	private JSONObject mCondition;
	private JSONArray mConditions;
	private JSONObject root;

	@Override
	public void accept(ActionPublish action) {
		try {
			JSONObject json= acceptBasePublish(action);
			json.put(TAG_VALUE, action.getValue());
			if (mActions != null) {
				mActions.put(json);
			}
		} catch (JSONException e) {
			log.e(TAG, e);
		}
	}

	@Override
	public void accept(ActionToggle action) {
		JSONObject json= acceptBasePublish(action);
		mActions.put(json);
	}

	@Override
	public void acceptConditionTime(ConditionTime condition) {
		try {
			mCondition = new JSONObject();
			accept(mCondition, condition);
			mActions = new JSONArray();
			mCondition.put(TAG_ACTIONS, mActions);
			mConditions.put(mCondition);
		} catch (JSONException e) {
			log.e(TAG, e);
		}
	}

	public void accept(JSONObject json, ConditionTime condition) {
		try {
			json.put(TAG_SCHEDULING_PATTERN, condition.getSchedulingPattern());
			json.put(TAG_ACTIVE, condition.isActive());
			json.put(TAG_NAME, condition.getName());
			json.put(TAG_CLASS, condition.getClass().getSimpleName());
		} catch (JSONException e) {
			log.e(TAG, e);
		}
	}


	@Override
	public void acceptConditionTopic(ConditionTopic condition) {
		try {
			mCondition= new JSONObject();
			accept(mCondition, condition);
			JSONArray ja= new JSONArray();
			for (String item : condition.getTopics()) {
				ja.put(item);
			}
			mCondition.put(TAG_TOPICS, ja);
			mCondition.put(TAG_CONDITION, condition.getCondition());
			mActions= new JSONArray();
			mCondition.put(TAG_ACTIONS,mActions);
			mConditions.put(mCondition);

		} catch (JSONException e) {
			log.e(TAG, e);
		}
	}

	@Override
	public void accept(Rules rules) {
	}

	@Override
	public void accept(Varibales variables) {
		try {
			JSONObject json= new JSONObject();
			//maps
			JSONArray ja= new JSONArray();
			for (String item : variables.getTopics().keySet()) {
				JSONObject o= new JSONObject();
				o.put(item,variables.getTopics().get(item));
				ja.put(o);
			}

			json.put(TAG_ALIAS, ja);

			//values
			JSONArray ja1= new JSONArray();
			for (String item : variables.getVariables().keySet()) {
				JSONObject o= new JSONObject();
				o.put(item, variables.getVariables().get(item));
				ja1.put(o);
			}


			json.put(TAG_VALUES, ja1);

			root.put(TAG_VARIBALES,json);
		} catch (JSONException e) {
			log.e(TAG, e);
		}
	}

	public JSONObject acceptBasePublish(ActionBasePublish action) {
		JSONObject json= new JSONObject();
		try {
			json.put(TAG_CLASS, action.getClass().getSimpleName());
			json.put(TAG_NAME, action.getName());
			json.put(TAG_ACTIVE, action.isActive());
			json.put(TAG_DELAY, action.getDelay());
			json.put(TAG_QOS, action.getQos());
			json.put(TAG_RETAINED, action.isRetained());
			json.put(TAG_TOPIC, action.getTopic());

		} catch (JSONException e) {
			log.e(TAG, e);
		}
		return json;
	}

	public String getJSON(Rules rules, MqttConfig mqttConfig) {
		root = new JSONObject();
		try {
			root.put(TAG_VERSION, FILE_VERSION);
			root.put(TAG_MQTT, saveMqtt(root, mqttConfig));
			mConditions = new JSONArray();
			root.put(TAG_CONDITIONS, mConditions);
			rules.accept(this);
			String o = root.toString(4);
			o = o.replace("\\/", "/");
			return o;
		} catch (JSONException e) {
			log.e(TAG, e);
		}
		return null;
	}

	public void save(File file, Rules rules, MqttConfig mqttConfig) throws IOException {
		FileWriter fw = new FileWriter(file);
		fw.write(getJSON(rules, mqttConfig));
		fw.close();
	}

//	public void save(File file, Rules rules, MqttConfig mqttConfig) throws IOException {
//		root=new JSONObject();
//		try {
//			root.put(TAG_VERSION, FILE_VERSION);
//			root.put(TAG_MQTT, saveMqtt(root,mqttConfig));
//			mConditions= new JSONArray();
//			root.put(TAG_CONDITIONS, mConditions);
//			rules.accept(this);
//			FileWriter fw = new FileWriter(file);
//	        fw.write(root.toString(4));
//	        fw.close();
//		} catch (JSONException e) {
//			log.e(TAG, e);
//		}
//	}

	private JSONObject saveMqtt(JSONObject root2, MqttConfig mqttConfig) {
		JSONObject json= new JSONObject();
		try {

			mqttConfig.setId(UUID.randomUUID().toString());
			json.put(TAG_USERNAME, mqttConfig.getUserName());
			json.put(TAG_PASSWORD, mqttConfig.getPassword());
			json.put(TAG_URL, mqttConfig.getServerURI());
			json.put(TAG_ID, mqttConfig.getId());

			json.put(TAG_MQTTSTART, saveMqttMessage(mqttConfig.getStartMsg()));
			json.put(TAG_MQTTWILL, saveMqttMessage(mqttConfig.getWillMsg()));
			json.put(TAG_MQTTCONFIG, saveMqttMessage(mqttConfig.getConfigMsg()));

		} catch (JSONException e) {
			log.e(TAG, e);
		}
		return json;
	}


	private JSONObject saveMqttMessage(MqttMyMessage msg) {
		JSONObject json= new JSONObject();
		try {
			json.put(TAG_TOPIC, msg.topic);
			if (msg.message != null && !msg.message.isEmpty()) {
				json.put(TAG_MESSAGE, msg.message);
				json.put(TAG_RETAINED, msg.retained);
			}
			json.put(TAG_QOS, msg.qos);
		} catch (JSONException e) {
			log.e(TAG, e);
		}
		return json;

	}



}
