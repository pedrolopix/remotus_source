package remotus.rules;


public interface IEngine {

	boolean subscrive(String topic);
	public void unsubscrive(String topicFilter);
	void publish(String topic, String value, boolean retained, int qos);

	void schedule(String schedulingPattern, SchedulerTask task);
	ExpressionIntf GetExpression();
	public LogIntf getLog();

	void unschedule(SchedulerTask task);

}
