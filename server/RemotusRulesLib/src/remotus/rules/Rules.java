package remotus.rules;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import remotus.rules.conditions.Condition;

public class Rules implements Visitor {
	public static final ScheduledExecutorService worker =  Executors.newSingleThreadScheduledExecutor();
	List<Condition> mRules;
	Varibales mTopics;
	IEngine mEngine;
	ExpressionIntf mExpression;

	public Rules(IEngine model) {
		super();
		mEngine=model;
		mTopics= new Varibales();
		mRules= new ArrayList<Condition>();
		mExpression = model.GetExpression();
	}

	public void receivedTopic(final String topic, final String value) {
		mTopics.setVariableValue(topic, value);

		for (final Condition c : mRules) {
			Thread t= new Thread(){
				@Override
				public void run() {
					c.topicEvent(topic, value);
				};
			};
			t.start();
		}

	}

	public void addRule(Condition condition) {
		condition.setRules(this);
		mRules.add(condition);
	}

	public List<Condition> getRules() {
		return mRules;
	}

	public void addTopicAlias(String var, String topic) {
		mTopics.setTopicAlias(var, topic);
	}

	public void start() {

		for (String var : mTopics.getVariables().keySet()) {
			if (var.startsWith("/")) {
				publish(var, mTopics.getVariableValue(var), 0, false);
				subscrive(var);
			}
		}

		for (Condition c : mRules) {
			c.start();
		}
	}

	public void stop() {

		for (String var : mTopics.getVariables().keySet()) {
			if (var.startsWith("/")) {
				unsubscrive(var);
			}
		}

		for (Condition c : mRules) {
			c.stop();
		}
	}

	public void subscrive(String topic) {
		topic = mTopics.translateKey(topic);
		mEngine.subscrive(topic);
	}

	public void unsubscrive(String topic) {
		topic = mTopics.translateKey(topic);
		mEngine.unsubscrive(topic);
	}

	public void publish(String topic, String value, int qos,	boolean retained) {
		mTopics.setVariableValue(topic, value);
		mEngine.publish(topic, value, retained, qos);
	}

	public String calcExpression(String expression) {
		return mExpression.calc(translateVars(expression));
	}

	public String translateVars(String expression) {
		synchronized (mTopics) {
			return mTopics.translateVars(expression);
		}
	}

	public String translateKey(String key) {
		return mTopics.translateKey(key);
	}

	@Override
	public void accept(Visit visit) {
		mTopics.accept(visit);
		visit.accept(this);
		for (Condition c : mRules) {
			c.accept(visit);
		}
	}

	public Varibales getVariables() {
		return mTopics;
	}

	public void schedule(Object sender, String schedulingPattern, SchedulerTask task) {
		getModel().getLog().d("schedule", sender.toString() + " " + schedulingPattern);
		mEngine.schedule(schedulingPattern, task);
	}

	public void unschedule(Object conditionTime, SchedulerTask task) {
		mEngine.unschedule(task);
	}

	public void deleteTopic(String alias) {
		mTopics.deleteAlias(alias);
	}

	public void setVariableValue(String key, String value) {
		mTopics.setVariableValue(key, value);
	}

	public void deleteVariable(String name) {
		mTopics.deleteVariable(name);
	}

	public void deleteRule(Condition rule) {
		mRules.remove(rule);
	}

	public Condition getConditionId(int id) {
		for (Condition rule : mRules) {
			if (rule.getId() == id) {
				return rule;
			}
		}
		return null;
	}

	public IEngine getModel() {
		return mEngine;
	}


}
