package remotus.rules;

import java.util.HashMap;
import java.util.Map;

import remotus.rules.actions.Action;
import remotus.rules.actions.ActionPublish;
import remotus.rules.actions.ActionToggle;
import remotus.rules.conditions.Condition;
import remotus.rules.conditions.ConditionTime;
import remotus.rules.conditions.ConditionTopic;



public class Factory extends Base{
	private static final String TAG = "Factory";
	private Map<Class<? extends Condition>,String> mConditions;
	private Map<Class<? extends Action>,String> mActions;
	  
	public Factory(LogIntf log) {
		super(log);
		mConditions= new HashMap<Class<? extends Condition>, String>();
		mActions= new HashMap<Class<? extends Action>, String>();
		
		registerCondition(ConditionTime.class,"Time condition");
		registerCondition(ConditionTopic.class,"Topic condition");
		
		registerAction(ActionPublish.class,"Publish action");
		registerAction(ActionToggle.class,"Toggle action");
	}

	private void registerAction(Class<? extends Action> clazz, String name) {
		mActions.put(clazz, name);
	}

	private void registerCondition(Class<? extends Condition> clazz, String name) {
		mConditions.put(clazz,name);
	}
	
	public Condition createCondition(String clazz) {
		for (Class<? extends Condition> c : mConditions.keySet()) {
			if (c.getSimpleName().equals(clazz)) {
				try {
					return c.newInstance();
				} catch (InstantiationException e) {
					log.e(TAG, e);
				} catch (IllegalAccessException e) {
					log.e(TAG, e);
				}
			}
		}
		
		return null;
	}
	
	public Action createAction(String clazz) {
		for (Class<? extends Action> c : mActions.keySet()) {
			if (c.getSimpleName().equals(clazz)) {
				try {
					return c.newInstance();
				} catch (InstantiationException e) {
					log.e(TAG, e);
				} catch (IllegalAccessException e) {
					log.e(TAG, e);
				}
			}
		}
		
		return null;
	}
	
	
}
