package remotus.rules;

import remotus.rules.actions.ActionPublish;
import remotus.rules.actions.ActionToggle;
import remotus.rules.conditions.ConditionTime;
import remotus.rules.conditions.ConditionTopic;


public interface Visit {
	void acceptConditionTopic(ConditionTopic condition);

	void acceptConditionTime(ConditionTime condition);
	void accept(ActionPublish action);
	void accept(ActionToggle action);
	void accept(Rules rules);
	void accept(Varibales topics);
}
