package remotus.rules;

import it.sauronsoftware.cron4j.Task;

public abstract class SchedulerTask extends Task {

	private String mId;

	public void setId(String id) {
		mId = id;
	}

	public String getId() {
		return mId;
	}

}
