package remotus.rules;

public interface Visitor {
	void accept(Visit visit);
}
