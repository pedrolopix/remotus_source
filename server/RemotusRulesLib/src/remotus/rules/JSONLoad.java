package remotus.rules;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import remotus.rules.actions.Action;
import remotus.rules.actions.ActionBasePublish;
import remotus.rules.actions.ActionPublish;
import remotus.rules.actions.ActionToggle;
import remotus.rules.conditions.Condition;
import remotus.rules.conditions.ConditionTime;
import remotus.rules.conditions.ConditionTopic;

public class JSONLoad extends Base implements JSONTAGS{

	private static final String TAG = "JSONLoad";
	private JSONObject root;
	private Rules mRules;
	private final Factory factory;
	private MqttConfig mMqttConfig;


	public JSONLoad(LogIntf log) {
		super(log);
		factory= new Factory(log);
	}

	public boolean setJSON(String json, Rules rules, MqttConfig mqttConfig) {
		this.mRules=rules;
		this.mMqttConfig=mqttConfig;
		root=new JSONObject();
		try {
			root = new JSONObject(json);

			// verifica versão
		    int version=root.getInt(TAG_VERSION);
		    if (version!=FILE_VERSION) {
				log.e(TAG, "Ficheiro com versão errada");
		    	return false;
		    }

			JSONObject json1;
			json1 = root.getJSONObject(TAG_MQTT);
			loadMqtt(json1);

			json1 = root.optJSONObject(TAG_VARIBALES);
		    if (json!=null) {
				loadVariables(json1);
		    }

		    JSONArray jsona = root.getJSONArray(TAG_CONDITIONS);
		    loadConditions(jsona);

		    return true;
		} catch (JSONException e) {
			log.e(TAG,e);
			return false;
		}
	}

	public boolean load(File file, Rules rules, MqttConfig mqttConfig) throws IOException {

		StringBuffer fileData = new StringBuffer();
		BufferedReader reader = new BufferedReader(new FileReader(file));
		char[] buf = new char[1024];
		int numRead = 0;
		while ((numRead = reader.read(buf)) != -1) {
			String readData = String.valueOf(buf, 0, numRead);
			fileData.append(readData);
		}
		reader.close();
		return setJSON(fileData.toString(), rules, mqttConfig);
	}

	private void loadMqtt(JSONObject json) {
		if (json==null) {
			return;
		}
		try {
			loadMessage(json.getJSONObject(TAG_MQTTSTART), mMqttConfig.getStartMsg());
			loadMessage(json.getJSONObject(TAG_MQTTWILL), mMqttConfig.getWillMsg());
			loadMessage(json.getJSONObject(TAG_MQTTCONFIG), mMqttConfig.getConfigMsg());

			String s;
			s = json.optString(TAG_URL, "");
			mMqttConfig.setServerURI(s);
			s = json.optString(TAG_USERNAME, "");
			mMqttConfig.setUserName(s);
			s = json.optString(TAG_PASSWORD, "");
			mMqttConfig.setPassword(s);
			s = json.optString(TAG_ID, "");
			mMqttConfig.setId(s);

		} catch (JSONException e) {
			log.e(TAG, "mqtt", e);
		}
	}

	private void loadMessage(JSONObject json, MqttMyMessage msg) {
		if (json==null) {
			return;
		}
		try {
			msg.topic=json.getString(TAG_TOPIC);
			msg.message = json.optString(TAG_MESSAGE, "");
			msg.qos = json.getInt(TAG_QOS);
			msg.retained = json.optBoolean(TAG_RETAINED, true);

		} catch (JSONException e) {
			log.e(TAG, "mqtt", e);
		}
	}

	private void loadConditions(JSONArray jsona) {
		if (jsona==null) {
			return;
		}
		try {
			for(int i=0; i<jsona.length(); i++) {
				JSONObject json = jsona.getJSONObject(i);
				String clazz=json.getString(TAG_CLASS);
				Condition condition=factory.createCondition(clazz);
				if (condition==null) {
					log.e(TAG, "Não existe a condição " + clazz);
				} else {
					loadCondition(json, condition);
				}
			}
		} catch (JSONException e) {
			log.e(TAG, "Conditions", e);
		}
	}

	private void loadCondition(JSONObject json, Condition condition) {
		if (condition==null) {
			return;
		}
		condition.setActive(json.optBoolean(TAG_ACTIVE,true));
		// condition.setName(json.optString(TAG_MESSAGE,condition.getClass().getSimpleName()));
		condition.setName(json.optString(TAG_NAME, ""));
		if (condition instanceof ConditionTopic) {
			ConditionTopic ct = (ConditionTopic) condition;

			loadConditionTopic(json, ct);
		}
		else if (condition instanceof ConditionTime) {
			ConditionTime ct = (ConditionTime) condition;
			loadConditionTime(json, ct);
		}
		try {
			JSONArray actions = json.optJSONArray(TAG_ACTIONS);
			if (actions==null) {
				log.i(TAG, "Rule " + condition + " sem ações");
			}
			loadActions(actions,condition);
			mRules.addRule(condition);
		} catch (JSONException e) {
			log.e(TAG, e);
		}
	}

	private void loadActions(JSONArray actions, Condition condition) throws JSONException {
		if (actions==null) {
			return;
		}
		for(int i=0;i<actions.length();i++) {
			loadAction(actions.getJSONObject(i),condition);
		}
	}

	private void loadAction(JSONObject json, Condition condition) {
		if (json==null) {
			return;
		}
		try {
			String clazz=json.getString(TAG_CLASS);
			if (clazz==null) {
				log.i(TAG, "classe de ac????o est?? vazio");
			}
			Action action=factory.createAction(clazz);
			if (action==null) {
				log.e(TAG,"N??o existe a ac????o "+clazz);
				return;
			}

			if (action instanceof ActionPublish) {
				ActionPublish a = (ActionPublish) action;
				loadActionPublic(json, a);
			} else
			if (action instanceof ActionToggle) {
				ActionToggle a = (ActionToggle) action;
				loadActionToggle(json, a);
			}

			condition.addAction(action);
		} catch (JSONException e) {
			log.e(TAG,e);
		}
	}

	private void loadActionBasePublish(JSONObject json, ActionBasePublish a) {
		try {
			String topic=json.getString(TAG_TOPIC);
			int qos=json.getInt(TAG_QOS);
			boolean retained=json.getBoolean(TAG_RETAINED);
			int delay=json.getInt(TAG_DELAY);
			boolean active = json.optBoolean(TAG_ACTIVE, true);
			String name = json.optString(TAG_NAME, "");
			a.set(active, name, topic, "", qos, retained, delay);
		} catch (JSONException e) {
			log.e(TAG, e);
		}

	}

	private void loadActionPublic(JSONObject json, ActionPublish a) {
		loadActionBasePublish(json, a);
		String value;
		try {
			value = json.getString(TAG_VALUE);
			a.setValue(value);
		} catch (JSONException e) {
			log.e(TAG, e);
		}
	}

	private void loadActionToggle(JSONObject json, ActionToggle a) {
		loadActionBasePublish(json, a);
	}

	private void loadConditionTime(JSONObject json, ConditionTime condition) {
		String c = json.optString(TAG_SCHEDULING_PATTERN);
		condition.setSchedulingPattern(c);
	}

	private void loadConditionTopic(JSONObject json, ConditionTopic condition) {
		try {
			loadConditionTime(json, condition);
			String c = json.optString(TAG_CONDITION);
			condition.setCondition(c);
			JSONArray topics = json.optJSONArray(TAG_TOPICS);
			if (topics != null) {
				for (int i = 0; i < topics.length(); i++) {
					condition.addTopic(topics.getString(i));
				}
			}
		} catch (JSONException e) {
			log.e(TAG,e);
		}
	}

//	private void loadVariables(JSONObject json) {
//		if (json==null) return;
//		try {
//			JSONObject values=json.getJSONObject(TAG_VALUES);
//			if (values!=null) {
//				String[] names = JSONObject.getNames(values);
//
//				for (String name : names) {
//					mRules.getVariables().setValue(name, values.getString(name));
//				}
//			}
//			JSONObject alias=json.getJSONObject(TAG_ALIAS);
//			if (alias!=null) {
//				String[] names = JSONObject.getNames(alias);
//				for (String name : names) {
//					mRules.addTopicAlias(name, alias.getString(name));
//				}
//			}
//		} catch (JSONException e) {
//			log.e(TAG,"variables.values", e);
//		}
//
//	}

	private void loadVariables(JSONObject json) {
		if (json==null) {
			return;
		}
		try {
			JSONArray values=json.optJSONArray(TAG_VALUES);
			if (values!=null) {
				for (int i=0; i<values.length(); i++) {
					JSONObject o=values.getJSONObject(i);
					Iterator<String> iter = o.keys();

					while (iter.hasNext()) {
						String name=iter.next();
						mRules.getVariables().setVariableValue(name, o.getString(name));
					}
				}
			}
			JSONArray alias=json.optJSONArray(TAG_ALIAS);
			if (alias!=null) {
//				String[] names = JSONObject.getNames(alias);
//				for (String name : names) {
//					mRules.addTopicAlias(name, alias.getString(name));
//				}
				for (int i=0; i<alias.length(); i++) {
					JSONObject o=alias.getJSONObject(i);
					Iterator<String> iter = o.keys();

					while (iter.hasNext()) {
						String name=iter.next();
						mRules.addTopicAlias(name, o.getString(name));
					}
				}
			}
		} catch (JSONException e) {
			log.e(TAG,"variables.values", e);
		}

	}


}
