package remotus.rules;

import java.util.Calendar;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Varibales {
	private final BiMap<String, String> topics;
	private final Map<String, String> variables;

	public Varibales() {
		super();
		variables= new ConcurrentHashMap<String, String>();
		topics= new BiMap<String,String>();
	}

	public void setTopicAlias(String alias, String topic) {
		topics.put(alias, topic);
	}

	public void setVariableValue(String key, String value) {
		String s=translateKey(key);
		variables.put(s, value);
	}

	public String getVariableValue(String key) {
		if (key == null) {
			return "";
		}
		String s=translateKey(key);
		return variables.get(s);
	}

	public String translateKey(String key) {
		if (key.equalsIgnoreCase("day")) {
			return String.valueOf(Calendar.getInstance().get(Calendar.DAY_OF_WEEK));
		}
		if (key.equalsIgnoreCase( "month")) {
			return String.valueOf(Calendar.getInstance().get(Calendar.MONTH));
		}
		if (key.equalsIgnoreCase( "year")) {
			return String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
		}
		if (key.equalsIgnoreCase( "hour")) {
			return String.valueOf(Calendar.getInstance().get(Calendar.HOUR));
		}
		if (key.equalsIgnoreCase( "minute")) {
			return String.valueOf(Calendar.getInstance().get(Calendar.MINUTE));
		}
		if (key.equalsIgnoreCase( "second")) {
			return String.valueOf(Calendar.getInstance().get(Calendar.SECOND));
		}
		if (key.equalsIgnoreCase( "millisecond")) {
			return String.valueOf(Calendar.getInstance().get(Calendar.MILLISECOND));
		}


		String k=topics.get(key);
		if (k==null) {
			return key;
		} else {
			return k;
		}
	}

	public String translateVars(String var) {
		for (String v : topics.keySet()) {
			String value = getVariableValue(v);
			if (value!=null) {
				var=var.replaceAll(v, value);
			}
		}
		for (String v : variables.keySet()) {
			String value=variables.get(v);
			if (value!=null) {
				var=var.replaceAll(v, value);
			}
		}
		return var;
	}


	public BiMap<String, String> getTopics() {
		return topics;
	}

	public Map<String, String> getVariables() {
		return variables;
	}

	public void accept(Visit visit) {
		visit.accept(this);
	}

	public String[] variablesToStringArray() {
		String[] v = new String[variables.size()];
		int i=0;
		for (String item : variables.keySet()) {
			v[i] = item + " = " + variables.get(item);
			i++;
		}
		return v;
	}

	public void deleteVariable(String key) {
		variables.remove(key);
	}

	public void deleteAlias(String alias) {
		topics.removeByKey(alias);
	}

	public String[] topicAliasToStringArray() {
		String[] v = new String[topics.getMap().size()];
		int i = 0;
		for (String item : topics.getMap().keySet()) {
			v[i] = item + " = " + topics.getMap().get(item);
			i++;
		}
		return v;
	}

	public String getTopicAlias(String alias) {
		if (alias == null) {
			return "";
		}
		String k = topics.get(alias);
		if (k == null) {
			return "";
		} else {
			return k;
		}
	}

}
