package remotus.rules.actions;

import remotus.rules.Visit;

public class ActionToggle extends ActionBasePublish {
	@Override
	public void execute() {
		mValue="=!"+mTopic;
		super.execute();
	}

	@Override
	protected String beforePublish(String v) {
		boolean b=v=="1";
		return b?"1":"0";
	}

	@Override
	public void accept(Visit visit) {
		visit.accept(this);
	}
}
