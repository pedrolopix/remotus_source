package remotus.rules.actions;

import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import remotus.rules.Rules;

public abstract class ActionBasePublish extends Action{
	protected int mDelay;
	protected String mTopic;
	protected String mValue;
	protected int mQos;
	boolean mRetained;
	private ScheduledFuture<?> sheduled;

	@Override
	public void execute() {
		if (!mActive) {
			return;
		}
		if (mValue==null || mValue.isEmpty()) {
			mValue="=value";
		}
		if (sheduled!=null) {
			if (!sheduled.isDone()) {
				sheduled.cancel(false);
			}
		}
		Runnable task = new Runnable() {
		    @Override
			public void run() {
		    	String v=mCondition.translateVars(mValue);
		    	v= mRules.calcExpression(v);
		    	v=beforePublish(v);
		    	mRules.publish( mTopic, v, mQos, mRetained);
		    }
		  };
		sheduled= Rules.worker.schedule(task, mDelay, TimeUnit.MILLISECONDS);
	}


	protected String beforePublish(String v) {
		return v;
	}


	public void set(boolean active, String name, String topic, String value, int qos, boolean retained, int delay) {
		mTopic=topic;
		mValue=value;
		mQos=qos;
		mRetained=retained;
		mDelay=delay;
		mActive=active;
		mName = name;

	}

	@Override
	public void start() {
		super.start();
		mRules.subscrive(mTopic);
	}

	@Override
	public void stop() {
		mRules.unsubscrive(mTopic);
		super.stop();
	}

	public int getDelay() {
		return mDelay;
	}

	public void setDelay(int delay) {
		this.mDelay = delay;
	}

	public String getTopic() {
		return mTopic;
	}

	public void setTopic(String topic) {
		this.mTopic = topic;
	}

	public String getValue() {
		return mValue;
	}

	public void setValue(String value) {
		this.mValue = value;
	}

	public Object getQos() {
		return mQos;
	}

	public void setQos(int qos) {
		this.mQos = qos;
	}

	public boolean isRetained() {
		return mRetained;
	}

	public void setRetained(boolean retained) {
		this.mRetained = retained;
	}

}
