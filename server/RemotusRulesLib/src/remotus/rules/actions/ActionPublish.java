package remotus.rules.actions;

import remotus.rules.Visit;

public class ActionPublish extends ActionBasePublish {

	@Override
	public void accept(Visit visit) {
		visit.accept(this);
	}

}
