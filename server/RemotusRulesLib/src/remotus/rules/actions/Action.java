package remotus.rules.actions;

import remotus.rules.Rules;
import remotus.rules.Visitor;
import remotus.rules.conditions.Condition;

public abstract  class Action  implements Visitor{
	Rules mRules;
	Condition mCondition;
	Boolean mActive;
	int mId;
	static int mLastId = 0;
	String mName;

	public Action() {
		super();
		mId = mLastId++;
		mName = "Action " + mId;
	}

	public void setRules(Rules rules) {
		this.mRules=rules;
		mActive = true;

	}

	public void setCondition(Condition condition) {
		this.mCondition=condition;
	}

	public abstract void execute();

	public void start() {
	}

	public void stop() {
	}

	public Boolean isActive() {
		return mActive;
	}

	public void setActive(Boolean active) {
		this.mActive = active;
	}

	public int getId() {
		return mId;
	}

	public String getName() {
		return mName;
	}

	public void setName(String name) {
		this.mName = name;
	}

	@Override
	public String toString() {
		return mName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + mId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Action other = (Action) obj;
		if (mId != other.mId) {
			return false;
		}
		return true;
	}


}
