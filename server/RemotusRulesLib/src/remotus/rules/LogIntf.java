package remotus.rules;

public interface LogIntf {

	public  void e(String tag, Throwable e) ;
	public  void e(String tag, String msg);
	public  void d(String tag, String msg);
	public  void i(String tag, String msg) ;
	public  void e(String tag, String msg, Throwable e);
}
