package remotus.rules;

public interface JSONTAGS {
	static final int FILE_VERSION = 1;

	static final String TAG_ACTIONS = "actions";
	static final String TAG_ALIAS = "alias";
	static final String TAG_CLASS = "class";
	static final String TAG_CONDITIONS = "conditions";
	static final String TAG_CONDITION = "condition";
	static final String TAG_DELAY = "delay";
	static final String TAG_QOS = "qos";
	static final String TAG_RETAINED = "retainded";
	static final String TAG_SCHEDULING_PATTERN = "schedulingPattern";
	static final String TAG_TOPIC = "topic";
	static final String TAG_TOPICS = "topics";
	static final String TAG_VALUE = "value";
	static final String TAG_VALUES = "values";
	static final String TAG_VARIBALES = "variables";
	static final String TAG_VERSION = "version";
    static final String TAG_MQTT = "mqtt";
	static final String TAG_USERNAME = "username";
	static final String TAG_PASSWORD = "password";
	static final String TAG_URL = "url";
	static final String TAG_ID = "ID";
    static final String TAG_MQTTSTART = "start";
	static final String TAG_MQTTWILL = "will";
	static final String TAG_MQTTCONFIG = "config";
    static final String TAG_MESSAGE = "message";
    static final String TAG_NAME = "name";
	static final String TAG_ACTIVE = "active";
}
