package remotus.rules.conditions;

import java.util.ArrayList;
import java.util.List;
import remotus.rules.LogIntf;
import remotus.rules.Rules;
import remotus.rules.Visit;
import remotus.rules.Visitor;
import remotus.rules.actions.Action;

public abstract class Condition implements Visitor{
	Rules mRules;
	List<Action> mActions;
	private String mName;
	private boolean mActive;
	private final int id;
	private static int lastid = 0;
	private Boolean mExecution = false;

	public Condition() {
		super();
		id = lastid++;
		mActions=new ArrayList<Action>();
		mName = "Rule " + id;
	}

	public abstract void topicEvent(String topic, String value) ;

	public LogIntf getLog() {
		return mRules.getModel().getLog();
	}

	protected void executeAction() {
		if (!isActive()) {
			getLog().d("rule", toString() + " is not active!");
			return;
		}
		synchronized (mExecution) {
			if (mExecution) {
				getLog().d("rule", toString() + " in execution... canceled");
				return;
			}
		}
		synchronized (mExecution) {
			mExecution = true;
		}
		try {
			getLog().d("rule", toString() + " execution start");
			for (Action action : mActions) {
				action.execute();
			}
			getLog().d("rule", toString() + " execution end");
		} finally {
			synchronized (mExecution) {
				mExecution = false;
			}
		}
	}

	public void setRules(Rules rules) {
		this.mRules=rules;
		for (Action action : mActions) {
			action.setRules(rules);
		}
	}

	public void addAction(Action action) {
		action.setRules(mRules);
		action.setCondition(this);
		mActions.add(action);
	}

	public void start() {
		if (!isActive()) {
			return;
		}
		for (Action action : mActions) {
			action.start();
		}
	}

	public void stop() {
		if (!isActive()) {
			return;
		}
		for (Action action : mActions) {
			action.stop();
		}
	}

	public String translateVars(String mValue) {
		return mValue;
	}

	@Override
	public void accept(Visit visit) {
		for (Action action : mActions) {
			action.accept(visit);
		}
	}

	public boolean isActive() {
		return mActive;
	}

	public void setActive(boolean mActive) {
		this.mActive = mActive;
	}

	public String getName() {
		return mName;
	}

	public void setName(String mName) {
		this.mName = mName;
	}

	public int getId() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Condition other = (Condition) obj;
		if (id != other.id) {
			return false;
		}
		return true;
	}

	public List<Action> getActions() {
		return mActions;
	}

	@Override
	public String toString() {
		return mName;
	}

	public Action getActionID(int id) {
		for (Action action : mActions) {
			if (action.getId() == id) {
				return action;
			}
		}
		return null;
	}



}
