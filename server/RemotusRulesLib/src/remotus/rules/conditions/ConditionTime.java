package remotus.rules.conditions;


import it.sauronsoftware.cron4j.TaskExecutionContext;
import remotus.rules.SchedulerTask;
import remotus.rules.Visit;
import remotus.rules.actions.Action;

public class ConditionTime extends Condition {
	public class MyTask extends SchedulerTask {

		@Override
		public boolean canBePaused() {
			return false;
		}

		@Override
		public boolean canBeStopped() {
			return true;
		}

		@Override
		public boolean supportsCompletenessTracking() {
			return true;
		}

		@Override
		public boolean supportsStatusTracking() {
			return true;
		}

		@Override
		public void execute(TaskExecutionContext context) throws RuntimeException {
			getLog().d("rule", ConditionTime.this.toString() + " scheduler start");
			executeAction();
		}
	}

	private String mSchedulingPattern;
	MyTask task = null;//new MyTask(); // no android d�� erro a criar

	private MyTask getTask() {
		if (task == null) {
			task = new MyTask();
		}
		return task;
	}

	@Override
	public void topicEvent(String topic, String value) {
		//n??o implementado, este condi????o s?? interpreta timeEvents
	}

	@Override
	public void accept(Visit visit) {
		visit.acceptConditionTime(this);
		for (Action action : mActions) {
			action.accept(visit);
		}
	}

	@Override
	public void start() {

		if (mSchedulingPattern!=null && !mSchedulingPattern.isEmpty()) {
			mRules.schedule(this, mSchedulingPattern, getTask());
		}
		super.start();
	}

	@Override
	public void stop() {
		if (mSchedulingPattern != null && !mSchedulingPattern.isEmpty()) {
			mRules.unschedule(this, getTask());
		}
		super.stop();
	}

	public String getSchedulingPattern() {
		return mSchedulingPattern;
	}

	public void setSchedulingPattern(String schedulingPattern) {
		this.mSchedulingPattern = schedulingPattern;
	}

}
