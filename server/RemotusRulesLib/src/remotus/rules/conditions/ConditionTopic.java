package remotus.rules.conditions;

import java.util.ArrayList;
import java.util.List;
import remotus.rules.Visit;
import remotus.rules.actions.Action;

public class ConditionTopic extends ConditionTime {
	private static final String TAG_VALUE = "value";
	private final List<String> mTopics;
	private String mCondition;

	public ConditionTopic() {
		super();
		mTopics= new ArrayList<String>();
	}

	@Override
	public void topicEvent(String topic, String value) {
		for (String s : mTopics) {
			s=mRules.translateKey(s);
			if (topic.equals(s)) {
				  executeAction();
				  return;
			}
		}
	}

	private boolean executeCondition() {
		if (mCondition==null) {
			return true;
		}
		String v=translateVars("="+mCondition);
    	v= mRules.calcExpression(v);
		getLog().d("rule", toString() + " condition: (" + mCondition + ") => " + v);
		return v.equals("1")?true:false;
	}

	@Override
	protected void executeAction() {
		if (executeCondition()) {
			super.executeAction();
		}
	}

	@Override
	public void start() {
		super.start();
		for (String s : mTopics) {
			mRules.subscrive(s);
		}
	}

	@Override
	public void stop() {
		for (String s : mTopics) {
			mRules.unsubscrive(s);
		}
		super.stop();
	}

	public void addTopic(String topic) {
		mTopics.add(topic);
	}

	@Override
	public String translateVars(String expression) {
		String tag=TAG_VALUE;
		String t=tag;
		String v=super.translateVars(expression);
		for (int i=0; i<mTopics.size(); i++) {
			v=v.replaceAll(t, mTopics.get(i));
			t= tag+i+1;
		}
		return v;
	}

	@Override
	public void accept(Visit visit) {
		visit.acceptConditionTopic(this);
		for (Action action : mActions) {
			action.accept(visit);
		}
	}

	public List<String> getTopics() {
		return mTopics;
	}

	public String getCondition() {
		return mCondition;
	}

	public void setCondition(String condition) {
		this.mCondition = condition;
	}

}


