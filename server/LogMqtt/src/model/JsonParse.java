package model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import model.rules.Rules;
import model.rules.Topic;
import mqtt.MqttEngine;
import mqtt.MqttMyMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JsonParse {

	private static final String WILL = "will";
	private static final String START = "start";
	private static final String TOPIC = "topic";
	private static final String TOPICS = "topics";
	private static final String PASSWORD = "password";
	private static final String USER = "user";
	private static final String URI = "uri";
	private static final String MQTT = "mqtt";
	private static final String VERSION = "version";
	private static final String MESSAGE = "message";
	private static final String QOS = "qos";
	private static final String RETAINED = "retained";
	private static final String DATABASE = "database";
	private static final String DBTYPE = "type";
	private static final String DBHOST = "host";
	private static final String DBPORT = "port";
	private static final String DBNAME = "name";
	private static final String DBUSER = "user";
	private static final String DBPASSWORD = "password";
	private static final String DESTINATIONTOPIC = "destination";

/*
	private void pinToJSON(Pin pin, JSONObject json) throws JSONException {
		json.put(PIN,pin.getId());
		json.put(TOPIC,pin.getTopic());
		json.put(QOS,pin.getQos());
		json.put(RETAINED,pin.getRetained());
		json.put(ACTIVE,pin.isActive());
		json.put(PIN_TYPE, pin.getType().toString());
		json.put(THRESHOLD, pin.getThreshold());
	}
	*/
	private void JSONToTopic(Rules rules, JSONObject json) throws JSONException {		
		String topic=json.getString(TOPIC);
		Topic t = new Topic();
		t.setTopic(topic);
		rules.addTopic(t);
	}
	
	/*
	public void rulesToJSON(Rules rules, JSONObject json) throws JSONException {
		JSONArray arr;
		arr= new JSONArray();
		for (Pin pin : rules.getPins()) {
			JSONObject value=new JSONObject();
			pinToJSON(pin,value);
			arr.put(value);
		}
		json.put(PINS, arr);
		
		JSONObject value=new JSONObject();
		MessageToJson(rules.getWillMsg(),value);
		json.put(WILL, value);
		value=new JSONObject();
		MessageToJson(rules.getStartMsg(),value);
		json.put(START, value);
		
	}

	private void MessageToJson(MqttMyMessage msg, JSONObject json) throws JSONException {
		json.put(TOPIC, msg.topic);
		json.put(MESSAGE, msg.message);
		json.put(QOS, msg.qos);
		json.put(RETAINED, msg.retained);
	}
*/
	private void JsonToMessage(MqttMyMessage msg, JSONObject json) throws JSONException {
		msg.topic=json.getString(TOPIC);
		msg.message=json.getString(MESSAGE);
		msg.qos=json.getInt(QOS);
		msg.retained=json.getBoolean(RETAINED);
	}
	
	private void jsonToDatabase(Database database, JSONObject json) throws JSONException {
		database.setType(json.getString(DBTYPE));
		database.setHost(json.getString(DBHOST));
		database.setPort(json.getInt(DBPORT));
		database.setName(json.getString(DBNAME));
		database.setUser(json.getString(DBUSER));
		database.setPassword(json.getString(DBPASSWORD));
	}	
	
	private void jsonToRules(Rules rules, JSONObject json) throws JSONException {
		JSONArray arr;
		arr=json.optJSONArray(TOPICS);
		if (arr!=null) {
			for (int i = 0; i < arr.length(); i++) {
				JSONToTopic(rules,arr.getJSONObject(i));
			}
		}
		
		JSONObject o;
		o=json.getJSONObject(WILL);
		JsonToMessage(rules.getWillMsg(),o);
		o=json.getJSONObject(START);
		JsonToMessage(rules.getStartMsg(),o);
	}
	


	/*
	
	private void mqttToJson(MqttEngine mqtt, JSONObject json) throws JSONException {
		json.put(URI, mqtt.getServerURI());
		json.put(USER, mqtt.getUserName());
		json.put(PASSWORD, mqtt.getPassword());
	}
*/	
	private void jsonToMqtt(MqttEngine mqtt, JSONObject json) throws JSONException {
		mqtt.setServerURI(json.getString(URI));
		mqtt.setUserName(json.getString(USER));
		mqtt.setPassword(json.getString(PASSWORD));
	}
	
	private void jsonToDestinationTopic(Topic topic, JSONObject json) throws JSONException {
		topic.setTopic(json.getString(TOPIC));
		topic.setRetained(json.getBoolean(RETAINED));
		topic.setQos(json.getInt(QOS));
	}

	/*	
	public void save( MqttEngine mqtt, Rules rules, File file) throws JSONException, IOException {
		
		rules.setWillMsg(mqtt.getWillMsg());
		rules.setStartMsg(mqtt.getStartMsg());
		
		JSONObject json= new JSONObject();
		json.put(VERSION, 1);
		JSONObject aux = new  JSONObject();

		
		aux = new  JSONObject();
		json.put(MQTT, aux);
		mqttToJson(mqtt,aux);
		
		rulesToJSON(rules,json);
		
        FileWriter fw = new FileWriter(file);
        fw.write(json.toString());
        fw.close();
	}
*/
	
	public JSONObject topicToJson(Rules rules) throws JSONException, IOException {
		JSONObject json= new JSONObject();
		json.put(VERSION, 1);
		rulesToJSON(rules, json);
		return json;
	}
	
	public void rulesToJSON(Rules rules, JSONObject json) throws JSONException {
		JSONArray arr = new JSONArray();
		for (Topic t : rules.getTopics()) {
			JSONObject value=new JSONObject();
			value.put(TOPIC,t.getTopic());
			arr.put(value);
		}
		json.put(TOPICS, arr);
	}

	public void load(MqttEngine mqtt, Rules rules, Database db, File file, Topic destinationTopic) throws IOException, JSONException, JsonParseException {
		StringBuffer fileData = new StringBuffer();
        BufferedReader reader = new BufferedReader(new FileReader(file));
        char[] buf = new char[1024];
        int numRead=0;
        while((numRead=reader.read(buf)) != -1){
            String readData = String.valueOf(buf, 0, numRead);
            fileData.append(readData);
        }
        reader.close();
		JSONObject json= new JSONObject(fileData.toString());
		int version= json.getInt(VERSION);
		if (version!=1) throw new JsonParseException("Versão do ficheiro não suportada!");
		
		jsonToMqtt(mqtt,json.getJSONObject(MQTT));
		jsonToRules(rules,json);
		jsonToDatabase(db,json.getJSONObject(DATABASE));
		jsonToDestinationTopic(destinationTopic,json.getJSONObject(DESTINATIONTOPIC));
		
		mqtt.setStartMsg(rules.getStartMsg());
		mqtt.setWillMsg(rules.getWillMsg());
		
	}

}
