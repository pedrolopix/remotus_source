package model;

public class JsonParseException extends Exception {

	public JsonParseException(String string) {
		super(string);
	}

	private static final long serialVersionUID = 1L;

}
