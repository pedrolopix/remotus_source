package model.rules;

public class Topic {

	private String topic;
	private int qos;
	private boolean retained;

	public Topic() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}
	
	public int getQos() {
		return qos;
	}

	public void setQos(int qos) {
		this.qos = qos;
	}

	public boolean isRetained() {
		return retained;
	}

	public void setRetained(boolean retained) {
		this.retained = retained;
	}
	
	
	
}
