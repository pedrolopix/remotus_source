package model.rules;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import mqtt.MqttMyMessage;

public class Rules {
	private MqttMyMessage willMsg;
	private MqttMyMessage startMsg;
	private List<Topic> topics;

	public Rules() {
		super();
		willMsg= new MqttMyMessage();
		startMsg= new MqttMyMessage();
		topics = new ArrayList<>();
	}


	@Override
	public String toString() {
		return topics.toString();
	}

	public MqttMyMessage getWillMsg() {
		return willMsg;
	}
	
	public MqttMyMessage getStartMsg() {
		return startMsg;
	}

	public void setWillMsg(MqttMyMessage msg) {
		this.willMsg=msg;
	}

	public void setStartMsg(MqttMyMessage msg) {
		this.startMsg=msg;
	}


	public void addTopic(Topic topic) {
		this.topics.add(topic);
	}

	public Collection<Topic> getTopics() {
		return topics;
	}
	



	
	
}
