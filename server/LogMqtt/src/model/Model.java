package model;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import net.engio.mbassy.BusConfiguration;
import net.engio.mbassy.listener.Listener;

import org.json.JSONException;
import org.json.JSONObject;

import database.DatabaseException;
import database.DatabaseManager;
import model.rules.Rules;
import model.rules.Topic;
import mqtt.MqttEngine;
import event.EventBus;
import event.EventMqttMessageArrived;

public class Model {
	public enum State {START, MQTT_CONNECTING, MQTT_CONNECTED, RUNNING, ERROR, ARDUINO_ERROR, CLOSED,
		DATABASE_CONNECTING, DATABASE_CONNECTED }
	private static final String TAG = "Model";
	private EventBus eventBus;
	private Log log;
	private MqttEngine mqttEngine;
	private State state;	
	private Rules rules;
	private DatabaseManager database;
	private Database db;
	private Topic destinationTopic;
	
	public Model() {
		super();
		log= new Log(this);
		eventBus=new EventBus(BusConfiguration.Default());
		mqttEngine=new MqttEngine(this);
		db = new Database();
		destinationTopic = new Topic();
		//rules = new Rules();
		//teste();
		state=State.START;
		getEventBus().subscribe(this);
	}
	
	private void teste() {
		//mqttEngine.setServerURI("tcp://127.0.0.1:1883");
		mqttEngine.setServerURI("tcp://192.168.1.10:1883");
		mqttEngine.setUserName("carlos");
		mqttEngine.setPassword("carlos");
		
		Topic t = new Topic();
		t.setTopic("/arduino/1/luz");
		rules.addTopic(t);
		Topic t1 = new Topic();
		t1.setTopic("/arduino/1/switch");
		rules.addTopic(t1);
		
		db.setType("MySql");
		db.setHost("192.168.1.10");
		db.setPort(3306);
		db.setName("LogMqtt");
		db.setUser("root");
		db.setPassword("pi");
		//database = DatabaseManager.getInstance("MySql", "192.168.1.10", "3306", "LogMqtt", "root", "pi");
	}
	
	public void close(){
		getEventBus().unsubscribe(this);
		mqttEngine.stop();
		database.stop();
		setState(State.CLOSED);
	}
	
	public void setState(State state) {
		log.d("mqttstate", this.state.toString()+"->"+state.toString());
		this.state=state;
	}
	
	public void start() {
		if (state!=State.START) {
			close();
		}
		
		while (true) {
			switch (state) {
			case START:
				connectMqtt();
				break;
			case MQTT_CONNECTING:
				break;
			case MQTT_CONNECTED:
				databaseStart();
				break;
			case DATABASE_CONNECTING:
				break;
			case DATABASE_CONNECTED:
				initTopics();
				setState(State.RUNNING);
				break;
			case RUNNING:
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					setState(State.ERROR);
				}
				break;
			case ERROR:	
				close();
				break;
			case CLOSED:
				return;
			default:
				break;
			}	
		}
	}
	
	private void initTopics() {
		for (Topic t : rules.getTopics()) {
			this.getLog().d(TAG, "Subcribe topic" +t.getTopic());
			this.subscrive(t.getTopic());
		}
		sendTopics();
	}
	
	private void sendTopics() {
		//mqttEngine.publish("/carlos/1", "ribeiro12222", true, 0);
		JsonParse jp= new JsonParse();
		JSONObject a = null;
		try {
			a = jp.topicToJson(rules);
		} catch (JSONException | IOException e) {
			getLog().d(TAG, "Ocorreu um erro ao converter os t��picos para publicar.");
		} finally{
			getLog().d(TAG, "Atualizar a lista de t��picos");
			mqttEngine.publish(destinationTopic.getTopic(), a.toString(), destinationTopic.isRetained(), destinationTopic.getQos());
		}
	}

	public void subscrive(String topic) {
		mqttEngine.subscrive(topic);
	}

	private void databaseStart() {
		setState(State.DATABASE_CONNECTING);
		try {
			database = DatabaseManager.getInstance(db.getType(), db.getHost(), ""+db.getPort(), db.getName(), db.getUser(), db.getPassword());
			if (database.isValid())
				setState(State.DATABASE_CONNECTED);
			else
				setState(State.ERROR);
		} catch (DatabaseException e){
			getLog().d(TAG, e.getMessage());
			setState(State.ERROR);
		}
		
	}

	public EventBus getEventBus() {
		return eventBus;
	}
	
	public void connectMqtt() {
		setState(State.MQTT_CONNECTING);
		try {
			mqttEngine.connect();
			setState(State.MQTT_CONNECTED);
		} catch (Exception e) {
			log.e(TAG, e);
			setState(State.ERROR);
		}
	}
	
	public Log getLog() {
		return log;
	}
	
	public void loadfile(File file) throws IOException, JSONException, JsonParseException{
		rules = new Rules();
		JsonParse jp= new JsonParse();
		jp.load(mqttEngine, rules, db, file, destinationTopic);
		//eventBus.publishAsync(new EventFileOpen(file));
	}
	
	@Listener
	public void onReceiveMqttMessage(final EventMqttMessageArrived event) {	
		String name= event.getTopic();
		Date date = new Date();
		database.save(date, event.getTopic(), event.getMessage().toString());
		getLog().d(TAG, "onReceiveMqttMessage "+name);
		
	}

}
