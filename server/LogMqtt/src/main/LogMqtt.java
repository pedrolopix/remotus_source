package main;

import java.io.File;
import java.io.IOException;
import org.json.JSONException;

import viewConsola.ControllerConsola;

import model.JsonParseException;
import model.Model;


public class LogMqtt {

	private Model model;
	
	public LogMqtt(){
		super();
		model = new Model();
	}
	
	private void start(String[] args){
		boolean startconsole=false;
		boolean startgui=false;
		boolean debug=false;
		
		if (args.length > 0) {
			for (int i = 0; i < args.length; i++) {
				if (args[i].equals("-o"))
					try {
						if (args.length>i + 1) {
							model.loadfile(new File(args[i + 1]));
							startconsole=true;
						}
					} catch (IOException | JSONException | JsonParseException e) {
						e.printStackTrace();
					}
				else if (args[i].equals("-gui")) {
					startgui=true;
					startconsole=false;
					//ControllerGui.getInstance().startGUI(model);
				}
				else if (args[i].equals("-d")) {
					debug=true;
					startconsole=true;
				}
			}
			if (startconsole && !startgui) {
				System.out.println("LogMqtt - a iniciar...");
				ControllerConsola.getInstance().start(model, true, debug);
				System.out.println("LogMqtt - terminado");
			}
		} else {
			ajuda();
		}
		
	}
	
	private void ajuda() {
		System.out.println("LogMqtt");
		System.out.println("");
		System.out.println("opções: ");
		System.out.println(" -d modo de debug");
		System.out.println(" -o <NomeFicheiro> lê ficheiro de configurações");
		System.out.println(" -gui interface gráfico");
		System.out.println("");
		System.out.println("Exemplo: LogMqtt -d -o config.json -gui");
		System.out.println("");
	}

	public static void main(String[] args) {
		LogMqtt app = new LogMqtt();
		app.start(args);
	}

}
