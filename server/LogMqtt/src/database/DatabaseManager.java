package database;

import java.util.Date;

public abstract class DatabaseManager {
	
    protected static String DB_SERVERHOST;
    protected static String DB_SERVERPORT;
    protected static String DB_NAME;
    protected static String DB_USERNAME;
    protected static String DB_PASSWORD;

    private static DatabaseManager instance;
    
    public static DatabaseManager getInstance(String typeDB, String host, String port, String database, String user, String password) throws DatabaseException{
        if (instance == null) {
            instance = DatabaseFactory.getDatabaseManager(typeDB);
            instance.initDB(host, port, database, user, password);
            String driverName = instance.getDriverName();
            try {
				Class.forName(driverName);
            } catch (ClassNotFoundException e) {
            	//e.printStackTrace();
            }
        }
        return instance;
    }

	protected abstract String getDriverName();
    
    public abstract void save(Date date, String topic, String message);
    
    private void initDB(final String host, final String port, final String database, final String user, final String password) throws DatabaseException {
    	DB_SERVERHOST = host;
    	DB_SERVERPORT = port;
    	DB_NAME = database;
    	DB_USERNAME = user;
    	DB_PASSWORD = password;
    	createShemaDatabase();
    }
    
    protected DatabaseManager getInstance(){
    	return instance;
    }
    
    public abstract boolean isValid();

	public abstract void stop();
	
	protected abstract void createShemaDatabase() throws DatabaseException;
}
