package database;

public abstract class DatabaseFactory {

	private static DBMySql mysqlDatabase;
	private static DBFile fileDatabase;
	
	public static DatabaseManager getDatabaseManager(String dbType) {
        if (dbType.equals("MySql")) {
            if (mysqlDatabase == null) 
            	mysqlDatabase = new DBMySql();
            return mysqlDatabase;
        }
        else if (dbType.equals("File")) {
        	if (mysqlDatabase == null) 
        		fileDatabase = new DBFile();
        	return fileDatabase;
        }
        return null;
    }
	
}
