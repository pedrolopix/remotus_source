package database;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DBFile extends DatabaseManager {

	private static final String NEWLINE = System.getProperty("line.separator");
	private static final String FORMAT_DATE = "yyyy-MM-dd HH:mm:ss.S";
	private File file;
	
	@Override
	protected String getDriverName() {
		return "";
	}

	@Override
	public void save(Date date, String topic, String message) {
		if (file==null)
			file = new File(DB_SERVERHOST);
		
		if (!isValid())
			return;
		
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new FileWriter(file, true));
			writer.write(new SimpleDateFormat(FORMAT_DATE).format(date) + "\t" + topic + "\t" + message + NEWLINE);
		} catch (IOException e) {
		} finally {
			try {
				writer.close();
			} catch (IOException e) {
			}
		}
	}

	@Override
	public boolean isValid() {
		return true;
	}

	@Override
	public void stop() {
		file = null;
	}

	@Override
	protected void createShemaDatabase() throws DatabaseException{
		// TODO Auto-generated method stub
		
	}


}
