package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Date;

import com.mysql.jdbc.DatabaseMetaData;

public class DBMySql extends DatabaseManager {
	private static int TIMEOUT = 10;
	private Connection conn = null;
	
	public DBMySql(){
		
	}

	@Override
	protected String getDriverName() {
		return "com.mysql.jdbc.Driver";
	}

	@Override
	public void save(Date date, String topic, String message) {
		if (conn==null || isClosed()) createConnection();
		if (conn==null) return;
		PreparedStatement stm = null; 
		try {
			String query = "INSERT INTO Log(date, topic, message) Values (?, ?, ?)";
			stm = conn.prepareStatement(query.toString());
			stm.setTimestamp(1, new Timestamp(date.getTime()));
			stm.setString(2, topic);  
			stm.setString(3, message); 
			stm.executeUpdate();  
		} catch (SQLException ex) {
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		} finally {
			if (stm != null) {
				try {
					stm.close();
				} catch (SQLException ex) {
				} 
				stm = null;
			}
			try {
				conn.close();
			} catch (SQLException e) {
			}
		}
	}
	
	private boolean isClosed() {
		try {
			if (conn.isClosed())
				return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	private void createConnection() {
		try {
			DriverManager.setLoginTimeout(5);
			conn = DriverManager.getConnection("jdbc:mysql://"+DB_SERVERHOST+ ":" + DB_SERVERPORT+"/"+DB_NAME,DB_USERNAME,DB_PASSWORD);
		} catch (SQLException e) {
		}
	}

	@Override
	public boolean isValid() {
		createConnection();
		try {
			return (conn.isValid(TIMEOUT));
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public void stop() {
		try {
			if (conn!=null)
				conn.close();
		} catch (SQLException e) {
		}
	}

	@Override
	protected void createShemaDatabase() throws DatabaseException {
		try {
			DriverManager.setLoginTimeout(TIMEOUT);
			Connection c = null;
			try {
				c = DriverManager.getConnection("jdbc:mysql://" + DB_SERVERHOST
						+ ":" + DB_SERVERPORT, DB_USERNAME, DB_PASSWORD);
			} catch (SQLException ex) {
				throw new DatabaseException(
						"Ocorreu um erro na liga����o ao motor de base de dados.");
			}
			DatabaseMetaData metadata = null;
			ResultSet rs = null;
			boolean exists = false;

			metadata = (DatabaseMetaData) c.getMetaData();
			rs = metadata.getCatalogs();

			while (rs.next()) {
				String tableSchema = rs.getString(1);
				if (tableSchema.equalsIgnoreCase(DB_NAME)) {
					exists = true;
					break;
				}
			}
			String sql = "";
			Statement stmDB = null, stmTable = null;
			if (!exists) {
				stmDB = c.createStatement();
				sql = "CREATE DATABASE " + DB_NAME;
				stmDB.executeUpdate(sql.toString());
				stmDB.close();
			}
			Connection c1 = DriverManager.getConnection("jdbc:mysql://"
					+ DB_SERVERHOST + ":" + DB_SERVERPORT + "/" + DB_NAME,
					DB_USERNAME, DB_PASSWORD);
			stmTable = c1.createStatement();
			sql = "CREATE TABLE IF NOT EXISTS Log ( "
					+ "id bigint(20) NOT NULL AUTO_INCREMENT, "
					+ "date timestamp NULL DEFAULT NULL, "
					+ "topic varchar(255) DEFAULT NULL, "
					+ "message varchar(255) DEFAULT NULL, "
					+ "PRIMARY KEY (id) "
					+ ") ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";
			stmTable.executeUpdate(sql);
			stmTable.close();
		} catch (SQLException e) {
			throw new DatabaseException("Ocorreu um erro na base de dados"
					+ e.getMessage());
		}
	}
}
