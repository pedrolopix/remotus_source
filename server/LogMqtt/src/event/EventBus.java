package event;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import net.engio.mbassy.BusConfiguration;
import net.engio.mbassy.MBassador;

public class EventBus extends MBassador<Event>{

	public EventBus(BusConfiguration configuration) {
		super(configuration);
	}

	public void mqttMessageArrived(String topic, MqttMessage message) {
		publishAsync(new EventMqttMessageArrived(topic,message));
	}

	public void mqttDeliveryComplete(IMqttDeliveryToken token) {
		// TODO Auto-generated method stub
		
	}

	public void mqttConnectionLost(Throwable throwable) {
		// TODO Auto-generated method stub
		
	}

	public void logError(String tag, String log) {
		// TODO Auto-generated method stub
		publishAsync(new EventLog(tag, log));
	}

}
