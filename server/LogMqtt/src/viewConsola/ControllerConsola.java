package viewConsola;

import model.Model;
import net.engio.mbassy.listener.Listener;
import event.Event;

public class ControllerConsola {
	private static ControllerConsola instance;
	private boolean debug;
	
	public static ControllerConsola getInstance() {
		if (instance == null)
			instance = new ControllerConsola();
		return instance;
	}
	
	public void start(final Model model, Boolean wait, boolean debug) {
		this.debug=debug;
		model.getEventBus().subscribe(this);
		Thread t= new  Thread() {
			@Override
			public void run() {
				model.start();
			};
		};
		t.start();
		if (wait) {
	 		try {
				t.join();
			} catch (InterruptedException e) {
	
			}
		}
	}
	
	@Listener
    public void onLog(final Event event) {
		if (debug) {
			System.out.println(event.toString());
		}
	}

}
