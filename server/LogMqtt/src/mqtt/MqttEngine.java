package mqtt;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.eclipse.paho.client.mqttv3.MqttSecurityException;
import org.eclipse.paho.client.mqttv3.MqttTopic;

import model.Model;

public class MqttEngine {
	private static final String TAG = "MqttEngine";
	private MqttClient client;
	private String serverURI;
	private String clientId; 
	private String userName;
	private String password;
	private Model model;
	private MqttMyMessage willMsg;
	private MqttMyMessage startMsg;
	
	private MqttCallback callback=new MqttCallback() {
		
		@Override
		public void connectionLost(Throwable throwable) {
			model.getEventBus().mqttConnectionLost(throwable);
		}

		@Override
		public void messageArrived(String topic, MqttMessage message)
				throws Exception {
			model.getEventBus().mqttMessageArrived(topic,message);
			
		}

		@Override
		public void deliveryComplete(IMqttDeliveryToken token) {
			model.getEventBus().mqttDeliveryComplete(token);
			
		}
	};
	

	public MqttEngine(Model model) {
		this.model=model;
		userName="";
		password="";
	}

	public void start() {
		
	}

	public boolean subscrive(String topicFilter) {
		if (client==null) return false;
		try {
			client.subscribe(topicFilter);
			model.getLog().i(TAG, "subscribe "+topicFilter);
		} catch (MqttSecurityException e) {
			model.getLog().e(TAG,e);
			return false;
		} catch (MqttException e) {
			model.getLog().e(TAG,e);
			return false;
		}
		return true;
	}
	
	public void connect() throws MqttSecurityException, MqttException {
		clientId = MqttClient.generateClientId();
		model.getLog().i(TAG, "A ligar a "+getServerURI().toString());
		client= new MqttClient(getServerURI(), clientId, new org.eclipse.paho.client.mqttv3.persist.MemoryPersistence() );
		MqttConnectOptions options= new MqttConnectOptions();
		options.setUserName(getUserName());
		options.setPassword(getPassword().toCharArray());
		options.setCleanSession(true);
		if (willMsg!=null) {
			if (!willMsg.topic.isEmpty())
		   	    options.setWill(willMsg.topic,willMsg.message.getBytes(),willMsg.qos,willMsg.retained);
		}
		client.connect(options);
		client.setCallback(callback);
		
		if (startMsg!=null) {
			if (!startMsg.topic.isEmpty())
			   	publish(startMsg.topic,startMsg.message, startMsg.retained, startMsg.qos);
		}
		
		model.getLog().i(TAG, "ligado a "+getServerURI().toString());
	}
	
	public void disconnect() {
		if (client!=null) {
			try {
				client.disconnect();
			} catch (MqttException e) {
				model.getLog().e(TAG, e);
			}
		}
	}
	
	public void stop() {
		disconnect();
	}

	public String getServerURI() {
		return serverURI;
	}

	public void setServerURI(String serverURI) {
		this.serverURI = serverURI;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public void publish(String topic, int value, boolean retained, int qos) {
		publish(topic, String.valueOf(value), retained, qos);
	}

	public void publish(String topic, String text, boolean retained, int qos) {
		if (client==null) {
			model.getLog().e(TAG, "cliente não está ligado");
			return;
		}
		MqttTopic t = client.getTopic(topic);
		try {
			MqttMessage mqttMessage = new MqttMessage(text.getBytes());
			mqttMessage.setRetained(retained);
			mqttMessage.setQos(qos);
			MqttDeliveryToken token = t.publish(mqttMessage);
			token.waitForCompletion();
		} catch (MqttPersistenceException e) {
			model.getLog().e(TAG,e);
		} catch (MqttException e) {
			model.getLog().e(TAG,e);
		}
	}

	public void setWillMsg(MqttMyMessage msg) {
		this.willMsg=msg;
	}
	
	public void setStartMsg(MqttMyMessage msg) {
		this.startMsg=msg;
	}

	public MqttMyMessage getWillMsg() {
		return willMsg;
	}

	public MqttMyMessage getStartMsg() {
		return startMsg;
	}
	
	
	
}
