package remotusservice.event;

public class EventError extends Event {
	private final String tag;
	private String log;

	public EventError(String tag, String log) {
		super();
		this.tag = tag;
		this.log = log;
	}

	public String getLog() {
		return log;
	}

	public void setLog(String log) {
		this.log = log;
	}

	@Override
	public String toString() {
		return "[" + tag + "] " + log;
	}
}
