package remotusservice.mqtt;


import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.eclipse.paho.client.mqttv3.MqttSecurityException;
import org.eclipse.paho.client.mqttv3.MqttTopic;
import remotusservice.model.Model;
import remotus.rules.Base;
import remotus.rules.MqttConfig;


public class MqttEngine extends Base{
	private static final String TAG = "MqttEngine";
	private MqttClient client;
	private MqttConfig config;

	private final MqttCallback callback=new MqttCallback() {

		@Override
		public void connectionLost(Throwable throwable) {
			Model.getEventBus().mqttConnectionLost(throwable);
		}

		@Override
		public void messageArrived(String topic, MqttMessage message)
				throws Exception {
			Model.getEventBus().mqttMessageArrived(topic,message);
		}

		@Override
		public void deliveryComplete(IMqttDeliveryToken token) {
			Model.getEventBus().mqttDeliveryComplete(token);

		}
	};
	private String clientId;


	public MqttEngine(Model model) {
		super(model.log);
		config= new MqttConfig();
	}

	public void start() {

	}

	public void unsubscrive(String topicFilter) {
		if (client==null) {
			return;
		}
		try {
			log.d(TAG, "unsubscrive " + topicFilter);
			client.unsubscribe(topicFilter);
		} catch (MqttException e) {
			//se der erro ?? porque j?? est?? desligado.
		}

	}

	public boolean subscrive(String topicFilter) {
		if (client==null) {
			return false;
		}
		try {
			client.subscribe(topicFilter);
			log.d(TAG, "subscribe " + topicFilter);
		} catch (MqttSecurityException e) {
			log.e(TAG,e);
			return false;
		} catch (MqttException e) {
			log.e(TAG,e);
			return false;
		}
		return true;
	}

	public void connect() throws MqttSecurityException, MqttException {
		if (config.getServerURI()==null) {
			log.e(TAG, "Endere��o do servidor inv��lido");
			return;
		}


		clientId = MqttClient.generateClientId();
		log.d(TAG, "A ligar a " + config.getServerURI().toString());
		log.d(TAG, "como " + clientId);
		
		client= new MqttClient(config.getServerURI(), clientId, new org.eclipse.paho.client.mqttv3.persist.MemoryPersistence() );
		MqttConnectOptions options= new MqttConnectOptions();
		options.setUserName(config.getUserName());
		options.setPassword(config.getPassword().toCharArray());
		options.setCleanSession(true);
		if (!config.getWillMsg().topic.isEmpty()) {
			options.setWill(config.getWillMsg().topic,
					config.getWillMsg().message.getBytes(),
					config.getWillMsg().qos,
					config.getWillMsg().retained);
		}

		client.connect(options);
		client.setCallback(callback);

		if (!config.getStartMsg().topic.isEmpty()) {
			publish(config.getStartMsg().topic,
		   			config.getStartMsg().message,
		   			config.getStartMsg().retained,
		   			config.getStartMsg().qos);
		}


		log.d(TAG, "ligado a " + config.getServerURI().toString());

	}

	public void disconnect() {
		if (client!=null) {
			try {
				log.d(TAG, "A desligar de " + config.getServerURI().toString());
				client.disconnect();
			} catch (MqttException e) {
				log.e(TAG, e);
			}
		}
	}

	public void stop() {
		disconnect();
	}


	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}


	public void publish(String topic, int value, boolean retained, int qos) {
		publish(topic, String.valueOf(value), retained, qos);
	}

	public void publish(String topic, String text, boolean retained, int qos) {
		if (client==null) {
			log.e(TAG, "cliente no est�� ligado");
			return;
		}
		MqttTopic t = client.getTopic(topic);
		try {
			MqttMessage mqttMessage = new MqttMessage(text.getBytes());
			mqttMessage.setRetained(retained);
			mqttMessage.setQos(qos);
			MqttDeliveryToken token = t.publish(mqttMessage);
			token.waitForCompletion();
		} catch (MqttPersistenceException e) {
			log.e(TAG,e);
		} catch (MqttException e) {
			log.e(TAG,e);
		}
	}



	public MqttConfig getConfig() {
		return config;
	}

	public void setConfig(MqttConfig config) {
		this.config = config;
	}



}
