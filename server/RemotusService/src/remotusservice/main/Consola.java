package remotusservice.main;

import java.text.SimpleDateFormat;
import java.util.Date;
import net.engio.mbassy.listener.Listener;
import remotusservice.event.EventError;
import remotusservice.event.EventLog;
import remotusservice.model.Log;
import remotusservice.model.Model;

public class Consola {

	private static final String TAG = "Consola";
	private final Model model;

	public Consola(Model model) {
		this.model=model;
		// model.testeFiles();
		Model.getEventBus().subscribe(this);
	}

	public void start() {
		if (Log.debug) {
			System.out.println("Debug activo");
		}

		try {

			model.start();
			System.in.read();
		} catch (Exception e) {
			model.log.e(TAG,e);
		}
	}

	@Listener
	public void onEventLog(EventLog event) {
		if (Log.debug) {
			Date dNow = new Date();
			SimpleDateFormat ft = new SimpleDateFormat("HH:mm:ss,SSS");
			System.out.print(ft.format(dNow) + " ");
			System.out.println(event.toString());
		}
	}

	@Listener
	public void onEventError(EventError event) {
		Date dNow = new Date();
		SimpleDateFormat ft = new SimpleDateFormat("HH:mm:ss,SSS");
		System.out.print(ft.format(dNow) + " ");
		System.out.println(event.toString());
	}

	public void ajuda() {
		System.out.println("opções: ");
		System.out.println(" -o <NomeFicheiro> l?? ficheiro de configurações");
		System.out.println(" -d modo de debug");
		System.out.println("");
		System.out.println("Exemplo: RemotusService -o config.json -d");
		System.out.println("");
	}

	void header() {
		System.out.println("RemotusService");
		System.out.println("");
	}

	public void setDebug(boolean debug) {
		Log.debug=debug;
	}

}
