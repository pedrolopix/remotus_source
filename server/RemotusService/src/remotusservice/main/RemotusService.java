package remotusservice.main;

import java.io.File;

import remotusservice.model.Model;


public class RemotusService {
	
	public static void main(String[] args) {
		Model model= new Model();
		//model.testeFiles();
		Consola c=new Consola(model);
		c.header();
		boolean load=false;
		if (args.length > 0) {
			for (int i = 0; i < args.length; i++) {
				if (args[i].equals("-d")) {
					c.setDebug(true);
				}
				if (args[i].equals("-o")) {
					if (args.length>i + 1) {
						load=true;
						if (!model.loadFile(new File(args[i + 1]))) return;
					}
				}
			}
		}
		
		//model.testeFiles();
		if (load) {
			c.start();
		} else {
			c.ajuda();
		}
	}
	
	
}
