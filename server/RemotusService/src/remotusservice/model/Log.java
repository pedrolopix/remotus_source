package remotusservice.model;

import remotus.rules.LogIntf;

public class Log implements LogIntf{
	public static boolean debug;

	@Override
	public  void e(String tag, Throwable e) {
		Model.getEventBus().logError(tag, e.toString());
		if (debug) {
			e.printStackTrace();
		}
	}

	@Override
	public  void e(String tag, String msg) {
		Model.getEventBus().logError(tag, msg);
	}

	@Override
	public  void d(String tag, String msg) {
		Model.getEventBus().logDebug(tag, msg);
	}

	@Override
	public  void i(String tag, String msg) {
		Model.getEventBus().logError(tag, msg);
	}

	@Override
	public  void e(String tag, String msg, Throwable e) {
		Model.getEventBus().logError(tag, msg+":"+e.toString());
		if (debug) {
			e.printStackTrace();
		}
	}
}
