package remotusservice.model;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import remotus.rules.Base;
import remotus.rules.ExpressionIntf;


public class Expression extends Base implements ExpressionIntf{

	private static final String TAG = "Expression";
	private ScriptEngineManager mgr;
	private ScriptEngine engine;
	
	public Expression(Model model) {
		super(model.log);
		mgr = new ScriptEngineManager();
		engine = mgr.getEngineByName("JavaScript");
	}


	public String calc(String expression) {
		
		if (expression.startsWith("=")) {
			StringBuffer ret=new StringBuffer(expression);
			try {
				ret.deleteCharAt(0);
				Object result = engine.eval(ret.toString());
				if (result instanceof Boolean) {
					if ((Boolean)result) return "1";
					return "0";
				}
				if (result==null) {
					return "0";
				}
				return String.valueOf(Math.round((Double)result));
			} catch (ScriptException e) {
				log.e(TAG, "expression: "+expression+"\ntranslated:"+ret.toString(),e);
				//e.printStackTrace();
				return "0";
			}
		}
		return expression;
	}
}
