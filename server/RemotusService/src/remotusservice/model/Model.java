package remotusservice.model;

import it.sauronsoftware.cron4j.Scheduler;
import java.io.File;
import java.io.IOException;
import net.engio.mbassy.BusConfiguration;
import net.engio.mbassy.listener.Listener;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttSecurityException;
import remotusservice.event.EventBus;
import remotusservice.event.EventMqttMessageArrived;
import remotusservice.mqtt.MqttEngine;
import remotus.rules.Base;
import remotus.rules.ExpressionIntf;
import remotus.rules.IEngine;
import remotus.rules.JSONLoad;
import remotus.rules.JSONSave;
import remotus.rules.MqttConfig;
import remotus.rules.Rules;
import remotus.rules.SchedulerTask;
import remotus.rules.actions.ActionPublish;
import remotus.rules.actions.ActionToggle;
import remotus.rules.conditions.ConditionTime;
import remotus.rules.conditions.ConditionTopic;

public class Model extends Base implements IEngine{
	private static final String TAG = "Model";
	private final MqttEngine mMqttEngine;
	private static EventBus mEventBus;
	private Rules mRules;
	private final Scheduler mScheduler;
	private final Expression mExpression;
	private final String lastId = "";
	private String mLastId;

	static {
		mEventBus= new EventBus(BusConfiguration.Default());
	}

	public Model() {
		super(new Log());
		mExpression= new Expression(this);
		mMqttEngine= new MqttEngine(this);
		mRules= new Rules(this);
		mEventBus.subscribe(this);
		mScheduler = new Scheduler();
	}

	public void testeFiles() {
		//teste
		mMqttEngine.getConfig().setServerURI("tcp://127.0.0.1:1883");
		mMqttEngine.getConfig().setUserName("Pedro");
		mMqttEngine.getConfig().setPassword("ze");
		mMqttEngine.getConfig().getConfigMsg().setTopic("/remotusserver/config");

		mRules.addTopicAlias("ZeLuz","/casa/quarto/ze/luz");
		mRules.addTopicAlias("ZeSwitch","/casa/quarto/ze/switch");
		ConditionTopic ct=new ConditionTopic();
		ct.addTopic("ZeLuz");
		ct.setCondition("value>10");
		ct.setSchedulingPattern("* * * * *"); // por minuto
		ActionPublish ap= new ActionPublish();
		ap.set(true, "", "/arduino/ze/luz", "=!/arduino/ze/luz", 0, true, 0);
		ActionToggle at= new ActionToggle();
		at.set(true, "", "/arduino/ana/luz", "", 0, true, 1000);
		ct.addAction(at);

		at= new ActionToggle();
		at.set(true, "", "/arduino/ana/luz", "", 0, true, 2000);
		ct.addAction(at);

		ct.addAction(ap);

		mRules.addRule(ct);

		ct=new ConditionTopic();
		ct.addTopic("ZeSwitch");
		ct.addTopic("/arduino/ana/luz");
		mRules.addRule(ct);

		ConditionTime ctime = new ConditionTime();
		ctime.setSchedulingPattern("* * * * *");
		at = new ActionToggle();
		at.set(true, "", "/arduino/pedro/luz", "", 0, true, 2000);
		ctime.addAction(at);

		mRules.addRule(ctime);

		mRules.getVariables().setVariableValue("var1", "123");
		mRules.getVariables().setVariableValue("var2", "345");

		JSONSave sv= new JSONSave(log);
		try {
			sv.save(new File("remotusService.json"), mRules, mMqttEngine.getConfig());
		} catch (IOException e) {
			e.printStackTrace();
		}

		mRules= new Rules(this);
		JSONLoad jl=new JSONLoad(log);
		try {
			jl.load(new File("remotusService.json"), mRules, mMqttEngine.getConfig());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			sv.save(new File("remotusServiceNew.json"), mRules, mMqttEngine.getConfig());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void start() throws MqttSecurityException, MqttException, IOException {
		if (mScheduler.isStarted()) {
			mScheduler.stop();
		}
		mMqttEngine.connect();
		String topic = mMqttEngine.getConfig().getConfigMsg().getTopic();
		if (topic != null && !topic.isEmpty()) {
			if (mLastId == "" || !mLastId.equals(mMqttEngine.getConfig().getId())) {
				JSONSave jl = new JSONSave(log);
				String value = jl.getJSON(mRules, mMqttEngine.getConfig());
				mLastId = mMqttEngine.getConfig().getId();
				publish(topic, value, mMqttEngine.getConfig().getConfigMsg().retained, mMqttEngine.getConfig().getConfigMsg().qos);
			}
		}
		mRules.start();
		mScheduler.start();
		if (topic != null && !topic.isEmpty()) {
			subscrive(topic);
		}
	}

	public void stop() {
		if (mScheduler.isStarted()) {
			mScheduler.stop();
		}
		mRules.stop();
		mMqttEngine.disconnect();
	}

	@Listener
	public void onReceiveMqttMessage(final EventMqttMessageArrived event) {
		if (event.getTopic().equals(mMqttEngine.getConfig().getConfigMsg().getTopic())) {

			JSONLoad lv = new JSONLoad(log);
			Rules mRules1 = new Rules(this);
			MqttConfig config = new MqttConfig();

			lv.setJSON(event.getMessage().toString(), mRules1, config);
			if (mLastId.equals(config.getId())) {
				return;
			}
			stop();
			mRules = mRules1;
			config.setPassword(mMqttEngine.getConfig().getPassword());
			config.setServerURI(mMqttEngine.getConfig().getServerURI());
			config.setUserName(mMqttEngine.getConfig().getUserName());

			mMqttEngine.setConfig(config);
			try {
				start();
			} catch (MqttSecurityException e1) {
				log.e(TAG, e1);
			} catch (MqttException e1) {
				log.e(TAG, e1);
			} catch (IOException e1) {
				log.e(TAG, e1);
			}
			JSONSave sv = new JSONSave(log);
			try {
				sv.save(new File("remotusService.json"), mRules, mMqttEngine.getConfig());
			} catch (IOException e) {
				log.e(TAG, e);
			}
			return;
		}
		mRules.receivedTopic(event.getTopic(),event.getMessage().toString());

	}

	public boolean loadFile(File file)  {
		if (! file.exists()) {
			log.e(TAG, "Não existe o ficheiro " + file);
			return false;
		}
		mRules= new Rules(this);
		JSONLoad jl=new JSONLoad(log);
		try {
			jl.load(file, mRules, mMqttEngine.getConfig());
			mLastId = mMqttEngine.getConfig().getId();
			return true;
		} catch (IOException e) {
			log.e(TAG, "Erro a ler o ficheiro "+file.getAbsolutePath(),e);
		}
		return false;
	}

	@Override
	public boolean subscrive(String topicFilter) {
		return mMqttEngine.subscrive(topicFilter);
	}


	@Override
	public void unsubscrive(String topicFilter) {
		mMqttEngine.unsubscrive(topicFilter);
	}

	public static EventBus getEventBus() {
		return mEventBus;
	}


	public MqttEngine getMqttEngine() {
		return mMqttEngine;
	}

	public Rules getRules() {
		return mRules;
	}

	@Override
	public void publish(String topic, String value, boolean retained, int qos) {
		mMqttEngine.publish(topic, value, retained, qos);
	}

	@Override
	public void schedule(String schedulingPattern, SchedulerTask task) {
		String id = mScheduler.schedule(schedulingPattern, task);
		task.setId(id);
	}

	@Override
	public void unschedule(SchedulerTask task) {
		mScheduler.deschedule(task.getId());
	}

	@Override
	public ExpressionIntf GetExpression() {
		return mExpression;
	}





}
