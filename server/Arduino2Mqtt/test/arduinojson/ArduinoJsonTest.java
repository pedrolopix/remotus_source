package arduinojson;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ArduinoJsonTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testToArduino() {
		ArduinoData a = ArduinoJson.toArduino("{\"name\":\"a1\", \"analog\":[1,2,3,4,5,6], \"digital\":[0,1,2,3,4,5,6,7,8,9,10,11,12,13]}");
		assertEquals("nome", "a1", a.getName());
		Integer ai[] = a.getAnalog().toArray(new Integer[6]);
		Integer di[] = a.getDigital().toArray(new Integer[6]);
		assertArrayEquals(new Integer[]{1,2,3,4,5,6},ai);
		assertArrayEquals(new Integer[]{0,1,2,3,4,5,6,7,8,9,10,11,12,13},di);
	}

}
