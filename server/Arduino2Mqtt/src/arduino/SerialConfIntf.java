package arduino;


public interface SerialConfIntf {

	int getBoundRate();
	int getDatabits();
	char getParity();
	float getStopbits();
	String getPortName();
	void setBoundRate(int rate);
	void setDatabits(int dataBits);
	void setParity(char parity);
	void setStopbits(float stopBits);
	void setPortName(String portName);

}
