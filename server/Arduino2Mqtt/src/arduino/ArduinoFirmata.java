package arduino;

import gnu.io.CommPortIdentifier;
import gnu.io.NoSuchPortException;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.UnsupportedCommOperationException;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;


public class ArduinoFirmata implements Runnable,  SerialConfIntf{
    public static final byte INPUT  = 0;
    public static final byte OUTPUT = 1;
    public static final byte ANALOG = 2;
    public static final byte PWM    = 3;
    public static final byte SERVO  = 4;
    public static final byte SHIFT  = 5;
    public static final byte I2C    = 6;
    public static final boolean LOW   = false;
    public static final boolean HIGH  = true;
    private final byte MAX_DATA_BYTES  = 32;
    private final byte DIGITAL_MESSAGE = (byte)0x90;
    private final byte ANALOG_MESSAGE  = (byte)0xE0;
    private final byte REPORT_ANALOG   = (byte)0xC0;
    private final byte REPORT_DIGITAL  = (byte)0xD0;
    private final byte SET_PIN_MODE    = (byte)0xF4;
    private final byte REPORT_VERSION  = (byte)0xF9;
    private final byte SYSTEM_RESET    = (byte)0xFF;
    private final byte START_SYSEX     = (byte)0xF0;
    private final byte END_SYSEX       = (byte)0xF7;
	private int waitForData = 0;
    private byte executeMultiByteCommand = 0;
    private byte multiByteChannel = 0;
    private byte[] storedInputData = new byte[MAX_DATA_BYTES];
    private boolean parsingSysex = false;
    private int sysexBytesRead = 0;
    private int[] digitalOutputData = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    private int[] digitalInputData  = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    private boolean[] digitalData  = { false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false };
    private int[] analogInputData   = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    private int[] analogThreshold   = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    private int majorVersion = 0;
    private int minorVersion = 0;

	private String portName;
	private InputStream input;
	private OutputStream output;
	private int boundRate = 57600;
	private char parity = 'N';
	private float stopbits;
	private int dataBits = 8;
	private List<BoardPinChangedIntf> listners;
	private Thread thReceive;
	private SerialPort port;

	public ArduinoFirmata() {
		super();
		listners= new ArrayList<BoardPinChangedIntf>();
		thReceive = null;
	}

	public void start() throws SerialException {
		CommPortIdentifier portId;
		try {
			portId = CommPortIdentifier.getPortIdentifier(portName);
		
			port = (SerialPort) portId.open("serial talk", 4000);
			input = port.getInputStream();
			output = port.getOutputStream();
	
			int myparity = SerialPort.PARITY_NONE;
			int mystopbits = SerialPort.STOPBITS_1;
			switch (parity) {
			case 'E':
				myparity = SerialPort.PARITY_EVEN;
				break;
			case 'O':
				myparity = SerialPort.PARITY_ODD;
				break;
			case 'M':
				myparity = SerialPort.PARITY_MARK;
				break;
			case 'S':
				myparity = SerialPort.PARITY_SPACE;
				break;
			}
	
			if (stopbits == 1.5f)
				mystopbits = SerialPort.STOPBITS_1_5;
			else if (stopbits == 2)
				mystopbits = SerialPort.STOPBITS_2;
	
			port.setSerialPortParams(boundRate, dataBits, mystopbits, myparity);
			
			Thread.sleep(3000);
			
			byte[] writeData = {0, 1};
	        for (byte i = 0; i < 6; i++) {
	            write((byte)(REPORT_ANALOG | i));
	            write((byte)1);
	        }
	        for (byte i = 0; i < 2; i++) {
	            write((byte)(REPORT_DIGITAL | i));
	            write((byte)1);
	        }
			
			thReceive = new Thread(this);
			thReceive.start();
		
		} catch (NoSuchPortException e) {
			stop();
			throw new SerialException("Porta "+portName+" em uso");
		}  catch (UnsupportedCommOperationException e) {
			stop();
			throw new SerialException("Comando não suportado na porta "+portName);
		} catch (PortInUseException e) {
			stop();
			throw new SerialException("Porta "+portName+" em uso");
		} catch (IOException e) {
			stop();
			throw new SerialException("Erro de E/S na porta "+portName);
		} catch (InterruptedException e) {
			stop();
			throw new SerialException("Erro de E/S na porta "+portName);
		} 

	}

	public void stop() {
		if (thReceive != null) {
			thReceive.interrupt();
			try {
				thReceive.join();
			} catch (InterruptedException e) {
			}
		}
		if (port != null) {
			port.close();

		}
		port = null;
		input = null;
		output = null;
	}
	
	
	@Override
	public void run() {
		try {
			while (!Thread.interrupted()) {
				
				while (input.available() > 0) {
					byte buf[] = new byte[4096];
					int b = input.read(buf);
					for (int i=0; i<b; i++) {
						processInput(buf[i]);
					}
				}
				Thread.sleep(10);
			}
		} catch (IOException e) {
			e.printStackTrace();
			stop();
		} catch (InterruptedException e) {
			//
		}
	}

	public int getMajorVersion() {
		return majorVersion;
	}

	public int getMinorVersion() {
		return minorVersion;
	}

	public void setPortName(String portName) {
		this.portName=portName;
		
	}

	@Override
	public int getBoundRate() {
		return boundRate;
	}

	@Override
	public int getDatabits() {
		return dataBits;
	}

	@Override
	public char getParity() {
		return parity;
	}

	@Override
	public float getStopbits() {
		return stopbits;
	}

	@Override
	public String getPortName() {
		return portName;
	}

	@Override
	public void setBoundRate(int boundRate) {
		this.boundRate=boundRate;
	}

	@Override
	public void setDatabits(int dataBits) {
		this.dataBits=dataBits;
	}

	@Override
	public void setParity(char parity) {
		this.parity=parity;
	}

	@Override
	public void setStopbits(float stopBits) {
		this.stopbits=stopBits;
	}
	
	public void write(byte[] writeData){
        try{
            if (isOpen()) output.write(writeData);
        }
        catch(IOException e){
            stop();
        }
    }

    private boolean isOpen() {
		return port!=null;
	}

	public void write(byte writeData){
        byte[] _writeData = {(byte)writeData};
        write(_writeData);
    }

    public void reset(){
        write(SYSTEM_RESET);
    }

    public void sysex(byte command, byte[] data){
        // http://firmata.org/wiki/V2.1ProtocolDetails#Sysex_Message_Format
        if(data.length > 32) return;
        byte[] writeData = new byte[data.length+3];
        writeData[0] = START_SYSEX;
        writeData[1] = command;
        for(int i = 0; i < data.length; i++){
            writeData[i+2] = (byte)(data[i] & 127); // 7bit
        }
        writeData[writeData.length-1] = END_SYSEX;
        write(writeData);
    }

    public boolean digitalRead(int pin) {
        return ((digitalInputData[pin >> 3] >> (pin & 0x07)) & 0x01) > 0;
    }

    public int analogRead(int pin) {
        return analogInputData[pin];
    }

    public void pinMode(int pin, byte mode) {
        byte[] writeData = {SET_PIN_MODE, (byte)pin, mode};
        write(writeData);
    }

    public void digitalWrite(int pin, int value) {
        byte portNumber = (byte)((pin >> 3) & 0x0F);
        if (value==0) digitalOutputData[portNumber] &= ~(1 << (pin & 0x07));
        else digitalOutputData[portNumber] |= (1 << (pin & 0x07));
        byte[] writeData = {
            SET_PIN_MODE, (byte)pin, OUTPUT,
            (byte)(DIGITAL_MESSAGE | portNumber),
            (byte)(digitalOutputData[portNumber] & 0x7F),
            (byte)(digitalOutputData[portNumber] >> 7)
        };
        write(writeData);
    }


    public void analogWrite(int pin, int value) {
        byte[] writeData = {
            SET_PIN_MODE, (byte)pin, PWM,
            (byte)(ANALOG_MESSAGE | (pin & 0x0F)),
            (byte)(value & 0x7F),
            (byte)(value >> 7)
        };
        write(writeData);
    }

    public void servoWrite(int pin, int angle){
        byte[] writeData = {
            SET_PIN_MODE, (byte)pin, SERVO,
            (byte)(ANALOG_MESSAGE | (pin & 0x0F)),
            (byte)(angle & 0x7F),
            (byte)(angle >> 7)
        };
        write(writeData);
    }

    private void setDigitalInputs(int portNumber, int portData) {
    	if (digitalInputData[portNumber]!=portData) {
			digitalInputData[portNumber]=portData;
		}
		for (int i=0; i<13; i++) {
			boolean v=digitalRead(i);
			if (digitalData[i]!=v) {
				for (BoardPinChangedIntf item : listners) {
					item.onDigitalChange(i, v);
				}
			}
			digitalData[i]=v;
		}
    }

    private void setAnalogInput(int pin, int value) {
        if (Math.abs(analogInputData[pin]-value)>analogThreshold[pin]) {
			analogInputData[pin]=value;
			for (BoardPinChangedIntf item : listners) {
				item.onAnalogicChange(pin, value);
			}
		}
    }

    private void setVersion(int majorVersion, int minorVersion) {
        this.majorVersion = majorVersion;
        this.minorVersion = minorVersion;
    }

    private void processInput(byte inputData){
        byte command;
        if(parsingSysex){
            if(inputData == END_SYSEX){
                parsingSysex = false;
                byte sysexCommand = storedInputData[0];
                byte[] sysexData = new byte[sysexBytesRead-1];
                System.arraycopy(storedInputData, 1, sysexData, 0, sysexBytesRead-1);
                //if(dataHandler != null) dataHandler.onSysex(sysexCommand, sysexData);
            }
            else{
                if(sysexBytesRead < storedInputData.length){
                    storedInputData[sysexBytesRead] = inputData;
                    sysexBytesRead++;
                }
            }
        }
        else if(waitForData > 0 && inputData < 128){
            waitForData--;
            storedInputData[waitForData] = inputData;
            if(executeMultiByteCommand != 0 && waitForData == 0){
                switch(executeMultiByteCommand){
                case DIGITAL_MESSAGE:
                    setDigitalInputs(multiByteChannel, (storedInputData[0] << 7) + storedInputData[1]);
                    break;
                case ANALOG_MESSAGE:
                    setAnalogInput(multiByteChannel, (storedInputData[0] << 7) + storedInputData[1]);
                    break;
                case REPORT_VERSION:
                    setVersion(storedInputData[1], storedInputData[0]);
                    break;
                }
            }
        }
        else {
            if(inputData < 0xF0){
                command = (byte)(inputData & 0xF0);
                multiByteChannel = (byte)(inputData & 0x0F);
            }
            else{
                command = inputData;
            }
            switch(command){
            case START_SYSEX:
                parsingSysex = true;
                sysexBytesRead = 0;
                break;
            case DIGITAL_MESSAGE:
            case ANALOG_MESSAGE:
            case REPORT_VERSION:
                waitForData = 2;
                executeMultiByteCommand = command;
                break;
            }
        }
    }

	public void addListener(BoardPinChangedIntf boardPinChanged) {
		listners.add(boardPinChanged);
	}

	public String[] getAvailablePorts() {
		List<String> p= new ArrayList<String>();
		Enumeration pList = CommPortIdentifier.getPortIdentifiers();
	    while (pList.hasMoreElements()) {
	      CommPortIdentifier cpi = (CommPortIdentifier) pList.nextElement();
	      if (cpi.getPortType() == CommPortIdentifier.PORT_SERIAL) {
	    	  p.add(cpi.getName());
	      }
	     }
	    return p.toArray(new String[0]);
	}

	public void join() {
		if (thReceive!=null) {
			try {
				thReceive.join();
			} catch (InterruptedException e) {
			}
		}
		
	}

	public void setAnalogThreshold(int pin, int analogThreshold) {
		this.analogThreshold[pin]=analogThreshold;
		
	}

	
	
	

}
