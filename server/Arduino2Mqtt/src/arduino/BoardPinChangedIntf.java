package arduino;

public interface BoardPinChangedIntf {
	public void onDigitalChange(int pin, boolean value);
	public void onAnalogicChange(int pin, int value);

}
