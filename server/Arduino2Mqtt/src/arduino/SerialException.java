package arduino;

public class SerialException extends Exception {

	private static final long serialVersionUID = 1L;

	public SerialException(String msg) {
		super(msg);
	}

}
