package model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import arduino.SerialConfIntf;
import model.rules.AnalogPin;
import model.rules.AnalogType;
import model.rules.DigitalPin;
import model.rules.DigitalType;
import model.rules.Pin;
import model.rules.Rules;
import mqtt.MqttEngine;
import mqtt.MqttMyMessage;

public class JsonParse {

	private static final String WILL = "will";
	private static final String START = "start";
	private static final String THRESHOLD = "threshold";
	private static final String ACTIVE = "active";
	private static final String RETAINED = "retained";
	private static final String QOS = "qos";
	private static final String TOPIC = "topic";
	private static final String PIN = "pin";
	private static final String ANALOG_TYPE = "analogType";
	private static final String DIGITAL_TYPE = "digitalType";
	private static final String PASSWORD = "password";
	private static final String USER = "user";
	private static final String URI = "uri";
	private static final String NAME = "name";
	private static final String STOPBITS = "stopbits";
	private static final String PARITY = "parity";
	private static final String DATABITS = "databits";
	private static final String RATE = "rate";
	private static final String DIGITAL_PINS = "digitalPins";
	private static final String ANALOG_PINS = "analogPins";
	private static final String MQTT = "mqtt";
	private static final String SERIAL = "serial";
	private static final String VERSION = "version";
	private static final String MESSAGE = "message";


	private void pinToJSON(Pin pin, JSONObject json) throws JSONException {
		json.put(PIN,pin.getId());
		json.put(TOPIC,pin.getTopic());
		json.put(QOS,pin.getQos());
		json.put(RETAINED,pin.getRetained());
		json.put(ACTIVE,pin.isActive());
	}
	
	private void JSONToPin(Pin pin, JSONObject json) throws JSONException {
		pin.setId(json.getInt(PIN));
		pin.setTopic(json.getString(TOPIC));
		pin.setQos(json.getInt(QOS));
		pin.setRetained(json.getBoolean(RETAINED));
		pin.setActive(json.getBoolean(ACTIVE));
	}
	
	private void AnalogPinToJSON(AnalogPin pin, JSONObject json) throws JSONException {
		pinToJSON(pin, json);
		json.put(ANALOG_TYPE, pin.getType().toString());
		json.put(THRESHOLD, pin.getThreshold());
	}
	
	private void JSONToAnalogPin(Rules rules, JSONObject json) throws JSONException {
		List<AnalogPin> list = rules.getAnalogPins();
		int pinNum=json.getInt(PIN);
		AnalogPin pin = list.get(pinNum);
		pin.setType(AnalogType.valueOf(json.getString(ANALOG_TYPE)));
		pin.setThreshold(json.getInt(THRESHOLD));
		JSONToPin(pin,json);
	}
	
	private void DigitalPinToJSON(DigitalPin pin, JSONObject json) throws JSONException {
		pinToJSON(pin, json);
		json.put(DIGITAL_TYPE, pin.getType().toString());
	}

	private void JSONToDigitalPin(Rules rules, JSONObject json) throws JSONException {
		List<DigitalPin> list = rules.getDigitalPins();
		int pinNum=json.getInt(PIN);
		DigitalPin pin = list.get(pinNum);
		pin.setType(DigitalType.valueOf(json.getString(DIGITAL_TYPE)));
		JSONToPin(pin,json);
	}
	
	public void rulesToJSON(Rules rules, JSONObject json) throws JSONException {
		JSONArray arr;
		arr= new JSONArray();
		for (AnalogPin pin : rules.getAnalogPins()) {
			JSONObject value=new JSONObject();
			AnalogPinToJSON(pin,value);
			arr.put(value);
		}
		json.put(ANALOG_PINS, arr);
		arr= new JSONArray();
		for (DigitalPin pin : rules.getDigitalPins()) {
			JSONObject value=new JSONObject();
			DigitalPinToJSON(pin,value);
			arr.put(value);
		}
		json.put(DIGITAL_PINS, arr);
		JSONObject value=new JSONObject();
		MessageToJson(rules.getWillMsg(),value);
		json.put(WILL, value);
		value=new JSONObject();
		MessageToJson(rules.getStartMsg(),value);
		json.put(START, value);
		
	}

	private void MessageToJson(MqttMyMessage msg, JSONObject json) throws JSONException {
		json.put(TOPIC, msg.topic);
		json.put(MESSAGE, msg.message);
		json.put(QOS, msg.qos);
		json.put(RETAINED, msg.retained);
	}

	private void JsonToMessage(MqttMyMessage msg, JSONObject json) throws JSONException {
		msg.topic=json.getString(TOPIC);
		msg.message=json.getString(MESSAGE);
		msg.qos=json.getInt(QOS);
		msg.retained=json.getBoolean(RETAINED);
	}
	
	
	private void jsonToRules(Rules rules, JSONObject json) throws JSONException {
		JSONArray arr;
		arr=json.optJSONArray(ANALOG_PINS);
		if (arr!=null) {
			for (int i = 0; i < arr.length(); i++) {
				JSONToAnalogPin(rules,arr.getJSONObject(i));
			}
		}
		arr=json.optJSONArray(DIGITAL_PINS);
		if (arr!=null) {
			for (int i = 0; i < arr.length(); i++) {
				JSONToDigitalPin(rules,arr.getJSONObject(i));
			}
		}
		
		JSONObject o;
		o=json.getJSONObject(WILL);
		JsonToMessage(rules.getWillMsg(),o);
		o=json.getJSONObject(START);
		JsonToMessage(rules.getStartMsg(),o);
		
	}

	private void serialToJson(SerialConfIntf serial, JSONObject json) throws JSONException {
		json.put(RATE, serial.getBoundRate());
		//json.put(DATABITS, serial.getDatabits());
		//json.put(PARITY, serial.getParity());
		//json.put(STOPBITS, serial.getStopbits());
		json.put(NAME, serial.getPortName());
	}
	
	private void jsonToSerial(SerialConfIntf serial, JSONObject json) throws JSONException {
		serial.setBoundRate(json.getInt(RATE));
		//serial.setDatabits(json.getInt(DATABITS));
		//serial.setParity(json.getString(PARITY).charAt(0));
		//serial.setStopbits(json.getInt(STOPBITS));
		serial.setPortName(json.getString(NAME));
	}
	
	private void mqttToJson(MqttEngine mqtt, JSONObject json) throws JSONException {
		json.put(URI, mqtt.getServerURI());
		json.put(USER, mqtt.getUserName());
		json.put(PASSWORD, mqtt.getPassword());
	}
	
	private void jsonToMqtt(MqttEngine mqtt, JSONObject json) throws JSONException {
		mqtt.setServerURI(json.getString(URI));
		mqtt.setUserName(json.getString(USER));
		mqtt.setPassword(json.getString(PASSWORD));
	}

		
	public void save(SerialConfIntf serial, MqttEngine mqtt, Rules rules, File file) throws JSONException, IOException {
		
		rules.setWillMsg(mqtt.getWillMsg());
		rules.setStartMsg(mqtt.getStartMsg());
		
		JSONObject json= new JSONObject();
		json.put(VERSION, 1);
		JSONObject aux = new  JSONObject();
		json.put(SERIAL, aux);
		serialToJson(serial,aux);
		
		aux = new  JSONObject();
		json.put(MQTT, aux);
		mqttToJson(mqtt,aux);
		
		rulesToJSON(rules,json);
		
        FileWriter fw = new FileWriter(file);
        fw.write(json.toString());
        fw.close();
	}


	public void load(SerialConfIntf serial, MqttEngine mqtt, Rules rules, File file) throws IOException, JSONException, JsonParseException {
		StringBuffer fileData = new StringBuffer();
        BufferedReader reader = new BufferedReader(new FileReader(file));
        char[] buf = new char[1024];
        int numRead=0;
        while((numRead=reader.read(buf)) != -1){
            String readData = String.valueOf(buf, 0, numRead);
            fileData.append(readData);
        }
        reader.close();
		JSONObject json= new JSONObject(fileData.toString());
		//verifica versão
		//JSONObject json1= json.getJSONObject(VERSION);
		int version= json.getInt(VERSION);
		if (version!=1) throw new JsonParseException("Versão do ficheiro não suportada!");
		
		jsonToSerial(serial,json.getJSONObject(SERIAL));
		jsonToMqtt(mqtt,json.getJSONObject(MQTT));
		jsonToRules(rules,json);
		
		mqtt.setStartMsg(rules.getStartMsg());
		mqtt.setWillMsg(rules.getWillMsg());
		
	}



	
	


}
