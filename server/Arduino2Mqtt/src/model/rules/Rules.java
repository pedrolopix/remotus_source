package model.rules;

import java.util.ArrayList;
import java.util.List;

import mqtt.MqttMyMessage;

import org.eclipse.paho.client.mqttv3.MqttMessage;

public class Rules {
	private MqttMyMessage willMsg;
	private MqttMyMessage startMsg;
	private List<AnalogPin> analogPins;
	private List<DigitalPin> digitalPins;
	
	public Rules() {
		super();
		willMsg= new MqttMyMessage();
		startMsg= new MqttMyMessage();
		analogPins= new ArrayList<AnalogPin>();
		digitalPins= new ArrayList<DigitalPin>();
		//inicializa pins digitais
		for(int i=0; i<14; i ++) {
			digitalPins.add(new DigitalPin(i));
		}
		
		//inicializa pins analógicos
		for(int i=0; i<6; i ++) {
			analogPins.add(new AnalogPin(i));
		}
	}

	public List<AnalogPin> getAnalogPins() {
		return analogPins;
	}

	public List<DigitalPin> getDigitalPins() {
		return digitalPins;
	}
	
	public void defineDigital(int i, DigitalType type, String topic) {
		DigitalPin pin = digitalPins.get(i);
		pin.setType(type);
		pin.setTopic(topic);
		pin.setActive(true);
	}
	
	public void defineAnalog(int i, AnalogType type, String topic) {
		AnalogPin pin = analogPins.get(i);
		pin.setType(type);
		pin.setTopic(topic);
		pin.setActive(true);
	}

	@Override
	public String toString() {
		return digitalPins+"\n"+analogPins;
	}

	public MqttMyMessage getWillMsg() {
		return willMsg;
	}
	
	public MqttMyMessage getStartMsg() {
		return startMsg;
	}

	public void setWillMsg(MqttMyMessage msg) {
		this.willMsg=msg;
	}

	public void setStartMsg(MqttMyMessage msg) {
		this.startMsg=msg;
	}
	
}
