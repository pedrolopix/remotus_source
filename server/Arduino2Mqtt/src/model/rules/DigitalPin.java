package model.rules;

public class DigitalPin extends Pin{
	private DigitalType type;
	
	public DigitalPin(int id) {
		super(id);
		type=DigitalType.INPUT;
	}

	public DigitalPin(int id, String topic, boolean retained, int qos, DigitalType type) {
		super(id, topic, retained, qos);
		this.type=type;
	}

	public DigitalType getType() {
		return type;
	}

	public void setType(DigitalType type) {
		this.type = type;
	}

	@Override
	public boolean isInput() {
		return type==DigitalType.INPUT;
	}

	@Override
	public String toString() {
		return "D "+super.toString()+" ("+type.toString()+")";
	}

    

}
