package model.rules;

public enum DigitalType {
	INPUT, 
	OUTPUT, 
	SERVO, 
	PWM, 
}
