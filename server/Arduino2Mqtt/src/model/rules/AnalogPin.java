package model.rules;


public class AnalogPin extends Pin {
	private AnalogType type;
	private int threshold=255;
	
	public AnalogPin(int id) {
		super(id);
		type=AnalogType.ANALOG;
	}

	public AnalogPin(int id, String topic, boolean retained, int qos, AnalogType type) {
		super(id, topic, retained, qos);
		this.type=type;
	}

	public AnalogType getType() {
		return type;
	}

	public void setType(AnalogType type) {
		this.type = type;
	}

	@Override
	public boolean isInput() {
		return type==AnalogType.INPUT || type==AnalogType.ANALOG;
	}

	@Override
	public String toString() {
		return "A "+super.toString()+" ("+type.toString()+")";
	}

	public int getThreshold() {
		return threshold;
	}

	public void setThreshold(int threshold) {
		this.threshold = threshold;
	}

}
