package model;

import java.io.IOException;
import java.util.List;

import arduino.ArduinoFirmata;
import event.EventArduinoAnalogChange;
import event.EventArduinoDigitalChange;
import event.EventMqttMessageArrived;
import event.EventSendArduino;
import model.rules.AnalogPin;
import model.rules.DigitalPin;
import model.rules.Pin;
import net.engio.mbassy.listener.Listener;

public class Engine {
	
	private static final String TAG = "Engine";
	private Model model;


	public Engine(Model model) {
		this.model=model;
		model.getEventBus().subscribe(this);
	}

	@Listener
	public void onReceiveMqttMessage(final EventMqttMessageArrived event) {
		String name= event.getTopic();
		List<Pin> pins=model.getOutputPinTopic(name);
		int value = Integer.parseInt(event.getMessage().toString());
		for (Pin pin : pins) {
			try {
				model.sendPinValue(pin,value);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
				model.getLog().e(TAG,e);
				model.setState(Model.State.ERROR);
			}
		}
	}
	
	@Listener
	public void onReceiveArduinoAnalog(EventArduinoAnalogChange data) {
		AnalogPin pin=model.getAnalogPin(data.getPin());
		ArduinoToMQtt(pin, data.getValue());
	}
	
	@Listener
	public void onReceiveArduinoDigital(EventArduinoDigitalChange data) {
		DigitalPin pin=model.getDigitalPin(data.getPin());
		ArduinoToMQtt(pin, data.getValue()?1:0);
	}
	
	private void ArduinoToMQtt(Pin pin, int value) {
		if (pin.isActive() && pin.isInput() && pin.getTopic().length()!=0) {
			model.publish(pin.getTopic(),value,pin.getRetained(), pin.getQos());
		}
	}

	@Listener
	public void onEventSendArduino(EventSendArduino data) {
		//model.getArduinoEngine().send(data.getName(),data.getPort(),data.getData());
	}

	public void start() throws IOException {
		//configure
		for (DigitalPin pin: model.getRules().getDigitalPins()) {
			if (!pin.isActive()) continue;
			switch (pin.getType()) {
			case INPUT:
				model.pinMode(pin.getId(),ArduinoFirmata.INPUT);
				break;
			case OUTPUT:
				model.pinMode(pin.getId(),ArduinoFirmata.OUTPUT);
				if (pin.getTopic().length()>0) model.subscrive(pin.getTopic());
				break;
			case SERVO:
				model.pinMode(pin.getId(),ArduinoFirmata.SERVO);
				if (pin.getTopic().length()>0) model.subscrive(pin.getTopic());
				break;			
			case PWM:
				model.pinMode(pin.getId(),ArduinoFirmata.PWM);
				if (pin.getTopic().length()>0) model.subscrive(pin.getTopic());
				break;				
			default:
				break;
			}
		}
	
		for (AnalogPin pin: model.getRules().getAnalogPins()) {
			model.setAnalogThreshold(pin.getId(), pin.getThreshold());
		}
				
		model.getEventBus().subscribe(this);
	}
	
	public void stop() {
		model.getEventBus().unsubscribe(this);
	}
}
