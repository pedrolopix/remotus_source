package model;


import arduino.ArduinoFirmata;
import arduino.BoardPinChangedIntf;
import arduino.SerialException;
import event.EventArduinoAnalogChange;
import event.EventArduinoDigitalChange;
import event.EventBus;
import event.EventFileOpen;
import model.rules.*;
import mqtt.MqttEngine;
import mqtt.MqttMyMessage;
import net.engio.mbassy.BusConfiguration;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttSecurityException;
import org.json.JSONException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Model {
	public enum State {START, MQTT_CONNECTING, MQTT_CONNECTED, ARDUINO_CONNECTING, 
			ARDUINO_CONNECTED, RUNNING, ERROR, ARDUINO_ERROR, CLOSED}
	private EventBus eventBus;
	private Rules rules;
	private ArduinoFirmata arduino;
	private MqttEngine mqttEngine;
	private Engine engine;
	private Log log;
	private State state;
	
	private BoardPinChangedIntf boardPinChanged= new BoardPinChangedIntf() {
		@Override
		public void onDigitalChange(int pin, boolean value) {
			eventBus.publishAsync(new EventArduinoDigitalChange(pin,value));
		}

		@Override
		public void onAnalogicChange(int pin, int value) {
			eventBus.publishAsync(new EventArduinoAnalogChange(pin,value));
		}
	};
	
	public Model() {
		super();
		log= new Log(this);
		eventBus=  new EventBus(BusConfiguration.Default());
		arduino= new ArduinoFirmata();
		arduino.addListener(boardPinChanged);
		mqttEngine=new MqttEngine(this);
		engine=new Engine(this);
    	rules=new Rules();
    	state=State.START;
	}
	
	public void close(){
		engine.stop();
		arduino.stop();
		mqttEngine.stop();
		setState(State.CLOSED);
	}

	public void setState(State state) {
		log.d("mqttstate", this.state.toString()+"->"+state.toString());
		this.state=state;
	}

	public void start() {
		if (state!=State.START) {
			close();
		}
		
		while (true) {
			switch (state) {
			case START:
				connectMqtt();
				break;
			case MQTT_CONNECTING:
				break;
			case MQTT_CONNECTED:
				arduinoStart();
				break;
			case ARDUINO_CONNECTING:
				break;
			case ARDUINO_CONNECTED:
				setState(State.RUNNING);
				engineStart();
				break;
			case ARDUINO_ERROR:
				setState(State.ERROR);
				break;
			case RUNNING:
				try {
					getArduino().join();
					Thread.sleep(100);
				} catch (InterruptedException e) {
					setState(State.ERROR);
				}
				break;
			case ERROR:	
				close();
				break;
			case CLOSED:
				return;
			default:
				break;
			}	
		}
	}

	public void engineStart() {
		try {
			Thread.sleep(3000);
			engine.start();
		} catch (IOException e) {
			log.e("engine", e);
			setState(State.ERROR);
		} catch (InterruptedException e) {
			setState(State.ERROR);
		}
	}
	
	public EventBus getEventBus() {
		return eventBus;
	}

	public void connectMqtt() {
		setState(State.MQTT_CONNECTING);
		try {
			mqttEngine.connect();
			setState(State.MQTT_CONNECTED);
		} catch (MqttException e) {
			log.e("mqt", e);
			setState(State.ERROR);
		}
	}
	
	public void connectMqtt(String host, String userName, String password) throws MqttSecurityException, MqttException {
		mqttEngine.setServerURI(host);
		mqttEngine.setUserName(userName);
		mqttEngine.setPassword(password);
		mqttEngine.connect();
	}

	public void publish(String topic, int value, boolean retained, int qos) {
		mqttEngine.publish(topic, value, retained, qos);
		
	}
	public void publish(String topic, String value, boolean retained, int qos) {
		mqttEngine.publish(topic, value, retained, qos);
	}
	
	public void subscrive(String topic) {
		mqttEngine.subscrive(topic);
	}
	
	public void arduinoStart() {
		try {
			setState(State.ARDUINO_CONNECTING);
			arduino.start();
			setState(State.ARDUINO_CONNECTED);
		} catch (SerialException e) {
			log.e("arduino", e.getMessage());
			setState(State.ARDUINO_ERROR);
		}
	}
	
	public void arduinoStart(String portName) {
		arduino.setPortName(portName);
		arduinoStart();
	}

	public Log getLog() {
		return log;
	}

	public String[] getAvailablePorts() {
		return arduino.getAvailablePorts();
	}

	public AnalogPin getAnalogPin(int pin) {
		return rules.getAnalogPins().get(pin);
	}

	public DigitalPin getDigitalPin(int pin) {
		return rules.getDigitalPins().get(pin);
	}

	public void saveTo(File file) throws IOException, JSONException {
		JsonParse jp= new JsonParse();
		jp.save(arduino,mqttEngine,rules, file);
	}
	
	public void loadfile(File file) throws IOException, JsonParseException, JSONException {
		rules = new Rules();
		JsonParse jp= new JsonParse();
		jp.load(arduino,mqttEngine,rules, file);
		eventBus.publishAsync(new EventFileOpen(file));
	}

	public Rules getRules() {
		return rules;
	}

	public MqttEngine getMqttEngine() {
		return mqttEngine;
	}

	public List<Pin> getOutputPinTopic(String topic) {
		List<Pin> ret= new ArrayList<Pin>();
		for (Pin pin : rules.getAnalogPins()) {
			if (pin.isOutput() && topic.equals(pin.getTopic())) ret.add(pin);
		}
		for (Pin pin : rules.getDigitalPins()) {
			if (pin.isOutput() && topic.equals(pin.getTopic())) ret.add(pin);
		}
		return ret;
	}

	public void sendPinValue(Pin pin, int value) throws IOException {
		if (pin instanceof AnalogPin) {
			//arduinoBoard.analogWrite(pin.getId(), value);
			arduino.analogWrite(pin.getId(), value);
		} else if (pin instanceof DigitalPin){
			//arduinoBoard.digitalWrite(pin.getId(), value);
			if (((DigitalPin) pin).getType()==DigitalType.PWM) {
				arduino.analogWrite(pin.getId(), value);
			} else {
			//if (((DigitalPin) pin).getType()==DigitalType.OUTPUT) {
				arduino.digitalWrite(pin.getId(), value);
			}
//			else {
//				arduino.analogWrite(pin.getId(), value);
//			}
		}
	}

	public void pinMode(int pin, byte mode) {
		arduino.pinMode(pin, mode);
	}

	public ArduinoFirmata getArduino() {
		return arduino;
	}

	public void setAnalogThreshold(int pin, int analogThreshold) {
		arduino.setAnalogThreshold(pin, analogThreshold);
		
	}

	public void setWillMsg(MqttMyMessage msg) {
		mqttEngine.setWillMsg(msg);
	}

	public void setStartMsg(MqttMyMessage msg) {
		mqttEngine.setWillMsg(msg);
	}
	

}
