package arduinojson;

import event.EventBus;
import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import model.Model;


public class ArduinoEngine implements Runnable, SerialPortEventListener{

	private Model model;
	private Thread thread;
	private String port;
	private SerialPort serialPort;
	/**
	* A BufferedReader which will be fed by a InputStreamReader 
	* converting the bytes into characters 
	* making the displayed results codepage independent
	*/
	private BufferedReader input;
	/** The output stream to the port */
	private OutputStream output;
	private CommPortIdentifier portId;
	/** Milliseconds to block while waiting for port open */
	private static final int TIME_OUT = 2000;
	/** Default bits per second for COM port. */
	private static final int DATA_RATE = 9600;
	private static final String TAG = "ArduinoEngine";
	

	public ArduinoEngine(Model model) {
		super();
		this.model=model;
		this.model.getEventBus().subscribe(this);
	}

	public void start() {
		model.getLog().i(TAG,"inicia na porta "+port);
		if (thread!=null) {
			thread.interrupt();
			try {
				thread.wait();
			} catch (InterruptedException e) {
				//é para parar ignorar o erro
			}
		}
		thread= new Thread(this);
		thread.setDaemon(true);
		thread.start();
	}

	public synchronized void close() {
		model.getLog().i(TAG,"stop");
		if (serialPort != null) {
			serialPort.removeEventListener();
			serialPort.close();
		}
	}

	public EventBus getEventBus() {
		return model.getEventBus();
	}

	public String[] getAvailablePorts() {

	    List<String> list = new ArrayList<String>();

	    Enumeration<?> portList = CommPortIdentifier.getPortIdentifiers();

	    while (portList.hasMoreElements()) {
	        CommPortIdentifier portId = (CommPortIdentifier) portList.nextElement();
	        if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL) {
	            list.add(portId.getName());
	        }
	    }

	    return list.toArray(new String[list.size()]);
	}

	public void setPort(String port) {
		this.port=port;
	}

	@Override
	public void run() {
		initialize();
		
	}

	public String getPort() {
		return port;
	}
	
	public void initialize() {
	    Enumeration<?> portList = CommPortIdentifier.getPortIdentifiers();

	    while (portList.hasMoreElements()) {
	        CommPortIdentifier portId = (CommPortIdentifier) portList.nextElement();
	        if (portId.getName().equalsIgnoreCase(port)) {
	            this.portId=portId;
	        }
	    }

		if (portId == null) {
			model.getLog().i(TAG,"Could not find COM port.");
			return;
		}

		try {
			// open serial port, and use class name for the appName.
			serialPort = (SerialPort) portId.open(this.getClass().getName(), TIME_OUT);

			// set port parameters
			serialPort.setSerialPortParams(DATA_RATE,
					SerialPort.DATABITS_8,
					SerialPort.STOPBITS_1,
					SerialPort.PARITY_NONE);

			// open the streams
			input = new BufferedReader(new InputStreamReader(serialPort.getInputStream()));
			output = serialPort.getOutputStream();

			// add event listeners
			serialPort.addEventListener(this);
			serialPort.notifyOnDataAvailable(true);
		} catch (Exception e) {
			model.getLog().e(TAG,e);
		}
	}

	@Override
	public synchronized void serialEvent(SerialPortEvent event) {
		if (event.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
			try {
				String inputLine=input.readLine();
				model.getLog().d(TAG,inputLine);
				
			} catch (Exception e) {
				model.getLog().e(TAG,e);
			}
		}
	}


	
	public Model getModel() {
		return model;
	}

	public synchronized void send(String name, int port, int data) {
		String json=ArduinoJson.portToJson(port, data);
		if (json==null) return;
		try {
			model.getLog().d(TAG,"send: "+json);
			output.write(json.getBytes());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
