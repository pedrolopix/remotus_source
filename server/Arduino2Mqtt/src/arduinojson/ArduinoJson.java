package arduinojson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ArduinoJson {
	
	private static final String NAME = "name";
	private static final String DIGITAL = "digital";
	private static final String ANALOG = "analog";


	public static ArduinoData toArduino(String json) {
		ArduinoData ad= new ArduinoData();
		JSONObject obj;
		try {
			obj = new JSONObject(json);
			ad.setName(obj.getString(NAME));
			
			JSONArray analog = obj.getJSONArray(ANALOG);
			for(int i=0; i<analog.length(); i++) {
				ad.getAnalog().add(analog.getInt(i));
			}
			JSONArray digital = obj.getJSONArray(DIGITAL);
			for(int i=0; i<digital.length(); i++) {
				ad.getDigital().add(digital.getInt(i));
			}
			
			return ad;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	public static String portToJson(int port, int data) {
		return "\" { \"digital\": { \"7\": "+data+", \"8\": "+data+" } }";
		// { "digital": { "7": 0, "8": 128 } }
//		JSONObject j= new JSONObject();
//		try {
//			JSONObject d= new JSONObject();
//			d.put("7", data);
//			d.put("8", data);
//				
//			j.put("digital", d);
//			
//			return j.toString();
//		} catch (JSONException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			return null;
//		}
	}
}
