package arduinojson;

import java.util.ArrayList;
import java.util.List;

import event.Event;

public class ArduinoData  extends Event{
	private String name;
	private List<Integer> analog;
	private List<Integer> digital;
	
	public ArduinoData() {
		super();
		analog = new ArrayList<Integer>();
		digital = new ArrayList<Integer>();
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public List<Integer> getAnalog() {
		return analog;
	}

	public List<Integer> getDigital() {
		return digital;
	}

	@Override
	public String toString() {
		return id+" ArduinoData [name=" + name + ", analog=" + analog+ ", digital=" + digital + "]";
	}

	
}
