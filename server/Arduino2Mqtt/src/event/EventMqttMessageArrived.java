package event;

import org.eclipse.paho.client.mqttv3.MqttMessage;

public class EventMqttMessageArrived extends Event {

	private MqttMessage message;
	private String topic;

	public EventMqttMessageArrived(String topic, MqttMessage message) {
		this.setTopic(topic);
		this.setMessage(message);
		
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public MqttMessage getMessage() {
		return message;
	}

	public void setMessage(MqttMessage message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return id+". EventMqttMessageArrived [message=" + message + ", topic="
				+ topic + "]";
	}


	
	
}
