package event;

public class EventArduinoDigitalChange extends Event {

	private int pin;
	private boolean value;

	public EventArduinoDigitalChange(int pin, boolean value) {
		super();
		this.pin = pin;
		this.value = value;
	}

	public int getPin() {
		return pin;
	}

	public boolean getValue() {
		return value;
	}
	
	@Override
	public String toString() {
		return "ArduinoDigital [pin=" + pin + ", value=" + value+ "]";
	}
	

}
