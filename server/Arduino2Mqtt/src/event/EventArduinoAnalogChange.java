package event;

public class EventArduinoAnalogChange extends Event {
	private int pin;
	private int value;

	public EventArduinoAnalogChange(int pin, int value) {
		super();
		this.pin = pin;
		this.value = value;
	}

	public int getPin() {
		return pin;
	}

	public int getValue() {
		return value;
	}

	@Override
	public String toString() {
		return "ArduinoAnalog [pin=" + pin + ", value=" + value+ "]";
	}
	
	
	
}
