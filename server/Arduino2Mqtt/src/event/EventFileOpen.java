package event;

import java.io.File;

public class EventFileOpen extends Event{
	private File file;
	
	
	public EventFileOpen(File file) {
		super();
		this.file = file;
	}


	@Override
	public String toString() {
		return "Open "+file.getAbsolutePath();
	}


	public File getFile() {
		return file;
	}


}
