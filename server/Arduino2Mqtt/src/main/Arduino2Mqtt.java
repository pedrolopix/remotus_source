package main;

import java.io.File;
import java.io.IOException;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.json.JSONException;

import ViewConsola.ControllerConsola;
import model.JsonParseException;
import model.Model;
import viewGui.ControllerGui;

public class Arduino2Mqtt {
	/**
	 * Launch the application.
	 */
	private Model model;

	public Arduino2Mqtt() {
		super();
		model = new Model();
	}

	private void start(String[] args) {
		boolean startconsole=false;
		boolean startgui=false;
		boolean debug=false;
		if (args.length > 0) {
			for (int i = 0; i < args.length; i++) {
				if (args[i].equals("-o"))
					try {
						if (args.length>i + 1) {
							model.loadfile(new File(args[i + 1]));
							startconsole=true;
						}
					} catch (IOException | JsonParseException e) {
						e.printStackTrace();
					} catch (JSONException e) {
						e.printStackTrace();
					}
				else if (args[i].equals("-gui")) {
					startgui=true;
					startconsole=false;
					ControllerGui.getInstance().startGUI(model);
				}
				else if (args[i].equals("-p")) {
					portas();
					return;
				}
				else if (args[i].equals("-d")) {
					debug=true;
				}
			}
			if (startconsole && !startgui) {
				System.out.println("Android2Mqtt - a iniciar...");
				ControllerConsola.getInstance().start(model, true, debug);
				System.out.println("Android2Mqtt - terminado");
			}
		} else {
			ajuda();
		}		
	}

	private void portas() {
		int i;
		String[] ports = model.getAvailablePorts();
		System.out.println("Android2Mqtt");
		System.out.println("Portas disponiveis: ");
		
		for(i=0; i<ports.length;i++) {
			System.out.println("  "+ports[i]);
		}
		System.out.println("");
	}

	private void ajuda() {
		System.out.println("Android2Mqtt");
		System.out.println("");
		System.out.println("opções: ");
		System.out.println(" -o <NomeFicheiro> lê ficheiro de configurações");
		System.out.println(" -gui interfece gráfico");
		System.out.println(" -p mostra portas disponiveis");
		System.out.println(" -d modo de debug");
		System.out.println("");
		System.out.println("Exemplo: Android2Mqtt -o config.json -gui");
		System.out.println("");
	}

	public static void main(String[] args) {
		try {
			System.setProperty("apple.laf.useScreenMenuBar", "true");
			System.setProperty(
					"com.apple.mrj.application.apple.menu.about.name",
					"Arduino2Mqtt");
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException
				| IllegalAccessException | UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		Arduino2Mqtt app = new Arduino2Mqtt();
		app.start(args);

	}

}
