package viewGui;

import java.awt.EventQueue;

import model.Model;
import event.EventBus;

public class ControllerGui {
	private static ControllerGui instance;
	private Model model;

	public ControllerGui() {
		super();
	}

	public EventBus getEventBus() {
		return getModel().getEventBus();
	}

	public static ControllerGui getInstance() {
		if (instance == null)
			instance = new ControllerGui();
		return instance;
	}

	public Model getModel() {
		return model;
	}


	public void startGUI(Model model) {
		this.model=model;
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					MainWindow window = new MainWindow();
					window.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}
