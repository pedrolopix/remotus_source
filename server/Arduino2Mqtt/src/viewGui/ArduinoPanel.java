package viewGui;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.factories.FormFactory;

import event.EventFileOpen;

import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import model.rules.Pin;
import net.engio.mbassy.listener.Listener;


public class ArduinoPanel extends CustomPanel {

	private JComboBox comboBox;
	private JList list;
	private DefaultListModel listModel;
	/**
	 * Create the panel.
	 */
	public ArduinoPanel() {
		listModel = new DefaultListModel();  
		setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("max(129dlu;default):grow"),
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,},
			new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("default:grow"),}));
		
		JLabel lblNewLabel = new JLabel("Ligação:");
		add(lblNewLabel, "2, 2, right, default");
		
		comboBox = new JComboBox(model.getAvailablePorts());
		add(comboBox, "4, 2, fill, default");
		
		JButton btnNewButton = new JButton("Ligar");
		btnNewButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String s= (String)comboBox.getSelectedItem();
				model.arduinoStart(s);
				model.engineStart();
			}
		});
		add(btnNewButton, "6, 2");
		
		list = new JList(listModel);
		JScrollPane scrollPane = new JScrollPane(list);
		add(scrollPane, "2, 4, 5, 1, fill, fill");
		ctrl.getEventBus().subscribe(this);
		onEventFileOpen(null);
	}
	
	@Listener
	public void onEventFileOpen(final EventFileOpen event) {
		SwingUtilities.invokeLater(new Runnable() {
            @Override
			public void run() {
            	listModel.clear();
            	for (Pin pin : model.getRules().getAnalogPins()) {
					if (pin.isActive()) {
						listModel.addElement(pin.toString());
					}
				}
            	for (Pin pin : model.getRules().getDigitalPins()) {
					if (pin.isActive()) {
						listModel.addElement(pin.toString());
					}
				}
            	
            	comboBox.setSelectedItem(model.getArduino().getPortName());
            }
		});
		
	}

}
