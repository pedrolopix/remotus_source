package viewGui;

import java.awt.BorderLayout;

import javax.swing.JPanel;

import model.Model;

public class CustomPanel extends JPanel {

	
	protected ControllerGui ctrl=ControllerGui.getInstance();
	protected Model model=ctrl.getModel();
	
	/**
	 * Create the panel.
	 */
	public CustomPanel() {

	}

	public CustomPanel(BorderLayout borderLayout) {
		super(borderLayout);
	}

	public Model getModel() {
		return model;
	}
}
