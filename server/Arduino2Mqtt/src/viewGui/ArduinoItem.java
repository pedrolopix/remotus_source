
package viewGui;

import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.factories.FormFactory;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JTextField;

public class ArduinoItem extends CustomPanel {
	private JTextField txtValue;
	private JTextField txtTopic;
	private JComboBox cboType;

	/**
	 * Create the panel.
	 */
	public ArduinoItem() {
		setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),},
			new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,}));
		
		JLabel lblPin = new JLabel("Pin");
		add(lblPin, "2, 2, right, default");
		
	    cboType = new JComboBox<String>();
		add(cboType, "4, 2, fill, default");
		
		txtTopic = new JTextField();
		add(txtTopic, "6, 2, fill, default");
		txtTopic.setColumns(10);
		
		txtValue = new JTextField();
		add(txtValue, "8, 2, fill, default");
		txtValue.setColumns(10);
	}
}
