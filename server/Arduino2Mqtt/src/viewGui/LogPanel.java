package viewGui;
import java.awt.BorderLayout;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import net.engio.mbassy.listener.Listener;
import net.engio.mbassy.listener.Mode;
import viewGui.comps.CheckBoxList;
import event.Event;


public class LogPanel extends CustomPanel implements ListSelectionListener{

	private JList list;
	private DefaultListModel listModel;
	private CheckBoxList checkBoxList;

	/**
	 * Create the panel.
	 */
	public LogPanel() {
		super(new BorderLayout());
		listModel = new DefaultListModel();  
	    list = new JList(listModel);
	    list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	    list.setSelectedIndex(0);
        list.addListSelectionListener(this);
        list.setVisibleRowCount(5);
        JScrollPane listScrollPane = new JScrollPane(list);
        add(listScrollPane, BorderLayout.CENTER);
        
        
        
        checkBoxList = new CheckBoxList();
        JScrollPane listScrollChecks = new JScrollPane(checkBoxList);
        add(listScrollChecks, BorderLayout.WEST);
		ctrl.getEventBus().subscribe(this);
	}
	
	
	@Listener(delivery = Mode.Sequential)
    public void answerAvailable(final Event event) {
		SwingUtilities.invokeLater(new Runnable() {
                    @Override
					public void run() {
                    	boolean checked=checkBoxList.addItemUnique(event.getClass().getSimpleName().toString());
                    	if (!checked) return;
                    	
                    	listModel.addElement(event.toString());
                    	//Select the new item and make it visible.
                    	int index=listModel.getSize()-1;
                        list.setSelectedIndex(index);
                        list.ensureIndexIsVisible(index);
                    }
            });
    }


	@Override
	public void valueChanged(ListSelectionEvent e) {
		
	}
	

}
