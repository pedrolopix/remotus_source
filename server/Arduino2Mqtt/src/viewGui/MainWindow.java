package viewGui;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;

import model.JsonParseException;
import net.miginfocom.swing.MigLayout;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

import org.json.JSONException;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

public class MainWindow {

	private JFrame frame;

	/**
	 * Create the application.
	 */
	public MainWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 1015, 439);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new MigLayout("", "[300px:n,grow][5px:n:5px][300px:n,grow]", "[top][grow,fill]"));
		
		JTabbedPane tabbedPane = new JTabbedPane(SwingConstants.TOP);
		frame.getContentPane().add(tabbedPane, "cell 0 0,grow");
		
		MqttPanel mqttPanel = new MqttPanel();
		tabbedPane.addTab("MQTT", null, mqttPanel, null);
		
		JTabbedPane tabbedPane_1 = new JTabbedPane(SwingConstants.TOP);
		frame.getContentPane().add(tabbedPane_1, "cell 2 0,grow");
		
		ArduinoPanel arduinoPanel = new ArduinoPanel();
		tabbedPane_1.addTab("Arduino", null, arduinoPanel, null);
		
		JTabbedPane tabbedPane_2 = new JTabbedPane(SwingConstants.TOP);
		frame.getContentPane().add(tabbedPane_2, "cell 0 1 3 1,grow");
		
		LogPanel logPanel = new LogPanel();
		tabbedPane_2.addTab("Log", null, logPanel, null);
		
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		
		JMenu mnFicheiros = new JMenu("Ficheiros");
		menuBar.add(mnFicheiros);
		
		JMenuItem mntmGravar = new JMenuItem("Gravar");
		mntmGravar.addActionListener(new ActionListener() {
		    @Override
			public void actionPerformed(ActionEvent ae) {
		    	JFileChooser fc = new JFileChooser();
				int r=fc.showSaveDialog(frame);
				if (r==JFileChooser.APPROVE_OPTION) {
					File file = fc.getSelectedFile();
					try {
						ControllerGui.getInstance().getModel().saveTo(file);
					} catch (JSONException | IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
		    }
		});
		
		JMenuItem mntmLer = new JMenuItem("Ler");
		mntmLer.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser fc = new JFileChooser();
				int r=fc.showOpenDialog(frame);
				if (r==JFileChooser.APPROVE_OPTION) {
					File file = fc.getSelectedFile();
					try {
						ControllerGui.getInstance().getModel().loadfile(file);
					} catch (JSONException | IOException | JsonParseException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		});
		mnFicheiros.add(mntmLer);
		
		mnFicheiros.add(mntmGravar);
	}

	public void setVisible(boolean b) {
		frame.setVisible(b);
		
	}
}
