package viewGui;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.factories.FormFactory;

import event.EventFileOpen;

import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttSecurityException;

import mqtt.MqttEngine;
import net.engio.mbassy.listener.Listener;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class MqttPanel extends CustomPanel {
	private JTextField txthost;
	private JTextField textUsername;
	private JPasswordField textPassword;
	private JTextField textTopic;
	private JTextField textMensagem;

	/**
	 * Create the panel.
	 */
	public MqttPanel() {
		setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("max(50dlu;default)"),
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("max(120dlu;default):grow"),
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("max(59dlu;default)"),},
			new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("default:grow"),}));
		
		JLabel lblHost = new JLabel("Servidor:");
		add(lblHost, "2, 2, right, default");
		
		txthost = new JTextField();
		txthost.setText("tcp://localhost:1883");
		add(txthost, "4, 2, fill, default");
		txthost.setColumns(10);
		
		JLabel lblUtilizador = new JLabel("Utilizador:");
		add(lblUtilizador, "2, 4, right, default");
		
		textUsername = new JTextField();
		add(textUsername, "4, 4, fill, default");
		textUsername.setColumns(10);
		
		JLabel lblPassword = new JLabel("Palavra-passe:");
		add(lblPassword, "2, 6, right, default");
		
		textPassword = new JPasswordField();
		add(textPassword, "4, 6, fill, default");
		textPassword.setColumns(10);
		
		JButton btnLigar = new JButton("Ligar");
		btnLigar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					model.connectMqtt(txthost.getText(), textUsername.getText(),textPassword.getText());
				} catch (MqttSecurityException e1) {
					JOptionPane.showMessageDialog(null, "Erro de autenticação!");
					e1.printStackTrace();
				} catch (MqttException e1) {
					JOptionPane.showMessageDialog(null, "Erro de ligação ao mqtt!");
					e1.printStackTrace();
				}
			}
		});
		add(btnLigar, "6, 6");
		
		JLabel lblTpico = new JLabel("Tópico");
		add(lblTpico, "2, 8, right, default");
		
		textTopic = new JTextField();
		add(textTopic, "4, 8, fill, default");
		textTopic.setColumns(10);
		
		JButton btnSubscrever = new JButton("Subscrever");
		btnSubscrever.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				model.subscrive(textTopic.getText());
			}
		});
		add(btnSubscrever, "6, 8");
		
		JLabel lblMensagem = new JLabel("Mensagem:");
		add(lblMensagem, "2, 10, right, top");
		
		textMensagem = new JTextField();
		add(textMensagem, "4, 10, fill, top");
		textMensagem.setColumns(10);
		
		JButton btnEnviar = new JButton("Enviar");
		btnEnviar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				model.publish(textTopic.getText(), textMensagem.getText(), false, 0);
			}
		});
		add(btnEnviar, "6, 10, default, top");
		ctrl.getEventBus().subscribe(this);
		onEventFileOpen(null);
	}
	
	@Listener
	public void onEventFileOpen(final EventFileOpen event) {
		SwingUtilities.invokeLater(new Runnable() {
            @Override
			public void run() {
				MqttEngine mqtt = model.getMqttEngine();
				txthost.setText(mqtt.getServerURI());
				textPassword.setText(mqtt.getPassword());
				textUsername.setText(mqtt.getUserName());
            }
		});
	}

}
