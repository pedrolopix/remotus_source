from string import Template

def main():
    #delete existing html files
    for file in glob.glob('site/*.html'):
        os.remove(file)

    #get settings
    j = open("settings.json")
    d = json.load(j)
    data = ast.literal_eval(json.dumps(d['project']))
    pages = ast.literal_eval(json.dumps(d['pages']))

    for page in pages:
        data['title'] = page["title"]
        makePage(pages, data, page)
    
    print '-----done - site generated inside /site------'

def makePage(pages, data, currentPage):
    #draw out html
    html = []
    #html start
    html.append(Template(open('templates/html-start.html').read()).substitute(data))
    #nav
    html.append(Template(open('templates/nav-start.html').read()).substitute(data))
    html.append(getNav(pages, currentPage['title']))
    html.append(open('templates/nav-end.html').read())
    #content
    html.append(Template(open('templates/' + currentPage['template']).read()).substitute(data))
    #html end
    html.append(Template(open('templates/html-end.html').read()).substitute(data))
    
    #write to file
    fileName = urlify(currentPage['title'].lower()) + '.html'
    if (currentPage['template'] == 'home.html'):
        fileName = 'index.html'
    f = open('site/' + fileName, 'w+')
    f.write('\n'.join(html))

def getNav(pages, currentPageTitle):
    nav = []
    for item in pages:
        fileName = urlify(item['title'].lower()) + '.html'
        if(item['template'] == 'home.html'):
            fileName = 'index.html'
        if(currentPageTitle != item['title']):
            #adding extra tab characters to maintain indentation in the html
            nav.append('\t\t\t\t\t\t\t<li><a href="'+fileName+'">'+item['title']+'</a></li>')
        else:
            nav.append('\t\t\t\t\t\t\t<li class="active"><a href="'+fileName+'">'+item['title']+'</a></li>')
    return '\n'.join(nav)

def urlify(s):
     # Remove all non-word characters (everything except numbers and letters)
     s = re.sub(r"[^\w\s]", '', s)
     # Replace all runs of whitespace with a single dash
     s = re.sub(r"\s+", '-', s)
     return s

if __name__ == '__main__':
    main()