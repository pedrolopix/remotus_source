# Bootstrap Website Starter

While creating a quick static HTML website, do you end up creating a bunch of html files, navigation that links em up and a contact form (with validation) over and over again? This project aims to reduce the time required to put together a basic HTML5 (mobile friendly) website by using Bootstrap, jQuery and Python(to use optionally to generate a static html5 website) for small sites with commonly used pages:

* index (with a slideshow of images)
* about (2 columns with profile pic and lorem ipsum text)
* services (2 columns with profile pic and lorem ipsum text)
* gallery (categorized image thumbnails)
* contact (with a form and basic validation)

[Demo](http://walmik.info/demos/html-website-starter-kit/)

## How to use (without the python tool)
* Simply copy the /site folder along with its contents to your webserver's sites location
* Add some text that suits your site and change images in the gallery section
* Customize the layout (use some nice fonts from Google or add a div over the header with a nice image etc)
* If you need to name the pages differently, then you ll need to manually rename and rewire them. (A python tool is included to avoid this)

This kit comes with a python script that generates custom pages for a mobile friendly Bootstrap based HTML5 website. This way you do not have to be limited to the way the page are wired up by default in the /site folder.

## How to use the python tool that comes with this kit:

* Edit the settings.json file in this directory according to your requirements
* For the templates, use page.html for generic pages (the rest are self explainatory)
* Run `python makesite.py`
* The required site structure will generate inside /site 
